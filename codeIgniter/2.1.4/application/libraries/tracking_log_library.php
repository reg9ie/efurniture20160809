<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tracking_log_library {
    var $CI = FALSE;
    var $user = FALSE;
    var $table = 'tracking_logs';

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->database();

        $this->create_table();

        $this->CI->load->spark('php-activerecord/0.0.2');

        $user = User::get();
        if($user) $this->user = $user->to_array();
    }
    
    public function log() {
        $this->CI->db->insert($this->table, array(
            'user_id' => $this->user ? $this->user['id'] : 0,
            'session_id' => $this->CI->session->userdata('session_id'),
            'ip' => $this->CI->input->server('REMOTE_ADDR'),
            'platform' => $this->CI->agent->platform(),
            'browser' => $this->CI->agent->browser(),
            'version' => $this->CI->agent->version(),
            'mobile' => $this->CI->agent->mobile(),
            'robot' => $this->CI->agent->robot(),
            'user_agent' => $this->CI->agent->agent_string(),
            'request_uri' => $this->CI->input->server('REQUEST_URI'),
            'referer' => $this->CI->agent->referrer(),
            'time' => date('Y-m-d H:i:s')
        ));
    }

    private function create_table() {
        if(!$this->CI->db->table_exists('tracking_logs'))
        {
            $this->CI->load->dbforge();
            $fields = array('id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
                            'user_id' => array('type' => 'INT', 'unsigned' => TRUE),
                            'session_id' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'ip' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'platform' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'browser' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'version' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'mobile' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'robot' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'user_agent' => array('type' =>'TEXT'),
                            'request_uri' => array('type' =>'TEXT'),
                            'referer' => array('type' =>'TEXT'),
                            'time' => array('type' => 'DATETIME'));
            $this->CI->dbforge->add_field($fields);
            $this->CI->dbforge->add_key('id', TRUE);
            $this->CI->dbforge->create_table('tracking_logs');
        }
    }
}