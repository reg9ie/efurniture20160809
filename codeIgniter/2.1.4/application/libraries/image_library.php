<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Image_library {
    public $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    public function upload($path = FALSE) {
        if($path === FALSE) return FALSE;

        $realPath = $this->get_path($path);
        if(!is_dir($realPath))
        {
            $oldmask = umask(0);
            mkdir($realPath, 0777);
            umask($oldmask);
        }

        $config['upload_path'] = $realPath;
        $config['allowed_types'] = configImageAllowedTypes;
        $config['max_size'] = configImageMaxSize*1024;
        $config['encrypt_name'] = TRUE;

        $this->CI->load->library('upload', $config);
        if($this->CI->upload->do_upload('file'))
        {
            $file = $this->CI->upload->data();
            return $file['file_name'];
        }
        else return FALSE;
    }

    public function uploads($path = FALSE, $thumbnail = FALSE) {
        if($path === FALSE) return FALSE;

        $realPath = $this->get_path($path);
        if(!is_dir($realPath))
        {
            $oldmask = umask(0);
            mkdir($realPath, 0777);
            umask($oldmask);
        }

        $config['upload_path'] = $realPath;
        $config['allowed_types'] = configImageAllowedTypes;
        $config['max_size'] = configImageMaxSize;
        $config['encrypt_name'] = TRUE;

        $this->CI->load->library('upload');
        $this->CI->upload->initialize($config);
        if($this->CI->upload->do_multi_upload('files'))
        {
            $file_names = array();
            $files = $this->CI->upload->get_multi_upload_data();
            foreach((array)$files as $i => $f)
            {
                $file_names[] = $f['file_name'];
            }
            return $file_names;
        }
        else return FALSE;
    }

    public function del($path = FALSE, $filename = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        
        if(file_exists($realPath.$filename)) unlink($realPath.$filename);
        
        $thumbname = $this->thumbname($filename);
        if(file_exists($realPath.$thumbname)) unlink($realPath.$thumbname);
    }

    public function move($path = FALSE, $filename = FALSE, $dest = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        if($dest === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        $realDest = $this->get_path($dest);
        
        if(file_exists($realPath.$filename) && file_exists($realDest.$filename) == FALSE) rename($realPath.$filename, $realDest.$filename);
        
        $thumbname = $this->thumbname($filename);
        if(file_exists($realPath.$thumbname) && file_exists($realDest.$thumbname) == FALSE) rename($realPath.$thumbname, $realDest.$thumbname);
    }

    public function thumbnail($path = FALSE, $filename = FALSE, $width = FALSE, $height = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;

        $realPath = $this->get_path($path);

        $config['image_library'] = 'gd2';
        $config['source_image'] = $realPath.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;

        if($width === FALSE && $height === FALSE)
        {
            $config['width'] = configImageThumbnailWidth;
            $config['height'] = configImageThumbnailHeight;
            $config['thumb_marker'] = '_thumb';
        }
        elseif($width !== FALSE && $height === FALSE)
        {
            $config['width'] = $width;
            $config['height'] = 99999999;
            $config['thumb_marker'] = '_'.$width.'x';
        }
        elseif($width === FALSE && $height !== FALSE)
        {
            $config['width'] = 99999999;
            $config['height'] = $height;
            $config['thumb_marker'] = '_x'.$height;
        }
        else
        {
            $config['width'] = $width;
            $config['height'] = $height;
            $config['thumb_marker'] = '_'.$width.'x'.$height;
        }
        
        $this->CI->load->library('image_lib');
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->resize();

        $path_parts = pathinfo($filename);
        $thumbname = $path_parts['filename'].$config['thumb_marker'].'.'.$path_parts['extension'];

        return $thumbname;
    }
    
    public function get_path($path = FALSE) {
        if($path === FALSE) return FALSE;

        $path_array = explode('/', $path);
        switch($path_array[0])
        {
            case 'temp':
                $realPath = configPathImageTemp;
                break;
            case 'site':
                $realPath = configPathImageSite;
                break;
            case 'news':
                $realPath = configPathImageNews;
                break;
            case 'faq':
                $realPath = configPathImageFaq;
                break;
            case 'category':
                $realPath = configPathImageCategory;
                break;
            case 'page':
                $realPath = configPathImagePage;
                break;
            case 'article':
                $realPath = configPathImageArticle;
                break;
            case 'acategory':
                $realPath = configPathImageAcategory;
                break;
            case 'photo':
                $realPath = configPathImagePhoto;
                break;
            case 'album':
                $realPath = configPathImageAlbum;
                break;
            case 'product':
                $realPath = configPathImageProduct;
                break;
            case 'pcategory':
                $realPath = configPathImagePcategory;
                break;
            case 'portfolio':
                $realPath = configPathImagePortfolio;
                break;
            case 'shop':
                $realPath = configPathImageShop;
                break;
            case 'store':
                $realPath = configPathImageStore;
                break;
            case 'activity':
                $realPath = configPathImageActivity;
                break;
            case 'signup':
                $realPath = configPathImageSignup;
                break;
            case 'link':
                $realPath = configPathImageLink;
                break;
            case 'user':
                $realPath = configPathImageUser;
                break;
            case 'avatar':
                $realPath = configPathImageAvatar;
                break;
            case 'image':
                $realPath = configPathImageImage;
                break;
            case 'background':
                $realPath = configPathImageBackground;
                break;
            case 'ad':
                $realPath = configPathImageAd;
                break;
            case 'board':
                $realPath = configPathImageBoard;
                break;
            case 'epaper':
                $realPath = configPathImageEpaper;
                break;
            default:
                return FALSE;
        }
        if(count($path_array) == 1) return $realPath;

        unset($path_array[0]);
        return $realPath.implode('/', $path_array).'/';
    }

    public function get_url($path = FALSE) {
        switch($path)
        {
            case 'temp':
                return configUrlImageTemp;
            case 'site':
                return configUrlImageSite;
            case 'news':
                return configUrlImageNews;
            case 'faq':
                return configUrlImageFaq;
            case 'category':
                return configUrlImageCategory;
            case 'page':
                return configUrlImagePage;
            case 'article':
                return configUrlImageArticle;
            case 'acategory':
                return configUrlImageAcategory;
            case 'photo':
                return configUrlImagePhoto;
            case 'album':
                return configUrlImageAlbum;
            case 'product':
                return configUrlImageProduct;
            case 'pcategory':
                return configUrlImagePcategory;
            case 'portfolio':
                return configUrlImagePortfolio;
            case 'shop':
                return configUrlImageShop;
            case 'store':
                return configUrlImageStore;
            case 'activity':
                return configUrlImageActivity;
            case 'signup':
                return configUrlImageSignup;
            case 'link':
                return configUrlImageLink;
            case 'user':
                return configUrlImageUser;
            case 'avatar':
                return configUrlImageAvatar;
            case 'image':
                return configUrlImageImage;
            case 'background':
                return configUrlImageBackground;
            case 'ad':
                return configUrlImageAd;
            case 'board':
                return configUrlImageBoard;
            case 'epaper':
                return configUrlImageEpaper;
            default:
                return FALSE;
        }
    }

    public function thumbname($filename = FALSE, $thumb_marker = '_thumb') {
        $path_parts = pathinfo($filename);

        $ext = $path_parts['extension'];
        $main = substr($filename, 0, strpos($filename, '.'.$ext));

        $thumbname = $main.$thumb_marker.'.'.$ext;

        return $thumbname;
    }
}