<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_library {
    var $CI = FALSE;
    var $config = FALSE;
    var $user = FALSE;
    
    function __construct() {
        $this->CI =& get_instance();
        
        $this->config = Config::all_to_array();
        $this->config['environment']         = configEnvironment;
        $this->config['timezone']            = configTimezone;
        $this->config['url_domain']          = configUrlDomain;
        $this->config['url_admin']           = configUrlAdmin;
        $this->config['url_base']            = configUrlBase;
        $this->config['url_image']           = configUrlImage;
        $this->config['path_base']           = configPathBase;
        $this->config['path_image']          = configPathImage;

        $objUser = User::get();
        if($objUser) $this->user = $objUser->to_array();
    }
    
    public function get() {
        if(empty($this->config['facebook_id'])) return FALSE;
        if(empty($this->config['facebook_api'])) return FALSE;
        if(empty($this->config['facebook_secret'])) return FALSE;

        
    }
}