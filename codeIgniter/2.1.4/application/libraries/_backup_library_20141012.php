<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Backup_library {
    var $CI = FALSE;
    var $config = FALSE;
    var $user = FALSE;
    
    function __construct() {
        set_time_limit(14400);

        $this->CI =& get_instance();
        $this->CI->load->spark('php-activerecord/0.0.2');

        $this->config = Config::all_to_array();
        $this->config['environment']         = configEnvironment;
        $this->config['timezone']            = configTimezone;
        $this->config['url_domain']          = configUrlDomain;
        $this->config['url_admin']           = configUrlAdmin;
        $this->config['url_base']            = configUrlBase;
        $this->config['url_image']           = configUrlImage;
        $this->config['path_base']           = configPathBase;
        $this->config['path_image']          = configPathImage;

        $objUser = User::get();
        if($objUser) $this->user = $objUser->to_array();
    }
    
    public function backup() {
        if(file_exists(configPathBackup) === FALSE) mkdir(configPathBackup);

        $date = date('Ymd');
        if(file_exists(configPathBackup.$date.'.zip')) return FALSE;
        if(file_exists(configPathBackup.$date.'/'))
        {
            if(time() - filemtime(configPathBackup.$date.'/') < 7200) return FALSE;
            directory_delete(configPathBackup.$date.'/');
        }

        mkdir(configPathBackup.$date.'/');

        $this->CI->load->dbutil();
        $backup = $this->CI->dbutil->backup(array('ignore' => array('error_logs', 'tracking_logs'), 'format' => 'txt'));
        write_file(configPathBackup.$date.'/db.sql', $backup);
        if($this->CI->db->table_exists('error_logs'))
        {
            $backup = $this->CI->dbutil->backup(array('tables' => array('error_logs'), 'format' => 'txt'));
            write_file(configPathBackup.$date.'/error_logs.sql', $backup);
        }
        if($this->CI->db->table_exists('tracking_logs'))
        {
            $backup = $this->CI->dbutil->backup(array('tables' => array('tracking_logs'), 'format' => 'txt'));
            write_file(configPathBackup.$date.'/tracking_logs.sql', $backup);
        }

        directory_copy(configPathBase.'web/', configPathBackup.$date.'/web/');
        directory_copy(configPathBase.'css/', configPathBackup.$date.'/css/');
        directory_copy(configPathBase.'js/', configPathBackup.$date.'/js/');
        directory_copy(configPathBase.'image/', configPathBackup.$date.'/image/');
        directory_copy(configPathBase.'font/', configPathBackup.$date.'/font/');
        directory_copy(configPathBase.'sound/', configPathBackup.$date.'/sound/');

        $this->CI->load->library('zip');
        $this->CI->zip->read_dir(configPathBackup.$date.'/', FALSE);
        $this->CI->zip->archive(configPathBackup.$date.'.zip');
        $this->CI->zip->clear_data();

        $dirs = glob(configPathBackup.'*', GLOB_ONLYDIR);
        foreach((array)$dirs as $i => $d)
        {
            directory_delete($d);
        }

        $map = directory_map(configPathBackup, 1);
        if(count($map) > 30)
        {
            foreach((array)$map as $i => $m)
            {
                $map[$i] = str_replace('.zip', '', $m);
            }

            sort($map);
            unlink(configPathBackup.$map[0].'.zip');
        }
    }
}