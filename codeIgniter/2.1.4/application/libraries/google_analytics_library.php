<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Google_analytics_library {
    var $CI = FALSE;
    var $config = FALSE;
    var $user = FALSE;
    
    function __construct() {
        $this->CI =& get_instance();
        
        $this->config = Config::all_to_array();
        $this->config['environment']         = configEnvironment;
        $this->config['timezone']            = configTimezone;
        $this->config['url_domain']          = configUrlDomain;
        $this->config['url_admin']           = configUrlAdmin;
        $this->config['url_base']            = configUrlBase;
        $this->config['url_image']           = configUrlImage;
        $this->config['path_base']           = configPathBase;
        $this->config['path_image']          = configPathImage;

        $objUser = User::get();
        if($objUser) $this->user = $objUser->to_array();
    }
    
    public function get() {
        if(empty($this->config['google_analysis_account'])) return FALSE;
        if(empty($this->config['google_analysis_key_file_name'])) return FALSE;
        if(empty($this->config['google_analysis_profile_id'])) return FALSE;

        include 'gapi.php';
        
        $ga = new gapi($this->config['google_analysis_account'], $this->config['google_analysis_key_file_name']);
        if($ga->getToken() === NULL) return FALSE;

        $ga->requestReportData(
            $this->config['google_analysis_profile_id'],
            array('date'),
            array('pageviews', 'visits', 'uniquePageviews'),
            array('date'),
            null,
            date('Y-m-d', strtotime('-29 days')),
            date('Y-m-d')
        );
        $results = $ga->getResults();

        $analysis['results'] = $results;
        $analysis['all']['visits'] = 0;
        $analysis['all']['pageviews'] = 0;
        $analysis['today']['visits'] = 0;
        $analysis['today']['pageviews'] = 0;
        $analysis['yesterday']['visits'] = 0;
        $analysis['yesterday']['pageviews'] = 0;

        foreach ($results as $result) {
            if($result->getDate() == date('Ymd'))
            {
                $analysis['today']['visits'] = $result->getVisits();
                $analysis['today']['pageviews'] = $result->getPageviews();
            }
            if($result->getDate() == date('Ymd', strtotime("yesterday")))
            {
                $analysis['yesterday']['visits'] = $result->getVisits();
                $analysis['yesterday']['pageviews'] = $result->getPageviews();
            }
            $analysis['all']['visits'] = $analysis['all']['visits']+$result->getVisits();
            $analysis['all']['pageviews'] = $analysis['all']['pageviews']+$result->getPageviews();
        }

        return $analysis;
    }
}