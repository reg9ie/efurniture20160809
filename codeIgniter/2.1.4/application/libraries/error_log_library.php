<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Error_log_library {
    var $CI = FALSE;
    var $config = FALSE;
    var $user = FALSE;
    var $table = 'error_logs';

    var $levels = array(
        E_ERROR             => 'Error',
        E_WARNING           => 'Warning',
        E_PARSE             => 'Parsing Error',
        E_NOTICE            => 'Notice',
        E_CORE_ERROR        => 'Core Error',
        E_CORE_WARNING      => 'Core Warning',
        E_COMPILE_ERROR     => 'Compile Error',
        E_COMPILE_WARNING   => 'Compile Warning',
        E_USER_ERROR        => 'User Error',
        E_USER_WARNING      => 'User Warning',
        E_USER_NOTICE       => 'User Notice',
        E_STRICT            => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'Catchable error',
        E_DEPRECATED        => 'Runtime Notice',
        E_USER_DEPRECATED   => 'User Warning'
    );

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->library('user_agent');

        $this->create_table();

        set_error_handler(array($this, 'error_handler'), E_ALL);
        set_exception_handler(array($this, 'exception_handler'));
        register_shutdown_function(array($this, 'shutdown_handler'));
    }
    
    function error_handler($severity, $message, $filepath, $line) {
        $user_agent = $this->CI->agent->agent_string();
        $data = array(
            'no' => $severity,
            'type' => (isset($this->levels[$severity]) ? $this->levels[$severity] : $severity),
            'message' => $message,
            'post' => json_encode($_POST),
            'file' => $filepath,
            'line' => $line,
            'url' => current_url(),
            'user_agent' => $user_agent ? $user_agent : '',
            'time' => date('Y-m-d H:i:s')
        );
        $this->CI->db->insert($this->table, $data);
    }

    function exception_handler($exception) {
        $user_agent = $this->CI->agent->agent_string();
        $data = array(
            'no' => $exception->getCode(),
            'type' => (isset($this->levels[$exception->getCode()]) ? $this->levels[$exception->getCode()] : $exception->getCode()),
            'message' => $exception->getMessage(),
            'post' => json_encode($_POST),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'url' => current_url(),
            'user_agent' => $user_agent ? $user_agent : '',
            'time' => date('Y-m-d H:i:s')
        );
        $this->CI->db->insert($this->table, $data);
    }

    function shutdown_handler() {
        if(($error = error_get_last()) && $error['type'] == E_ERROR)
        {
            $user_agent = $this->CI->agent->agent_string();
            $data = array(
                'no' => $error['type'],
                'type' => (isset($this->levels[$error['type']]) ? $this->levels[$error['type']] : 'Error'),
                'message' => $error['message'],
                'post' => json_encode($_POST),
                'file' => $error['file'],
                'line' => $error['line'],
                'url' => current_url(),
                'user_agent' => $user_agent ? $user_agent : '',
                'time' => date('Y-m-d H:i:s')
            );
            $this->CI->db->insert($this->table, $data);
        }
    }

    private function create_table() {
        $this->CI->load->dbforge();
        if(!$this->CI->db->table_exists('error_logs'))
        {
            $fields = array('id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
                            'no' => array('type' => 'INT', 'unsigned' => TRUE),
                            'type' => array('type' =>'VARCHAR', 'constraint' => '32'),
                            'message' => array('type' =>'MEDIUMTEXT'),
                            'post' => array('type' =>'MEDIUMTEXT'),
                            'file' => array('type' =>'VARCHAR', 'constraint' => '255'),
                            'line' => array('type' => 'INT', 'unsigned' => TRUE),
                            'url' => array('type' =>'VARCHAR', 'constraint' => '255'),
                            'user_agent' => array('type' =>'VARCHAR', 'constraint' => '255'),
                            'time' => array('type' => 'DATETIME'));
            $this->CI->dbforge->add_field($fields);
            $this->CI->dbforge->add_key('id', TRUE);
            $this->CI->dbforge->create_table('error_logs');
        }
    }
}