<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Image_library {

    public $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    public function upload($path = FALSE, $thumbnail = FALSE) {
        if($path === FALSE) return FALSE;

        $realPath = $this->get_path($path);
        if(is_dir($realPath) === FALSE) mkdir($realPath);

        $config['upload_path'] = $realPath;
        $config['allowed_types'] = configImageAllowedTypes;
        $config['max_size'] = configImageMaxSize;
        $config['encrypt_name'] = TRUE;

        $this->CI->load->library('upload', $config);
        if($this->CI->upload->do_upload('file'))
        {
            $file = $this->CI->upload->data();
            return $file['file_name'];
        }
        else return FALSE;
    }

    public function del($path = FALSE, $filename = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        
        if(file_exists($realPath.$filename)) unlink($realPath.$filename);
        
        $thumbname = $this->thumbname($filename);
        if(file_exists($realPath.$thumbname)) unlink($realPath.$thumbname);
    }

    public function move($path = FALSE, $filename = FALSE, $dest = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        if($dest === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        $realDest = $this->get_path($dest);
        
        if(file_exists($realPath.$filename) && file_exists($realDest.$filename) == FALSE) rename($realPath.$filename, $realDest.$filename);
        
        $thumbname = $this->thumbname($filename);
        if(file_exists($realPath.$thumbname) && file_exists($realDest.$thumbname) == FALSE) rename($realPath.$thumbname, $realDest.$thumbname);
    }

    public function thumbnail($path = FALSE, $filename = FALSE, $width = FALSE, $height = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;

        $realPath = $this->get_path($path);

        $config['image_library'] = 'gd2';
        $config['source_image'] = $realPath.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;

        if($width === FALSE && $height === FALSE)
        {
            $config['width'] = configImageThumbnailWidth;
            $config['height'] = configImageThumbnailHeight;
            $config['thumb_marker'] = '_thumb';
        }
        elseif($width !== FALSE && $height === FALSE)
        {
            $config['width'] = $width;
            $config['height'] = 99999999;
            $config['thumb_marker'] = '_'.$width.'x';
        }
        elseif($width === FALSE && $height !== FALSE)
        {
            $config['width'] = 99999999;
            $config['height'] = $height;
            $config['thumb_marker'] = '_x'.$height;
        }
        else
        {
            $config['width'] = $width;
            $config['height'] = $height;
            $config['thumb_marker'] = '_'.$width.'x'.$height;
        }
        
        $this->CI->load->library('image_lib', $config);
        $this->CI->image_lib->resize();

        $path_parts = pathinfo($filename);
        $thumbname = $path_parts['filename'].$config['thumb_marker'].'.'.$path_parts['extension'];

        return $thumbname;
    }
    
    public function get_path($path = FALSE) {
        switch($path)
        {
            case 'temp':
                return configPathImageTemp;
                break;
            case 'site':
                return configPathImageSite;
                break;
            case 'photo':
                return configPathImagePhoto;
                break;
            case 'album':
                return configPathImageAlbum;
                break;
            case 'article':
                return configPathImageArticle;
                break;
            case 'acategory':
                return configPathImageAcategory;
                break;
            case 'product':
                return configPathImageProduct;
                break;
            case 'pcategory':
                return configPathImagePcategory;
                break;
            case 'page':
                return configPathImagePage;
                break;
            case 'category':
                return configPathImageCategory;
                break;
            default:
                return FALSE;
                break;
        }
    }

    public function get_url($path = FALSE) {
        switch($path)
        {
            case 'temp':
                return configUrlImageTemp;
                break;
            case 'site':
                return configUrlImageSite;
                break;
            case 'photo':
                return configUrlImagePhoto;
                break;
            case 'album':
                return configUrlImageAlbum;
                break;
            case 'article':
                return configUrlImageArticle;
                break;
            case 'acategory':
                return configUrlImageAcategory;
                break;
            case 'product':
                return configUrlImageProduct;
                break;
            case 'pcategory':
                return configUrlImagePcategory;
                break;
            case 'page':
                return configUrlImagePage;
                break;
            case 'category':
                return configUrlImageCategory;
                break;
            default:
                return FALSE;
                break;
        }
    }

    public function thumbname($filename = FALSE, $thumb_marker = '_thumb') {
        $path_parts = pathinfo($filename);

        $ext = $path_parts['extension'];
        $main = substr($filename, 0, strpos($filename, '.'.$ext));

        $thumbname = $main.$thumb_marker.'.'.$ext;

        return $thumbname;
    }
}