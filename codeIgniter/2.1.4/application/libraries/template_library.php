<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Template_library {
    var $CI = FALSE;
    var $config = FALSE;
    var $user = FALSE;
    
    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->spark('php-activerecord/0.0.2');

        $this->create_table();

        $this->config = Config::all_to_array();
        $this->config['environment']         = configEnvironment;
        $this->config['timezone']            = configTimezone;
        $this->config['url_domain']          = configUrlDomain;
        $this->config['url_admin']           = configUrlAdmin;
        $this->config['url_base']            = configUrlBase;
        $this->config['url_image']           = configUrlImage;
        $this->config['path_base']           = configPathBase;
        $this->config['path_image']          = configPathImage;

        $objUser = User::get();
        if($objUser) $this->user = $objUser->to_array();

        $this->CI->backup_library->backup();
        $this->CI->tracking_log_library->log();
    }
    
    public function frontend($data = FALSE) {
        $default_css = array(
            'css/jquery/jquery-ui.min.css',
            'css/bootstrap/3.1.0/bootstrap.min.css',
            'css/font-awesome/4.0.3/font-awesome.min.css',
            'css/common.css',
            'css/main.css'
        );
        $default_js = array(
            'js/jquery/jquery-1.9.0.min.js', 'js/jquery/jQUI-1.9.2.js',
            'js/bootstrap/3.1.0/bootstrap.min.js',
            'js/angularjs/1.2.12/angular.min.js',
            'js/moment/min/moment.min.js', 'js/moment/min/lang/zh-tw.js',
            'js/midway/1.0.0/midway.min.js',
            'js/common.js'
        );
        // $merge_default_css = $this->merge(array('path' => configPathBase.'css/', 'from' => $default_css, 'to' => 'merge_frontend_default.css'));
        // $merge_default_js = $this->merge(array('path' => configPathBase.'js/', 'from' => $default_js, 'to' => 'merge_frontend_default.js'));
        // if($merge_default_css !== FALSE) $default_css = $merge_default_css;
        // if($merge_default_js !== FALSE) $default_js = $merge_default_js;

        if(isset($data['css']) === FALSE || empty($data['css']) === TRUE)
        {
            $data['css'] = $default_css;
        }
        else
        {
            $data['css'] = array_merge((array)$default_css, (array)$data['css']);
        }

        if(isset($data['js']) === FALSE || empty($data['js']) === TRUE)
        {
            $data['js'] = $default_js;
        }
        else
        {
            $data['js'] = array_merge((array)$default_js, (array)$data['js']);
        }

        // $merge_css = $this->merge(array('path' => configPathBase.'css/', 'from' => $data['css'], 'extension' => 'css'));
        // $merge_js = $this->merge(array('path' => configPathBase.'js/', 'from' => $data['js'], 'extension' => 'js'));
        // if($merge_css !== FALSE) $data['css'] = $merge_css;
        // if($merge_js !== FALSE) $data['js'] = $merge_js;

        $var['css'] = $data['css'];
        $var['js'] = $data['js'];
        $var['config'] = $this->config;
        $var['user'] = $this->user;
        
        if(isset($data['json'])) $var['json'] = $data['json'];
        if(isset($data['data'])) $var['data'] = $data['data'];

        $var['uri'] = $this->CI->uri->uri_string();
        // $var['page'] = Page::find_by_uri($var['uri'])->to_array();
        // $var['pages'] = to_array(Page::all(array('conditions' => 'enable = 1', 'order' => 'sort asc')));

        $var['name'] = (isset($data['name']) ? $data['name'] : '');
        $var['title'] = ($var['name'] ? $var['name'].'-' : '').$this->config['site_name'];

        if(isset($data['html'])) $var['container'] = $this->CI->load->view($data['html'], $var, TRUE);

        $this->CI->load->view('/template/'.(isset($data['template']) ? $data['template'] : 'template'), $var);

        if($this->config['environment'] == 'development') $this->CI->output->enable_profiler(TRUE);
    }
    
    public function backend($data = FALSE) {
        $default_css = array(
            'css/jquery/jquery-ui.min.css',
            'css/bootstrap/3.1.0/bootstrap.min.css',
            'css/flat-ui/2.1.3_free/flat-ui.css', 'css/flat-ui/2.1.3_free/fix.css',
            'css/font-awesome/4.0.3/font-awesome.min.css',
            'css/google-code-prettify/20130304/prettify.css',
            'css/admin/common.css',
            'css/admin/main.css'
        );
        $default_js = array(
            'js/jquery/jquery-1.9.0.min.js', 'js/jquery/jQUI-1.9.2.js',
            'js/bootstrap/3.1.0/bootstrap.min.js',
            'js/flat-ui/2.1.3_free/bootstrap-select.js',
            'js/flat-ui/2.1.3_free/bootstrap-switch.js',
            'js/flat-ui/2.1.3_free/jquery.tagsinput.js',
            'js/flat-ui/2.1.3_free/jquery.placeholder.js',
            'js/flat-ui/2.1.3_free/flatui-checkbox.js',
            'js/flat-ui/2.1.3_free/application.js',
            'js/angularjs/1.2.12/angular.min.js',
            'js/moment/min/moment.min.js', 'js/moment/min/lang/zh-tw.js',
            'js/midway/1.0.0/midway.min.js',
            'js/google-code-prettify/20130304/run_prettify.js',
            'js/admin/common.js'
        );

        // $merge_default_css = $this->merge(array('path' => configPathBase.'css/', 'from' => $default_css, 'to' => 'merge_backend_default.css'));
        // $merge_default_js = $this->merge(array('path' => configPathBase.'js/', 'from' => $default_js, 'to' => 'merge_backend_default.js'));
        // if($merge_default_css !== FALSE) $default_css = $merge_default_css;
        // if($merge_default_js !== FALSE) $default_js = $merge_default_js;

        if(isset($data['css']) === FALSE || empty($data['css']) === TRUE)
        {
            $data['css'] = $default_css;
        }
        else
        {
            $data['css'] = array_merge((array)$default_css, (array)$data['css']);
        }

        if(isset($data['js']) === FALSE || empty($data['js']) === TRUE)
        {
            $data['js'] = $default_js;
        }
        else
        {
            $data['js'] = array_merge((array)$default_js, (array)$data['js']);
        }

        // $merge_css = $this->merge(array('path' => configPathBase.'css/', 'from' => $data['css'], 'extension' => 'css'));
        // $merge_js = $this->merge(array('path' => configPathBase.'js/', 'from' => $data['js'], 'extension' => 'js'));
        // if($merge_css !== FALSE) $data['css'] = $merge_css;
        // if($merge_js !== FALSE) $data['js'] = $merge_js;

        $var['css'] = $data['css'];
        $var['js'] = $data['js'];
        $var['config'] = $this->config;
        $var['user'] = $this->user;

        if(isset($data['json'])) $var['json'] = $data['json'];
        if(isset($data['data'])) $var['data'] = $data['data'];

        $var['uri'] = $this->CI->uri->segment_array();
        // $var['page'] = Page::find_by_uri($var['uri']);
        // $var['pages'] = Page::all(array('conditions' => 'enable = 1', 'order' => 'sort asc'));
        
        $var['name'] = (isset($data['name']) ? $data['name'] : '');
        $var['title'] = ($var['name'] ? $data['name'].'-' : '').$this->config['site_name'];
        
        if(isset($data['html'])) $var['container'] = $this->CI->load->view('/admin/'.$data['html'], $var, TRUE);

        $this->CI->load->view('admin/template/'.(isset($data['template']) ? $data['template'] : 'template'), $var);

        if($this->config['environment'] == 'development') $this->CI->output->enable_profiler(TRUE);
    }

    public function merge($data = FALSE) {
        if($data === FALSE) return FALSE;
        if(isset($data['path']) === FALSE) return FALSE;
        if(isset($data['from']) === FALSE) return FALSE;
        if(count($data['from']) === 1) return FALSE;
        
        if(isset($data['to']) === FALSE)
        {
            $data['to'] = '';
            foreach((array)$data['from'] as $key => $value)
            {
                list($dirname, $basename, $extension, $filename) = array_values(pathinfo($value));
                $data['to'] = $data['to'].$filename;
                if($key != count($data['from'])-1) $data['to'] = $data['to'].'_';
            }
            if(isset($data['extension']) === TRUE) $data['to'] = $data['to'].'.'.$data['extension'];
        }

        $need_merge = FALSE;
        $to_timestamp = 0;
        if(file_exists($data['path'].$data['to'])) $to_timestamp = filemtime($data['path'].$data['to']);

        $from_timestamp = 0;
        foreach((array)$data['from'] as $key => $value)
        {
            if(file_exists($data['path'].$value))
            {
                $from_timestamp = filemtime($data['path'].$value);
                if($from_timestamp >= $to_timestamp)
                {
                    $need_merge = TRUE;
                    break;
                }
            }
        }

        if($need_merge === FALSE) return $data['to'];
        
        $contents = '';
        foreach((array)$data['from'] as $key => $value)
        {
            if(file_exists($data['path'].$value))
            {
                $contents = $contents.file_get_contents($data['path'].$value, FILE_USE_INCLUDE_PATH)."\n";
            }
        }

        file_put_contents($data['path'].$data['to'], $contents);

        return $data['to'];
    }

    private function create_table() {
        if(!$this->CI->db->table_exists('configs'))
        {
            $this->CI->load->dbforge();
            $fields = array('id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
                            'name' => array('type' =>'VARCHAR', 'constraint' => '64'),
                            'value' => array('type' =>'VARCHAR', 'constraint' => '128'));
            $this->CI->dbforge->add_field($fields);
            $this->CI->dbforge->add_key('id', TRUE);
            $this->CI->dbforge->create_table('configs');
            $this->CI->db->query('CREATE UNIQUE INDEX name ON configs (name)');

            if(!Config::find_by_name('site_name')) Config::create(array('name' => 'site_name', 'value' => ''));
            if(!Config::find_by_name('site_keywords')) Config::create(array('name' => 'site_keywords', 'value' => ''));
            if(!Config::find_by_name('site_description')) Config::create(array('name' => 'site_description', 'value' => ''));
            if(!Config::find_by_name('site_logo')) Config::create(array('name' => 'site_logo', 'value' => ''));
            if(!Config::find_by_name('company_name')) Config::create(array('name' => 'company_name', 'value' => ''));
            if(!Config::find_by_name('company_address')) Config::create(array('name' => 'company_address', 'value' => ''));
            if(!Config::find_by_name('company_telephone')) Config::create(array('name' => 'company_telephone', 'value' => ''));
            if(!Config::find_by_name('company_email')) Config::create(array('name' => 'company_email', 'value' => ''));
            if(!Config::find_by_name('facebook_url')) Config::create(array('name' => 'facebook_url', 'value' => ''));
            if(!Config::find_by_name('facebook_id')) Config::create(array('name' => 'facebook_id', 'value' => ''));
            if(!Config::find_by_name('facebook_api')) Config::create(array('name' => 'facebook_api', 'value' => ''));
            if(!Config::find_by_name('facebook_secret')) Config::create(array('name' => 'facebook_secret', 'value' => ''));
            if(!Config::find_by_name('google_account')) Config::create(array('name' => 'google_account', 'value' => ''));
            if(!Config::find_by_name('google_password')) Config::create(array('name' => 'google_password', 'value' => ''));
            if(!Config::find_by_name('google_analysisProfileId')) Config::create(array('name' => 'google_analysisProfileId', 'value' => ''));
            if(!Config::find_by_name('google_analysisTrackingId')) Config::create(array('name' => 'google_analysisTrackingId', 'value' => ''));
        }
    }
}