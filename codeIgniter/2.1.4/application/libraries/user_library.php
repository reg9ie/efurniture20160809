<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_library {
    public $CI;
    public $self = FALSE;
    
    function __construct() {
        $this->CI =& get_instance();
        
        $this->CI->load->model('user_model');
        
        if($this->CI->input->cookie('user'))
        {
            $cookie = json_decode($this->CI->input->cookie('user'), TRUE);

            $user['id'] = $cookie['id'];
            $user['account'] = $cookie['account'];
            $user['name'] = $cookie['name'];
            $user['security'] = $cookie['security'];
            $this->CI->user->set_userdata('user', json_encode($user));
            unset($cookie, $user);
        }
        
        $session = json_decode($this->CI->session->userdata('user'), TRUE);
        if(isset($session['id']))
        {
            $users = $this->CI->user_model->get_user(array(
                'id' => $session['id']
            ));
            
            if(count($users) == 1 && $session['security'] == hash('sha512', $users[0]['account'] + $users[0]['password']))
            {
                $this->self = $users[0];
            }
            else
            {
                $this->CI->input->set_cookie('user', '', -2592000);
        
                $this->CI->session->unset_userdata('user');
                $this->CI->session->sess_destroy();
            }
        }
    }
    
    public function add_user($data = FALSE) {
        if($data === FALSE) return FALSE;
        
        $id = $this->CI->user_model->set_user($data);
        
        return $id;
    }
    
    public function get_user($data = FALSE) {
        $result = $this->CI->user_model->get_user($data);
        
        return $result;
    }

    public function edit_user($data = FALSE) {
        if($data === FALSE) return FALSE;
        if(isset($data['id']) === FALSE) return FALSE;
        
        $this->CI->user_model->set_user($data);
        
        return TRUE;
    }

    public function del_user($data = FALSE) {
        if($data === FALSE) return FALSE;

        $this->CI->user_model->del_user($data);
    }
    
    public function login($data = FALSE) {
        if($data === FALSE) return FALSE;

        if((isset($data['account']) === FALSE || isset($data['password']) === FALSE)) return FALSE;
        if(isset($data['cookie']) === FALSE) $data['cookie'] = FALSE;
        
        $result = $this->CI->user_model->get_user($data);
        
        if(count($result) != 1) return FALSE;

        $user['id'] = $result[0]['id'];
        $user['account'] = $result[0]['account'];
        $user['name'] = $result[0]['name'];
        $user['security'] = hash('sha512', $result[0]['account']+$result[0]['password']);
        
        $this->CI->session->set_userdata('user', json_encode($user));
        
        if($data['cookie'] === TRUE)
        {
            $this->CI->input->set_cookie('user', json_encode($user), 2592000);
        }
        else
        {
            $this->CI->input->set_cookie('user', '', -2592000);
        }
        
        return TRUE;
    }
    
    public function logout() {
        $this->CI->input->set_cookie('user', '', -2592000);
        
        $this->CI->session->unset_userdata('user');
        $this->CI->session->sess_destroy();
    }
    
    public function isLogin() {
        if(isset($this->self) === FALSE) return FALSE;
        if($this->self === FALSE) return FALSE;

        return TRUE;
    }
    
    public function isSelf($id = FALSE) {
        if($id === FALSE) return FALSE;
        if($this->self['id'] != $id) return FALSE;
        
        return TRUE;
    }
    
    public function isAdmin($id = FALSE) {
        if($id === FALSE) return FALSE;
        
        $result = $this->CI->user_model->get_user(array('id' => $id));
        
        if(count($result) != 1) return FALSE;
        if($result[0]['status'] != 'user_statusEnable') return FALSE;
        if($result[0]['role'] != 'user_roleAdmin') return FALSE;

        return TRUE;
    }
    
    public function exist_user_id($id = FALSE) {
        if($id === FALSE) return FALSE;
        
        $result = $this->CI->user_model->get_user(array('id' => $id));
        
        if(count($result) != 1) return FALSE;
        
        return TRUE;
    }
}