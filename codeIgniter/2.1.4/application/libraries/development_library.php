<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Development_library {
    public $CI;
    public $config;
    
    function __construct() {
        $this->CI =& get_instance();
        
        $this->config['mode']    = configDevelopmentMode;
    }
}