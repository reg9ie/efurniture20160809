<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class File_library {
    public $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    public function upload($path = FALSE) {
        if($path === FALSE) return FALSE;

        $realPath = $this->get_path($path);
        if(!is_dir($realPath))
        {
            $oldmask = umask(0);
            mkdir($realPath, 0777);
            umask($oldmask);
        }

        $config['upload_path'] = $realPath;
        $config['allowed_types'] = configFileAllowedTypes;
        $config['max_size'] = configFileMaxSize;
        $config['encrypt_name'] = TRUE;

        $this->CI->load->library('upload', $config);
        if($this->CI->upload->do_upload('file'))
        {
            $file = $this->CI->upload->data();
            return $file['file_name'];
        }
        return FALSE;
    }

    public function uploads($path = FALSE, $thumbnail = FALSE) {
        if($path === FALSE) return FALSE;

        $realPath = $this->get_path($path);
        if(!is_dir($realPath))
        {
            $oldmask = umask(0);
            mkdir($realPath, 0777);
            umask($oldmask);
        }

        $config['upload_path'] = $realPath;
        $config['allowed_types'] = configImageAllowedTypes;
        $config['max_size'] = configFileMaxSize;
        $config['encrypt_name'] = TRUE;

        $this->CI->load->library('upload');
        $this->CI->upload->initialize($config);
        if($this->CI->upload->do_multi_upload('files'))
        {
            $file_names = array();
            $files = $this->CI->upload->get_multi_upload_data();
            foreach((array)$files as $i => $f)
            {
                $file_names[] = $f['file_name'];
            }
            return $file_names;
        }
        else return FALSE;
    }

    public function del($path = FALSE, $filename = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        
        if(file_exists($realPath.$filename)) unlink($realPath.$filename);
    }

    public function move($path = FALSE, $filename = FALSE, $dest = FALSE) {
        if($path === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;
        if($dest === FALSE) return FALSE;
        
        $realPath = $this->get_path($path);
        $realDest = $this->get_path($dest);
        
        if(file_exists($realPath.$filename) && file_exists($realDest.$filename) == FALSE) rename($realPath.$filename, $realDest.$filename);
    }
    
    public function get_path($path = FALSE) {
        if($path === FALSE) return FALSE;

        $path_array = explode('/', $path);
        switch($path_array[0])
        {
            case 'temp':
                $realPath = configPathFileTemp;
                break;
            case 'download':
                $realPath = configPathFileDownload;
                break;
            case 'news':
                $realPath = configPathFileNews;
                break;
            case 'article':
                $realPath = configPathFileArticle;
                break;
            case 'product':
                $realPath = configPathFileProduct;
                break;
            default:
                return FALSE;
        }
        if(count($path_array) == 1) return $realPath;

        unset($path_array[0]);
        return $realPath.implode('/', $path_array).'/';
    }

    public function get_url($path = FALSE) {
        switch($path)
        {
            case 'temp':
                return configUrlFileTemp;
            case 'download':
                return configUrlFileDownload;
            case 'news':
                return configUrlFileNews;
            case 'article':
                return configUrlFileArticle;
            case 'product':
                return configUrlFileProduct;
            default:
                return FALSE;
        }
    }
}