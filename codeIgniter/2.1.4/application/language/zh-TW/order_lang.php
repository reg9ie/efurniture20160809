<?php

$lang['order_pending'] = "未付款";
$lang['order_paid'] = "已付款";
$lang['order_remited'] = "已匯款";
$lang['order_processing'] = "處理中";
$lang['order_custom-made'] = "訂製中";
$lang['order_transfer-cargo'] = "調貨中";
$lang['order_ready'] = "準備出貨";
$lang['order_back-ordered'] = "延遲出貨";
$lang['order_out-of-stock'] = "缺貨";
$lang['order_unship'] = "未出貨";
$lang['order_shipping'] = "已出貨";
$lang['order_arrived'] = "已到貨";
$lang['order_unreturn'] = "未退貨";
$lang['order_returning'] = "退貨中";
$lang['order_return-check'] = "審核中";
$lang['order_unrefund'] = "未退款";
$lang['order_refunding'] = "退款中";
$lang['order_refunded'] = "已退款";
$lang['order_returned'] = "已退貨";
$lang['order_exchanging'] = "換貨中";
$lang['order_exchange-check'] = "審核中";
$lang['order_exchange-shipping'] = "貨運中";
$lang['order_exchanged'] = "已換貨";
$lang['order_canceling'] = "取消中";
$lang['order_cancel'] = "取消";
$lang['order_enable'] = "啟用";

$lang['order_atm'] = "ATM";
$lang['order_credit'] = "信用卡";