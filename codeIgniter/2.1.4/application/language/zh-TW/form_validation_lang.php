<?php

$lang['required']			= "%s 欄位必須存在.";
$lang['isset']				= "%s 欄位必須有值.";
$lang['valid_email']		= "%s 欄位必須是可驗證的電子郵件位址.";
$lang['valid_emails']		= "%s 欄位必須是可驗證的電子郵件位址.";
$lang['valid_url']			= "%s 欄位必須是可驗證的網址.";
$lang['valid_ip']			= "%s 欄位必須是可驗證的IP.";
$lang['min_length']			= "%s 欄位最小長度為 %s.";
$lang['max_length']			= "%s 欄位最大長度為 %s.";
$lang['exact_length']		= "%s field must be exactly %s characters in length.";
$lang['alpha']				= "%s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "%s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "%s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "%s field must contain only numbers.";
$lang['is_numeric']			= "%s field must contain only numeric characters.";
$lang['integer']			= "%s field must contain an integer.";
$lang['regex_match']		= "%s field is not in the correct format.";
$lang['matches']			= "%s field does not match the %s field.";
$lang['is_unique'] 			= "%s field must contain a unique value.";
$lang['is_natural']			= "%s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "%s field must contain a number greater than zero.";
$lang['decimal']			= "%s field must contain a decimal number.";
$lang['less_than']			= "%s field must contain a number less than %s.";
$lang['greater_than']		= "%s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */