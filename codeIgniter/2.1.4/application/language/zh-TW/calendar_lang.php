<?php

$lang['cal_su']			= "Su";
$lang['cal_mo']			= "Mo";
$lang['cal_tu']			= "Tu";
$lang['cal_we']			= "We";
$lang['cal_th']			= "Th";
$lang['cal_fr']			= "Fr";
$lang['cal_sa']			= "Sa";
$lang['cal_sun']		= "日";
$lang['cal_mon']		= "一";
$lang['cal_tue']		= "二";
$lang['cal_wed']		= "三";
$lang['cal_thu']		= "四";
$lang['cal_fri']		= "五";
$lang['cal_sat']		= "六";
$lang['cal_sunday']		= "日";
$lang['cal_monday']		= "一";
$lang['cal_tuesday']	= "二";
$lang['cal_wednesday']	= "三";
$lang['cal_thursday']	= "四";
$lang['cal_friday']		= "五";
$lang['cal_saturday']	= "六";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "May";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Oct";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "January";
$lang['cal_february']	= "February";
$lang['cal_march']		= "March";
$lang['cal_april']		= "April";
$lang['cal_mayl']		= "May";
$lang['cal_june']		= "June";
$lang['cal_july']		= "July";
$lang['cal_august']		= "August";
$lang['cal_september']	= "September";
$lang['cal_october']	= "October";
$lang['cal_november']	= "November";
$lang['cal_december']	= "December";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */