<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function _sort($elements) {
    usort($elements, function($source, $target) {
        return $source['sort'] - $target['sort'];
    });

    $sort = 1;
    foreach((array)$elements as $index => $element)
    {
        $elements[$index]['sort'] = $sort;
        $sort++;
    }

    return $elements;
}

function _move_before($elements, $source_sort, $destination_sort) {
    foreach((array)$elements as $index => $element)
    {
        if($element['sort'] == $source_sort)
        {
            $source_index = $index;
        }

        if($element['sort'] == $destination_sort)
        {
            $target_index = $index;
        }
    }
    
    $target = array();
    $sort = 1;
    foreach((array)$elements as $index => $element)
    {
        if($element['sort'] != $source_sort && $element['sort'] != $destination_sort)
        {
            $element['sort'] = $sort;
            $target[] = $element;
            $sort++;
        }
        elseif($element['sort'] == $destination_sort)
        {
            $source = $elements[$source_index];
            $source['sort'] = $sort;
            $target[] = $source;
            $sort++;

            $element['sort'] = $sort;
            $target[] = $element;
            $sort++;
        }
    }
    
    return $target;
}

function _move_after($elements, $source_sort, $destination_sort) {
    foreach((array)$elements as $index => $element)
    {
        if($element['sort'] == $source_sort)
        {
            $source_index = $index;
        }

        if($element['sort'] == $destination_sort)
        {
            $target_index = $index;
        }
    }
    
    $target = array();
    $sort = 1;
    foreach((array)$elements as $index => $element)
    {
        if($element['sort'] != $source_sort && $element['sort'] != $destination_sort)
        {
            $element['sort'] = $sort;
            $target[] = $element;
            $sort++;
        }
        elseif($element['sort'] == $destination_sort)
        {
            $element['sort'] = $sort;
            $target[] = $element;
            $sort++;

            $source = $elements[$source_index];
            $source['sort'] = $sort;
            $target[] = $source;
            $sort++;
        }
    }
    
    return $target;
}