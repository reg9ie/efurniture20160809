<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('directory_map')) {
    function directory_map($source_dir, $directory_depth = 0, $hidden = FALSE) {
        if($fp = @opendir($source_dir))
        {
            $filedata   = array();
            $new_depth  = $directory_depth - 1;
            $source_dir = rtrim($source_dir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

            while (FALSE !== ($file = readdir($fp)))
            {
                // Remove '.', '..', and hidden files [optional]
                if(!trim($file, '.') OR ($hidden == FALSE && $file[0] == '.'))
                {
                    continue;
                }

                if(($directory_depth < 1 OR $new_depth > 0) && @is_dir($source_dir.$file))
                {
                    $filedata[$file] = directory_map($source_dir.$file.DIRECTORY_SEPARATOR, $new_depth, $hidden);
                }
                else
                {
                    $filedata[] = $file;
                }
            }

            closedir($fp);
            return $filedata;
        }

        return FALSE;
    }
}

if(!function_exists('directory_copy')) {
    function directory_copy($srcdir, $dstdir) {
        //preparing the paths
        $srcdir = rtrim($srcdir,'/');
        $dstdir = rtrim($dstdir,'/');

        //creating the destination directory
        if(!is_dir($dstdir)) mkdir($dstdir, 0777, true);

        //Mapping the directory
        $dir_map = directory_map($srcdir);

        foreach((array)$dir_map as $object_key => $object_value)
        {
            if(is_dir($srcdir.'/'.$object_key)) directory_copy($srcdir.'/'.$object_key, $dstdir.'/'.$object_key);//this is a directory
            else copy($srcdir.'/'.$object_value, $dstdir.'/'.$object_value);//This is a File not a directory
        }
    }
}

if(!function_exists('directory_delete')) {
    function directory_delete($dir) {
        if(file_exists($dir) === FALSE) return true;
        if(is_dir($dir) === FALSE || is_link($dir) === TRUE) return unlink($dir);
        
        foreach(scandir($dir) as $item)
        {
            if($item == '.' || $item == '..') continue;
            if(directory_delete($dir . "/" . $item) === FALSE)
            {
                @chmod($dir . "/" . $item, 0777);
                if (directory_delete($dir . "/" . $item) === FALSE) return false;
            }
        }

        return rmdir($dir);
    }
}