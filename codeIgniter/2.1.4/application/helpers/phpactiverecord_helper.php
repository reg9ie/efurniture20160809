<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function to_array($datas = FALSE, $attribute = FALSE) {
    if($datas === FALSE) return FALSE;

    $to_array = array();
    foreach((array)$datas as $data) {
        if($attribute === FALSE)
        {
            array_push($to_array, $data->to_array());
        }
        else
        {
            array_push($to_array, $data->to_array($attribute));
        }
    }

    return $to_array;
}

function to_json($datas = FALSE, $attribute = FALSE) {
    if($datas === FALSE) return FALSE;

    $to_array = array();
    foreach((array)$datas as $data) {
        if($attribute === FALSE)
        {
            array_push($to_array, $data->to_array());
        }
        else
        {
            array_push($to_array, $data->to_array($attribute));
        }
    }
    
    return json_encode($to_array);
}