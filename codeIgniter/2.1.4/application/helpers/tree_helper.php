<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function tree($tree = FALSE, $id_name = FALSE) {
    if($tree === FALSE) return FALSE;
    if($id_name === FALSE) $id_name = 'id';
    if(empty($tree)) return $tree;
    
    if($id_name != 'id') $tree = set_tree_id($tree, $id_name);

    $left = 1;
    $roots = get_tree_roots($tree);
    foreach((array)$roots as $key => $value)
    {
        list($tree , ) = rebuild_tree_subfunction($tree , $value['id'] , $left);
        $root = get_tree_node($tree, $value['id']);
        $left = $root['right'] + 1;
    }
    
    $tree = sort_tree($tree);
    $tree = write_tree_layer($tree);

    return $tree;
}

function set_tree_id($tree = FALSE, $id_name = FALSE) {
    if($tree === FALSE) return FALSE;
    if($id_name === FALSE) return FALSE;

    foreach((array)$tree as $key => $value)
    {
        $tree[$key]['id'] = $tree[$key][$id_name];
    }

    return $tree;
}

function rebuild_tree_subfunction($tree , $parent_id , $left_id) {
    $filter_tree = array();
    $right_id = $left_id + 1;
    foreach((array)$tree as $node)
    {
        if($node['parent'] == $parent_id) $filter_tree[] = $node;
    }
    
    foreach((array)$filter_tree as $node)
    {
        list($tree , $right_id) = rebuild_tree_subfunction($tree , $node['id'], $right_id);
    }
    
    for($i = 0 ; $i < count($tree) ; $i++)
    {
        if($tree[$i]['id'] == $parent_id)
        {
            $tree[$i]['left'] = $left_id;
            $tree[$i]['right'] = $right_id;
        }
    }
    
    return array($tree , $right_id + 1);
}

function get_tree_roots($tree = FALSE) {
    if($tree === FALSE) return FALSE;

    $roots = array();
    foreach((array)$tree as $node)
    {
        if($node['parent'] == 0) $roots[] = $node;
    }
    
    return $roots;
}

function get_tree_node($tree = FALSE, $node_id = FALSE) {
    if($tree === FALSE) return FALSE;
    if($node_id === FALSE) return FALSE;

    foreach((array)$tree as $node)
    {
        if($node['id'] == $node_id) return $node;
    }
    
    return FALSE;
}

function get_tree_parent_nodes($tree = FALSE , $root_node_id = FALSE) {
    if($tree === FALSE) return FALSE;
    if($root_node_id === FALSE) return FALSE;

    foreach((array)$tree as $node)
    {
        if($node['id'] == $root_node_id) $root = $node;
    }

    if(isset($root) === FALSE) return FALSE;
    
    $new_tree = array();
    foreach((array)$tree as $node)
    {
        if($node['left'] <= $root['left'] AND $node['right'] >= $root['right'] ) $new_tree[] = $node;
    }
    
    return $new_tree;
}

function get_tree_sub_nodes($tree , $root_node_id) {
    foreach((array)$tree as $node)
    {
        if($node['id'] == $root_node_id) $root = $node;
    }
    
    $new_tree = array();
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $root['left'] AND $node['right'] <= $root['right'] ) $new_tree[] = $node;
    }
    
    return $new_tree;
}

function get_tree_next_layer_nodes($tree , $root_node_id) {
    foreach((array)$tree as $node)
    {
        if($node['id'] == $root_node_id) $root = $node;
    }
    
    $new_tree = array();
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $root['left'] AND $node['right'] <= $root['right'] AND $node['layer'] == $root['layer']+1) $new_tree[] = $node;
    }
    
    return $new_tree;
}

function write_tree_layer($tree) {
    $tree[0]['layer'] = 0;
    $layer = 0;
    for($i = 1 ; $i < count($tree) ; $i++)
    {
        if(($tree[$i-1]['left'] + 1) == ($tree[$i]['left']))
        {
            $layer++;
        }
        else
        {
            for($ii = ($i-1) ; $ii >= 0 ; $ii--)
            {
                if(($tree[$ii]['right'] + 1) == $tree[$i]['left'])
                {
                    $layer = $tree[$ii]['layer'];
                }
            }
        }
        $tree[$i]['layer'] = $layer;
    }
    
    return $tree;
}

function sort_tree($tree = FALSE, $by = 'left') {
    if($tree === FALSE) return FALSE;

    $sort_tree = array();
    foreach((array)$tree as $node)
    {
        if(count($sort_tree) == 0) $sort_tree[] = $node;
        else
        {
            if($sort_tree[count($sort_tree) - 1][$by] <= $node[$by])
            {
                $sort_tree[] = $node;
            }
            else
            {
                $temp_node = $sort_tree[count($sort_tree) - 1];
                $sort_tree[count($sort_tree) - 1] = $node;
                $sort_tree[] = $temp_node;
                $sort_tree = sort_tree($sort_tree, $by);
            }
        }
    }
    
    $tree = $sort_tree;
    
    return $tree;
}

function query_previous_sibling_node($tree , $source_node_id) {
    foreach((array)$tree as $node)
    {
        if($node['id'] == $source_node_id)
        {
            $source_node_left = $node['left'];
            $next_sibling_node_right = $source_node_left - 1;
        }
    }
    
    foreach((array)$tree as $node)
    {
        if($node['right'] == $next_sibling_node_right) return $node;
    }
    
    return FALSE;
}

function query_next_sibling_node($tree , $source_node_id) {
    foreach((array)$tree as $node)
    {
        if($node['id'] == $source_node_id)
        {
            $source_node_right = $node['right'];
            $next_sibling_node_left = $source_node_right + 1;
        }
    }
    
    foreach((array)$tree as $node)
    {
        if($node['left'] == $next_sibling_node_left) return $node;
    }
    
    return FALSE;
}

function move_node_to_previous_sibling($tree , $source_node_id) {
    $previous_sibling_node = query_previous_sibling_node($tree , $source_node_id);

    $source_node = get_tree_node($tree , $source_node_id);

    if($previous_sibling_node == FALSE) return FALSE;
    if($source_node == FALSE) return FALSE;
    
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $previous_sibling_node['left'] AND $previous_sibling_node['right'] >= $node['right']) $previous_sibling_tree[] = $node;
        elseif($node['left'] >= $source_node['left'] AND $source_node['right'] >= $node['right']) $source_tree[] = $node;
    }
    
    $add_source_tree_is_done = FALSE;
    $add_previous_sibling_tree_is_done = FALSE;
    
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $previous_sibling_node['left'] AND $node['right']<= $previous_sibling_node['right'])
        {
            if($add_previous_sibling_tree_is_done == FALSE)
            {
                foreach((array)$source_tree as $source_tree_node)
                {
                    $result_tree[] = $source_tree_node;
                }
                $add_previous_sibling_tree_is_done = TRUE;
            }
        }
        elseif($node['left'] >= $source_node['left'] AND $node['right'] <= $source_node['right'])
        {
            if($add_previous_sibling_is_done == FALSE)
            {
                foreach((array)$previous_sibling_tree as $previous_sibling_tree_node)
                {
                    $result_tree[] = $previous_sibling_tree_node;
                }
                $add_previous_sibling_is_done = TRUE;
            }
        }
        else $result_tree[] = $node;
    }
    
    $result_tree = tree($result_tree);
    
    return $result_tree;
}

function move_node_to_next_sibling($tree , $source_node_id) {
    $next_sibling_node = query_next_sibling_node($tree , $source_node_id);
    $source_node = get_tree_node($tree , $source_node_id);
    
    if($next_sibling_node == FALSE) return FALSE;
    if($source_node == FALSE) return FALSE;
    
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $source_node['left'] AND $node['right'] <= $source_node['right']) $source_tree[] = $node;
        elseif($node['left'] >= $next_sibling_node['left'] AND $node['right'] <= $next_sibling_node['right']) $next_sibling_tree[] = $node;
    }
    
    $add_source_tree_is_done = FALSE;
    $add_next_sibling_tree_is_done = FALSE;
    
    foreach((array)$tree as $node)
    {
        if($node['left'] >= $source_node['left'] AND $node['right'] <= $source_node['right'])
        {
            if($add_next_sibling_tree_is_done == FALSE)
            {
                foreach((array)$next_sibling_tree as $next_sibling_tree_node)
                {
                    $result_tree[] = $next_sibling_tree_node;
                }
                $add_next_sibling_tree_is_done = TRUE;
            }
        }
        elseif($node['left'] >= $next_sibling_node['left'] AND $node['right'] <= $next_sibling_node['right'])
        {
            if($add_source_tree_is_done == FALSE)
            {
                foreach((array)$source_tree as $source_tree_node)
                {
                    $result_tree[] = $source_tree_node;
                }
                $add_source_tree_is_done = TRUE;
            }
        }
        else $result_tree[] = $node;
    }
    
    $result_tree = tree($result_tree);
    
    return $result_tree;
}

function insert_node_to_tree_structure($tree = FALSE, $parent_id = FALSE, $source = FALSE) {
    if($tree === FALSE) return array(FALSE, FALSE);
    if($parent_id === 0)
    {
        $tree[] = $source;
        return array(TRUE, $tree);
    }

    $insert = FALSE;
    foreach((array)$tree as $i => $n)
    {
        if($insert === TRUE) continue;
        if($n['right']-1 > $n['left'] && !isset($n['childrens'])) $n['childrens'] = array();

        if($n['id'] == $parent_id)
        {
            $n['childrens'][] = $source;

            $tree[$i] = $n;
            $insert = TRUE;
        }
        elseif($n['id'] != $parent_id && isset($n['childrens']))
        {
            foreach((array)$n['childrens'] as $j => $m)
            {
                if($insert === TRUE) continue;
                list($insert, $nodes) = insert_node_to_tree_structure($n['childrens'], $parent_id, $source);
                if($insert)
                {
                    $n['childrens'] = $nodes;
                    $tree[$i] = $n;
                }
            }
        }
    }

    return array($insert, $tree);
}

function to_tree_structure($tree = FALSE) {
    if($tree === FALSE) return FALSE;

    $new_tree = array();
    foreach((array)$tree as $i => $n)
    {
        list($insert, $new_tree) = insert_node_to_tree_structure($new_tree, $n['parent'], $n);
    }

    return $new_tree;
}

function tree_to_html_list($tree = FALSE, $parent = FALSE, $type = FALSE, $attributes = FALSE)
{
    if($tree === FALSE) return FALSE;
    if($parent === FALSE) $parent = 0;
    if($type === FALSE) $type = 'ul';

    $root_class = '';
    $root_attr = '';
    $node_class = '';
    $node_attr = '';
    $node_content_tag_start = '';
    $node_content_tag_end = '';
    if($attributes !== FALSE)
    {
        if(isset($attributes['root_class'])) $root_class = $attributes['root_class'];
        if(isset($attributes['root_attr'])) $root_attr = $attributes['root_attr'];
        if(isset($attributes['node_class'])) $node_class = $attributes['node_class'];
        if(isset($attributes['node_attr'])) $node_attr = $attributes['node_attr'];
        if(isset($attributes['node_content_tag_start'])) $node_content_tag_start = $attributes['node_content_tag_start'];
        if(isset($attributes['node_content_tag_end'])) $node_content_tag_end = $attributes['node_content_tag_end'];
    }

    $out = "<".$type.($root_class ? ' class="'.$root_class.'"' : '').">\n";
    foreach((array)$tree as $i => $n)
    {
        if($n['parent'] == $parent)
        {
            $out .= "<li".($node_class ? ' class="'.$node_class.'"' : '').">".$node_content_tag_start.$n['name'].$node_content_tag_end;
            
            $sub_nodes = array();
            foreach((array)$tree as $j => $m)
            {
                if($n['left'] < $m['left'] && $n['right'] > $m['right']) $sub_nodes[] = $m;
            }
            if($sub_nodes) $out .= tree_to_html_list($sub_nodes, $n['id'], $type, $attributes);

            $out .= "</li>\n";
        }
    }
    $out .= "</".$type.">\n";

    return $out;
}

// function test() {
//     $tree[0]['id'] = 1;
//     $tree[0]['parent'] = 0;
//     $tree[0]['name'] = '論壇';
//     $tree[0]['left'] = 0;
//     $tree[0]['right'] = 0;

//     $tree[1]['id'] = 2;
//     $tree[1]['parent'] = 1;
//     $tree[1]['name'] = '手機';
//     $tree[1]['left'] = 0;
//     $tree[1]['right'] = 0;

//     $tree[2]['id'] = 3;
//     $tree[2]['parent'] = 1;
//     $tree[2]['name'] = 'PDA';
//     $tree[2]['left'] = 0;
//     $tree[2]['right'] = 0;

//     $tree[3]['id'] = 4;
//     $tree[3]['parent'] = 10;
//     $tree[3]['name'] = 'CPU';
//     $tree[3]['left'] = 0;
//     $tree[3]['right'] = 0;

//     $tree[4]['id'] = 5;
//     $tree[4]['parent'] = 3;
//     $tree[4]['name'] = 'Android';
//     $tree[4]['left'] = 0;
//     $tree[4]['right'] = 0;

//     $tree[5]['id'] = 6;
//     $tree[5]['parent'] = 4;
//     $tree[5]['name'] = 'Intel';
//     $tree[5]['left'] = 0;
//     $tree[5]['right'] = 0;

//     $tree[6]['id'] = 7;
//     $tree[6]['parent'] = 1;
//     $tree[6]['name'] = '美容';
//     $tree[6]['left'] = 0;
//     $tree[6]['right'] = 0;

//     $tree[7]['id'] = 8;
//     $tree[7]['parent'] = 4;
//     $tree[7]['name'] = 'Amd';
//     $tree[7]['left'] = 0;
//     $tree[7]['right'] = 0;

//     $tree[8]['id'] = 9;
//     $tree[8]['parent'] = 2;
//     $tree[8]['name'] = 'Moto';
//     $tree[8]['left'] = 0;
//     $tree[8]['right'] = 0;

      
//     $tree[9]['id'] = 10;
//     $tree[9]['parent'] = 1;
//     $tree[9]['name'] = '電腦';
//     $tree[9]['left'] = 0;
//     $tree[9]['right'] = 0;

//     foreach((array)$tree as $node)
//     {
//       echo 'id '.$node['id'];
//       echo ' parent '.$node['parent'];
//       echo ' name '.$node['name'];
//       echo ' left '.$node['left'];
//       echo ' right '.$node['right'].'<br/>';
//     }

//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';

//     $tree = tree($tree);

//     foreach((array)$tree as $node)
//     {
//       echo 'id '.$node['id'];
//       echo ' parent '.$node['parent'];
//       echo ' name '.$node['name'];
//       echo ' left '.$node['left'];
//       echo ' right '.$node['right'].'<br/>';
//     }

//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';

//     $tree = get_tree_parent_nodes($tree , 10);

//     foreach((array)$tree as $node)
//     {
//       echo 'id '.$node['id'];
//       echo ' parent '.$node['parent'];
//       echo ' name '.$node['name'];
//       echo ' left '.$node['left'];
//       echo ' right '.$node['right'].'<br/>';
//     }

//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';

//     $tree = sort_tree($tree);

//     foreach((array)$tree as $node)
//     {
//       echo 'id '.$node['id'];
//       echo ' parent '.$node['parent'];
//       echo ' name '.$node['name'];
//       echo ' left '.$node['left'];
//       echo ' right '.$node['right'].'<br/>';
//     }

//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';
//     echo '<br/>';

//     $tree = move_node_to_previous_sibling($tree , 7);

//     foreach((array)$tree as $node)
//     {
//       echo 'id '.$node['id'];
//       echo ' parent '.$node['parent'];
//       echo ' name '.$node['name'];
//       echo ' left '.$node['left'];
//       echo ' right '.$node['right'].'<br/>';
//     }

//     echo tree_to_html_list($todos, 0, 'ul', array('root_class' => 'dd-lists', 'node_class' => 'dd-item', 'node_content_tag_start' => '<div>', 'node_content_tag_end' => '</div>'));
// }