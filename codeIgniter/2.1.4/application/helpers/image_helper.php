<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function thumbname($filename = false, $thumb_marker = '_thumb') {
    if($filename === false || empty($filename)) return '';

    $path_parts = pathinfo($filename);

    $ext = isset($path_parts['extension']) ? $path_parts['extension'] : 'img';
    $main = substr($filename, 0, strpos($filename, '.'.$ext));

    $thumbname = $main.$thumb_marker.'.'.$ext;

    return $thumbname;
}

function image_base64($file_fullpath = false) {
    if($file_fullpath === false || file_exists($file_fullpath) === false) return false;

    $type = mime_content_type($file_fullpath);
    $data = file_get_contents($file_fullpath);
    $base64 = 'data: '.$type.';base64, '.base64_encode($data);

    return $base64;
}