<?php
// Adds a third argument to timespan() that stops display of minutes/
// seconds in the final output
function timespan($seconds = 1, $time = '', $str_separate = false, $display_mins = true, $display_secs = true) {
    $CI = & get_instance();
    $CI->lang->load('date');

    if($str_separate === false) $str_separate = ', ';

    if (!is_numeric($seconds)) {
        $seconds = 1;
    }

    if (!is_numeric($time)) {
        $time = time();
    }

    if ($time <= $seconds) {
        $seconds = 1;
    } else {
        $seconds = $time - $seconds;
    }

    $str = '';
    $years = floor($seconds / 31536000);

    if ($years > 0) {
        $str .= ($years . ' ' . $CI->lang->line((($years > 1) ? 'date_years' : 'date_year')) . $str_separate);
    }

    $seconds -= $years * 31536000;
    $months = floor($seconds / 2628000);

    if ($years > 0 OR $months > 0) {
        if ($months > 0) {
            $str .= ($months . ' ' . $CI->lang->line((($months > 1) ? 'date_months' : 'date_month')) . $str_separate);
        }

        $seconds -= $months * 2628000;
    }

    $weeks = floor($seconds / 604800);

    if ($years > 0 OR $months > 0 OR $weeks > 0) {
        if ($weeks > 0) {
            $str .= ($weeks . ' ' . $CI->lang->line((($weeks > 1) ? 'date_weeks' : 'date_week')) . $str_separate);
        }

        $seconds -= $weeks * 604800;
    }

    $days = floor($seconds / 86400);

    if ($months > 0 OR $weeks > 0 OR $days > 0) {
        if ($days > 0) {
            $str .= ($days . ' ' . $CI->lang->line((($days > 1) ? 'date_days' : 'date_day')) . $str_separate);
        }

        $seconds -= $days * 86400;
    }

    $hours = floor($seconds / 3600);

    if ($days > 0 OR $hours > 0) {
        if ($hours > 0) {
            $str .= ($hours . ' ' . $CI->lang->line((($hours > 1) ? 'date_hours' : 'date_hour')) . $str_separate);
        }

        $seconds -= $hours * 3600;
    }

    // don't display minutes/seconds unless $display_mins
    // == true
    $minutes = floor($seconds / 60);
    $seconds -= $minutes * 60;
    if ($display_mins) {
        if ($days > 0 OR $hours > 0 OR $minutes > 0) {
            if ($minutes > 0) {
                $str .= ($minutes . ' ' . $CI->lang->line((($minutes > 1) ? 'date_minutes' : 'date_minute')) . $str_separate);
            }
        }
    }

    if ($display_secs && $seconds > 0) {
        $str .= ($seconds . ' ' . $CI->lang->line((($seconds > 1) ? 'date_seconds' : 'date_second')));
    }

    return trim($str);
}

function checkdatetime($datetime = FALSE) {
    if($datetime === FALSE) return FALSE;

    if(preg_match('/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/', $datetime, $matches))
    {
        if(checkdate($matches[2], $matches[3], $matches[1]))
        {
            return TRUE;
        }
    } 
    
    return false;
}

function weekofmonth($date = FALSE) {
    if($date === FALSE) $date = date('Y-m-d');

    $time = strtotime($date);
    $number = date("W", $time) - date("W", strtotime(date('Y-m-01', $time))) + 1;

    return $number;
}