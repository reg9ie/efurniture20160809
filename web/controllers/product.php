<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index($pcategory_id_dash_page = false, $product_id = false) {
        $explodes = explode('-', $pcategory_id_dash_page);
        $pcategory_id = $explodes[0] ? (int) $explodes[0] : 0;
        $page = isset($explodes[1]) ? (int) $explodes[1] : 1;

        if($pcategory_id_dash_page && $product_id) {
            $this->product_detail($pcategory_id_dash_page, $product_id);
            return false;
        }
        
        $objPtype = Ptype::find(array('conditions' => '`code` = "popular" AND `enable` = 1 AND `delete` = 0'));
        $objPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$pcategory_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objPcategory) {
            $objPcategory = Pcategory::first([
                'conditions' => '`ptype_id` = '.$objPtype->id.' AND `level` > -1 AND `enable` = 1 AND `delete` = 0',
                'order' => '`lft` ASC',
            ]);
            if(!$objPcategory) redirect(configLangURI.'/');
            $pcategory_id = $objPcategory->id;
        }
        if($objPcategory->level == 0) {
            $this->category_lists($pcategory_id);
            return false;
        }
        else {
            $this->product_lists($pcategory_id, $page);
            return false;
        }
    }

    public function category_lists($pcategory_id = false) {
        $objPtype = Ptype::find(array('conditions' => '`code` = "popular" AND `enable` = 1 AND `delete` = 0'));
        $objPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$pcategory_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objPcategory) redirect(configLangURI.'/product');
        $pcategory = $objPcategory->to_array();

        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `lft` > '.$objPcategory->lft.' AND `rght` < '.$objPcategory->rght.' AND `enable` = 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $pcategories = to_array($objPcategories, ['methods' => ['_tree_name', '_image']]);

        $search_text = (isset($_GET['search_text']) && $_GET['search_text'] ? urldecode($_GET['search_text']) : '');

        $this->template_library->frontend([
            'name' => $pcategory['name'],
            'html' => 'product_category_lists',
            'css' => '',
            'js' => [
                'assets/js/product.js',
            ],
            'json' => '',
            'data' => [
                'pcategory' => $pcategory,
                'pcategories' => $pcategories,
                'search_text' => $search_text,
            ]
        ]);
    }

    public function product_lists($pcategory_id = false, $page = false) {
        $objPtype = Ptype::find(array('conditions' => '`code` = "popular" AND `enable` = 1 AND `delete` = 0'));
        $objPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$pcategory_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objPcategory) redirect(configLangURI.'/product');
        $pcategory = $objPcategory->to_array();

        $objParentPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$objPcategory->parent.' AND `enable` = 1 AND `delete` = 0'
        ]);
        $pcategory['parent_name'] = $objParentPcategory ? $objParentPcategory->name : '';

        $objPgroups = Pgroup::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `level` > -1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $pgroups = to_array($objPgroups);

        $count = 12;
        $total = 0;
        $sort = '`sort` ASC';
        $query['conditions'] = '`ptype_id` = '.$objPtype->id.' AND `pcategory_id` = '.$pcategory_id.' AND `enable` = 1 AND `delete` = 0';
        $search_text = (isset($_GET['search_text']) && $_GET['search_text'] ? urldecode($_GET['search_text']) : '');
        if($search_text) $query['conditions'] = $query['conditions'].' AND ('.
            '`no` LIKE "%'.$search_text.'%" OR '.
            '`name` LIKE "%'.$search_text.'%" OR '.
            '`title` LIKE "%'.$search_text.'%" OR '.
            '`intro` LIKE "%'.$search_text.'%" OR '.
            '`description` LIKE "%'.$search_text.'%" OR '.
            '`content` LIKE "%'.$search_text.'%"'.
        ')';
        $total = Product::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objProducts = Product::all($query);
        $products = to_array($objProducts, ['methods' => ['_image']]);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $wishes = json_decode($this->input->cookie('wishes'), true);
        foreach ($products as $i => $p) {
            $find = false;
            foreach ($wishes as $j => $w) {
                if($find) continue;
                if($w['product_id'] == $p['id']) {
                    $find = true;
                    unset($wishes[$j]);
                }
            }
            $products[$i]['wish'] = $find ? true : false;
        }

        $this->template_library->frontend([
            'name' => $pcategory['name'],
            'html' => 'product_lists',
            'css' => '',
            'js' => [
                'assets/js/product.js',
            ],
            'json' => '',
            'data' => [
                'search_text' => $search_text,
                'pgroups' => $pgroups,
                'pcategory' => $pcategory,
                'products' => $products,
                'pagination' => $pagination,
            ]
        ]);
    }

    public function product_detail($pcategory_id = false, $product_id = false) {
        $objPtype = Ptype::find(array('conditions' => '`code` = "popular" AND `enable` = 1 AND `delete` = 0'));
        $objPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$pcategory_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objPcategory) redirect(configLangURI.'/product');
        $pcategory = $objPcategory->to_array();

        $objParentPcategory = Pcategory::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$objPcategory->parent.' AND `enable` = 1 AND `delete` = 0'
        ]);
        $pcategory['parent_pcategory'] = $objParentPcategory ? $objParentPcategory->to_array() : false;

        $objProduct = Product::find([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `id` = '.$product_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objProduct) redirect(configLangURI.'/product');
        $objProduct->view_times++;
        $objProduct->save();
        $product = $objProduct->to_array(['methods' => ['images', '_image', '_custom_image']]);

        $objProducts = Product::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `enable` = 1 AND `delete` = 0',
            'limit' => '20'
        ]);
        $products = to_array($objProducts, ['methods' => ['_image']]);
        shuffle($products);

        $search_text = (isset($_GET['search_text']) && $_GET['search_text'] ? urldecode($_GET['search_text']) : '');

        $find = false;
        $wishes = json_decode($this->input->cookie('wishes'), true);
        foreach ($wishes as $i => $w) {
            if($find) continue;
            if($w['product_id'] == $product_id) $find = true;
        }
        $product['wish'] = $find ? true : false;

        $this->template_library->frontend(array(
            'name' => $product['name'],
            'html' => 'product_detail',
            'css' => [],
            'js' => [
                'assets/js/product.js',
                'assets/js/contact.js',
            ],
            'json' => 'var json_product = '.json_encode($product).';',
            'data' => [
                'pcategory' => $pcategory,
                'product' => $product,
                'products' => $products,
                'search_text' => $search_text,
            ]
        ));
    }
}