<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Wish_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $wishes = json_decode($this->input->cookie('wishes'), true);

        $this->template_library->frontend([
            'name' => '追蹤清單',
            'html' => 'wish_lists',
            'css' => '',
            'js' => ['assets/js/wish.js'],
            'json' => 'var json_wishes = '.json_encode($wishes).';',
            'data' => ['wishes' => $wishes]
        ]);
    }
}