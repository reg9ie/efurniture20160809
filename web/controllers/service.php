<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Service_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->template_library->frontend([
            'name' => '農友服務',
            'html' => 'service',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => []
        ]);
    }
}