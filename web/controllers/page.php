<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index($page = false) {
        if($page === false) redirect('/');

        $objPage = Page::find(array('conditions' => '`code` = "'.$page.'" AND `enable` = "1" AND `delete` = "0"'));
        if(!$objPage) redirect('/');
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => $objPage->name,
            'html' => 'page',
            'css' => [],
            'js' => [],
            'json' => '',
            'data' => ['page' => $page]
        ]);
    }
}