<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $objPage = Page::find(['conditions' => '`code` = "contact" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => $page['title'],
            'html' => 'contact',
            'css' => '',
            'js' => ['assets/js/contact.js'],
            'json' => '',
            'data' => ['page' => $page]
        ]);
    }
}