<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class About_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $objPage = Page::find(array('conditions' => '`code` = "about" AND `enable` = "1" AND `delete` = "0"'));
        if(!$objPage) redirect('/');
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => $objPage->name,
            'html' => 'about',
            'css' => [],
            'js' => [],
            'json' => '',
            'data' => ['page' => $page]
        ]);
    }
}