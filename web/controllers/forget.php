<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Forget_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $user = array(
            'email' => ''
        );
        
        $this->template_library->frontend(array(
            'name' => '忘記密碼',
            'html' => 'forget',
            'css' => '',
            'js' => array('js/member.js'),
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => array()
        ));
    }
}