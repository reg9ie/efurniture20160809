<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Logout_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        User::logout();

        redirect('/');
    }
}