<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Queue_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin');
        if(User::is_permission('mail_queue') === FALSE) redirect('admin');
    }
    
    public function index() {
        redirect('admin/queue/lists');
    }
    
    public function lists() {
        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`id` DESC';

        $query['conditions'] = '';
        $total = MailQueue::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objMailQueues = MailQueue::all($query);
        $mail_queues = to_array($objMailQueues);

        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => '郵件序列',
            'html' => 'queue/lists',
            'css' => '',
            'js' => array('assets/js/admin/queue.js?'.time()),
            'json' => 'var json_mail_queues = '.json_encode($mail_queues).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }
}