<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Board_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin');
        if(User::is_permission('board_manage') === false) redirect('admin');
    }
    
    public function index() {
        redirect('admin/board/lists');
    }
    
    public function lists() {
        $objPage = Page::find(['conditions' => '`code` = "home" AND `enable` = 1 AND `delete` = 0']);

        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`page_id` = "'.$objPage->id.'" AND `delete` = 0';
        $total = Board::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objBoards = Board::all($query);
        $boards = to_array($objBoards, [
            'except' => 'content',
            'methods' => ['_image']
        ]);
        
        $search['page_id'] = $objPage->id;
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = (ceil($total / $count) > 0 ? ceil($total / $count) : 1);
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = ($page - 1 > 1 ? $page - 1 : 1);
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = ($pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end']);
        $pagination['last'] = $pagination['pages'];

        $objBcategories = Bcategory::all([
            'conditions' => '`delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        $bcategories = to_array($objBcategories, ['methods' => ['_tree_name']]);

        $this->template_library->backend([
            'name' => '廣告管理',
            'html' => 'board/lists',
            'css' => [],
            'js' => ['assets/js/admin/board.js?'.time()],
            'json' => 'var json_boards = '.json_encode($boards).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';'.
                      'var json_bcategories = '.json_encode($bcategories).';',
            'data' => []
        ]);
    }

    public function board_add() {
        $objPage = Page::find(['conditions' => '`code` = "home" AND `enable` = 1 AND `delete` = 0']);

        $objBcategories = Bcategory::all([
            'conditions' => '`delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        $bcategories = to_array($objBcategories);

        $board = [
            'page_id' => ($objPage ? $objPage->id : ''),
            'bcategory_id' => ($objBcategories ? $objBcategories[0]->id : ''),
            'name' => '',
            'enable' => 1
        ];

        $this->template_library->backend(array(
            'name' => '新增廣告',
            'html' => 'board/board_add',
            'css' => [],
            'js' => ['assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/board.js?'.time()],
            'json' => 'var json_board = '.json_encode($board).';'.
                      'var json_bcategories = '.json_encode($bcategories).';',
            'data' => []
        ));
    }

    public function board_edit($id = false) {
        $objBoard = Board::find_by_id_and_delete($id, 0);
        if(!$objBoard) redirect('admin/board/lists');
        $board = $objBoard->to_array(['methods' => ['_image']]);

        $objBcategories = Bcategory::all([
            'conditions' => '`delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        $bcategories = to_array($objBcategories);

        $this->template_library->backend([
            'name' => '編輯廣告',
            'html' => 'board/board_edit',
            'css' => '',
            'js' => ['assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/board.js?'.time()],
            'json' => 'var json_board = '.json_encode($board).';'.
                      'var json_bcategories = '.json_encode($bcategories).';',
            'data' => []
        ]);
    }

    public function bcategory_add() {
        $objBcategories = Bcategory::all([
            'conditions' => '`level` < 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $bcategories = to_array($objBcategories, ['methods' => ['_tree_name']]);

        $bcategory = [
            'name' => '',
            'parent' => $bcategories[0]['id'],
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增分類',
            'html' => 'board/bcategory_add',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/board.js?'.time()],
            'json' => 'var json_bcategories = '.json_encode($bcategories).';'.
                      'var json_bcategory = '.json_encode($bcategory).';',
            'data' => []
        ]);
    }

    public function bcategory_edit($id = false) {
        $objBcategory = Bcategory::find_by_id($id);
        if(!$objBcategory) redirect('admin/board/lists#bcategory');
        $bcategory = $objBcategory->to_array();
        
        $objBcategories = Bcategory::all([
            'conditions' => '(`lft` < '.$objBcategory->lft.' OR `rght` > '.$objBcategory->rght.') AND `level` < 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $bcategories = to_array($objBcategories, ['methods' => ['_tree_name']]);

        $this->template_library->backend([
            'name' => '編輯分類',
            'html' => 'board/bcategory_edit',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/board.js?'.time()],
            'json' => 'var json_bcategories = '.json_encode($bcategories).';'.
                      'var json_bcategory = '.json_encode($bcategory).';',
            'data' => []
        ]);
    }
}