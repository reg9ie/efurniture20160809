<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Log_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
        if(User::is_permission('config_manage') === FALSE) redirect('admin');
    }
    
    public function index() {
        $objErrorLogs = ErrorLog::all(array('order' => 'time desc', 'limit' => 100));
        $errorLogs = to_array($objErrorLogs);

        $this->template_library->backend(array(
            'name' => '網站紀錄',
            'html' => 'log/lists',
            'css' => [],
            'js' => array('assets/js/admin/log.js?'.time()),
            'data' => array('errorLogs' => $errorLogs)
        ));
    }
}