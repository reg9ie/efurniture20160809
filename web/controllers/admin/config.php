<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Config_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin/login');
        if(User::is_permission('config_manage') === false) redirect('admin');
    }
    
    public function index() {
        $config = Config::all_to_array();
        $config['_site_logo'] = $config['site_logo'] ? 'image/site/'.$config['site_logo'] : '';

        $objMailAccounts = MailAccount::all(['conditions' => '`delete` = 0', 'order' => '`id` ASC']);
        $mail_accounts = to_array($objMailAccounts);

        $objCompanies = Company::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        $companies = to_array($objCompanies);

        $objPayments = Payment::all(['conditions' => '`enable` = 1 AND `delete` = 0', 'order' => '`sort` ASC']);
        $payments = to_array($objPayments);

        $objShippings = Shipping::all(['order' => '`sort` ASC']);
        $shippings = to_array($objShippings);

        $this->template_library->backend([
            'name' => '網站設定',
            'html' => 'config/site',
            'js' => ['assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/config.js?'.time()],
            'json' => 'var json_config = '.json_encode($config).';'.
                      'var json_mail_accounts = '.json_encode($mail_accounts).';'.
                      'var json_companies = '.json_encode($companies).';'.
                      'var json_payments = '.json_encode($payments).';'.
                      'var json_shippings = '.json_encode($shippings).';',
            'data' => ['config' => $config]
        ]);
    }
}