<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Image_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin');
        if(User::is_permission('image_manage') === FALSE) redirect('admin');
    }
    
    public function index() {
        redirect('admin/image/lists');
    }
    
    public function lists() {
        $objPage = Page::find(array('conditions' => '`code` = "home" AND `enable` = 1 AND `delete` = 0'));

        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`page_id` = "'.$objPage->id.'" AND `delete` = 0';
        $total = Image::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objImages = Image::all($query);
        $images = to_array($objImages, array(
            'except' => 'content',
            'methods' => array('_image')
        ));

        $search['page_id'] = $objPage->id;
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => '形象管理',
            'html' => 'image/lists',
            'css' => '',
            'js' => array('assets/js/admin/image.js?'.time()),
            'json' => 'var json_images = '.json_encode($images).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }

    public function image_add() {
        $objPage = Page::find(array('conditions' => '`code` = "home" AND `enable` = 1 AND `delete` = 0'));

        $image = array(
            'page_id' => ($objPage ? $objPage->id : ''),
            'name' => '',
            'enable' => 1
        );
        
        $this->template_library->backend(array(
            'name' => '新增形象',
            'html' => 'image/image_add',
            'css' => '',
            'js' => array('assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/image.js?'.time()),
            'json' => 'var json_image = '.json_encode($image).';',
            'data' => array()
        ));
    }

    public function image_edit($id = FALSE) {
        $objImage = Image::find_by_id_and_delete($id, 0);
        if(!$objImage) redirect('admin/image/lists');
        $image = $objImage->to_array(array('methods' => array('_image')));

        $this->template_library->backend(array(
            'name' => '編輯形象',
            'html' => 'image/image_edit',
            'css' => '',
            'js' => array('assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/image.js?'.time()),
            'json' => 'var json_image = '.json_encode($image).';',
            'data' => array()
        ));
    }
}