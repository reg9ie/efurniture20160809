<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Logout_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $user = User::logout();

        redirect('/admin/login');
    }
}