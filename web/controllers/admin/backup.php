<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Backup_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin');
        if(User::is_permission('backup_manage') === false) redirect('admin');
    }
    
    public function index() {
        redirect('admin/backup/lists');
    }
    
    public function lists() {
        $backups = [];
        foreach(new DirectoryIterator(configPathBackup) as $i => $e) {
            if($e->getFileName() == '.' || $e->getFileName() == '..') continue;

            $filename = explode('_', $e->getFileName());
            $date = (int)$filename[0];
            $time = isset($filename[1]) ? sprintf('%1$04u', (int)$filename[1]) : '0000';

            $backups[$date.$time] = [
                'filename' => $e->getFileName(),
                'size' => number_format($e->getSize() / 1024 / 1024, 2).'MB',
                'created_at' => date('Y-m-d H:i:s', $e->getMTime())
            ];
        }
        krsort($backups);
        $backups = array_merge($backups);

        $this->template_library->backend([
            'name' => '備份管理',
            'html' => 'backup/lists',
            'css' => [],
            'js' => ['assets/js/admin/backup.js?'.time()],
            'json' => 'var json_backups = '.json_encode($backups).';',
            'data' => []
        ]);
    }
}