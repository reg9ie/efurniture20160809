<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin/login');
        if(User::is_permission('right_manage') === false) redirect('admin');
    }
    
    public function index() {
        $objRoles = Role::all(['conditions' => '`enable` = 1', 'order' => 'id ASC']);
        $roles = to_array($objRoles);

        $count = 10;
        $total = 0;
        $page = 1;

        $query['conditions'] = '`delete` = "0" AND `role_id` != "0"';
        $total = User::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = 'id DESC';

        $users = [];
        $objUsers = User::all($query);
        if($objUsers) {
            $users = to_array($objUsers,[
                'only' => ['id', 'account', 'email', 'name', 'role_id', 'enable', 'created_at'],
                'methods' => ['_role_name']
            ]);
        }

        $search['type'] = 'admin';
        $search['text'] = '';

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $objPermissions = Permission::all(['conditions' => '`enable` = 1', 'order' => 'id ASC']);
        $permissions = to_array($objPermissions);

        foreach((array)$roles as $i => $r) {
            $objPermissions = Permission::all([
                'select' => 'permissions.*, '.$r['id'].' AS role_id, permissions.id AS permission_id, rp.enable',
                'joins' => 'LEFT JOIN (SELECT role_id, permission_id, enable FROM role_permissions WHERE role_id = '.$r['id'].') rp ON(permissions.id = rp.permission_id)',
                'conditions' => 'permissions.enable = 1',
                'order' => 'id ASC'
            ]);
            $roles[$i]['permissions'] = to_array($objPermissions);
        }

        $this->template_library->backend([
            'name' => '權限管理',
            'html' => 'admin/lists',
            'css' => [],
            'js' => ['assets/js/admin/admin.js?'.time()],
            'json' => 'var json_roles = '.json_encode($roles).';'.
                      'var json_users = '.json_encode($users).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';'.
                      'var json_permissions = '.json_encode($permissions).';',
            'data' => []
        ]);
    }

    public function user_add() {
        $objRoles = Role::all(['conditions' => '`enable` = 1', 'order' => 'id ASC']);
        $roles = to_array($objRoles);

        $user = [
            'account' => '',
            'password' => '',
            'role_id' => ($objRoles ? $objRoles[0]->id : ''),
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增管理者帳號',
            'html' => 'admin/user_add',
            'css' => [],
            'js' => ['assets/js/admin/admin.js?'.time()],
            'json' => 'var json_roles = '.json_encode($roles).';'.
                      'var json_user = '.json_encode($user).';',
            'data' => []
        ]);
    }

    public function user_edit($id = false) {
        $objRoles = Role::all(['conditions' => '`enable` = 1', 'order' => 'id ASC']);
        $roles = to_array($objRoles);

        $objUser = User::find_by_id($id);
        if(!$objUser) redirect('admin/admin/lists');
        $user = $objUser->to_array(['except' => 'password']);

        $this->template_library->backend([
            'name' => '編輯管理者帳號',
            'html' => 'admin/user_edit',
            'css' => [],
            'js' => ['assets/js/admin/admin.js?'.time()],
            'json' => 'var json_roles = '.json_encode($roles).';'.
                      'var json_user = '.json_encode($user).';',
            'data' => []
        ]);
    }
}