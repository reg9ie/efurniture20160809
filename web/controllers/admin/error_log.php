<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Error_log_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin');
    }
    
    public function index() {
        redirect('admin/error_log/lists');
    }
    
    public function lists() {
        $count = 10;
        $total = 0;
        $page = 1;

        $total = ErrorLog::count();

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = '`id` DESC';
        $objErrorLogs = ErrorLog::all($query);
        $error_logs = to_array($objErrorLogs);

        $search['no'] = '';
        $search['type'] = '';
        $search['message'] = '';
        $search['file'] = '';
        $search['line'] = '';
        $search['url'] = '';
        $search['user_agent'] = '';
        $search['time'] = '';

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => 'Error Log',
            'html' => 'error_log/lists',
            'css' => '',
            'js' => array('assets/js/admin/error_log.js?'.time()),
            'json' => 'var json_error_logs = '.json_encode($error_logs).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }
}