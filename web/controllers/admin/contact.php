<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin');
        if(User::is_permission('contact_manage') === false) redirect('admin');
    }
    
    public function index() {
        redirect('admin/contact/lists');
    }
    
    public function lists() {
        $objContacts = Contact::all(array(
            'conditions' => '`delete` = 0',
            'order' => '`created_at` DESC'
        ));
        $contacts = to_array($objContacts);

        $this->template_library->backend([
            'name' => '留言列表',
            'html' => 'contact/lists',
            'css' => '',
            'js' => 'assets/js/admin/contact.js',
            'json' => 'var json_contacts = '.json_encode($contacts).';',
            'data' => []
        ]);
    }

    public function edit($id = false) {
        if($id === false) redirect('admin/contact/lists');

        $objContact = Contact::find_by_id_and_delete($id, 0);
        if(!$objContact) redirect('admin/contact/lists');
        $contact = $objContact->to_array();

        $this->template_library->backend([
            'name' => '回應留言',
            'html' => 'contact/edit',
            'css' => '',
            'js' => 'assets/js/admin/contact.js',
            'json' => 'var json_contact = '.json_encode($contact).';',
            'data' => []
        ]);
    }
}