<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
        if(User::is_permission('article_manage') === FALSE) redirect('admin');
    }

    public function index() {
        redirect('admin/news/lists');
    }

    public function lists() {
        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`delete` = 0';
        $total = News::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objNewses = News::all($query);
        $newses = to_array($objNewses, ['except' => 'content', 'methods' => ['_image']]);
        
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend([
            'name' => '最新消息',
            'html' => 'news/lists',
            'css' => '',
            'js' => ['assets/js/admin/news.js?'.time()],
            'json' => 'var json_newses = '.json_encode($newses).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => []
        ]);
    }

    public function news_add() {
        $objNtypes = Ntype::all(['conditions' => '`enable` = 1 AND `delete` = 0']);
        $ntypes = to_array($objNtypes);

        $news = [
            'ntype_id' => ($ntypes ? $ntypes[0]['id'] : ''),
            'name' => '',
            'content' => '',
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增最新消息',
            'html' => 'news/news_add',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/news.js?'.time()],
            'json' => 'var json_news = '.json_encode($news).';'.
                      'var json_ntypes = '.json_encode($ntypes).';',
            'data' => []
        ]);
    }

    public function news_edit($id = FALSE) {
        $objNtypes = Ntype::all(['conditions' => '`enable` = 1 AND `delete` = 0']);
        $ntypes = to_array($objNtypes);

        $objNews = News::find_by_id_and_delete($id, 0);
        if(!$objNews) redirect('admin/news/lists#news');
        $news = $objNews->to_array(['methods' => '_image']);

        $this->template_library->backend([
            'name' => '編輯最新消息',
            'html' => 'news/news_edit',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/news.js?'.time()],
            'json' => 'var json_news = '.json_encode($news).';'.
                      'var json_ntypes = '.json_encode($ntypes).';',
            'data' => []
        ]);
    }
}