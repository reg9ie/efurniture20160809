<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
    }

    public function index() {
        redirect('admin/page/lists');
    }

    public function lists() {
        $objPages = Page::all(['conditions' => '`enable` = 1 AND `delete` = 0', 'order' => '`sort` ASC']);
        $pages = to_array($objPages, ['methods' => ['_name', 'images', 'images_en'], 'except' => 'content']);
        
        $this->template_library->backend([
            'name' => '頁面形象',
            'html' => 'page/lists',
            'css' => '',
            'js' => ['assets/js/admin/page.js?'.time()],
            'json' => 'var json_pages = '.json_encode($pages).';',
            'data' => []
        ]);
    }

    public function page_edit($id = FALSE) {
        $objPages = Page::find_by_id_and_delete($id, 0);
        if(!$objPages) redirect('admin/page/lists');
        $page = $objPages->to_array(['methods' => ['_name', 'images', 'images_en']]);

        $this->template_library->backend([
            'name' => '編輯頁面',
            'html' => 'page/page_edit',
            'css' => '',
            'js' => [
                'assets/tinymce/4.0.10/tinymce.min.js',
                'assets/ajaxfileupload/ajaxfileupload.js',
                'assets/js/admin/page.js?'.time()
            ],
            'json' => 'var json_page = '.json_encode($page).';',
            'data' => []
        ]);
    }
}