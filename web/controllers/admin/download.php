<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Download_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
        if(User::is_permission('download_manage') === FALSE) redirect('admin');
    }

    public function index() {
        redirect('admin/download/lists');
    }

    public function lists() {
        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`delete` = 0';
        $total = Download::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objDownloads = Download::all($query);
        $downloads = to_array($objDownloads, ['except' => 'content', 'methods' => ['_file']]);
        
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend([
            'name' => '檔案下載',
            'html' => 'download/lists',
            'css' => '',
            'js' => ['assets/js/admin/download.js?'.time()],
            'json' => 'var json_downloads = '.json_encode($downloads).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => []
        ]);
    }

    public function download_add() {
        $download = [
            'name' => '',
            'content' => '',
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增檔案',
            'html' => 'download/download_add',
            'css' => '',
            'js' => ['assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/download.js?'.time()],
            'json' => 'var json_download = '.json_encode($download).';',
            'data' => []
        ]);
    }

    public function download_edit($id = FALSE) {
        $objDownload = Download::find_by_id_and_delete($id, 0);
        if(!$objDownload) redirect('admin/download/lists');
        $download = $objDownload->to_array(['methods' => '_file']);

        $this->template_library->backend([
            'name' => '編輯檔案',
            'html' => 'download/download_edit',
            'css' => '',
            'js' => ['assets/ajaxfileupload/ajaxfileupload.js',
                     'assets/js/admin/download.js?'.time()],
            'json' => 'var json_download = '.json_encode($download).';',
            'data' => []
        ]);
    }
}