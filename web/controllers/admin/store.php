<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Store_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin');
        if(User::is_permission('store_manage') === FALSE) redirect('admin');
    }
    
    public function index() {
        redirect('admin/store/lists');
    }
    
    public function lists() {
        $objStores = Store::all(array(
            'conditions' => '`delete` = 0',
            'order' => '`sort` ASC'
        ));
        $stores = to_array($objStores, array('methods' => array('_image_thumb')));
        
        $this->template_library->backend(array(
            'name' => '店鋪管理',
            'html' => 'store/lists',
            'css' => '',
            'js' => array('assets/js/admin/store.js'),
            'json' => 'var json_stores = '.json_encode($stores).';',
            'data' => array()
        ));
    }

    public function add() {
        $store = array(
            'name' => '',
            'enable' => 1
        );

        $this->template_library->backend(array(
            'name' => '新增店鋪',
            'html' => 'store/add',
            'css' => '',
            'js' => array('assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/store.js?'.time()),
            'json' => 'var json_store = '.json_encode($store).';',
            'data' => array()
        ));
    }

    public function edit($id = FALSE) {
        $objStore = Store::find_by_id_and_delete($id, 0);
        if(!$objStore) redirect('admin/store/lists');
        $store = $objStore->to_array(array('methods' => array('_image')));

        $this->template_library->backend(array(
            'name' => '編輯店鋪',
            'html' => 'store/edit',
            'css' => '',
            'js' => array('assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/store.js?'.time()),
            'json' => 'var json_store = '.json_encode($store).';',
            'data' => array()
        ));
    }
}