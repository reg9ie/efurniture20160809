<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        if(User::count(['conditions' => '`enable` = 1 AND `delete` = 0']) == 0) {
            // 建立預設 Admin 帳號
            User::create(['account' => 'efurn', 'password' => 'gjfkORRL5890', 'role_id' => 1, 'enable' => 1]);
        }

        if($this->input->post() !== false) {
            if($this->input->post('account') === false) return false;
            if($this->input->post('password') === false) return false;

            $account = $this->input->post('account', true);
            $password = $this->input->post('password', true);

            $objUser = User::login($account, $password);

            if($objUser && User::is_admin()) redirect('admin');
            elseif($objUser && User::is_admin() === false) redirect('');
        }

        $this->template_library->backend(array(
            'name' => '登入',
            'html' => 'login',
            'css' => 'assets/css/admin/login.css?'.time(),
            'js' => '',
            'template' => 'template_login'
        ));
    }
}