<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin');
        if(User::is_permission('product_manage') === false) redirect('admin');
    }
    
    public function index() {
        redirect('admin/product/lists');
    }
    
    public function lists() {
        $ptypes = [];
        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        if($objPtypes) $ptypes = to_array($objPtypes);

        $ptype_id = 0;
        if(isset($_GET['ptype_id']) && $_GET['ptype_id']) $ptype_id = $_GET['ptype_id'];
        else $ptype_id = ($ptypes ? $ptypes[0]['id'] : '');
        
        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`products`.`ptype_id` = '.$ptype_id.' AND `products`.`delete` = 0';
        $total = Product::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objProducts = Product::all($query);
        $products = to_array($objProducts, ['except' => 'content', 'methods' => ['_image', '_image_thumb']]);

        $search['ptype_id'] = $ptype_id;
        $search['pcategory_id'] = '';
        $search['pgroup_id'] = '';
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $pcategories = [];
        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        if($objPcategories) $pcategories = to_array($objPcategories, ['methods' => ['_tree_name', '_image', '_image_thumb']]);

        $pgroups = [];
        $objPgroups = Pgroup::all([
            'conditions' => '`ptype_id` = '.$ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        if($objPgroups) $pgroups = to_array($objPgroups);

        $this->template_library->backend([
            'name' => '商品管理',
            'html' => 'product/lists',
            'css' => [],
            'js' => ['assets/js/admin/product.js?'.time()],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pcategories = '.json_encode($pcategories).';'.
                      'var json_pgroups = '.json_encode($pgroups).';'.
                      'var json_products = '.json_encode($products).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => []
        ]);
    }

    public function product_add() {
        $ptypes = [];
        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        if($objPtypes) $ptypes = to_array($objPtypes);

        $ptype_id = 0;
        if(isset($_GET['ptype_id']) && $_GET['ptype_id']) $ptype_id = $_GET['ptype_id'];
        else $ptype_id = ($ptypes ? $ptypes[0]['id'] : '');

        $pcategories = [];
        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        if($objPcategories) $pcategories = to_array($objPcategories);

        $pcategory_id = 0;
        if(isset($_GET['pcategory_id']) && $_GET['pcategory_id']) $pcategory_id = $_GET['pcategory_id'];
        else $pcategory_id = ($pcategories ? $pcategories[0]['id'] : '');

        $pgroups = [];
        $objPgroups = Pgroup::all([
            'conditions' => '`ptype_id` = '.$ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        if($objPgroups) $pgroups = to_array($objPgroups);

        $pgroup_id = 0;
        if(isset($_GET['pgroup_id']) && $_GET['pgroup_id']) $pgroup_id = $_GET['pgroup_id'];
        else $pgroup_id = ($pgroups ? $pgroups[0]['id'] : '');

        $product = [
            'ptype_id' => $ptype_id,
            'pcategory_id' => $pcategory_id,
            'pgroup_id' => $pgroup_id,
            'name' => '',
            'title' => '',
            'description' => '',
            'content' => '',
            'custom1' => '<h4>訂作範圍</h4><ul><li>皮質、皮色</li><li>尺寸</li><li>左右變動</li><li>拉釦</li></ul>',
            'custom1_en' => '',
            'custom1_cn' => '',
            'custom2' => '<h4>訂作範圍</h4><ul><li>皮質、皮色</li><li>尺寸</li><li>左右變動</li><li>拉釦</li></ul>',
            'custom2_en' => '',
            'custom2_cn' => '',
            'custom_image' => '',
            'image' => '',
            'images' => [],
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增商品',
            'html' => 'product/product_add',
            'css' => '',
            'js' => [
                        'assets/tinymce/4.0.10/tinymce.min.js',
                        'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                        'assets/ajaxfileupload/ajaxfileupload.js',
                        'assets/js/admin/product.js?'.time()
                    ],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pcategories = '.json_encode($pcategories).';'.
                      'var json_pgroups = '.json_encode($pgroups).';'.
                      'var json_product = '.json_encode($product).';',
            'data' => []
        ]);
    }

    public function product_edit($id = false) {
        $ptypes = [];
        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        if($objPtypes) $ptypes = to_array($objPtypes);

        $objProduct = Product::find_by_id_and_delete($id, 0);
        if(!$objProduct) redirect('admin/product/lists');
        $product = $objProduct->to_array(['methods' => ['_image', 'images', '_custom_image']]);

        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$objProduct->ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        $pcategories = to_array($objPcategories);

        $pgroups = [];
        $objPgroups = Pgroup::all([
            'conditions' => '`ptype_id` = '.$objProduct->ptype_id.' AND `delete` = 0 AND `level` > -1',
            'order' => '`lft` ASC'
        ]);
        if($objPgroups) $pgroups = to_array($objPgroups);

        $this->template_library->backend([
            'name' => '編輯商品',
            'html' => 'product/product_edit',
            'css' => '',
            'js' => [
                        'assets/tinymce/4.0.10/tinymce.min.js',
                        'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                        'assets/ajaxfileupload/ajaxfileupload.js',
                        'assets/js/admin/product.js?'.time()
                    ],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pcategories = '.json_encode($pcategories).';'.
                      'var json_pgroups = '.json_encode($pgroups).';'.
                      'var json_product = '.json_encode($product).';',
            'data' => []
        ]);
    }

    public function pcategory_add() {
        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        $ptypes = to_array($objPtypes);

        $ptype_id = 0;
        if(isset($_GET['ptype_id']) && $_GET['ptype_id']) $ptype_id = $_GET['ptype_id'];
        else $ptype_id = ($ptypes ? $ptypes[0]['id'] : '');

        $objPcategories = Pcategory::all(['conditions' => '`ptype_id` = '.$ptype_id.' AND `level` < 1 AND `delete` = 0', 'order' => '`lft` ASC']);
        $pcategories = to_array($objPcategories, ['methods' => ['_tree_name']]);
        $pcategory_id = 0;
        if(isset($_GET['pcategory_id']) && $_GET['pcategory_id']) $pcategory_id = $_GET['pcategory_id'];
        else $pcategory_id = ($pcategories ? $pcategories[0]['id'] : '');

        $pcategory = [
            'ptype_id' => $ptype_id,
            'name' => '',
            'parent' => $pcategory_id,
            'image' => '',
            'images' => '',
            '_images' => [],
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增目錄',
            'html' => 'product/pcategory_add',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/product.js?'.time()],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pcategories = '.json_encode($pcategories).';'.
                      'var json_pcategory = '.json_encode($pcategory).';',
            'data' => []
        ]);
    }

    public function pcategory_edit($id = false) {
        $objPcategory = Pcategory::find_by_id($id);
        if(!$objPcategory) redirect('admin/product/lists#pcategory');
        $pcategory = $objPcategory->to_array(['methods' => ['_tree_name', '_image']]);
        
        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$objPcategory->ptype_id.' AND (`lft` < '.$objPcategory->lft.' OR `rght` > '.$objPcategory->rght.') AND `level` < 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $pcategories = to_array($objPcategories, ['methods' => ['_tree_name', '_image']]);

        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        $ptypes = to_array($objPtypes);

        $this->template_library->backend([
            'name' => '編輯目錄',
            'html' => 'product/pcategory_edit',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/product.js?'.time()],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pcategories = '.json_encode($pcategories).';'.
                      'var json_pcategory = '.json_encode($pcategory).';',
            'data' => []
        ]);
    }

    public function pgroup_add() {
        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        $ptypes = to_array($objPtypes);

        $ptype_id = 0;
        if(isset($_GET['ptype_id']) && $_GET['ptype_id']) $ptype_id = $_GET['ptype_id'];
        else $ptype_id = ($ptypes ? $ptypes[0]['id'] : '');

        $objPgroups = Pgroup::all(['conditions' => '`delete` = 0', 'order' => '`lft` ASC']);
        $pgroups = to_array($objPgroups, ['methods' => ['_tree_name']]);

        $pgroup_id = 0;
        if(isset($_GET['pgroup_id']) && $_GET['pgroup_id']) $pgroup_id = $_GET['pgroup_id'];
        else $pgroup_id = ($pgroups ? $pgroups[0]['id'] : '');

        $pgroup = [
            'ptype_id' => $ptype_id,
            'name' => '',
            'parent' => $pgroup_id,
            'image' => '',
            'images' => '',
            '_images' => [],
            'enable' => 1
        ];

        $this->template_library->backend([
            'name' => '新增類別',
            'html' => 'product/pgroup_add',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/product.js?'.time()],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pgroups = '.json_encode($pgroups).';'.
                      'var json_pgroup = '.json_encode($pgroup).';',
            'data' => []
        ]);
    }

    public function pgroup_edit($id = false) {
        $objPgroup = Pgroup::find_by_id($id);
        if(!$objPgroup) redirect('admin/product/lists#pgroup');
        $pgroup = $objPgroup->to_array();
        
        $objPgroups = Pgroup::all([
            'conditions' => '(`lft` < '.$objPgroup->lft.' OR `rght` > '.$objPgroup->rght.') AND `level` < 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $pgroups = to_array($objPgroups, ['methods' => ['_tree_name']]);

        $objPtypes = Ptype::all(array('conditions' => '`enable` = 1 AND `delete` = 0'));
        $ptypes = to_array($objPtypes);

        $this->template_library->backend([
            'name' => '編輯類別',
            'html' => 'product/pgroup_edit',
            'css' => '',
            'js' => ['assets/tinymce/4.0.10/tinymce.min.js',
                     'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                     'assets/js/admin/product.js?'.time()],
            'json' => 'var json_ptypes = '.json_encode($ptypes).';'.
                      'var json_pgroups = '.json_encode($pgroups).';'.
                      'var json_pgroup = '.json_encode($pgroup).';',
            'data' => []
        ]);
    }
}