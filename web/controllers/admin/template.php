<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Template_controller extends CI_Controller {
    var $emails;

    function __construct() {
        parent::__construct();

        if(User::is_admin() === false) redirect('admin');
        if(User::is_permission('template_manage') === false) redirect('admin');

        $this->emails = array();
        $this->emails[] = array('name' => '測試', 'filename' => 'test.php');
        $this->emails[] = array('name' => '會員 註冊', 'filename' => 'user_register.php');
        $this->emails[] = array('name' => '會員 忘記密碼', 'filename' => 'user_forget_password.php');
        $this->emails[] = array('name' => '會員 訂單 成立', 'filename' => 'user_order_buy.php');
        // $this->emails[] = array('name' => '會員 訂單 取消', 'filename' => 'user_order_canceled.php');
        $this->emails[] = array('name' => '會員 訂單 已付款', 'filename' => 'user_order_paid.php');
        $this->emails[] = array('name' => '會員 訂單 已出貨', 'filename' => 'user_order_shipping.php');
        $this->emails[] = array('name' => '系統 訂單 成立', 'filename' => 'sys_order_buy.php');
        $this->emails[] = array('name' => '系統 訂單 已付款', 'filename' => 'sys_order_paid.php');
        $this->emails[] = array('name' => '系統 聯絡我們', 'filename' => 'sys_contact.php');
        $this->emails[] = array('name' => '系統 聯絡我們 回應', 'filename' => 'sys_contact_reply.php');
    }
    
    public function index() {
        redirect('admin/template/lists');
    }

    public function lists() {
        $emails = $this->emails;

        $this->template_library->backend(array(
            'name' => '郵件樣板',
            'html' => 'template/lists',
            'css' => '',
            'js' => array(),
            'json' => '',
            'data' => array('emails' => $emails)
        ));
    }

    public function email_edit($lang = false, $filename = false) {
        if($lang === false || $filename === false) redirect('admin/template/lists');
        
        $emails = $this->emails;
        $filepath = configPathBase.'web/views/'.$lang.'/email/'.$filename;

        if(file_exists($filepath) == false) redirect('admin/template/lists');

        $find = false;
        foreach((array)$emails as $i => $e) {
            if($find == true) continue;
            if($e['filename'] == $filename) {
                $email = $e;
                $find = true;
            }
        }
        if($find === false) redirect('admin/template/lists');
        
        if($this->input->post() !== false) {
            $html = $this->input->post('html');
            write_file($filepath, $html);
        }

        $email['html'] = read_file($filepath);

        $this->template_library->backend(array(
            'name' => '編輯樣板',
            'html' => 'template/email_edit',
            'css' => '',
            'js' => [
                'assets/ace/1.1.9/src-min/ace.js',
                'assets/js/admin/template.js?'.time()
            ],
            'json' => '',
            'data' => array('email' => $email)
        ));
    }
}