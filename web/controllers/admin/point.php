<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Point_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin');
        if(User::is_permission('point_manage') === FALSE) redirect('admin');
    }
    
    public function index() {
        redirect('admin/point/lists');
    }
    
    public function lists() {
        $query['conditions'] = '`delete` = 0';
        $total = Point::count($query);
        $count = 10;
        $page = 1;

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = '`created_at` DESC';
        $objPoints = Point::all($query);
        $points = to_array($objPoints, array('methods' => '_created_at'));
        
        $search['user_id'] = '';
        $search['del'] = 0;
        $search['name'] = '';

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => '點數管理',
            'html' => 'point/lists',
            'css' => '',
            'js' => array('assets/js/admin/point.js?'.time()),
            'json' => 'var json_points = '.json_encode($points).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }

    public function add($user_id = FALSE) {
        if($user_id === FALSE) redirect('admin/point');

        $objUser = User::find_by_id($user_id);
        if(!$objUser) redirect('admin/point');

        $point = array(
            'user_id' => $objUser->id,
            'user_name' => $objUser->name,
            'name' => '',
            'point' => 0
        );

        $this->template_library->backend(array(
            'name' => '新增點數',
            'html' => 'point/add',
            'css' => '',
            'js' => array('assets/js/admin/point.js?'.time()),
            'json' => 'var json_point = '.json_encode($point).';',
            'data' => array()
        ));
    }

    public function edit($id = FALSE) {
        $objPoint = Point::find_by_id_and_delete($id, 0);
        if(!$objPoint) redirect('admin/point');
        $point = $objPoint->to_array();

        $this->template_library->backend(array(
            'name' => '編輯點數',
            'html' => 'point/edit',
            'css' => '',
            'js' => array('assets/js/admin/point.js?'.time()),
            'json' => 'var json_point = '.json_encode($point).';',
            'data' => array()
        ));
    }
}