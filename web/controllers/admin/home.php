<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Home_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        if(User::is_admin() === FALSE) redirect('admin/login');
    }
    
    public function index() {
        $this->load->library('google_analytics_library');
        $analysis = $this->google_analytics_library->get();

        $analysis_title = date('Y-m-d',strtotime('-30 day')).' - '.date('Y-m-d');

        $sysinfo = array();
        $sysinfo['online_number'] = shell_exec('ps -ef|grep httpd|wc -l');
        $sysinfo['online_percent'] = number_format((int)$sysinfo['online_number'] / 150 * 100, 2);

        $cpu = sys_getloadavg();
        $sysinfo['cpu_percent'] = number_format($cpu[1] * 100, 2);

        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(' ', $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $sysinfo['memory_percent'] = number_format($mem[2] / $mem[1] * 100, 2);

        $swap = explode(' ', $free_arr[3]);
        $swap = array_filter($swap, function($e) {
            return ($e !== '');
        });
        $swap = array_merge($swap);
        $sysinfo['swap_percent'] = number_format($swap[2] / $swap[1] * 100, 2);

        // $disk = (int)shell_exec('du -sh '.dirname(configPathBase));
        // $sysinfo['disk_percent'] = number_format((int)$disk / 20, 2);
        $sysinfo['disk_percent'] = 100 - number_format(disk_free_space('/') / disk_total_space('/') * 100, 2);

        $sysinfo['mail_queue_count'] = MailQueue::count(array('conditions' => '`sended` = 0 AND `sended_times` < 3'));

        $this->template_library->backend(array(
            'name' => '控制中心',
            'html' => 'home',
            'js' => array('https://www.google.com/jsapi',
                          'assets/js/admin/home.js?'.time()),
            'json' => 'var analysis_title = "'.$analysis_title.'";',
            'data' => array('analysis' => $analysis, 'sysinfo' => $sysinfo)
        ));
    }
}