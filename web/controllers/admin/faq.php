<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Faq_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
        if(User::is_permission('article_manage') === FALSE) redirect('admin');
    }

    public function index() {
        redirect('admin/faq/lists');
    }

    public function lists() {
        $objAtype = Atype::find(array('conditions' => '`code` = "faq" AND `enable` = 1'));

        $objAcategories = Acategory::all(array(
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ));
        $acategories = to_array($objAcategories);

        $count = 10;
        $total = 0;
        $page = 1;
        $sort = '`sort` ASC';

        $query['conditions'] = '`atype_id` = "'.$objAtype->id.'" AND `delete` = 0';
        $total = Article::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objArticles = Article::all($query);
        $articles = to_array($objArticles, array(
            'except' => 'content',
            'methods' => array('_image')
        ));
        
        $search['atype_id'] = $objAtype->id;
        $search['name'] = '';
        $search['delete'] = 0;
        $search['count'] = $count;
        $search['page'] = $page;
        $search['sort'] = $sort;

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => '常見問題',
            'html' => 'faq/lists',
            'css' => '',
            'js' => array('assets/js/admin/faq.js?'.time()),
            'json' => 'var json_acategories = '.json_encode($acategories).';'.
                      'var json_articles = '.json_encode($articles).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }

    public function article_add() {
        $objAtype = Atype::find(array('conditions' => '`code` = "faq" AND `enable` = 1'));

        $objAcategories = Acategory::all(array(
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ));
        $acategories = to_array($objAcategories);

        $acategory_id = 0;
        if(isset($_GET['acategory_id']) && $_GET['acategory_id']) {
            $acategory_id = $_GET['acategory_id'];
        }
        else {
            $objAcategory = Acategory::find(array('conditions' => '`level` > -1 AND `delete` = 0', 'order' => '`lft` ASC'));
            if($objAcategory) $acategory_id = $objAcategory->id;
        }

        $article = array(
            'atype_id' => $objAtype->id,
            'acategory_id' => $acategory_id,
            'name' => '',
            'content' => '',
            'enable' => 1
        );

        $this->template_library->backend(array(
            'name' => '新增常見問題',
            'html' => 'faq/article_add',
            'css' => '',
            'js' => array('assets/tinymce/4.0.10/tinymce.min.js',
                          'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                          'assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/faq.js?'.time()),
            'json' => 'var json_article = '.json_encode($article).';'.
                      'var json_acategories = '.json_encode($acategories).';',
            'data' => array()
        ));
    }

    public function article_edit($id = FALSE) {
        $objAtype = Atype::find(array('conditions' => '`code` = "faq" AND `enable` = 1'));

        $objArticles = Article::find_by_id_and_delete($id, 0);
        if(!$objArticles) redirect('admin/faq/lists#article');
        $article = $objArticles->to_array();

        $objAcategories = Acategory::all(array(
            'conditions' => '`atype_id` = "'.$objAtype->id.'" AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ));
        $acategories = to_array($objAcategories);

        $this->template_library->backend(array(
            'name' => '編輯常見問題',
            'html' => 'faq/article_edit',
            'css' => '',
            'js' => array('assets/tinymce/4.0.10/tinymce.min.js',
                          'assets/tinymce/4.0.10/jquery.tinymce.min.js',
                          'assets/ajaxfileupload/ajaxfileupload.js',
                          'assets/js/admin/faq.js?'.time()),
            'json' => 'var json_article = '.json_encode($article).';'.
                      'var json_acategories = '.json_encode($acategories).';',
            'data' => array()
        ));
    }

    public function acategory_add() {
        $objAtype = Atype::find(array('conditions' => '`code` = "faq" AND `enable` = 1'));
        $objRootAcategory = Acategory::find(['conditions' => '`atype_id` = '.$objAtype->id.' AND `title` = "root" AND `delete` = 0']);

        $acategory = array(
            'atype_id' => $objAtype->id,
            'name' => '',
            'parent' => $objRootAcategory->id,
            'enable' => 1
        );

        $this->template_library->backend(array(
            'name' => '新增分類',
            'html' => 'faq/acategory_add',
            'css' => '',
            'js' => array('assets/js/admin/faq.js?'.time()),
            'json' => 'var json_acategory = '.json_encode($acategory).';',
            'data' => array()
        ));
    }

    public function acategory_edit($id = FALSE) {
        $objAcategory = Acategory::find_by_id($id);
        if(!$objAcategory) redirect('admin/faq/lists#acategory');
        $acategory = $objAcategory->to_array();

        $this->template_library->backend(array(
            'name' => '編輯分類',
            'html' => 'faq/acategory_edit',
            'css' => '',
            'js' => array('assets/js/admin/faq.js?'.time()),
            'json' => 'var json_acategory = '.json_encode($acategory).';',
            'data' => array()
        ));
    }
}