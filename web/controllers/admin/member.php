<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Member_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        if(User::is_admin() === FALSE) redirect('admin/login');
        if(User::is_permission('member_manage') === FALSE) redirect('admin');
    }
    
    public function index() {
        $count = 10;
        $total = 0;
        $page = 1;
        $sort = 'id DESC';

        $query['conditions'] = '`delete` = "0" AND `role_id` = "0"';
        $total = User::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $users = array();
        $objUsers = User::all($query);
        if($objUsers)
        {
            $users = to_array($objUsers,array(
                'only' => array('id', 'account', 'email', 'name', 'vip', 'vip_start_at', 'vip_end_at', 'black', 'point', 'enable', 'created_at'),
                'methods' => array('_role_name', '_birthday', '_vip_start_at', '_vip_end_at')
            ));
        }

        $search['type'] = 'member_vip_black';
        $search['text'] = '';

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->template_library->backend(array(
            'name' => '會員管理',
            'html' => 'member/lists',
            'js' => 'assets/js/admin/member.js?'.time(),
            'json' => 'var json_users = '.json_encode($users).';'.
                      'var json_search = '.json_encode($search).';'.
                      'var json_pagination = '.json_encode($pagination).';',
            'data' => array()
        ));
    }

    public function user_add() {
        $user = array('account' => '',
                      'password' => '',
                      'vip' => 0,
                      'enable' => 1);

        $this->template_library->backend(array(
            'name' => '新增會員帳號',
            'html' => 'member/user_add',
            'js' => 'assets/js/admin/member.js?'.time(),
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => array()
        ));
    }

    public function user_edit($id = FALSE) {
        $objUser = User::find_by_id($id);
        if(!$objUser) redirect('admin/member');
        $user = $objUser->to_array(array(
            'except' => 'password',
            'methods' => array('_vip_start_at', '_vip_end_at')
        ));
        $user['vip_start_at'] = $user['_vip_start_at'];
        $user['vip_end_at'] = $user['_vip_end_at'];

        if(isset($user['birthday']) && $user['birthday']) $user['birthday'] = date('Y-m-d', strtotime($user['birthday']));

        $this->template_library->backend(array(
            'name' => '編輯會員帳號',
            'html' => 'member/user_edit',
            'js' => 'assets/js/admin/member.js?'.time(),
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => array()
        ));
    }
}