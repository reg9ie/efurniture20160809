<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Member_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $objUser = User::get();
        if(!$objUser) redirect('/login');
    }
    
    public function index() {
        $objUser = User::get();
        $user = $objUser->to_array(['except' => 'password']);
        $user['birthday'] = date('Y-m-d', strtotime($user['birthday']));
        $user['birthday_year'] = date('Y', strtotime($user['birthday']));
        $user['birthday_month'] = date('m', strtotime($user['birthday']));
        $user['birthday_day'] = date('d', strtotime($user['birthday']));

        $this->template_library->frontend([
            'name' => '會員專區',
            'html' => 'member_dashboard',
            'css' => [],
            'js' => ['assets/js/member.js'],
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => []
        ]);
    }

    public function edit() {
        $objUser = User::get();
        $user = $objUser->to_array(['except' => 'password']);
        $user['birthday'] = date('Y-m-d', strtotime($user['birthday']));
        $user['birthday_year'] = date('Y', strtotime($user['birthday']));
        $user['birthday_month'] = date('m', strtotime($user['birthday']));
        $user['birthday_day'] = date('d', strtotime($user['birthday']));

        $this->template_library->frontend([
            'name' => '會員資料管理',
            'html' => 'member_edit',
            'css' => [],
            'js' => ['assets/js/member.js'],
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => []
        ]);
    }

    public function orders() {
        // $page = 1
        $objUser = User::get();

        $query['conditions'] = '`user_id` = '.$objUser->id.' AND `enable` = 1 AND `delete` = 0';
        // $total = Order::count($query);
        // $count = 10;
        
        // $query['limit'] = $count;
        // $query['offset'] = ($page - 1) * $count;
        $query['order'] = '`id` DESC';
        $objOrders = Order::all($query);
        $orders = to_array($objOrders, ['methods' => [
            '_paysystem_bank_at',
            '_status_pay',
            '_status_ship',
            '_status_return',
            '_status_refund',
            '_shipping_at',
            '_created_at'
        ]]);

        // $pagination = array();
        // $pagination['count'] = $count;
        // $pagination['total'] = $total;
        // $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        // $pagination['page'] = $page;
        // $pagination['first'] = 1;
        // $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        // $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        // $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        // $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        // $pagination['last'] = $pagination['pages'];
        // $pagination['pages'] = ($pagination['pages'] < 10 ? sprintf('%1$02u', $pagination['pages']) : $pagination['pages']);
        // $pagination['page'] = ($pagination['page'] < 10 ? sprintf('%1$02u', $pagination['page']) : $pagination['page']);

        $this->template_library->frontend([
            'name' => '訂單查詢',
            'html' => 'member_orders',
            'css' => [],
            'js' => ['assets/js/member.js'],
            'json' => 'var json_orders = '.json_encode($orders).';',
            'data' => ['orders' => $orders] //'pagination' => $pagination
        ]);
    }

    public function order($order_id = FALSE) {
        $objUser = User::get();
        $objOrder = Order::find_by_id_and_delete($order_id, 0);
        if(!$objOrder) redirect('/member/orders');
        if($objUser->id != $objOrder->user_id) redirect('/member/orders');

        $order = $objOrder->to_array(['methods' => [
            '_paysystem_bank_at',
            '_status_pay',
            '_status_ship',
            '_status_return',
            '_status_refund',
            '_shipping_at',
            '_created_at'
        ]]);
        $order['products'] = to_array($objOrder->order_products);

        $this->template_library->frontend([
            'name' => '購物清單確認',
            'html' => 'member_order',
            'css' => [],
            'js' => ['assets/js/member.js'],
            'json' => '',
            'data' => ['order' => $order]
        ]);
    }
}