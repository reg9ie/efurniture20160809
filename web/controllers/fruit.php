<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fruit_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->template_library->frontend([
            'name' => '認識水果',
            'html' => 'fruit',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => []
        ]);
    }
}