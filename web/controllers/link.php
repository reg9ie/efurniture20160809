<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Link_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->template_library->frontend([
            'name' => '交易行情',
            'html' => 'link',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => []
        ]);
    }
}