<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index($news_id = false) {
        $objPage = Page::find(['conditions' => '`code` = "news" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $news = [];
        $objNewses = News::all(['conditions' => '`enable` = 1 AND `delete` = 0', 'order' => '`sort` ASC']);
        $newses = to_array($objNewses, array('methods' => ['_image', '_content', '_created_at']));
        if($newses) {
            if($news_id === false) $news_id = $newses[0]['id'];

            $objNews = News::find(['conditions' => '`id` = '.$news_id.' AND `enable` = 1 AND `delete` = 0']);
            if(!$objNews) redirect(configLangURI.'/');
            $news = $objNews->to_array(['methods' => ['_created_at']]);
        }

        $this->template_library->frontend([
            'name' => $page['title'],
            'html' => 'news',
            'css' => '',
            'js' => '',
            'json' => '',
            'data' => ['page' => $page, 'newses' => $newses, 'news' => $news]
        ]);
    }
}