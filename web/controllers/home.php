<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Home_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $objPage = Page::find(['conditions' => '`code` = "home" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $popular = [
            'pcategories' => [],
            'pgroups' => [],
            'products' => [],
        ];
        $objPtype = Ptype::find(array(
            'conditions' => '`code` = "popular" AND  `enable` = 1 AND `delete` = 0'
        ));
        $objPopularPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `level` > -1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $popular['pcategories'] = to_array($objPopularPcategories, ['methods' => ['_tree_name', '_image']]);
        $objPgroups = Pgroup::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `level` > -1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $popular['pgroups'] = to_array($objPgroups);
        $objProducts = Product::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `home` = 1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ]);
        $popular['products'] = to_array($objProducts, ['methods' => ['_image', 'images']]);

        $wishes = json_decode($this->input->cookie('wishes'), true);
        foreach ($popular['products'] as $i => $p) {
            $find = false;
            foreach ($wishes as $j => $w) {
                if($find) continue;
                if($w['product_id'] == $p['id']) {
                    $find = true;
                    unset($wishes[$j]);
                }
            }
            $popular['products'][$i]['wish'] = $find ? true : false;
        }

        $custom = [
            'products' => [],
        ];
        $objPtype = Ptype::find(array(
            'conditions' => '`code` = "custom" AND  `enable` = 1 AND `delete` = 0'
        ));
        $objProducts = Product::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `home` = 1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ]);
        $custom['products'] = to_array($objProducts, ['methods' => ['_image', 'images']]);

        $this->template_library->frontend(array(
            'name' => $page['title'],
            'html' => 'home',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => [
                'page' => $page,
                'popular' => $popular,
                'custom' => $custom,
            ]
        ));
    }
}