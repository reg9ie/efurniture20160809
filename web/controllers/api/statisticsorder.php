<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Statisticsorder_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $date = $this->input->post('date', TRUE);

        $pagination = array('total' => 0, 'count' => $count, 'pages' => 1, 'page' => $page);

        $query['conditions'] = ($date ? '`date` LIKE "'.$date.'"' : '');
        $total = Statisticsorder::count($query);

        $sort = $sort ? $sort : '`date` DESC';

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $statisticsorders = array();
        $objStatisticsorders = Statisticsorder::all($query);
        if($objStatisticsorders)
        {
            $statisticsorders = to_array($objStatisticsorders, array('methods' => '_date'));
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ($count == 'all' ? 1 : (ceil($total / $count) > 0 ? ceil($total / $count) : 1));
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('statisticsorders' => $statisticsorders, 'pagination' => $pagination)
        ));
        return TRUE;
    }

    public function calc() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('date') === FALSE) return FALSE;

        $date = $this->input->post('date', TRUE);

        $this->load->library('statistics_library');
        $statisticsorder = $this->statistics_library->order_calc($date);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('statisticsorder' => $statisticsorder)
        ));
        return TRUE;
    }

    public function product_get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $ptype_id = $this->input->post('ptype_id', TRUE);
        $pcategory_id = $this->input->post('pcategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $del = $this->input->post('del', TRUE);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['select'] = '`products`.`id` as `product_id`, '.
                           '`products`.`no` as `product_no`, '.
                           '`products`.`name` as `product_name`, '.
                           '`product_colors`.`id` as `product_color_id`, '.
                           '`product_colors`.`image` as `product_image`, '.
                           '`product_colors`.`name` as `product_color_name`, '.
                           '`product_specs`.`id` as `product_spec_id`, '.
                           '`product_specs`.`name` as `product_spec_name`, '.
                           '`product_specs`.`price` as `product_spec_price`, '.
                           '`product_specs`.`price_special` as `product_spec_price_special`, '.
                           '`product_stocks`.`stock` as `stock`, '.
                           '`product_stocks`.`preorder` as `preorder`, '.
                           '`product_stocks`.`ordered` as `ordered`, '.
                           '`products`.`view_times` as `view_times`';
        $query['conditions'] = '`products`.`delete` = 0 AND `product_specs`.`delete` = 0 AND `product_colors`.`delete` = 0';
        $query['joins'] = array('product', 'product_spec', 'product_color');

        if($name)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`name` LIKE "%'.$name.'%" OR `products`.`no` LIKE "%'.$name.'%"';
        }

        $total = ProductStock::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;
        $objProducts = ProductStock::all($query);
        $products = to_array($objProducts);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ($count == 'all' ? 1 : (ceil($total / $count) > 0 ? ceil($total / $count) : 1));
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'products' => $products,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }

    public function order_product_get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $sort = $this->input->post('sort', TRUE);
        $ptype_id = $this->input->post('ptype_id', TRUE);
        $pcategory_id = $this->input->post('pcategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $start = $this->input->post('start', TRUE);
        $end = $this->input->post('end', TRUE);
        $del = $this->input->post('del', TRUE);

        $query['joins'] = array('product',
                                'product_spec',
                                'product_color',
                                'JOIN `orders` o ON(`order_products`.`order_id` = o.`id`)',
                                'LEFT JOIN `product_stocks` ps ON(`order_products`.`product_id` = ps.`product_id` AND `order_products`.`product_spec_id` = ps.`product_spec_id` AND `order_products`.`product_color_id` = ps.`product_color_id`)');
        $query['conditions'] = '(o.`created_at` >= "'.$start.' 00:00:00" AND o.`created_at` <= "'.$end.' 23:59:59") AND '.
                               'o.`enable` = 1 AND o.`delete` = 0 AND '.
                               '`products`.`delete` = 0 AND `product_specs`.`delete` = 0 AND `product_colors`.`delete` = 0';
        if($name) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                         '(`products`.`name` LIKE "%'.$name.'%" OR `products`.`no` LIKE "%'.$name.'%" OR '.
                                         '`product_colors`.`name` LIKE "%'.$name.'%" OR '.
                                         '`product_specs`.`name` LIKE "%'.$name.'%")';
        $query['select'] = '`order_products`.*, '.
                           '`products`.`id` as `product_id`, '.
                           '`products`.`no` as `product_no`, '.
                           '`products`.`name` as `product_name`, '.
                           '`products`.`view_times` as `view_times`, '.
                           '`product_colors`.`id` as `product_color_id`, '.
                           '`product_colors`.`image` as `product_image`, '.
                           '`product_colors`.`name` as `product_color_name`, '.
                           '`product_specs`.`id` as `product_spec_id`, '.
                           '`product_specs`.`name` as `product_spec_name`, '.
                           '`product_specs`.`price` as `product_spec_price`, '.
                           '`product_specs`.`price_special` as `product_spec_price_special`, '.
                           'ps.`stock` as `stock`, '.
                           'ps.`preorder` as `preorder`, '.
                           'ps.`ordered` as `ordered`, '.
                           'o.`status_remit` as `status_remit`, '.
                           'o.`status_pay` as `status_pay`, '.
                           'o.`status_ship` as `status_ship`, '.
                           'o.`status_return` as `status_return`, '.
                           'o.`status_refund` as `status_refund`';
        $total = OrderProduct::count($query);

        $query['order'] = '`products`.`id` DESC, `product_colors`.`sort`, `product_specs`.`sort` ASC';
        $objProducts = OrderProduct::all($query);
        
        $products = array();
        foreach ($objProducts as $i => $o)
        {
            $find = FALSE;
            foreach ($products as $j => $p)
            {
                if($p['product_id'] == $o->product_id &&
                   $p['product_spec_id'] == $o->product_spec_id &&
                   $p['product_color_id'] == $o->product_color_id)
                {
                    $find = TRUE;
                    if($o->status_pay == Order::status('paid')) $products[$j]['ordered'] += $o->quantity;
                    else $products[$j]['preorder'] += $o->quantity;
                }
            }

            if($find === FALSE)
            {
                $product = $o->to_array();
                $product['preorder'] = 0;
                $product['ordered'] = 0;
                
                if($o->status_pay == Order::status('paid')) $product['ordered'] += $o->quantity;
                else $product['preorder'] += $o->quantity;

                $products[] = $product;
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'products' => $products
            )
        ));
        return TRUE;
    }

    public function order_get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('product_id') === FALSE) return FALSE;
        if($this->input->post('product_color_id') === FALSE) return FALSE;
        if($this->input->post('product_spec_id') === FALSE) return FALSE;

        $sort = $this->input->post('sort', TRUE);
        $product_id = $this->input->post('product_id', TRUE);
        $product_color_id = $this->input->post('product_color_id', TRUE);
        $product_spec_id = $this->input->post('product_spec_id', TRUE);
        $start = $this->input->post('start', TRUE);
        $end = $this->input->post('end', TRUE);

        $query['conditions'] = '`delete` = 0';
        $query['order'] = $sort ? $sort : '`id` DESC';

        if($start && $end)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '(`created_at` >= "'.$start.' 00:00:00" AND `created_at` <= "'.$end.' 23:59:59")';
        }

        $orders = array();
        $order_ids = array();
        $objOrderProducts = OrderProduct::all(array(
            'conditions' => '`product_id` = '.$product_id.' AND '.
                            '`product_color_id` = '.$product_color_id.' AND '.
                            '`product_spec_id` = '.$product_spec_id
        ));
        foreach((array)$objOrderProducts as $i => $o)
        {
            $order_ids[$i] = $o->order_id;
        }
        $order_ids = implode(',', $order_ids);
        if($order_ids)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '`id` IN ('.$order_ids.')';

            $objOrders = Order::all($query);
            if($objOrders)
            {
                $orders = to_array($objOrders,
                    array('methods' => array('_created_at', '_payment', '_status_remit', '_status_pay', '_status_ship', '_status_return', '_status_refund'),
                          'include' => array('order_products')));
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('orders' => $orders)
        ));
        return TRUE;
    }
}