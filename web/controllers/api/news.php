<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('count') === false) return false;
        if($this->input->post('page') === false) return false;

        $count = $this->input->post('count', true);
        $page = $this->input->post('page', true);
        $sort = $this->input->post('sort', true);
        $ntype_id = $this->input->post('ntype_id', true);
        $name = $this->input->post('name', true);
        $delete = $this->input->post('delete', true);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        if($ntype_id) {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`ntype_id` = "'.$ntype_id.'"';
        }
        if($name) {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`name` LIKE "%'.$name.'%"';
        }
        if($delete !== false) {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';
        }

        $total = News::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        if($sort) $query['order'] = $sort;

        $newses = [];
        $objNewss = News::all($query);
        if($objNewss) $newses = to_array($objNewss);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => [
                'newses' => $newses,
                'pagination' => $pagination
            ]
        ]);
        return true;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('ntype_id') === false) return false;
        if($this->input->post('name') === false) return false;

        $ntype_id = $this->input->post('ntype_id', true);
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $home = $this->input->post('home', true);
        $enable = $this->input->post('enable', true);

        if($ntype_id !== false) {
            $objNtype = Ntype::find_by_id($ntype_id);
            if($objNtype) {
                $data['ntype_id'] = $objNtype->id;
                $data['ntype_name'] = $objNtype->name;
            }
        }
        if($name !== false) $data['name'] = $name;
        if($name_en !== false) $data['name_en'] = $name_en;
        if($name_cn !== false) $data['name_cn'] = $name_cn;
        if($content !== false) $data['content'] = $content;
        if($content_en !== false) $data['content_en'] = $content_en;
        if($content_cn !== false) $data['content_cn'] = $content_cn;
        if($image !== false) $data['image'] = $image;
        if($home !== false) $data['home'] = $home;
        if($enable !== false) $data['enable'] = $enable;

        if(isset($data)) {
            $objNews = News::create($data);
            $news = $objNews->to_array();

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['news' => $news]
            ]);
            return true;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $ntype_id = $this->input->post('ntype_id', true);
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $home = $this->input->post('home', true);
        $enable = $this->input->post('enable', true);

        $objNews = News::find($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($ntype_id !== false && $objNews->ntype_id != $ntype_id) {
            $objNtype = Ntype::find_by_id($ntype_id);
            if($objNtype) {
                $data['ntype_id'] = $objNtype->id;
                $data['ntype_name'] = $objNtype->name;
                $data['ntype_name_en'] = $objNtype->name_en;
                $data['ntype_name_cn'] = $objNtype->name_cn;
            }
        }
        if($name !== false && $objNews->name != $name) $data['name'] = $name;
        if($name_en !== false && $objNews->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objNews->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($content !== false && $objNews->content != $content) $data['content'] = $content;
        if($content_en !== false && $objNews->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== false && $objNews->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($image !== false && $objNews->image != $image) $data['image'] = $image;
        if($home !== false && $objNews->home != $home) $data['home'] = $home;
        if($enable !== false && $objNews->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objNews->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $enable = $this->input->post('enable', true);
        $home = $this->input->post('home', true);

        $objNews = News::find_by_id($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($enable !== false && $objNews->enable != $enable) $data['enable'] = $enable;
        if($home !== false && $objNews->home != $home) $data['home'] = $home;

        if(isset($data)) {
            $objNews->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objNews = News::find($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objNews->delete = 0;
        $objNews->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objNews = News::find($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objNews->delete = 1;
        $objNews->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $objNews = News::find($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objNews->delete();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ]);
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objNews = News::find_by_id($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objNewss = News::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objNewss as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== false) {
            $objNews = array_splice($objNewss, $find, 1);
            array_splice($objNewss, $find-1, 0, $objNews);
        }

        $count = count($objNewss);
        $max = 999999999;
        foreach((array)$objNewss as $i => $o) {
            $o->sort = $max - ($count - $i - 1);
            $o->save();
        }
        $newses = to_array($objNewss, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['newses' => $newses]
        ]);
        return true;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objNews = News::find_by_id($id);
        if(!$objNews) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objNewss = News::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objNewss as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i < count($objNewss)-1) $find = $i;
        }
        if($find !== false) {
            $objNews = array_splice($objNewss, $find, 1);
            array_splice($objNewss, $find+1, 0, $objNews);
        }

        $count = count($objNewss);
        $max = 999999999;
        foreach((array)$objNewss as $i => $o) {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $newses = to_array($objNewss, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['newses' => $newses]
        ]);
        return true;
    }
}