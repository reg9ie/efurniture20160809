<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pcategory_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('node');
    }

    public function get() {
        if(User::is_admin() === false) return false;

        $ptype_id = $this->input->post('ptype_id', true);

        $query['conditions'] = '';
        if($ptype_id) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`ptype_id` = '.$ptype_id;

        $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`title` != "root" AND `delete` = "0"';
        $query['order'] = '`lft` ASC';
        
        $pcategories = array();
        $objPcategories = Pcategory::all($query);
        if($objPcategories) $pcategories = to_array($objPcategories, ['methods' => ['_tree_name', '_image', '_image_thumb']]);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'pcategories' => $pcategories
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;

        $ptype_id = $this->input->post('ptype_id');
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $alias = $this->input->post('alias');
        $title = $this->input->post('title');
        $title_en = $this->input->post('title_en');
        $title_cn = $this->input->post('title_cn');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $product_size = $this->input->post('product_size');
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if(!$name) {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return false;
        }

        if($ptype_id !== false) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
                $data['ptype_name_en'] = $objPtype->name_en;
                $data['ptype_name_cn'] = $objPtype->name_cn;
                $data['ptype_code'] = $objPtype->code;
            }
        }
        if($name !== false) $data['name'] = $name;
        if($name_en !== false) $data['name_en'] = $name_en;
        if($name_cn !== false) $data['name_cn'] = $name_cn;
        if($alias !== false) $data['alias'] = $alias;
        if($title !== false) $data['title'] = $title;
        if($title_en !== false) $data['title_en'] = $title_en;
        if($title_cn !== false) $data['title_cn'] = $title_cn;
        if($image !== false) $data['image'] = $image;
        if($description !== false) $data['description'] = $description;
        if($description_en !== false) $data['description_en'] = $description_en;
        if($description_cn !== false) $data['description_cn'] = $description_cn;
        if($product_size !== false) $data['product_size'] = $product_size;
        if($parent !== false) $data['parent'] = $parent;
        if($enable !== false) $data['enable'] = $enable;

        if(isset($data)) {
            $objPcategory = Pcategory::create($data);
            
            $this->node->rebuild_tree('pcategories');

            $objPcategory = Pcategory::find($objPcategory->id);
            $objPcategory->tree_name = str_repeat('　　', $objPcategory->level).$objPcategory->name;
            $objPcategory->tree_name_en = str_repeat('　　', $objPcategory->level).$objPcategory->name_en;
            $objPcategory->tree_name_cn = str_repeat('　　', $objPcategory->level).$objPcategory->name_cn;
            $objPcategory->save();
            $pcategory = $objPcategory->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '修改成功',
                'data' => array('pcategory' => $pcategory)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        $ptype_id = $this->input->post('ptype_id');
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $alias = $this->input->post('alias');
        $title = $this->input->post('title');
        $title_en = $this->input->post('title_en');
        $title_cn = $this->input->post('title_cn');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $product_size = $this->input->post('product_size');
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if(!$name) {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return false;
        }

        $objPcategory = Pcategory::find($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($ptype_id !== false && $objPcategory->ptype_id != $ptype_id) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
                $data['ptype_name_en'] = $objPtype->name_en;
                $data['ptype_name_cn'] = $objPtype->name_cn;
                $data['ptype_code'] = $objPtype->code;
            }
        }
        if($name !== false && $objPcategory->name != $name) $data['name'] = $name;
        if($name_en !== false && $objPcategory->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objPcategory->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($alias !== false && $objPcategory->alias != $alias) $data['alias'] = $alias;
        if($title !== false && $objPcategory->title != $title) $data['title'] = $title;
        if($title_en !== false && $objPcategory->title_en != $title_en) $data['title_en'] = $title_en;
        if($title_cn !== false && $objPcategory->title_cn != $title_cn) $data['title_cn'] = $title_cn;
        if($image !== false && $objPcategory->image != $image) $data['image'] = $image;
        if($description !== false && $objPcategory->description != $description) $data['description'] = $description;
        if($product_size !== false && $objPcategory->product_size != $product_size) $data['product_size'] = $product_size;
        if($parent !== false && $objPcategory->parent != $parent) $data['parent'] = $parent;
        if($enable !== false && $objPcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPcategory->update_attributes($data);

            $this->node->rebuild_tree('pcategories');

            $objPcategory = Pcategory::find($id);
            $objPcategory->tree_name = str_repeat('　　', $objPcategory->level).$objPcategory->name;
            $objPcategory->save();

            $objSubPcategories = Pcategory::all(array(
                'conditions' => '`lft` > '.$objPcategory->lft.' AND `rght` < '.$objPcategory->rght,
                'order' => '`lft` ASC'
            ));
            foreach ($objSubPcategories as $i => $objSubPcategory) {
                $objSubPcategory->tree_name = str_repeat('　　', $objSubPcategory->level).$objSubPcategory->name;
                $objSubPcategory->save();
            }

            $objProducts = Product::find_all_by_pcategory_id($id);
            foreach($objProducts as $i => $P) {
                $P->pcategory_name = $objPcategory->name;
                $P->pcategory_name_en = $objPcategory->name_en;
                $P->pcategory_name_cn = $objPcategory->name_cn;
                $P->save();
            }

            if(isset($data['name']) || isset($data['name_en']) || isset($data['name_cn'])) {
                Product::update_all([
                    'conditions' => 'pcategory_id = '.$id,
                    'set' => [
                        'Pcategory_name' => $data['name'],
                        'Pcategory_name_en' => $data['name_en'],
                        'Pcategory_name_cn' => $data['name_cn'],
                    ],
                ]);
            }
        }
        
        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array()
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objPcategory = Pcategory::find_by_id($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($enable !== false && $objPcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPcategory->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPcategory = Pcategory::find($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPcategory->delete = 0;
        $objPcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPcategory = Pcategory::find($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPcategory->delete = 1;
        $objPcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPcategory = Pcategory::find($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPcategory->delete();

        $this->node->rebuild_tree('pcategories');

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);

        $objPcategory = Pcategory::find_by_id($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objTargetPcategory = Pcategory::find(array(
            'conditions' => '`parent` = '.$objPcategory->parent.' AND `lft` < '.$objPcategory->lft,
            'order' => '`lft` DESC'
        ));
        if($objTargetPcategory) {
            $this->node->update_node($objPcategory->id, 'pcategories', 'before', $objTargetPcategory->id, array('name' => $objPcategory->name));
        }

        $objPcategories = Pcategory::all(array(
            'order' => '`lft` ASC'
        ));
        $pcategories = to_array($objPcategories, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('pcategories' => $pcategories)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);

        $objPcategory = Pcategory::find_by_id($id);
        if(!$objPcategory) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objTargetPcategory = Pcategory::find(array(
            'conditions' => '`parent` = '.$objPcategory->parent.' AND `lft` > '.$objPcategory->lft,
            'order' => '`rght` ASC'
        ));
        if($objTargetPcategory) {
            $this->node->update_node($objPcategory->id, 'pcategories', 'after', $objTargetPcategory->id, array('name' => $objPcategory->name));
        }

        $objPcategories = Pcategory::all(array(
            'order' => '`lft` ASC'
        ));
        $pcategories = to_array($objPcategories, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('pcategories' => $pcategories)
        ));
        return TRUE;
    }
}