<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Error_log_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $no = $this->input->post('no', TRUE);
        $type = $this->input->post('type', TRUE);
        $message = $this->input->post('message', TRUE);
        $file = $this->input->post('file', TRUE);
        $line = $this->input->post('line', TRUE);
        $url = $this->input->post('url', TRUE);
        $user_agent = $this->input->post('user_agent', TRUE);
        $time = $this->input->post('time', TRUE);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );
        
        $query['conditions'] = '';
        if($no) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'no LIKE "%'.$no.'%"';
        if($type) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'type LIKE "%'.$type.'%"';
        if($message) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'message LIKE "%'.$message.'%"';
        if($file) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'file LIKE "%'.$file.'%"';
        if($line) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'line LIKE "%'.$line.'%"';
        if($url) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'url LIKE "%'.$url.'%"';
        if($user_agent) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'user_agent LIKE "%'.$user_agent.'%"';
        if($time) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'time LIKE "%'.$time.'%"';

        $total = ErrorLog::count();

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $objErrorLogs = ErrorLog::all($query);
        $error_logs = to_array($objErrorLogs);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'error_logs' => $error_logs,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objErrorLog = ErrorLog::find($id);
        if(empty($objErrorLog))
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objErrorLog->delete();
        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force_all() {
        if(User::is_admin() === FALSE) return FALSE;

        ErrorLog::table()->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }
}