<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Download_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('count') === false) return false;
        if($this->input->post('page') === false) return false;

        $count = $this->input->post('count', true);
        $page = $this->input->post('page', true);
        $sort = $this->input->post('sort', true);
        $name = $this->input->post('name', true);
        $delete = $this->input->post('delete', true);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        if($name) {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`name` LIKE "%'.$name.'%"';
        }
        if($delete !== false) {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';
        }

        $total = Download::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        if($sort) $query['order'] = $sort;

        $downloads = [];
        $objDownloads = Download::all($query);
        if($objDownloads) $downloads = to_array($objDownloads);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => [
                'downloads' => $downloads,
                'pagination' => $pagination
            ]
        ]);
        return true;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;

        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $file = $this->input->post('file', true);
        $enable = $this->input->post('enable', true);

        if($name !== false) $data['name'] = $name;
        if($name_en !== false) $data['name_en'] = $name_en;
        if($name_cn !== false) $data['name_cn'] = $name_cn;
        if($description !== false) $data['description'] = $description;
        if($description_en !== false) $data['description_en'] = $description_en;
        if($description_cn !== false) $data['description_cn'] = $description_cn;
        if($content !== false) $data['content'] = $content;
        if($content_en !== false) $data['content_en'] = $content_en;
        if($content_cn !== false) $data['content_cn'] = $content_cn;
        if($image !== false) $data['image'] = $image;
        if($file !== false) $data['file'] = $file;
        if($enable !== false) $data['enable'] = $enable;

        if(isset($data)) {
            $objDownload = Download::create($data);
            $download = $objDownload->to_array();

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['download' => $download]
            ]);
            return true;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $file = $this->input->post('file', true);
        $enable = $this->input->post('enable', true);

        $objDownload = Download::find($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($name !== false && $objDownload->name != $name) $data['name'] = $name;
        if($name_en !== false && $objDownload->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objDownload->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($description !== false && $objDownload->description != $description) $data['description'] = $description;
        if($description_en !== false && $objDownload->description_en != $description_en) $data['description_en'] = $description_en;
        if($description_cn !== false && $objDownload->description_cn != $description_cn) $data['description_cn'] = $description_cn;
        if($content !== false && $objDownload->content != $content) $data['content'] = $content;
        if($content_en !== false && $objDownload->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== false && $objDownload->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($image !== false && $objDownload->image != $image) $data['image'] = $image;
        if($file !== false && $objDownload->file != $file) $data['file'] = $file;
        if($enable !== false && $objDownload->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objDownload->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $enable = $this->input->post('enable', true);

        $objDownload = Download::find_by_id($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($enable !== false && $objDownload->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objDownload->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objDownload = Download::find($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objDownload->delete = 0;
        $objDownload->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objDownload = Download::find($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objDownload->delete = 1;
        $objDownload->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $objDownload = Download::find($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objDownload->delete();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ]);
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objDownload = Download::find_by_id($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objDownloads = Download::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objDownloads as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== false) {
            $objDownload = array_splice($objDownloads, $find, 1);
            array_splice($objDownloads, $find-1, 0, $objDownload);
        }

        $count = count($objDownloads);
        $max = 999999999;
        foreach((array)$objDownloads as $i => $o) {
            $o->sort = $max - ($count - $i - 1);
            $o->save();
        }
        $downloads = to_array($objDownloads, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['downloads' => $downloads]
        ]);
        return true;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objDownload = Download::find_by_id($id);
        if(!$objDownload) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objDownloads = Download::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objDownloads as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i < count($objDownloads)-1) $find = $i;
        }
        if($find !== false) {
            $objDownload = array_splice($objDownloads, $find, 1);
            array_splice($objDownloads, $find+1, 0, $objDownload);
        }

        $count = count($objDownloads);
        $max = 999999999;
        foreach((array)$objDownloads as $i => $o) {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $downloads = to_array($objDownloads, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['downloads' => $downloads]
        ]);
        return true;
    }
}