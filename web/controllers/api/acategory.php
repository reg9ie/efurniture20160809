<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Acategory_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('node');
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;

        $query['conditions'] = '`title` != "root" AND `delete` = "0"';
        $query['order'] = '`lft` ASC';

        $acategories = array();
        $objAcategories = Acategory::all($query);
        if($objAcategories) $acategories = to_array($objAcategories);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'acategories' => $acategories
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('atype_id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $atype_id = $this->input->post('atype_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if($atype_id !== FALSE)
        {
            $objAtype = Atype::find_by_id($atype_id);
            if($objAtype)
            {
                $data['atype_id'] = $objAtype->id;
                $data['atype_name'] = $objAtype->name;
                $data['atype_name_en'] = $objAtype->name_en;
                $data['atype_name_cn'] = $objAtype->name_cn;
                $data['atype_code'] = $objAtype->code;
            }
        }
        if(!$name)
        {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return FALSE;
        }

        if($atype_id) $data['atype_id'] = $atype_id;
        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($parent) $data['parent'] = $parent;
        if($enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objAcategory = Acategory::create($data);
            
            $this->node->rebuild_tree('acategories');

            $objAcategory = Acategory::find($objAcategory->id);
            $objAcategory->tree_name = str_repeat('　　', $objAcategory->level).$objAcategory->name;
            $objAcategory->tree_name_en = str_repeat('　　', $objAcategory->level).$objAcategory->name_en;
            $objAcategory->tree_name_cn = str_repeat('　　', $objAcategory->level).$objAcategory->name_cn;
            $objAcategory->save();
            $acategory = $objAcategory->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('acategory' => $acategory)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $atype_id = $this->input->post('atype_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objAcategory = Acategory::find($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($atype_id !== FALSE && $objAcategory->atype_id != $atype_id)
        {
            $objAtype = Atype::find_by_id($atype_id);
            if($objAtype)
            {
                $data['atype_id'] = $objAtype->id;
                $data['atype_name'] = $objAtype->name;
                $data['atype_name_en'] = $objAtype->name_en;
                $data['atype_name_cn'] = $objAtype->name_cn;
                $data['atype_code'] = $objAtype->code;
            }
        }
        if($name !== FALSE && $objAcategory->name != $name) $data['name'] = $name;
        if($name_en !== FALSE && $objAcategory->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== FALSE && $objAcategory->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($parent !== FALSE && $objAcategory->parent != $parent) $data['parent'] = $parent;
        if($enable !== FALSE && $objAcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objAcategory->update_attributes($data);

            $this->node->rebuild_tree('acategories');

            $objAcategory = Acategory::find($id);
            $objAcategory->tree_name = str_repeat('　　', $objAcategory->level).$objAcategory->name;
            $objAcategory->tree_name_en = str_repeat('　　', $objAcategory->level).$objAcategory->name_en;
            $objAcategory->tree_name_cn = str_repeat('　　', $objAcategory->level).$objAcategory->name_cn;
            $objAcategory->save();

            $objSubAcategories = Acategory::all(array(
                'conditions' => '`atype_id` = '.$objAcategory->atype_id.' AND `lft` > '.$objAcategory->lft.' AND `rght` < '.$objAcategory->rght,
                'order' => '`lft` ASC'
            ));
            foreach ($objSubAcategories as $i => $objSubAcategory) {
                $objSubAcategory->tree_name = str_repeat('　　', $objSubAcategory->level).$objSubAcategory->name;
                $objSubAcategory->save();
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objAcategory = Acategory::find_by_id($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objAcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objAcategory->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objAcategory = Acategory::find($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objAcategory->delete = 0;
        $objAcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objAcategory = Acategory::find($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objAcategory->delete = 1;
        $objAcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objAcategory = Acategory::find($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objAcategory->delete();
        
        $this->node->rebuild_tree('acategories');

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function move_up() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objAcategory = Acategory::find_by_id($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objTargetAcategory = Acategory::find(array(
            'conditions' => '`parent` = '.$objAcategory->parent.' AND `lft` < '.$objAcategory->lft,
            'order' => '`lft` DESC'
        ));
        if($objTargetAcategory)
        {
            $this->node->update_node($objAcategory->id, 'acategories', 'before', $objTargetAcategory->id, array('name' => $objAcategory->name));
        }

        $objAcategories = Acategory::all(array(
            'order' => '`lft` ASC'
        ));
        $acategories = to_array($objAcategories, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('acategories' => $acategories)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objAcategory = Acategory::find_by_id($id);
        if(!$objAcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objTargetAcategory = Acategory::find(array(
            'conditions' => '`parent` = '.$objAcategory->parent.' AND `lft` > '.$objAcategory->lft,
            'order' => '`rght` ASC'
        ));
        if($objTargetAcategory)
        {
            $this->node->update_node($objAcategory->id, 'acategories', 'after', $objTargetAcategory->id, array('name' => $objAcategory->name));
        }

        $objAcategories = Acategory::all(array(
            'order' => '`lft` ASC'
        ));
        $acategories = to_array($objAcategories, array('only' => 'id', 'sort'));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('acategories' => $acategories)
        ));
        return TRUE;
    }
}