<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Config_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->helper('email');
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;

        $site_name = $this->input->post('site_name');
        $site_keywords = $this->input->post('site_keywords');
        $site_description = $this->input->post('site_description');
        $site_logo = $this->input->post('site_logo');
        
        $admin_email = $this->input->post('admin_email');
        
        $company_name = $this->input->post('company_name');
        $company_email = $this->input->post('company_email');
        $company_telephone = $this->input->post('company_telephone');
        $company_fax = $this->input->post('company_fax');
        $company_address = $this->input->post('company_address');
        $company_address_description = $this->input->post('company_address_description');
        $company_time = $this->input->post('company_time');
        $company_description = $this->input->post('company_description');
        $company_blog = $this->input->post('company_blog');
        $company_bank_name = $this->input->post('company_bank_name');
        $company_bank_code = $this->input->post('company_bank_code');
        $company_bank_account = $this->input->post('company_bank_account');
        $company_bank_account_name = $this->input->post('company_bank_account_name');

        $email_smtp_host = $this->input->post('email_smtp_host');
        $email_smtp_port = $this->input->post('email_smtp_port');
        $email_smtp_account = $this->input->post('email_smtp_account');
        $email_smtp_password = $this->input->post('email_smtp_password');
        $email_from = $this->input->post('email_from');
        $email_from_name = $this->input->post('email_from_name');
        $email_cc = $this->input->post('email_cc');
        $email_bcc = $this->input->post('email_bcc');
        $email_queue = $this->input->post('email_queue');

        $facebook_url = $this->input->post('facebook_url');
        $facebook_id = $this->input->post('facebook_id');
        $facebook_api_id = $this->input->post('facebook_api_id');
        $facebook_api_secret = $this->input->post('facebook_api_secret');

        $google_map_iframe = $this->input->post('google_map_iframe');

        // $google_account = $this->input->post('google_account');
        // $google_password = $this->input->post('google_password');
        // $google_analysisProfileId = $this->input->post('google_analysisProfileId');
        // $google_analysisTrackingId = $this->input->post('google_analysisTrackingId');
        $google_analysis_account = $this->input->post('google_analysis_account');
        $google_analysis_key_file_name = $this->input->post('google_analysis_key_file_name');
        $google_analysis_profile_id = $this->input->post('google_analysis_profile_id');
        $google_analysis_tracking_id = $this->input->post('google_analysis_tracking_id');
        
        $instagram_url = $this->input->post('instagram_url');
        $youtube_bid_url = $this->input->post('youtube_bid_url');
        $yahoo_bid_url = $this->input->post('yahoo_bid_url');

        $per_page_by_product = $this->input->post('per_page_by_product');
        $per_page_by_news = $this->input->post('per_page_by_news');
        $per_page_by_faq = $this->input->post('per_page_by_faq');
        $per_page_by_home_board = $this->input->post('per_page_by_home_board');
        $per_page_by_home_product = $this->input->post('per_page_by_home_product');
        $per_page_by_member_order = $this->input->post('per_page_by_member_order');

        $product_countdown_date = $this->input->post('product_countdown_date');
        $product_countdown_enable = $this->input->post('product_countdown_enable');

        $product_vip_discount = $this->input->post('product_vip_discount');
        $product_vip_discount_enable = $this->input->post('product_vip_discount_enable');

        $cart_discount = $this->input->post('cart_discount');
        $cart_discount_enable = $this->input->post('cart_discount_enable');
        $cart_tax = $this->input->post('cart_tax');

        $point_feedback_formula = $this->input->post('point_feedback_formula');
        $point_feedback_formula_enable = $this->input->post('point_feedback_formula_enable');

        $ezship_account = $this->input->post('ezship_account');
        $ezship_password = $this->input->post('ezship_password');

        // $allpay_test = $this->input->post('allpay_test');
        $allpay_account = $this->input->post('allpay_account');
        $allpay_password = $this->input->post('allpay_password');
        $allpay_merchant_id = $this->input->post('allpay_merchant_id');
        $allpay_hash_key = $this->input->post('allpay_hash_key');
        $allpay_hash_iv = $this->input->post('allpay_hash_iv');
        $allpay_item_name = $this->input->post('allpay_item_name');

        $paypal_test = $this->input->post('paypal_test');
        $paypal_account = $this->input->post('paypal_account');
        $paypal_item_name = $this->input->post('paypal_item_name');
        
        $exchange_usdcad = $this->input->post('exchange_usdcad');

        if($site_name !== false) $data['site_name'] = $site_name;
        if($site_keywords !== false) $data['site_keywords'] = $site_keywords;
        if($site_description !== false) $data['site_description'] = $site_description;
        if($site_logo !== false) $data['site_logo'] = $site_logo;
        
        if($admin_email !== false) $data['admin_email'] = $admin_email;

        if($company_name !== false) $data['company_name'] = $company_name;
        if($company_email !== false) $data['company_email'] = $company_email;
        if($company_telephone !== false) $data['company_telephone'] = $company_telephone;
        if($company_fax !== false) $data['company_fax'] = $company_fax;
        if($company_address !== false) $data['company_address'] = $company_address;
        if($company_address_description !== false) $data['company_address_description'] = $company_address_description;
        if($company_time !== false) $data['company_time'] = $company_time;
        if($company_description !== false) $data['company_description'] = $company_description;
        if($company_blog !== false) $data['company_blog'] = $company_blog;
        if($company_bank_name !== false) $data['company_bank_name'] = $company_bank_name;
        if($company_bank_code !== false) $data['company_bank_code'] = $company_bank_code;
        if($company_bank_account !== false) $data['company_bank_account'] = $company_bank_account;
        if($company_bank_account_name !== false) $data['company_bank_account_name'] = $company_bank_account_name;

        if($email_smtp_host !== false) $data['email_smtp_host'] = $email_smtp_host;
        if($email_smtp_port !== false) $data['email_smtp_port'] = $email_smtp_port;
        if($email_smtp_account !== false) $data['email_smtp_account'] = $email_smtp_account;
        if($email_smtp_password !== false) $data['email_smtp_password'] = $email_smtp_password;
        if($email_from !== false) $data['email_from'] = $email_from;
        if($email_from_name !== false) $data['email_from_name'] = $email_from_name;
        if($email_cc !== false) $data['email_cc'] = $email_cc;
        if($email_bcc !== false) $data['email_bcc'] = $email_bcc;
        if($email_queue !== false) $data['email_queue'] = $email_queue;

        if($facebook_url !== false) $data['facebook_url'] = $facebook_url;
        if($facebook_id !== false) $data['facebook_id'] = $facebook_id;
        if($facebook_api_id !== false) $data['facebook_api_id'] = $facebook_api_id;
        if($facebook_api_secret !== false) $data['facebook_api_secret'] = $facebook_api_secret;

        if($google_map_iframe !== false) $data['google_map_iframe'] = $google_map_iframe;

        // if($google_account !== false) $data['google_account'] = $google_account;
        // if($google_password !== false) $data['google_password'] = $google_password;
        // if($google_analysisProfileId !== false) $data['google_analysisProfileId'] = $google_analysisProfileId;
        // if($google_analysisTrackingId !== false) $data['google_analysisTrackingId'] = $google_analysisTrackingId;
        if($google_analysis_account !== false) $data['google_analysis_account'] = $google_analysis_account;
        if($google_analysis_key_file_name !== false) $data['google_analysis_key_file_name'] = $google_analysis_key_file_name;
        if($google_analysis_profile_id !== false) $data['google_analysis_profile_id'] = $google_analysis_profile_id;
        if($google_analysis_tracking_id !== false) $data['google_analysis_tracking_id'] = $google_analysis_tracking_id;
        
        if($instagram_url !== false) $data['instagram_url'] = $instagram_url;
        if($yahoo_bid_url !== false) $data['yahoo_bid_url'] = $yahoo_bid_url;
        if($youtube_bid_url !== false) $data['youtube_bid_url'] = $youtube_bid_url;

        if($per_page_by_product !== false) $data['per_page_by_product'] = $per_page_by_product;
        if($per_page_by_news !== false) $data['per_page_by_news'] = $per_page_by_news;
        if($per_page_by_faq !== false) $data['per_page_by_faq'] = $per_page_by_faq;
        if($per_page_by_home_board !== false) $data['per_page_by_home_board'] = $per_page_by_home_board;
        if($per_page_by_home_product !== false) $data['per_page_by_home_product'] = $per_page_by_home_product;
        if($per_page_by_member_order !== false) $data['per_page_by_member_order'] = $per_page_by_member_order;

        if($product_countdown_date !== false) $data['product_countdown_date'] = $product_countdown_date;
        if($product_countdown_enable !== false) $data['product_countdown_enable'] = $product_countdown_enable;
        
        if($product_vip_discount !== false) $data['product_vip_discount'] = $product_vip_discount;
        if($product_vip_discount_enable !== false) $data['product_vip_discount_enable'] = $product_vip_discount_enable;
        
        if($cart_discount !== false) $data['cart_discount'] = $cart_discount;
        if($cart_discount_enable !== false) $data['cart_discount_enable'] = $cart_discount_enable;
        if($cart_tax !== false) $data['cart_tax'] = $cart_tax;

        if($point_feedback_formula !== false) $data['point_feedback_formula'] = $point_feedback_formula;
        if($point_feedback_formula_enable !== false) $data['point_feedback_formula_enable'] = $point_feedback_formula_enable;

        if($ezship_account !== false) $data['ezship_account'] = $ezship_account;
        if($ezship_password !== false) $data['ezship_password'] = $ezship_password;

        // if($allpay_test !== false) $data['allpay_test'] = $allpay_test;
        if($allpay_account !== false) $data['allpay_account'] = $allpay_account;
        if($allpay_password !== false) $data['allpay_password'] = $allpay_password;
        if($allpay_merchant_id !== false) $data['allpay_merchant_id'] = $allpay_merchant_id;
        if($allpay_hash_key !== false) $data['allpay_hash_key'] = $allpay_hash_key;
        if($allpay_hash_iv !== false) $data['allpay_hash_iv'] = $allpay_hash_iv;
        if($allpay_item_name !== false) $data['allpay_item_name'] = $allpay_item_name;

        if($paypal_test !== false) $data['paypal_test'] = $paypal_test;
        if($paypal_account !== false) $data['paypal_account'] = $paypal_account;
        if($paypal_item_name !== false) $data['paypal_item_name'] = $paypal_item_name;
        
        if($exchange_usdcad !== false) $data['exchange_usdcad'] = $exchange_usdcad;

        if(isset($data)) {
            $objConfig = Config::find_by_name('site_logo');
            if($objConfig->value && $site_logo !== false && $objConfig->value != $site_logo) {
                $this->load->library('image_library');
                $this->image_library->del('site', $objConfig->value);
            }

            foreach((array)$data as $key => $value) {
                $config = Config::find_by_name($key);
                $config->value = $value;
                $config->save();
            }

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '修改成功',
                'data' => ''
            ]);
            return true;
        }
    }

    public function payment() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        
        $id = $this->input->post('id');
        $use = $this->input->post('use', true);
        $description = $this->input->post('description', true);
        $shippings = $this->input->post('shippings', true);
        $enable = $this->input->post('enable', true);

        $objPayment = Payment::find($id);
        if(!$objPayment) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($use !== false && $objPayment->use != $use) $data['use'] = $use;
        if($description !== false && $objPayment->description != $description) $data['description'] = $description;
        if($shippings !== false && $objPayment->shippings != $shippings) $data['shippings'] = $shippings;
        if($enable !== false && $objPayment->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPayment->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function shipping() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        
        $id = $this->input->post('id');
        $price = $this->input->post('price', true);
        $free = $this->input->post('free', true);
        $description = $this->input->post('description');
        $payments = $this->input->post('payments');
        $enable = $this->input->post('enable', true);

        $objShipping = Shipping::find($id);
        if(!$objShipping) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($price !== false && $objShipping->price != $price) $data['price'] = $price;
        if($free !== false && $objShipping->free != $free) $data['free'] = $free;
        if($description !== false && $objShipping->description != $description) $data['description'] = $description;
        if($payments !== false && $objShipping->payments != $payments) $data['payments'] = $payments;
        if($enable !== false && $objShipping->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objShipping->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }
}