<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Point_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $text = $this->input->post('text', TRUE);

        $pagination = array('total' => 0, 'count' => $count, 'pages' => 1, 'page' => $page);

        $query['conditions'] = '`delete` = 0';
        if($text)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '(user_name LIKE "%'.$text.'%" OR name LIKE "%'.$text.'%")';
        }

        $total = Point::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $points = array();
        $objPoints = Point::all($query);
        if($objPoints) $points = to_array($objPoints, array('methods' => '_created_at'));

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('points' => $points, 'pagination' => $pagination)
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('user_id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;
        if($this->input->post('point') === FALSE) return FALSE;

        $user_id = $this->input->post('user_id', TRUE);
        $order_id = $this->input->post('order_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $point = $this->input->post('point', TRUE);

        $objUser = User::find_by_id($user_id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'user_id',
                'message' => 'user_id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $data['user_id'] = $objUser->id;
        $data['user_account'] = $objUser->account;
        $data['user_name'] = $objUser->name;

        if($order_id) $data['order_id'] = $order_id;
        if($name) $data['name'] = $name;
        if($point) $data['point'] = $point;
        
        if(isset($data))
        {
            $objPoint = Point::create($data);
            $point = $objPoint->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('point' => $point)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $user_id = $this->input->post('user_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $point = $this->input->post('point', TRUE);

        $objPoint = Point::find($id);
        if(!$objPoint)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser = User::find($user_id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($name !== FALSE && $objPoint->name != $name) $data['name'] = $name;
        if($point !== FALSE && $objPoint->point != $point) $data['point'] = $point;

        if(isset($data))
        {
            $objPoint->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function del() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objPoint = Point::find($id);
        if(!$objPoint)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPoint->delete = 1;
        $objPoint->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function clean() {
        if(Point::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objPoint = Point::find($id);
        if(!$objPoint)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPoint->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
        return TRUE;
    }


    public function adds() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('user_id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;
        if($this->input->post('point') === FALSE) return FALSE;

        $user_id = $this->input->post('user_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $point = $this->input->post('point', TRUE);

        if($user_id && !is_array($user_id)) $user_id = array($user_id);
        if($name) $data['name'] = $name;
        if($point) $data['point'] = $point;

        if(isset($data))
        {
            $query['conditions'] = '`id` IN ('.implode(',', $user_id).')';
            $objUsers = User::all($query);
            if(!$objUsers)
            {
                $this->load->view('api/respone', array(
                    'status' => 'user_id',
                    'message' => 'user_id 不存在',
                    'data' => ''
                ));
                return FALSE;
            }

            foreach((array)$objUsers as $i => $o)
            {
                $data['user_id'] = $o->id;
                $data['user_account'] = $o->account;
                $data['user_name'] = $o->name;

                $objPoint = Point::create($data);
            }

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array()
            ));
            return TRUE;
        }
    }
}