<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Wish_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function add() {
        $objLoginUser = User::get();
        if($objLoginUser === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('product_id') === FALSE) return FALSE;
        if($this->input->post('product_spec_id') === FALSE) return FALSE;

        $product_id = $this->input->post('product_id', TRUE);
        $product_spec_id = $this->input->post('product_spec_id', TRUE);

        $objProduct = Product::find_by_id_and_enable_and_delete($product_id, 1, 0);
        if(!$objProduct) return FALSE;

        $objProductSpec = array();
        if($product_spec_id)
        {
            $objProductSpec = ProductSpec::find_by_id_and_enable_and_delete($product_spec_id, 1, 0);
            if(!$objProductSpec) return FALSE;
        }

        $objWish = Wish::find_by_user_id_and_product_id_and_product_spec_id($objLoginUser->id, $product_id, $product_spec_id);
        if(!$objWish)
        {
            $data = array(
                'user_id' => $objLoginUser->id,
                'product_id' => $product_id,
                'product_spec_id' => $product_spec_id,
                'product_no' => $objProduct->no,
                'product_name' => $objProduct->name,
                'product_image' => $objProduct->image,
                'product_image_thumb' => thumbname($objProduct->image),
                'product_price' => $objProduct->price,
                'product_price_vip' => $objProduct->price_vip,
                'product_price_special' => $objProduct->price_special
            );
            if($objProduct->special_start) $data['product_special_start'] = $objProduct->special_start;
            if($objProduct->special_end) $data['product_special_end'] = $objProduct->special_end;
            if($objProductSpec)
            {
                $data['product_spec_name'] = $objProductSpec->name;
                $data['product_spec_price'] = $objProductSpec->price;
                $data['product_spec_price_vip'] = $objProductSpec->price_vip;
                $data['product_spec_price_special'] = $objProductSpec->price_special;
            }

            $objWish = Wish::create($data);
        }
        $wish = $objWish->to_array();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '新增成功',
            'data' => array('wish' => $wish)
        ));
        return TRUE;
    }

    public function del() {
        $objLoginUser = User::get();
        if($objLoginUser === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('product_id') === FALSE) return FALSE;
        if($this->input->post('product_spec_id') === FALSE) return FALSE;

        $product_id = $this->input->post('product_id', TRUE);
        $product_spec_id = $this->input->post('product_spec_id', TRUE);

        $objProduct = Product::find_by_id_and_enable_and_delete($product_id, 1, 0);
        if(!$objProduct) return FALSE;

        $objProductSpec = array();
        if($product_spec_id)
        {
            $objProductSpec = ProductSpec::find_by_id_and_enable_and_delete($product_spec_id, 1, 0);
            if(!$objProductSpec) return FALSE;
        }

        $objWish = Wish::find_by_user_id_and_product_id_and_product_spec_id($objLoginUser->id, $product_id, $product_spec_id);
        if($objWish)
        {
            $objWish->delete();

            $objWishes = Wish::all(array(
                'conditions' => 'user_id = '.$objLoginUser->id,
                'order' => 'id DESC'
            ));
            $wishes = to_array($objWishes, array('methods' => array('_created_at')));

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '刪除成功',
                'data' => array('wishes' => $wishes)
            ));
            return TRUE;
        }

        return FALSE;
    }
}