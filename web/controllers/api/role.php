<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Role_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function add() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;
        if($this->input->post('code') === FALSE) return FALSE;

        $name = $this->input->post('name', TRUE);
        $code = $this->input->post('code', TRUE);

        $objRole = Role::find_by_name($name);
        if($objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 重複',
                'data' => array()
            ));
            return FALSE;
        }

        $objRole = Role::find_by_code($code);
        if($objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'code',
                'message' => 'code 重複',
                'data' => array()
            ));
            return FALSE;
        }

        if($name) $data['name'] = $name;
        if($code) $data['code'] = $code;

        if(isset($data))
        {
            $objRole = Role::create($data);
            $role = $objRole->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('role' => $role)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;
        if($this->input->post('code') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $name = $this->input->post('name', TRUE);
        $code = $this->input->post('code', TRUE);

        $objRole = Role::find_by_name($name);
        if($objRole && $objRole->id != $id)
        {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 重複',
                'data' => array()
            ));
            return FALSE;
        }

        $objRole = Role::find_by_code($code);
        if($objRole && $objRole->id != $id)
        {
            $this->load->view('api/respone', array(
                'status' => 'code',
                'message' => 'code 重複',
                'data' => array()
            ));
            return FALSE;
        }

        $objRole = Role::find($id);
        if(!$objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($name !== FALSE && $objRole->name != $name) $data['name'] = $name;
        if($code !== FALSE && $objRole->code != $code) $data['code'] = $code;

        if(isset($data))
        {
            $objRole->update_attributes($data);

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '修改成功',
                'data' => array()
            ));
            return TRUE;
        }
    }

    public function del() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objRole = Role::find($id);
        if(!$objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objRole->delete = 1;
        $objRole->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function clear() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objRole = Role::find($id);
        if(!$objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objRole->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
        return TRUE;
    }
}