<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Order_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function create() {
        if($this->input->post() === false) return false;
        if($this->input->post('recipient_name') === false) return false;
        if($this->input->post('recipient_cellphone') === false) return false;
        if($this->input->post('payment_id') === false) return false;
        if($this->input->post('shipping_id') === false) return false;
        if($this->input->post('price_all') === false) return false;

        $recipient_name = $this->input->post('recipient_name', true);
        $recipient_cellphone = $this->input->post('recipient_cellphone', true);
        $recipient_zip = $this->input->post('recipient_zip', true);
        $recipient_address = $this->input->post('recipient_address', true);
        $recipient_email = $this->input->post('recipient_email', true);
        $recipient_note = $this->input->post('recipient_note', true);

        $objUser = User::get();
        $cart = $this->session->userdata('cart');
        $cart['recipient_name'] = $recipient_name;
        $cart['recipient_cellphone'] = $recipient_cellphone;
        $cart['recipient_zip'] = $recipient_zip;
        $cart['recipient_address'] = $recipient_address;
        $cart['recipient_email'] = $recipient_email;
        $cart['recipient_note'] = $recipient_note;
        if($cart['config_cart_user'] && $objUser) {
            $cart['user_id'] = $objUser->id;
            $cart['user_account'] = $objUser->account;
            $cart['user_name'] = $objUser->name;
            $cart['user_gender'] = $objUser->gender;
            $cart['user_vip'] = $objUser->vip;
            $cart['user_black'] = $objUser->black;
        }

        if(isset($cart['buyer_name']) === false || empty($cart['buyer_name'])) return false;
        if(isset($cart['recipient_name']) === false || empty($cart['recipient_name'])) return false;
        if(isset($cart['recipient_cellphone']) === false || empty($cart['recipient_cellphone'])) return false;
        if(isset($cart['recipient_email']) === false || empty($cart['recipient_email'])) return false;
        if(isset($cart['payment_id']) === false || empty($cart['payment_id'])) return false;
        if(isset($cart['shipping_id']) === false || empty($cart['shipping_id'])) return false;
        if(isset($cart['price_all']) === false || empty($cart['price_all'])) return false;
        if(isset($cart['products']) === false || empty($cart['products'])) return false;

        $products = $cart['products'];
        foreach((array)$products as $i => $p) {
            $objProduct = Product::find_by_id_and_enable_and_delete($p['product_id'], 1, 0);
            if(!$objProduct) {
                $this->load->view('api/respone', array(
                    'status' => 'product',
                    'message' => '商品錯誤',
                    'data' => ''
                ));
                return false;
            }
            if($objProduct->stock - $p['quantity'] < 0) {
                $this->load->view('api/respone', array(
                    'status' => 'stock',
                    'message' => '商品庫存不足',
                    'data' => ''
                ));
                return false;
            }
        }

        if($cart['payment_id']) {
            $objPayment = Payment::find_by_id_and_enable_and_delete($cart['payment_id'], 1, 0);
            if(!$objPayment) return false;

            $cart['payment_id'] = $objPayment->id;
            $cart['payment_name'] = strip_tags($objPayment->name);
            $cart['payment_code'] = $objPayment->code;
        }

        if($cart['shipping_id']) {
            $objShipping = Shipping::find_by_id_and_enable_and_delete($cart['shipping_id'], 1, 0);
            if(!$objShipping) return false;

            if($objShipping->code == 'post' || $objShipping->code == 'delivery') {
                if(isset($cart['recipient_name']) === false || empty($cart['recipient_name'])) return false;
                if(isset($cart['recipient_cellphone']) === false || empty($cart['recipient_cellphone'])) return false;
                if(isset($cart['recipient_zip']) === false || empty($cart['recipient_zip'])) return false;
                if(isset($cart['recipient_address']) === false || empty($cart['recipient_address'])) return false;

                $cart['cvs_code'] = '';
                $cart['cvs_name'] = '';
                $cart['cvs_telephone'] = '';
                $cart['cvs_zip'] = '';
                $cart['cvs_address'] = '';
            }
            elseif($objShipping->code == 'cvs') {
                if(isset($cart['cvs_code']) === false || empty($cart['cvs_code'])) return false;
                if(isset($cart['cvs_name']) === false || empty($cart['cvs_name'])) return false;
                if(isset($cart['cvs_address']) === false || empty($cart['cvs_address'])) return false;

                $cart['recipient_zip'] = '';
                $cart['recipient_address'] = '';
            }

            $cart['shipping_id'] = $objShipping->id;
            $cart['shipping_name'] = $objShipping->name;
            $cart['shipping_code'] = $objShipping->code;
            $cart['shipping_price'] = $objShipping->price;
            $cart['shipping_free'] = $objShipping->free;
        }

        unset($cart['init']);
        unset($cart['products']);
        // if(empty($cart['invoice_date'])) unset($cart['invoice_date']);
        $cart['status_pay'] = Order::status('pending');
        $cart['enable'] = 1;
        $objOrder = Order::create($cart);

        foreach((array)$products as $i => $p) {
            $products[$i]['product_special'] = (filter_var($p['product_special'], FILTER_VALIDATE_BOOLEAN) ? 1 : 0);

            $objProduct = Product::find_by_id_and_enable_and_delete($p['product_id'], 1, 0);
            if(!$objProduct) continue;

            $objProduct->sold_times = $objProduct->sold_times + $p['quantity'];
            $objProduct->stock = $objProduct->stock - $p['quantity'];
            $objProduct->preorder = $objProduct->preorder + $p['quantity'];
            $objProduct->save();

            $orderProduct = array(
                'order_id' => $objOrder->id,
                'ptype_id' => $p['ptype_id'],
                'ptype_name' => $p['ptype_name'],
                'ptype_code' => $p['ptype_code'],
                'pcategory_id' => $p['pcategory_id'],
                'pcategory_name' => $p['pcategory_name'],
                'product_id' => $p['product_id'],
                'product_no' => $p['product_no'],
                'product_name' => $p['product_name'],
                'product_image' => $p['product_image'],
                'product_price' => $p['product_price'],
                'product_price_special' => $p['product_price_special'],
                'product_price_usd' => $p['product_price_usd'],
                'product_price_special_usd' => $p['product_price_special_usd'],
                'product_price_cad' => $p['product_price_cad'],
                'product_price_special_cad' => $p['product_price_special_cad'],
                'product_special' => $p['product_special'],
                'price' => $p['price'],
                'quantity' => $p['quantity'],
                'discount' => $p['discount'],
                'subtotal' => $p['subtotal']
            );
            if($p['product_price_special_start']) $orderProduct['product_price_special_start'] = $p['product_price_special_start'];
            if($p['product_price_special_end']) $orderProduct['product_price_special_end'] = $p['product_price_special_end'];
            if($p['product_price_special_start_usd']) $orderProduct['product_price_special_start_usd'] = $p['product_price_special_start_usd'];
            if($p['product_price_special_end_usd']) $orderProduct['product_price_special_end_usd'] = $p['product_price_special_end_usd'];
            if($p['product_price_special_start_cad']) $orderProduct['product_price_special_start_cad'] = $p['product_price_special_start_cad'];
            if($p['product_price_special_end_cad']) $orderProduct['product_price_special_end_cad'] = $p['product_price_special_end_cad'];

            OrderProduct::create($orderProduct);
        }

        $this->session->set_userdata(array('order_id' => $objOrder->id));

        $order = $objOrder->to_array();
        $order['products'] = $products;

        if($cart['config_cart_user'] && $objUser) {
            if(isset($cart['recipient_cellphone']) && $cart['recipient_cellphone'] && !$objUser->cellphone) $objUser->cellphone = $cart['recipient_cellphone'];
            if(isset($cart['recipient_zip']) && $cart['recipient_zip'] && !$objUser->zip) $objUser->zip = $cart['recipient_zip'];
            if(isset($cart['recipient_address']) && $cart['recipient_address'] && !$objUser->address) $objUser->address = $cart['recipient_address'];
            if(isset($cart['cvs_type']) && $cart['cvs_type']) $objUser->cvs_type = $cart['cvs_type'];
            if(isset($cart['cvs_code']) && $cart['cvs_code']) $objUser->cvs_code = $cart['cvs_code'];
            if(isset($cart['cvs_name']) && $cart['cvs_name']) $objUser->cvs_name = $cart['cvs_name'];
            if(isset($cart['cvs_telephone']) && $cart['cvs_telephone']) $objUser->cvs_telephone = $cart['cvs_telephone'];
            if(isset($cart['cvs_zip']) && $cart['cvs_zip']) $objUser->cvs_zip = $cart['cvs_zip'];
            if(isset($cart['cvs_address']) && $cart['cvs_address']) $objUser->cvs_address = $cart['cvs_address'];
            $objUser->save();
        }

        $this->load->library('mail_library');
        $this->mail_library->user_order_buy($objOrder->recipient_email, $order);
        $this->mail_library->sys_order_buy($objOrder->recipient_email, $order);

        if($cart['shipping_code'] == 'cvs') {
            $this->load->library('ezship_library');
            $this->ezship_library->xml_order_send($order);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '新增成功',
            'data' => array('order' => $order)
        ));
        return true;
    }

    public function edit_payment_atm() {
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        if($this->input->post('payment_code') === false) return false;
        if($this->input->post('paysystem_bank_name') === false) return false;
        if($this->input->post('paysystem_bank_code') === false) return false;
        if($this->input->post('paysystem_bank_account') === false) return false;
        if($this->input->post('paysystem_bank_price') === false) return false;
        if($this->input->post('paysystem_bank_user') === false) return false;
        if($this->input->post('paysystem_bank_at') === false) return false;

        $id = $this->input->post('id', true);
        $payment_code = $this->input->post('payment_code', true);
        $paysystem_type = $this->input->post('paysystem_type', true);
        $paysystem_bank_name = $this->input->post('paysystem_bank_name', true);
        $paysystem_bank_code = $this->input->post('paysystem_bank_code', true);
        $paysystem_bank_account = $this->input->post('paysystem_bank_account', true);
        $paysystem_bank_price = $this->input->post('paysystem_bank_price', true);
        $paysystem_bank_user = $this->input->post('paysystem_bank_user', true);
        $paysystem_bank_at = $this->input->post('paysystem_bank_at', true);
        
        $objOrder = Order::find_by_id($id);
        if(!$objOrder) return false;

        if($paysystem_type) $data['paysystem_type'] = $paysystem_type;
        if($paysystem_bank_name) $data['paysystem_bank_name'] = $paysystem_bank_name;
        if($paysystem_bank_code) $data['paysystem_bank_code'] = $paysystem_bank_code;
        if($paysystem_bank_account) $data['paysystem_bank_account'] = $paysystem_bank_account;
        if($paysystem_bank_price) $data['paysystem_bank_price'] = $paysystem_bank_price;
        if($paysystem_bank_user) $data['paysystem_bank_user'] = $paysystem_bank_user;
        if($paysystem_bank_at) $data['paysystem_bank_at'] = date('Y-m-d H:i:s', strtotime($paysystem_bank_at));

        if(isset($data) === false) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => '修改失敗',
                'data' => array()
            ));
            return false;
        }

        $objOrder->update_attributes($data);

        if($objOrder->status_remit != Order::status_remit('remited')) {
            $objOrder->update_attributes(array(
                'status_remit' => Order::status_remit('remited')
            ));
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => []
        ]);
        return true;
    }

    public function get() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('status') === false) return false;
        if($this->input->post('start') === false) return false;
        if($this->input->post('end') === false) return false;
        if($this->input->post('count') === false) return false;
        if($this->input->post('page') === false) return false;
        if($this->input->post('sort') === false) return false;

        $status = $this->input->post('status', true);
        $start = $this->input->post('start', true);
        $end = $this->input->post('end', true);
        $count = $this->input->post('count', true);
        $page = $this->input->post('page', true);
        $sort = $this->input->post('sort', true);
        $text = $this->input->post('text', true);
        $user_id = $this->input->post('user_id', true);

        $pagination = array('total' => 0, 'count' => $count, 'pages' => 1, 'page' => $page);

        $status_remit = array();
        $status_pay = array();
        $status_ship = array();
        $status_return = array();
        $status_refund = array();
        $enable = 1;
        if(!$status) {}
        elseif($status == Order::status('remited')) $status_remit = array(Order::status('remited'));
        elseif($status == Order::status('pending')) $status_pay = array(Order::status('pending'));
        elseif($status == Order::status('paid')) $status_pay = array(Order::status('paid'));
        elseif($status == Order::status('shipping')) $status_ship = array(Order::status('shipping'));
        elseif($status == Order::status('arrived')) $status_ship = array(Order::status('arrived'));
        elseif($status == Order::status('returning')) $status_return = array(Order::status('returning'));
        elseif($status == Order::status('returned')) $status_return = array(Order::status('returned'));
        elseif($status == Order::status('refunding')) $status_refund = array(Order::status('refunding'));
        elseif($status == Order::status('refunded')) $status_refund = array(Order::status('refunded'));
        elseif($status == Order::status('cancel')) $enable = 0;
        $query['conditions'] = '`delete` = 0 AND '.
                               ($user_id ? '`user_id` = '.$user_id.' AND ' : '').
                               ($text ? '(`no` LIKE "%'.$text.'%" OR '.
                                         '`buyer_email` LIKE "%'.$text.'%" OR '.
                                         '`buyer_name` LIKE "%'.$text.'%" OR '.
                                         '`buyer_cellphone` LIKE "%'.$text.'%" OR '.
                                         '`buyer_telephone` LIKE "%'.$text.'%" OR '.
                                         '`recipient_name` LIKE "%'.$text.'%" OR '.
                                         '`recipient_cellphone` LIKE "%'.$text.'%" OR '.
                                         '`recipient_telephone` LIKE "%'.$text.'%" OR '.
                                         '`recipient_email` LIKE "%'.$text.'%") AND ' : '').
                               ($status_remit ? '`status_remit` IN ('.implode(',', $status_remit).') AND ' : '').
                               ($status_pay ? '`status_pay` IN ('.implode(',', $status_pay).') AND ' : '').
                               ($status_ship ? '`status_ship` IN ('.implode(',', $status_ship).') AND ' : '').
                               ($status_return ? '`status_return` IN ('.implode(',', $status_return).') AND ' : '').
                               ($status_refund ? '`status_refund` IN ('.implode(',', $status_refund).') AND ' : '').
                               '`enable` = '.$enable.' AND '.
                               '`created_at` BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $total = Order::count($query);

        if($sort)
        {
            $_sort = explode(' ', $sort);
            if(count($_sort) == 2) $sort = '`'.$_sort[0].'` '.$_sort[1];
            else $sort = 'id DESC';
        }

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $orders = array();
        $objOrders = Order::all($query);
        if($objOrders)
        {
            $orders = to_array($objOrders,
                array('methods' => array('_created_at', '_payment', '_status_remit', '_status_pay', '_status_ship', '_status_return', '_status_refund'),
                      'include' => array('order_products')));
        }

        $sum = array(
            'total' => 0
        );
        foreach((array)$orders as $i => $o)
        {
            $sum['total'] += $o['price_total'];
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ($count == 'all' ? 1 : (ceil($total / $count) > 0 ? ceil($total / $count) : 1));
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('orders' => $orders, 'sum' => $sum, 'total' => $total, 'pagination' => $pagination)
        ));
        return true;
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        
        $id = $this->input->post('id', true);
        $shipping_no = $this->input->post('shipping_no', true);
        $shipping_at = $this->input->post('shipping_at', true);
        $status_remit = $this->input->post('status_remit', true);
        $status_pay = $this->input->post('status_pay', true);
        $status_ship = $this->input->post('status_ship', true);
        $status_return = $this->input->post('status_return', true);
        $status_refund = $this->input->post('status_refund', true);
        $enable = $this->input->post('enable', true);

        $objOrder = Order::find_by_id($id);
        if(!$objOrder)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($shipping_no !== false && $objOrder->shipping_no != $shipping_no) $data['shipping_no'] = $shipping_no;
        if($shipping_at !== false && $objOrder->shipping_at != $shipping_at) $data['shipping_at'] = $shipping_at;
        if($status_remit !== false && $objOrder->status_remit != $status_remit) $data['status_remit'] = $status_remit;
        if($status_pay !== false && $objOrder->status_pay != $status_pay) $data['status_pay'] = $status_pay;
        if($status_ship !== false && $objOrder->status_ship != $status_ship) $data['status_ship'] = $status_ship;
        if($status_return !== false && $objOrder->status_return != $status_return) $data['status_return'] = $status_return;
        if($status_refund !== false && $objOrder->status_refund != $status_refund) $data['status_refund'] = $status_refund;
        if($enable !== false && $objOrder->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $before_status_remit = $objOrder->status_remit;
            $before_status_pay = $objOrder->status_pay;
            $before_status_ship = $objOrder->status_ship;
            $before_enable = $objOrder->enable;

            if($status_ship !== false && 
               $before_status_ship != $status_ship &&
               $status_ship == Order::status('shipping'))
            {
                $data['shipping_at'] = date('Y-m-d H:i:s');
            }

            $objOrder->update_attributes($data);
            $order = $objOrder->to_array();

            $this->load->library('mail_library');
            if($status_remit !== false && $before_status_remit != $status_remit && $status_remit == Order::status('remited')) {}
            if($status_pay !== false && $before_status_pay != $status_pay && $status_pay == Order::status('paid'))
            {
                $objOrderProducts = OrderProduct::find_all_by_order_id($id);
                foreach((array)$objOrderProducts as $i => $e)
                {
                    $objProduct = Product::find_by_id($e->product_id);
                    if($objProduct) {
                        $objProduct->stock - $e->quantity;
                        $objProduct->save();
                    }
                }
                
                $this->mail_library->user_order_paid($objOrder->user->email, $order);
            }
            if($status_ship !== false && $before_status_ship != $status_ship && $status_ship == Order::status('shipping'))
            {
                $this->mail_library->user_order_shipping($objOrder->user->email, $order);
            }
            if($enable !== false && $before_enable != $enable && $enable == 0)
            {
                $this->mail_library->user_order_canceled($objOrder->user->email, $order);
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return true;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $enable = $this->input->post('enable', true);

        $objOrder = Order::find_by_id($id);
        if(!$objOrder)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($enable !== false && $objOrder->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objOrder->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objOrder = Order::find_by_id($id);
        if(!$objOrder)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objOrder->delete = 0;
        $objOrder->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objOrder = Order::find_by_id($id);
        if(!$objOrder)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objOrder->delete = 1;
        $objOrder->save();

        $objOrderProducts = OrderProduct::all(array(
            'conditions' => '`order_id` = '.$objOrder->id
        ));
        foreach((array)$objOrderProducts as $i => $o) {
            $objProduct = Product::find_by_id($o->product_id);
            if($objProduct) {
                $objProduct->stock = $objProduct->stock + $o->quantity;
                // $objProduct->sold_times = $objProduct->sold_times - $o->quantity;
                // if($objOrder->status_pay == Order::status('paid')) {
                //     $objProduct->ordered = $objProduct->ordered - $o->quantity;
                // }
                // else {
                //     $objProduct->preorder = $objProduct->preorder - $o->quantity;
                // }
                $objProduct->save();
            }
        }

        $objUser = User::find_by_id($objOrder->user_id);
        if($objUser) $objUser->update_attributes(['order_times' => $objUser->order_times - 1]);

        // $this->load->library('statistics_library');
        // $statisticsorder = $this->statistics_library->order_calc(date('Y-m-d', strtotime($objOrder->created_at)));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $objOrder = Order::find_by_id($id);
        if(!$objOrder)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objOrder->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function edits() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        
        $id = $this->input->post('id', true);
        $status_remit = $this->input->post('status_remit', true);
        $status_pay = $this->input->post('status_pay', true);
        $status_ship = $this->input->post('status_ship', true);
        $status_return = $this->input->post('status_return', true);
        $status_refund = $this->input->post('status_refund', true);
        $enable = $this->input->post('enable', true);

        if($id && !is_array($id)) $id = array($id);

        $query['conditions'] = '`id` IN ('.implode(',', $id).')';

        $objOrders = Order::all($query);
        if(!$objOrders)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($status_remit !== false) $data['status_remit'] = $status_remit;
        if($status_pay !== false) $data['status_pay'] = $status_pay;
        if($status_ship !== false) $data['status_ship'] = $status_ship;
        if($status_return !== false) $data['status_return'] = $status_return;
        if($status_refund !== false) $data['status_refund'] = $status_refund;
        if($enable !== false) $data['enable'] = $enable;

        if($objOrders && isset($data))
        {
            foreach((array)$objOrders as $i => $o)
            {
                $before_status_remit = $o->status_remit;
                $before_status_pay = $o->status_pay;
                $before_status_ship = $o->status_ship;
                $before_enable = $o->enable;

                if($status_ship !== false && 
                   $before_status_ship != $status_ship &&
                   $status_ship == Order::status('shipping'))
                {
                    $data['shipping_at'] = date('Y-m-d H:i:s');
                }

                $o->update_attributes($data);

                $this->load->library('mail_library');
                if($status_remit !== false && $before_status_remit != $status_remit && $status_remit == Order::status('remited')) {}
                if($status_pay !== false && $before_status_pay != $status_pay && $status_pay == Order::status('paid')) {
                    // $objOrderProducts = OrderProduct::find_all_by_order_id($o->id);
                    // foreach((array)$objOrderProducts as $j => $e) {
                    //     $objProduct = Product::find_by_id($e->product_id);
                    //     if($objProduct) {
                    //         $objProduct->preorder = $objProduct->preorder - $e->quantity;
                    //         $objProduct->ordered = $objProduct->ordered + $e->quantity;
                    //         $objProduct->save();
                    //     }
                    // }
                    
                    $this->mail_library->user_order_paid($o->user->email, $o->to_array());
                }
                if($status_ship !== false && $before_status_ship != $status_ship && $status_ship == Order::status('shipping'))
                {
                    $this->mail_library->user_order_shipping($o->user->email, $o->to_array());
                }
                if($enable !== false && $before_enable != $enable && $enable == 0)
                {
                    $this->mail_library->user_order_canceled($o->user->email, $o->to_array());
                }
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return true;
    }

    public function deletes_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        if($id && !is_array($id)) $id = array($id);

        $query['conditions'] = '`id` IN ('.implode(',', $id).')';

        $objOrders = Order::all($query);
        if(!$objOrders)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        // $this->load->library('statistics_library');

        foreach((array)$objOrders as $i => $o)
        {
            $o->delete = 1;
            $o->save();

            $objOrderProducts = OrderProduct::all(array(
                'conditions' => '`order_id` = '.$o->id
            ));
            foreach((array)$objOrderProducts as $j => $op)
            {
                $objProduct = Product::find_by_id($op->product_id);
                if($objProduct) {
                    $objProduct->stock = $objProduct->stock + $op->quantity;
                    // $objProduct->sold_times = $objProduct->sold_times - $op->quantity;
                    // if($o->status_pay == Order::status('paid')) {
                    //     $objProduct->ordered = $objProduct->ordered - $op->quantity;
                    // }
                    // else {
                    //     $objProduct->preorder = $objProduct->preorder - $op->quantity;
                    // }
                    $objProduct->save();
                }
            }

            $objUser = User::find_by_id($o->user_id);
            if($objUser) $objUser->update_attributes(['order_times' => $objUser->order_times - 1]);

            // $statisticsorder = $this->statistics_library->order_calc(date('Y-m-d', strtotime($o->$created_at)));
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return true;
    }

    public function ezship_xml_order_return() {
        trigger_error('xml_order_return');
        trigger_error(json_encode($this->input->post()));

        // $this->load->library('mail_library');
        // $this->mail_library->test('shawe.seven@gmail.com');
    }

    public function payment() {
        $order_id = $this->session->userdata('order_id');
        if(!$order_id) redirect(configLangURI.'/');

        $objOrder = Order::find_by_id_and_enable_and_delete($order_id, 1, 0);
        if(!$objOrder) redirect(configLangURI.'/');
        $order = $objOrder->to_array();

        $objPayment = Payment::find_by_id_and_enable_and_delete($objOrder->payment_id, 1, 0);
        if(!$objPayment) redirect(configLangURI.'/');
        if($objPayment->code == 'credit' || $objPayment->code == 'atm' || $objPayment->code == 'cvs') {
            $this->load->library('allpay_library');
            $this->allpay_library->checkout($order);
            return true;
        }
        if($objPayment->code == 'hncb') {
            $this->load->library('hncb_library');
            $this->hncb_library->checkout($order);
            return true;
        }
    }

    public function paypal_cancel($payment_code = false, $order_no = false) {
        trigger_error( json_encode($this->input->post()) );

        redirect(configLangURI.'/cart/cancel/');
    }

    public function paypal_receiver($payment_code = false, $order_no = false) {
        trigger_error( json_encode($this->input->post()) );
        /**
            {
                "mc_gross":"9.50",
                "protection_eligibility":"Ineligible",
                "address_status":"unconfirmed",
                "payer_id":"8ZEUFKZYQKL9C",
                "tax":"0.00",
                "address_street":"200491",
                "payment_date":"23:12:24 Jul 12, 2016 PDT",
                "payment_status":"Pending",
                "charset":"utf-8",
                "address_zip":"436",
                "first_name":"",
                "address_country_code":"TW",
                "address_name":" ",
                "notify_version":"3.8",
                "custom":"16070020",
                "payer_status":"unverified",
                "address_country":"Taiwan",
                "address_city":"",
                "quantity":"1",
                "payer_email":"oneericwang@gmail.com",
                "verify_sign":"AFcWxV21C7fd0v3bYYYRCpSSRl31A8AicCQFYXcNu3kxL79ZHzvaZSeH",
                "txn_id":"4FM51086WG4946351",
                "payment_type":"instant",
                "last_name":"",
                "address_state":"",
                "receiver_email":"amystree88@gmail.com",
                "pending_reason":"",
                "txn_type":"web_accept",
                "item_name":"AmysRose  ",
                "mc_currency":"USD",
                "item_number":"",
                "residence_country":"TW",
                "test_ipn":"1",
                "handling_amount":"0.00",
                "transaction_subject":"",
                "payment_gross":"9.50",
                "shipping":"0.00",
                "auth":"Ap6VL7.PEBXgHgS549r-DVBY5RIDqrwdShXu-pZzED9ZmEcXC58bBi8YjmtW7ZO0GmvIRME82UGzZTwv1fTuTBA"
            }
        **/

        if($payment_code === false) return false;
        if($order_no === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('payment_status') === false) return false;
        if($this->input->post('custom') === false) return false;

        $mc_gross = $this->input->post('mc_gross');
        $protection_eligibility = $this->input->post('protection_eligibility');
        $address_status = $this->input->post('address_status');
        $payer_id = $this->input->post('payer_id');
        $tax = $this->input->post('tax');
        $address_street = $this->input->post('address_street');
        $payment_date = $this->input->post('payment_date');
        $payment_status = $this->input->post('payment_status');
        $charset = $this->input->post('charset');
        $address_zip = $this->input->post('address_zip');
        $first_name = $this->input->post('first_name');
        $address_country_code = $this->input->post('address_country_code');
        $address_name = $this->input->post('address_name');
        $notify_version = $this->input->post('notify_version');
        $custom = $this->input->post('custom');
        $payer_status = $this->input->post('payer_status');
        $address_country = $this->input->post('address_country');
        $address_city = $this->input->post('address_city');
        $quantity = $this->input->post('quantity');
        $payer_email = $this->input->post('payer_email');
        $verify_sign = $this->input->post('verify_sign');
        $txn_id = $this->input->post('txn_id');
        $payment_type = $this->input->post('payment_type');
        $last_name = $this->input->post('last_name');
        $address_state = $this->input->post('address_state');
        $receiver_email = $this->input->post('receiver_email');
        $pending_reason = $this->input->post('pending_reason');
        $txn_type = $this->input->post('txn_type');
        $item_name = $this->input->post('item_name');
        $mc_currency = $this->input->post('mc_currency');
        $item_number = $this->input->post('item_number');
        $residence_country = $this->input->post('residence_country');
        $test_ipn = $this->input->post('test_ipn');
        $handling_amount = $this->input->post('handling_amount');
        $transaction_subject = $this->input->post('transaction_subject');
        $payment_gross = $this->input->post('payment_gross');
        $shipping = $this->input->post('shipping');
        $auth = $this->input->post('auth');

        if($payment_code != 'paypal') return false;
        if($custom != $order_no) return false;

        $objOrder = Order::find_by_no($order_no);
        if(!$objOrder) return false;

        if($test_ipn !== false) $objOrder->paysystem_test = $test_ipn;
        if($txn_id !== false) $objOrder->paysystem_no = $txn_id;
        if($txn_type !== false) $objOrder->paysystem_type = $txn_type;
        if($txn_type !== false) $objOrder->paysystem_name = $txn_type;
        if($payment_gross !== false) $objOrder->paysystem_amount = $payment_gross;
        if($auth !== false) $objOrder->paysystem_check = $auth;
        if($payment_status !== false) $objOrder->paysystem_status = $payment_status;
        // $objOrder->paysystem_message = '';
        // $objOrder->paysystem_fee = 0;
        $objOrder->paysystem_return_result .= date('Y-m-d H:i:s')."\n".json_encode($this->input->post())."\n";
        $objOrder->paysystem_at = date('Y-m-d H:i:s');
        $objOrder->save();

        // 付款成功
        if($payment_status == 'Completed') {
            if($objOrder->status_pay != Order::status('paid')) {
                $objOrder->update_attributes(['status_pay' => Order::status('paid')]);

                $objOrderProducts = OrderProduct::all(['conditions' => '`order_id` = '.$objOrder->id, 'order' => '`id` ASC']);
                // foreach((array)$objOrderProducts as $i => $e) {
                //     $objProduct = Product::find_by_id($e->product_id);
                //     if($objProduct) {
                //         $objProduct->preorder = $objProduct->preorder - $e->quantity;
                //         $objProduct->ordered = $objProduct->ordered + $e->quantity;
                //         $objProduct->save();
                //     }
                // }

                $order = $objOrder->to_array();
                $order['products'] = [];
                if($objOrderProducts) $order['products'] = to_array($objOrderProducts);

                $this->load->library('mail_library');
                $this->mail_library->user_order_paid($objOrder->recipient_email, $order);
                $this->mail_library->sys_order_paid($objOrder->recipient_email, $order);
            }

            redirect(configLangURI.'/cart/done/');
            return ture;
        }

        // 付款失敗
        redirect(configLangURI.'/cart/cancel/');
    }

    public function allpay_receiver($payment_code = false, $order_no = false) {
        trigger_error( json_encode($this->input->post()) );

        if($payment_code === false) return false;
        if($order_no === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('MerchantID') === false) return false;
        if($this->input->post('MerchantTradeNo') === false) return false;
        if($this->input->post('RtnCode') === false) return false;
        if($this->input->post('RtnMsg') === false) return false;
        if($this->input->post('TradeNo') === false) return false;
        if($this->input->post('TradeAmt') === false) return false;
        if($this->input->post('PaymentType') === false) return false;
        if($this->input->post('TradeDate') === false) return false;
        if($this->input->post('CheckMacValue') === false) return false;

        $MerchantID = $this->input->post('MerchantID');
        $MerchantTradeNo = $this->input->post('MerchantTradeNo');
        $RtnCode = $this->input->post('RtnCode');
        $RtnMsg = $this->input->post('RtnMsg');
        $TradeNo = $this->input->post('TradeNo');
        $TradeAmt = $this->input->post('TradeAmt');
        $PayAmt = $this->input->post('PayAmt');
        $RedeemAmt = $this->input->post('RedeemAmt');
        $PaymentDate = $this->input->post('PaymentDate');
        $PaymentType = $this->input->post('PaymentType');
        $PaymentTypeChargeFee = $this->input->post('PaymentTypeChargeFee');
        $TradeDate = $this->input->post('TradeDate');
        $SimulatePaid = $this->input->post('SimulatePaid');
        $CheckMacValue = $this->input->post('CheckMacValue');
        $BankCode = $this->input->post('BankCode'); //ATM ex: 004
        $vAccount = $this->input->post('vAccount'); //ATM ex: 3536004219198787
        $PaymentNo = $this->input->post('PaymentNo'); //CVS ex: GW130412257496
        $Barcode1 = $this->input->post('Barcode1'); //BARCODE1 ex: 021030627
        $Barcode2 = $this->input->post('Barcode2'); //BARCODE2 ex: 2470200001841540
        $Barcode3 = $this->input->post('Barcode3'); //BARCODE3 ex: 103027000000100
        $ExpireDate = $this->input->post('ExpireDate'); //ATM ex: 2014/08/07, CVS ex: yyyy/MM/dd HH:mm:ss

        if($payment_code != 'credit' || $payment_code != 'atm' || $payment_code != 'cvs') return false;
        if($MerchantTradeNo != $order_no) return false;

        $this->load->library('allpay_library');

        $objOrder = Order::find_by_no($order_no);
        if(!$objOrder) return false;

        if($SimulatePaid !== false) $objOrder->paysystem_test = $SimulatePaid;
        if($TradeNo !== false) $objOrder->paysystem_no = $TradeNo;
        if($PaymentType !== false) $objOrder->paysystem_type = $PaymentType;
        if($PaymentType !== false) $objOrder->paysystem_name = $this->allpay_library->payment_name($PaymentType);
        if($TradeAmt !== false) $objOrder->paysystem_amount = $TradeAmt;
        if($RtnMsg !== false) $objOrder->paysystem_message = $RtnMsg;
        if($PaymentTypeChargeFee !== false) $objOrder->paysystem_fee = $PaymentTypeChargeFee;
        if($RtnCode !== false) $objOrder->paysystem_status = $RtnCode;
        if($CheckMacValue !== false) $objOrder->paysystem_check = $CheckMacValue;
        if($payment_code == 'atm') { // ATM付款資訊
            if($PaymentType !== false) $objOrder->paysystem_bank_name = $this->allpay_library->payment_name($PaymentType);
            if($BankCode !== false) $objOrder->paysystem_bank_code = $BankCode;
            if($vAccount !== false) $objOrder->paysystem_bank_account = $vAccount;
            if($TradeAmt !== false) $objOrder->paysystem_bank_price = $TradeAmt;
            if($ExpireDate !== false) $objOrder->paysystem_bank_expire_at = $ExpireDate;
        }
        if($payment_code == 'cvs') { // 超商付款資訊
            if($PaymentType !== false) $objOrder->paysystem_cvs_name = $this->allpay_library->payment_name($PaymentType);
            if($PaymentNo !== false) $objOrder->paysystem_cvs_no = $PaymentNo;
            if($Barcode1 !== false) $objOrder->paysystem_cvs_barcode1 = $Barcode1;
            if($Barcode2 !== false) $objOrder->paysystem_cvs_barcode2 = $Barcode2;
            if($Barcode3 !== false) $objOrder->paysystem_cvs_barcode3 = $Barcode3;
            if($ExpireDate !== false) $objOrder->paysystem_cvs_expire_at = $ExpireDate;
        }
        $objOrder->paysystem_return_result .= date('Y-m-d H:i:s')."\n".json_encode($this->input->post())."\n";
        $objOrder->paysystem_at = date('Y-m-d H:i:s');
        $objOrder->save();

        // 付款成功
        if($RtnCode == 1 && $objOrder->status_pay != Order::status('paid')) {
            $objOrder->update_attributes(['status_pay' => Order::status('paid')]);

            $objOrderProducts = OrderProduct::all(['conditions' => '`order_id` = '.$objOrder->id, 'order' => '`id` ASC']);
            // foreach((array)$objOrderProducts as $i => $e) {
            //     $objProduct = Product::find_by_id($e->product_id);
            //     if($objProduct) {
            //         $objProduct->preorder = $objProduct->preorder - $e->quantity;
            //         $objProduct->ordered = $objProduct->ordered + $e->quantity;
            //         $objProduct->save();
            //     }
            // }

            $order = $objOrder->to_array();
            $order['products'] = [];
            if($objOrderProducts) $order['products'] = to_array($objOrderProducts);

            $this->load->library('mail_library');
            $this->mail_library->user_order_paid($objOrder->recipient_email, $order);
            $this->mail_library->sys_order_paid($objOrder->recipient_email, $order);
        }

        echo '1|ok';
        return false;
    }

    public function allpay_client_back($payment_code = false, $order_no = false) {
        if($payment_code === false) return false;
        if($order_no === false) return false;

        redirect(configLangURI.'/cart/done/');
    }
}