<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Image_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $page_id = $this->input->post('page_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $delete = $this->input->post('delete', TRUE);

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        if($page_id)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`page_id` = "'.$page_id.'"';
        }
        if($name)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`name` LIKE "%'.$name.'%"';
        }
        if($delete !== FALSE)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';
        }

        $total = Image::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        if($sort) $query['order'] = $sort;

        $images = array();
        $objImages = Image::all($query);
        if($objImages)
        {
            $images = to_array($objImages, array(
                'methods' => array('_image')
            ));
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'images' => $images,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;
        if($this->input->post('image') === FALSE) return FALSE;

        $page_id = $this->input->post('page_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $description = $this->input->post('description', TRUE);
        $content = $this->input->post('content');
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);
        $enable = $this->input->post('enable', TRUE);
        
        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'page_id',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($page_id)
        {
            $objPage = Page::find_by_id($page_id);
            if($objPage)
            {
                $data['page_id'] = $objPage->id;
                $data['page_name'] = $objPage->name;
                $data['page_code'] = $objPage->code;
            }
        }
        if($name) $data['name'] = $name;
        if($description) $data['description'] = $description;
        if($content) $data['content'] = $content;
        if($url) $data['url'] = $url;
        if($image) $data['image'] = $image;
        if($enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objImage = Image::create($data);
            $image = $objImage->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('image' => $image)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;
        if($this->input->post('image') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $description = $this->input->post('description', TRUE);
        $content = $this->input->post('content');
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objImage = Image::find_by_id($id);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'page_id',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($page_id !== FALSE && $objImage->page_id != $page_id)
        {
            $objPage = Page::find_by_id($page_id);
            if($objPage)
            {
                $data['page_id'] = $objPage->id;
                $data['page_name'] = $objPage->name;
                $data['page_code'] = $objPage->code;
            }
        }
        if($name !== FALSE && $objImage->name != $name) $data['name'] = $name;
        if($description !== FALSE && $objImage->description != $description) $data['description'] = $description;
        if($content !== FALSE && $objImage->content != $content) $data['content'] = $content;
        if($url !== FALSE && $objImage->url != $url) $data['url'] = $url;
        if($image !== FALSE && $objImage->image != $image) $data['image'] = $image;
        if($enable !== FALSE && $objImage->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objImage->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objImage = Image::find_by_id($id);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objImage->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objImage->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objImage = Image::find_by_id($id);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objImage->delete = 0;
        $objImage->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objImage = Image::find_by_id($id);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objImage->delete = 1;
        $objImage->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objImage = Image::find_by_id($id);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objImage->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function move_up() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);

        $objImage = Image::find_by_id_and_delete($id, 0);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        $find = FALSE;
        $objImages = Image::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objImages as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== FALSE)
        {
            $objImage = array_splice($objImages, $find, 1);
            array_splice($objImages, $find-1, 0, $objImage);
        }
        
        $count = count($objImages);
        $max = 999999999;
        foreach((array)$objImages as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $images = to_array($objImages, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('images' => $images)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);

        $objImage = Image::find_by_id_and_delete($id, 0);
        if(!$objImage)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        $objImages = Image::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        $images = to_array($objImages, array('only' => 'id'));

        $find = FALSE;
        $objImages = Image::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objImages as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i < count($objImages)-1) $find = $i;
        }
        if($find !== FALSE)
        {
            $objImage = array_splice($objImages, $find, 1);
            array_splice($objImages, $find+1, 0, $objImage);
        }
        
        $count = count($objImages);
        $max = 999999999;
        foreach((array)$objImages as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $images = to_array($objImages, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('images' => $images)
        ));
        return TRUE;
    }
}