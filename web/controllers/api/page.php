<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function add() {
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;
        if($this->input->post('code') === false) return false;

        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $code = $this->input->post('code', true);
        $title = $this->input->post('title', true);
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $images = $this->input->post('images');
        $image_en = $this->input->post('image_en', true);
        $images_en = $this->input->post('images_en');
        $sort = $this->input->post('sort', true);
        $enable = $this->input->post('enable', true);

        if($code) {
            $objPage = Page::find_by_code($code);
            if($objPage) {
                $this->load->view('api/respone', [
                    'status' => 'code',
                    'message' => 'code 重複',
                    'data' => ''
                ]);
                return false;
            }
        }

        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($code) $data['code'] = $code;
        if($title) $data['title'] = $title;
        if($content) $data['content'] = $content;
        if($content_en) $data['content_en'] = $content_en;
        if($content_cn) $data['content_cn'] = $content_cn;
        if($image) $data['image'] = $image;
        if($images) $data['images'] = json_encode($images);
        if($image_en) $data['image_en'] = $image_en;
        if($images_en) $data['images_en'] = json_encode($images_en);
        if($sort) $data['sort'] = $sort;
        if($enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPage = Page::create($data);
            $page = $objPage->to_array();

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['page' => $page]
            ]);
            return true;
        }
    }

    public function edit() {
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        if($this->input->post('name') === false) return false;
        if($this->input->post('code') === false) return false;

        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $code = $this->input->post('code', true);
        $title = $this->input->post('title', true);
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', true);
        $images = $this->input->post('images');
        $image_en = $this->input->post('image_en', true);
        $images_en = $this->input->post('images_en');
        $sort = $this->input->post('sort', true);
        $enable = $this->input->post('enable', true);

        $objPage = Page::find_by_code($code);
        if($objPage && $objPage->id != $id) {
            $this->load->view('api/respone', [
                'status' => 'code',
                'message' => 'code 重複',
                'data' => []
            ]);
            return false;
        }

        $objPage = Page::find($id);
        if(!$objPage) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($name !== false && $objPage->name != $name) $data['name'] = $name;
        if($name_en !== false && $objPage->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objPage->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($code !== false && $objPage->code != $code) $data['code'] = $code;
        if($title !== false && $objPage->title != $title) $data['title'] = $title;
        if($content !== false && $objPage->content != $content) $data['content'] = $content;
        if($content_en !== false && $objPage->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== false && $objPage->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($image !== false && $objPage->image != $image) $data['image'] = $image;
        if($images !== false && $objPage->images != json_encode($images)) $data['images'] = json_encode($images);
        if($image_en !== false && $objPage->image_en != $image_en) $data['image_en'] = $image_en;
        if($images_en !== false && $objPage->images_en != json_encode($images_en)) $data['images_en'] = json_encode($images_en);
        if($sort !== false && $objPage->sort != $sort) $data['sort'] = $sort;
        if($enable !== false && $objPage->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPage->update_attributes($data);

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '修改成功',
                'data' => []
            ]);
            return true;
        }
    }

    public function del() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objPage = Page::find($id);
        if(!$objPage) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objPage->delete = 1;
        $objPage->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
        return true;
    }

    public function clear() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objPage = Page::find($id);
        if(!$objPage) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objPage->delete();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ]);
        return true;
    }
}