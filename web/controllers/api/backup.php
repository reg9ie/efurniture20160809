<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Backup_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function get() {
        if(User::is_admin() === FALSE) return FALSE;

        $backups = array();
        foreach(new DirectoryIterator(configPathBackup) as $i => $e)
        {
            if($e->getFileName() == '.' || $e->getFileName() == '..') continue;

            $filename = explode('_', $e->getFileName());
            $date = (int)$filename[0];
            $time = isset($filename[1]) ? sprintf('%1$04u', (int)$filename[1]) : '0000';

            $backups[$date.$time] = array(
                'filename' => $e->getFileName(),
                'size' => number_format($e->getSize() / 1024 / 1024, 2).'MB',
                'created_at' => date('Y-m-d H:i:s', $e->getMTime())
            );
        }
        krsort($backups);
        $backups = array_merge($backups);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'backups' => $backups
            )
        ));
        return TRUE;
    }

    public function backup($date = FALSE) {
        if(User::is_admin() === FALSE) return FALSE;
        if($date === FALSE) return FALSE;

        $this->backup_library->backup($date);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '備份成功',
            'data' => ''
        ));
    }

    public function download($filename = FALSE) {
        if(User::is_admin() === FALSE) return FALSE;
        if($filename === FALSE) return FALSE;

        if(!file_exists(configPathBackup.$filename))
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => '檔案不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $this->load->helper('download');

        force_download($filename, file_get_contents(configPathBackup.$filename));
    }

    public function delete() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('filename') === FALSE) return FALSE;

        $filename = $this->input->post('filename', TRUE);
        if(!file_exists(configPathBackup.$filename))
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => '檔案不存在',
                'data' => ''
            ));
            return FALSE;
        }

        unlink(configPathBackup.$filename);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
    }
}