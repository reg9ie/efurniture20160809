<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function get() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('count') === false) return false;
        if($this->input->post('page') === false) return false;
        if($this->input->post('sort') === false) return false;

        $count = $this->input->post('count', true);
        $page = $this->input->post('page', true);
        $sort = $this->input->post('sort', true);
        $ptype_id = $this->input->post('ptype_id', true);
        $pcategory_id = $this->input->post('pcategory_id', true);
        $attribute = $this->input->post('attribute', true);
        $name = $this->input->post('name', true);
        $delete = $this->input->post('delete', true);
        
        $pagination = [
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        ];

        $query['conditions'] = '';
        if($ptype_id) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`ptype_id` = "'.$ptype_id.'"';
        if($pcategory_id) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`pcategory_id` = "'.$pcategory_id.'"';
        if($attribute) {
            if($attribute == 'home') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`home` = 1';
            if($attribute == 'top') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`top` = 1';
            if($attribute == 'best') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`best` = 1';
            if($attribute == 'model_list') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`model_list` = 1';
            if($attribute == 'in_stock') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`in_stock` = 1';
            if($attribute == 'countdown') $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`countdown` = 1';
        }
        if($name) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'(`products`.`name` LIKE "%'.$name.'%" OR `products`.`no` LIKE "%'.$name.'%")';
        if($delete !== false) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`products`.`delete` = "'.$delete.'"';

        $total = Product::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $products = [];
        $objProducts = Product::all($query);
        if($objProducts) $products = to_array($objProducts, ['methods' => ['_image', '_image_thumb']]);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ($count == 'all' ? 1 : (ceil($total / $count) > 0 ? ceil($total / $count) : 1));
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => [
                'products' => $products,
                'pagination' => $pagination
            ]
        ]);
        return true;
    }

    public function related_get() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('count') === false) return false;
        if($this->input->post('page') === false) return false;
        if($this->input->post('sort') === false) return false;

        $count = $this->input->post('count', true);
        $page = $this->input->post('page', true);
        $sort = $this->input->post('sort', true);
        $pcategory_id = $this->input->post('pcategory_id', true);
        $name = $this->input->post('name', true);
        
        $pagination = [
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        ];

        $query['conditions'] = '`delete` = 0';
        if($pcategory_id) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`pcategory_id` = '.$pcategory_id;
        if($name) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'(`name` LIKE "%'.$name.'%" OR `no` LIKE "%'.$name.'%")';

        $total = Product::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $products = [];
        $objProducts = Product::all($query);
        if($objProducts) $products = to_array($objProducts, ['methods' => '_image']);

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => [
                'products' => $products,
                'pagination' => $pagination
            ]
        ]);
        return true;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('pcategory_id') === false) return false;
        if($this->input->post('name') === false) return false;

        $no = $this->input->post('no', true);
        $ptype_id = $this->input->post('ptype_id', true);
        $pcategory_id = $this->input->post('pcategory_id', true);
        $pgroup_id = $this->input->post('pgroup_id', true);
        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $title = $this->input->post('title', true);
        $title_en = $this->input->post('title_en', true);
        $title_cn = $this->input->post('title_cn', true);
        $image = $this->input->post('image', true);
        $images = $this->input->post('images');
        $intro = $this->input->post('intro');
        $intro_en = $this->input->post('intro_en');
        $intro_cn = $this->input->post('intro_cn');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $custom1 = $this->input->post('custom1');
        $custom1_en = $this->input->post('custom1_en');
        $custom1_cn = $this->input->post('custom1_cn');
        $custom2 = $this->input->post('custom2');
        $custom2_en = $this->input->post('custom2_en');
        $custom2_cn = $this->input->post('custom2_cn');
        $custom_image = $this->input->post('custom_image', true);
        $stock = $this->input->post('stock', true);
        $price = $this->input->post('price', true);
        $price_member = $this->input->post('price_member', true);
        $price_vip = $this->input->post('price_vip', true);
        $price_special = $this->input->post('price_special', true);
        $price_special_start = $this->input->post('price_special_start', true);
        $price_special_end = $this->input->post('price_special_end', true);
        $price_usd = $this->input->post('price_usd', true);
        $price_member_usd = $this->input->post('price_member_usd', true);
        $price_vip_usd = $this->input->post('price_vip_usd', true);
        $price_special_usd = $this->input->post('price_special_usd', true);
        $price_special_start_usd = $this->input->post('price_special_start_usd', true);
        $price_special_end_usd = $this->input->post('price_special_end_usd', true);
        $price_cad = $this->input->post('price_cad', true);
        $price_member_cad = $this->input->post('price_member_cad', true);
        $price_vip_cad = $this->input->post('price_vip_cad', true);
        $price_special_cad = $this->input->post('price_special_cad', true);
        $price_special_start_cad = $this->input->post('price_special_start_cad', true);
        $price_special_end_cad = $this->input->post('price_special_end_cad', true);
        $countdown = $this->input->post('countdown', true);
        $in_stock = $this->input->post('in_stock', true);
        $model_list = $this->input->post('model_list', true);
        $new = $this->input->post('new', true);
        $top = $this->input->post('top', true);
        $best = $this->input->post('best', true);
        $hot = $this->input->post('hot', true);
        $home = $this->input->post('home', true);
        $enable = $this->input->post('enable', true);
        $colors = $this->input->post('colors', true);
        $specs = $this->input->post('specs', true);
        $stocks = $this->input->post('stocks', true);
        $relateds = $this->input->post('relateds', true);

        if($no) $data['no'] = $no;
        if($ptype_id) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
            }
        }
        if($pcategory_id) {
            $objPcategory = Pcategory::find_by_id($pcategory_id);
            if($objPcategory) {
                $data['pcategory_id'] = $objPcategory->id;
                $data['pcategory_name'] = $objPcategory->name;
                $data['pcategory_name_en'] = $objPcategory->name_en;
                $data['pcategory_name_cn'] = $objPcategory->name_cn;
            }
        }
        if($pgroup_id) {
            $objPgroup = Pgroup::find_by_id($pgroup_id);
            if($objPgroup) {
                $data['pgroup_id'] = $objPgroup->id;
                $data['pgroup_name'] = $objPgroup->name;
                $data['pgroup_name_en'] = $objPgroup->name_en;
                $data['pgroup_name_cn'] = $objPgroup->name_cn;
            }
        }
        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($title) $data['title'] = $title;
        if($title_en) $data['title_en'] = $title_en;
        if($title_cn) $data['title_cn'] = $title_cn;
        if($image) $data['image'] = $image;
        if($images) $data['images'] = json_encode($images);
        if($intro) $data['intro'] = $intro;
        if($intro_en) $data['intro_en'] = $intro_en;
        if($intro_cn) $data['intro_cn'] = $intro_cn;
        if($description) $data['description'] = $description;
        if($description_en) $data['description_en'] = $description_en;
        if($description_cn) $data['description_cn'] = $description_cn;
        if($content) $data['content'] = $content;
        if($content_en) $data['content_en'] = $content_en;
        if($content_cn) $data['content_cn'] = $content_cn;
        if($custom1) $data['custom1'] = $custom1;
        if($custom1_en) $data['custom1_en'] = $custom1_en;
        if($custom1_cn) $data['custom1_cn'] = $custom1_cn;
        if($custom2) $data['custom2'] = $custom2;
        if($custom2_en) $data['custom2_en'] = $custom2_en;
        if($custom2_cn) $data['custom2_cn'] = $custom2_cn;
        if($custom_image) $data['custom_image'] = $custom_image;
        if($stock) $data['stock'] = $stock;
        if($price) $data['price'] = $price;
        if($price_member) $data['price_member'] = $price_member;
        if($price_vip) $data['price_vip'] = $price_vip;
        if($price_special) $data['price_special'] = $price_special;
        if($price_special_start) $data['price_special_start'] = date('Y-m-d 00:00:00', strtotime($price_special_start));
        if($price_special_end) $data['price_special_end'] = date('Y-m-d 23:59:59', strtotime($price_special_end));
        if($price_usd) $data['price_usd'] = $price_usd;
        if($price_member_usd) $data['price_member_usd'] = $price_member_usd;
        if($price_vip_usd) $data['price_vip_usd'] = $price_vip_usd;
        if($price_special_usd) $data['price_special_usd'] = $price_special_usd;
        if($price_special_start_usd) $data['price_special_start_usd'] = date('Y-m-d 00:00:00', strtotime($price_special_start_usd));
        if($price_special_end_usd) $data['price_special_end_usd'] = date('Y-m-d 23:59:59', strtotime($price_special_end_usd));
        if($price_cad) $data['price_cad'] = $price_cad;
        if($price_member_cad) $data['price_member_cad'] = $price_member_cad;
        if($price_vip_cad) $data['price_vip_cad'] = $price_vip_cad;
        if($price_special_cad) $data['price_special_cad'] = $price_special_cad;
        if($price_special_start_cad) $data['price_special_start_cad'] = date('Y-m-d 00:00:00', strtotime($price_special_start_cad));
        if($price_special_end_cad) $data['price_special_end_cad'] = date('Y-m-d 23:59:59', strtotime($price_special_end_cad));
        if($countdown) $data['countdown'] = $countdown;
        if($in_stock) $data['in_stock'] = $in_stock;
        if($model_list) $data['model_list'] = $model_list;
        if($new) $data['new'] = $new;
        if($top) $data['top'] = $top;
        if($best) $data['best'] = $best;
        if($hot) $data['hot'] = $hot;
        if($home) $data['home'] = $home;
        if($enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objProduct = Product::create($data);
            $product = $objProduct->to_array();

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['product' => $product]
            ]);
            return true;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $no = $this->input->post('no', true);
        $ptype_id = $this->input->post('ptype_id', true);
        $pcategory_id = $this->input->post('pcategory_id', true);
        $pgroup_id = $this->input->post('pgroup_id', true);
        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $title = $this->input->post('title', true);
        $title_en = $this->input->post('title_en', true);
        $title_cn = $this->input->post('title_cn', true);
        $image = $this->input->post('image', true);
        $images = $this->input->post('images');
        $intro = $this->input->post('intro');
        $intro_en = $this->input->post('intro_en');
        $intro_cn = $this->input->post('intro_cn');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $custom1 = $this->input->post('custom1');
        $custom1_en = $this->input->post('custom1_en');
        $custom1_cn = $this->input->post('custom1_cn');
        $custom2 = $this->input->post('custom2');
        $custom2_en = $this->input->post('custom2_en');
        $custom2_cn = $this->input->post('custom2_cn');
        $custom_image = $this->input->post('custom_image');
        $stock = $this->input->post('stock');
        $price = $this->input->post('price', true);
        $price_member = $this->input->post('price_member', true);
        $price_vip = $this->input->post('price_vip', true);
        $price_special = $this->input->post('price_special', true);
        $price_special_start = $this->input->post('price_special_start', true);
        $price_special_end = $this->input->post('price_special_end', true);
        $price_usd = $this->input->post('price_usd', true);
        $price_member_usd = $this->input->post('price_member_usd', true);
        $price_vip_usd = $this->input->post('price_vip_usd', true);
        $price_special_usd = $this->input->post('price_special_usd', true);
        $price_special_start_usd = $this->input->post('price_special_start_usd', true);
        $price_special_end_usd = $this->input->post('price_special_end_usd', true);
        $price_cad = $this->input->post('price_cad', true);
        $price_member_cad = $this->input->post('price_member_cad', true);
        $price_vip_cad = $this->input->post('price_vip_cad', true);
        $price_special_cad = $this->input->post('price_special_cad', true);
        $price_special_start_cad = $this->input->post('price_special_start_cad', true);
        $price_special_end_cad = $this->input->post('price_special_end_cad', true);
        $countdown = $this->input->post('countdown', true);
        $in_stock = $this->input->post('in_stock', true);
        $model_list = $this->input->post('model_list', true);
        $new = $this->input->post('new', true);
        $top = $this->input->post('top', true);
        $best = $this->input->post('best', true);
        $hot = $this->input->post('hot', true);
        $home = $this->input->post('home', true);
        $enable = $this->input->post('enable', true);
        $colors = $this->input->post('colors', true);
        $specs = $this->input->post('specs', true);
        $stocks = $this->input->post('stocks', true);
        $relateds = $this->input->post('relateds', true);

        $objProduct = Product::find_by_id($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($price_special_start) $price_special_start = date('Y-m-d 00:00:00', strtotime($price_special_start));
        if($price_special_end) $price_special_end = date('Y-m-d 23:59:59', strtotime($price_special_end));
        if($price_special_start_usd) $price_special_start_usd = date('Y-m-d 00:00:00', strtotime($price_special_start_usd));
        if($price_special_end_usd) $price_special_end_usd = date('Y-m-d 23:59:59', strtotime($price_special_end_usd));
        if($price_special_start_cad) $price_special_start_cad = date('Y-m-d 00:00:00', strtotime($price_special_start_cad));
        if($price_special_end_cad) $price_special_end_cad = date('Y-m-d 23:59:59', strtotime($price_special_end_cad));

        if($no !== false && $objProduct->no != $no) $data['no'] = $no;
        
        if($ptype_id !== false && $objProduct->ptype_id != $ptype_id) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
            }
        }
        if($pcategory_id !== false && $objProduct->pcategory_id != $pcategory_id) {
            $objPcategory = Pcategory::find_by_id($pcategory_id);
            if($objPcategory) {
                $data['pcategory_id'] = $objPcategory->id;
                $data['pcategory_name'] = $objPcategory->name;
                $data['pcategory_name_en'] = $objPcategory->name_en;
                $data['pcategory_name_cn'] = $objPcategory->name_cn;
            }
        }
        if($pgroup_id !== false && $objProduct->pgroup_id != $pgroup_id) {
            $objPgroup = Pgroup::find_by_id($pgroup_id);
            if($objPgroup) {
                $data['pgroup_id'] = $objPgroup->id;
                $data['pgroup_name'] = $objPgroup->name;
                $data['pgroup_name_en'] = $objPgroup->name_en;
                $data['pgroup_name_cn'] = $objPgroup->name_cn;
            }
        }
        if($name !== false && $objProduct->name != $name) $data['name'] = $name;
        if($name_en !== false && $objProduct->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objProduct->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($title !== false && $objProduct->title != $title) $data['title'] = $title;
        if($title_en !== false && $objProduct->title_en != $title_en) $data['title_en'] = $title_en;
        if($title_cn !== false && $objProduct->title_cn != $title_cn) $data['title_cn'] = $title_cn;
        if($image !== false) $data['image'] = $image;
        if($images !== false) $data['images'] = json_encode($images);
        if($intro !== false && $objProduct->intro != $intro) $data['intro'] = $intro;
        if($intro_en !== false && $objProduct->intro_en != $intro_en) $data['intro_en'] = $intro_en;
        if($intro_cn !== false && $objProduct->intro_cn != $intro_cn) $data['intro_cn'] = $intro_cn;
        if($description !== false && $objProduct->description != $description) $data['description'] = $description;
        if($description_en !== false && $objProduct->description_en != $description_en) $data['description_en'] = $description_en;
        if($description_cn !== false && $objProduct->description_cn != $description_cn) $data['description_cn'] = $description_cn;
        if($content !== false && $objProduct->content != $content) $data['content'] = $content;
        if($content_en !== false && $objProduct->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== false && $objProduct->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($custom1) $data['custom1'] = $custom1;
        if($custom1_en) $data['custom1_en'] = $custom1_en;
        if($custom1_cn) $data['custom1_cn'] = $custom1_cn;
        if($custom2) $data['custom2'] = $custom2;
        if($custom2_en) $data['custom2_en'] = $custom2_en;
        if($custom2_cn) $data['custom2_cn'] = $custom2_cn;
        if($custom_image) $data['custom_image'] = $custom_image;
        if($stock !== false && $objProduct->stock != $stock) $data['stock'] = $stock;
        if($price !== false && $objProduct->price != $price) $data['price'] = $price;
        if($price_member !== false && $objProduct->price_member != $price_member) $data['price_member'] = $price_member;
        if($price_vip !== false && $objProduct->price_vip != $price_vip) $data['price_vip'] = $price_vip;
        if($price_special !== false && $objProduct->price_special != $price_special) $data['price_special'] = $price_special;
        if($price_special_start !== false && $objProduct->price_special_start != $price_special_start) $data['price_special_start'] = $price_special_start;
        if($price_special_end !== false && $objProduct->price_special_end != $price_special_end) $data['price_special_end'] = $price_special_end;
        if($price_usd !== false && $objProduct->price_usd != $price_usd) $data['price_usd'] = $price_usd;
        if($price_member_usd !== false && $objProduct->price_member_usd != $price_member_usd) $data['price_member_usd'] = $price_member_usd;
        if($price_vip_usd !== false && $objProduct->price_vip_usd != $price_vip_usd) $data['price_vip_usd'] = $price_vip_usd;
        if($price_special_usd !== false && $objProduct->price_special_usd != $price_special_usd) $data['price_special_usd'] = $price_special_usd;
        if($price_special_start_usd !== false && $objProduct->price_special_start_usd != $price_special_start_usd) $data['price_special_start_usd'] = $price_special_start_usd;
        if($price_special_end_usd !== false && $objProduct->price_special_end_usd != $price_special_end_usd) $data['price_special_end_usd'] = $price_special_end_usd;
        if($price_cad !== false && $objProduct->price_cad != $price_cad) $data['price_cad'] = $price_cad;
        if($price_member_cad !== false && $objProduct->price_member_cad != $price_member_cad) $data['price_member_cad'] = $price_member_cad;
        if($price_vip_cad !== false && $objProduct->price_vip_cad != $price_vip_cad) $data['price_vip_cad'] = $price_vip_cad;
        if($price_special_cad !== false && $objProduct->price_special_cad != $price_special_cad) $data['price_special_cad'] = $price_special_cad;
        if($price_special_start_cad !== false && $objProduct->price_special_start_cad != $price_special_start_cad) $data['price_special_start_cad'] = $price_special_start_cad;
        if($price_special_end_cad !== false && $objProduct->price_special_end_cad != $price_special_end_cad) $data['price_special_end_cad'] = $price_special_end_cad;
        if($countdown !== false && $objProduct->countdown != $countdown) $data['countdown'] = $countdown;
        if($in_stock !== false && $objProduct->in_stock != $in_stock) $data['in_stock'] = $in_stock;
        if($model_list !== false && $objProduct->model_list != $model_list) $data['model_list'] = $model_list;
        if($new !== false && $objProduct->new != $new) $data['new'] = $new;
        if($top !== false && $objProduct->top != $top) $data['top'] = $top;
        if($best !== false && $objProduct->best != $best) $data['best'] = $best;
        if($hot !== false && $objProduct->hot != $hot) $data['hot'] = $hot;
        if($home !== false && $objProduct->home != $home) $data['home'] = $home;
        if($enable !== false && $objProduct->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objProduct->update_attributes($data);
            $product = $objProduct->to_array();
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return true;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $countdown = $this->input->post('countdown', true);
        $in_stock = $this->input->post('in_stock', true);
        $model_list = $this->input->post('model_list', true);
        $home = $this->input->post('home', true);
        $new = $this->input->post('new', true);
        $top = $this->input->post('top', true);
        $best = $this->input->post('best', true);
        $hot = $this->input->post('hot', true);
        $enable = $this->input->post('enable', true);

        $objProduct = Product::find_by_id($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($countdown !== false && $objProduct->countdown != $countdown) $data['countdown'] = $countdown;
        if($in_stock !== false && $objProduct->in_stock != $in_stock) $data['in_stock'] = $in_stock;
        if($model_list !== false && $objProduct->model_list != $model_list) $data['model_list'] = $model_list;
        if($home !== false && $objProduct->home != $home) $data['home'] = $home;
        if($new !== false && $objProduct->new != $new) $data['new'] = $new;
        if($top !== false && $objProduct->top != $top) $data['top'] = $top;
        if($best !== false && $objProduct->best != $best) $data['best'] = $best;
        if($hot !== false && $objProduct->hot != $hot) $data['hot'] = $hot;
        if($enable !== false && $objProduct->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objProduct->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objProduct = Product::find($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objProduct->delete = 0;
        $objProduct->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objProduct = Product::find($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objProduct->delete = 1;
        $objProduct->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return true;
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $objProduct = Product::find($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objProduct->delete();

        // $this->load->library('image_library');
        
        // $objProductColors = ProductColor::find_all_by_product_id($id);
        // foreach((array)$objProductColors as $i => $o) {
        //     $this->image_library->del('product', $o->image);
        //     $o->delete();
        // }

        // $objProductSpecs = ProductSpec::find_all_by_product_id($id);
        // foreach((array)$objProductSpecs as $i => $o) $o->delete();

        // $objProductStocks = ProductStock::find_all_by_product_id($id);
        // foreach((array)$objProductStocks as $i => $o) $o->delete();

        // $objProductRelateds = ProductRelated::find_all_by_product_id($id);
        // foreach((array)$objProductRelateds as $i => $o) $o->delete();

        // $objProductRelateds = ProductRelated::find_all_by_related_id($id);
        // foreach((array)$objProductRelateds as $i => $o) $o->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function deletes_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        if($id && !is_array($id)) $id = array($id);

        $query['conditions'] = '`id` IN ('.implode(',', $id).')';

        $objProducts = Product::all($query);
        if(!$objProducts) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($objProducts) {
            foreach((array)$objProducts as $i => $p) {
                $p->delete = 1;
                $p->save();
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return true;
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objProduct = Product::find_by_id($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $find = false;
        $objProducts = Product::all(array(
            'conditions' => '`products`.`delete` = 0',
            'order' => '`products`.`sort` ASC'
        ));
        foreach((array)$objProducts as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== false) {
            $objProduct = array_splice($objProducts, $find, 1);
            array_splice($objProducts, $find-1, 0, $objProduct);
        }

        $count = count($objProducts);
        $max = 999999999;
        foreach((array)$objProducts as $i => $o) {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $products = to_array($objProducts, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('products' => $products)
        ));
        return true;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objProduct = Product::find_by_id($id);
        if(!$objProduct) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $find = false;
        $objProducts = Product::all(array(
            'conditions' => '`products`.`delete` = 0',
            'order' => '`products`.`sort` ASC'
        ));
        foreach((array)$objProducts as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i < count($objProducts)-1) $find = $i;
        }
        if($find !== false) {
            $objProduct = array_splice($objProducts, $find, 1);
            array_splice($objProducts, $find+1, 0, $objProduct);
        }

        $count = count($objProducts);
        $max = 999999999;
        foreach((array)$objProducts as $i => $o) {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $products = to_array($objProducts, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('products' => $products)
        ));
        return true;
    }

    public function sortable() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('a') === false) return false;
        if($this->input->post('b') === false) return false;

        $a = $this->input->post('a', true);
        $b = $this->input->post('b', true);

        $objA = Product::find_by_id($a);
        if(!$objA) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objB = Product::find_by_id($b);
        if(!$objB) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        //往前搬移
        if($objA->sort > $objB->sort) {
            $objProducts = Product::all(array(
                'conditions' => '`delete` = 0 AND `sort` BETWEEN '.$objB->sort.' AND '.$objA->sort,
                'order' => '`sort` ASC'
            ));
            
            $sort = $objProducts[0]->sort;

            $objMove = array_pop($objProducts);
            array_unshift($objProducts, $objMove);
        }
         //往後搬移
        else {
            $objProducts = Product::all(array(
                'conditions' => '`delete` = 0 AND `sort` BETWEEN '.$objA->sort.' AND '.$objB->sort,
                'order' => '`sort` ASC'
            ));
            
            $sort = $objProducts[0]->sort;

            $objMove = array_shift($objProducts);
            array_push($objProducts, $objMove);
        }

        foreach($objProducts as $i => $o) {
            $o->sort = $sort;
            $o->save();
            
            $sort++;
        }
        $products = to_array($objProducts);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('products' => $products)
        ));
        return true;
    }
}