<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class File_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('image_library');
        $this->load->library('file_library');
    }

    public function upload() {
        if(User::is_login() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('type') === FALSE) return FALSE;
        if($this->input->post('path') === FALSE) return FALSE;
        if(empty($_FILES)) return FALSE;

        $type = $this->input->post('type', TRUE);
        $path = $this->input->post('path', TRUE);

        if($type == 'image')
        {
            if($_FILES['file']['size'] > configImageMaxSize*1024)
            {
                $this->load->view('api/respone', array(
                    'status' => 'size',
                    'message' => '上傳檔案容量'.$_FILES['file']['size'].' 超過上傳檔案限制'.(configImageMaxSize*1024),
                    'data' => ''
                ));
                return FALSE;
            }

            $filename = $this->image_library->upload($path);
            $thumbname = $this->image_library->thumbnail($path, $filename);

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '上傳成功',
                'data' => array('filename' => $filename, 'thumbname' => $thumbname)
            ));
        }
        elseif($type == 'file')
        {
            if($_FILES['file']['size'] > configFileMaxSize*1024)
            {
                $this->load->view('api/respone', array(
                    'status' => 'size',
                    'message' => '上傳檔案容量'.$_FILES['file']['size'].' 超過上傳檔案限制'.(configFileMaxSize*1024),
                    'data' => ''
                ));
                return FALSE;
            }

            $filename = $this->file_library->upload($path);

            $pathinfo = pathinfo(configPathFile.$path.'/'.$filename);

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '上傳成功',
                'data' => array('filename' => $filename, 'pathinfo' => $pathinfo)
            ));
            return TRUE;
        }

        return TRUE;
    }

    public function uploads() {
        if(User::is_login() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('type') === FALSE) return FALSE;
        if($this->input->post('path') === FALSE) return FALSE;
        if(empty($_FILES)) return FALSE;

        $type = $this->input->post('type', TRUE);
        $path = $this->input->post('path', TRUE);

        if($type == 'image')
        {
            $thumbnames = array();
            $filenames = $this->image_library->uploads($path);
            foreach((array)$filenames as $i => $f)
            {
                $thumbnames[] = $this->image_library->thumbnail($path, $f);
            }

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '上傳成功',
                'data' => array('filenames' => $filenames, 'thumbnames' => $thumbnames)));
            return TRUE;
        }
        elseif($type == 'file')
        {
            $filenames = $this->file_library->uploads($path);
            
            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '上傳成功',
                'data' => array('filenames' => $filenames)));
            return TRUE;
        }
    }

    public function del() {
        if(User::is_login() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('type') === FALSE) return FALSE;
        if($this->input->post('path') === FALSE) return FALSE;
        if($this->input->post('filename') === FALSE) return FALSE;

        $type = $this->input->post('type');
        $path = $this->input->post('path', TRUE);
        $filename = $this->input->post('filename', TRUE);

        if($type == 'image')
        {
            $path = $this->image_library->get_path($path);
            if($path === FALSE)
            {
                $this->load->view('api/respone', array(
                    'status' => 'error',
                    'message' => '路徑錯誤',
                    'data' => ''
                ));
                return FALSE;
            }

            $this->image_library->del($path, $filename);

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '刪除成功',
                'data' => ''));
        }

        return TRUE;
    }
}