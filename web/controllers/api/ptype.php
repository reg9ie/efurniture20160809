<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ptype_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('node');
    }

    public function get() {
        if(User::is_admin() === false) return false;

        $query['order'] = '`sort` ASC';
        
        $ptypes = [];
        $objPtypes = Ptype::all($query);
        if($objPtypes) $ptypes = to_array($objPtypes);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => ['ptypes' => $ptypes]
        ]);
        return true;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;

        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $eng = $this->input->post('eng', true);
        $table = $this->input->post('table');
        $sort = $this->input->post('sort', true);
        $enable = $this->input->post('enable', true);

        if(!$name) {
            $this->load->view('api/respone', [
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ]);
            return false;
        }

        if($name !== false) $data['name'] = $name;
        if($name_en !== false) $data['name_en'] = $name_en;
        if($name_cn !== false) $data['name_cn'] = $name_cn;
        if($eng !== false) $data['eng'] = $eng;
        if($table !== false) $data['table'] = $table;
        if($sort !== false) $data['sort'] = $sort;
        if($enable !== false) $data['enable'] = $enable;

        if(isset($data)) {
            $objPtype = Ptype::create($data);
            $ptype = $objPtype->to_array();
            
            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '修改成功',
                'data' => ['ptype' => $ptype]
            ]);
            return true;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $eng = $this->input->post('eng', true);
        $table = $this->input->post('table');
        $sort = $this->input->post('sort', true);
        $enable = $this->input->post('enable', true);

        if(!$name) {
            $this->load->view('api/respone', [
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ]);
            return false;
        }

        $objPtype = Ptype::find($id);
        if(!$objPtype) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($name !== false && $objPtype->name != $name) $data['name'] = $name;
        if($name_en !== false && $objPtype->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objPtype->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($eng !== false && $objPtype->eng != $eng) $data['eng'] = $eng;
        if($table !== false && $objPtype->table != $table) $data['table'] = $table;
        if($sort !== false && $objPtype->sort != $sort) $data['sort'] = $sort;
        if($enable !== false && $objPtype->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPtype->update_attributes($data);
        }
        
        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => []
        ]);
        return true;
    }

    public function del() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objPtype = Ptype::find($id);
        if(!$objPtype) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objPtype->delete();
        
        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
        return true;
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objPtype = Ptype::find_by_id($id);
        if(!$objPtype) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objPtypes = Ptype::all([
            'order' => '`sort` ASC'
        ]);
        foreach((array)$objPtypes as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== false) {
            $objPtype = array_splice($objPtypes, $find, 1);
            array_splice($objPtypes, $find-1, 0, $objPtype);
        }
        foreach((array)$objPtypes as $i => $o) {
            $o->sort = $i+1;
            $o->save();
        }
        $ptypes = to_array($objPtypes, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['ptypes' => $ptypes]
        ]);
        return true;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objPtype = Ptype::find_by_id($id);
        if(!$objPtype) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objPtypes = Ptype::all([
            'order' => '`sort` ASC'
        ]);
        $ptypes = to_array($objPtypes, ['only' => 'id']);
        foreach((array)$objPtypes as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i < count($objPtypes)-1) $find = $i;
        }
        if($find !== false) {
            $objPtype = array_splice($objPtypes, $find, 1);
            array_splice($objPtypes, $find+1, 0, $objPtype);
        }
        foreach((array)$objPtypes as $i => $o) {
            $o->sort = $i+1;
            $o->save();
        }
        $ptypes = to_array($objPtypes, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['ptypes' => $ptypes]
        ]);
        return true;
    }
}