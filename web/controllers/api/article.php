<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Article_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $atype_id = $this->input->post('atype_id', TRUE);
        $acategory_id = $this->input->post('acategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $delete = $this->input->post('delete', TRUE);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        if($atype_id)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`atype_id` = "'.$atype_id.'"';
        }
        if($acategory_id)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`acategory_id` = "'.$acategory_id.'"';
        }
        if($name)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`name` LIKE "%'.$name.'%"';
        }
        if($delete !== FALSE)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';
        }

        $total = Article::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        if($sort) $query['order'] = $sort;

        $articles = array();
        $objArticles = Article::all($query);
        if($objArticles)
        {
            $articles = to_array($objArticles);
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'articles' => $articles,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('atype_id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $atype_id = $this->input->post('atype_id', TRUE);
        $acategory_id = $this->input->post('acategory_id', TRUE);
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', TRUE);
        $home = $this->input->post('home', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $products = $this->input->post('products', TRUE);

        if($atype_id !== FALSE)
        {
            $objAtype = Atype::find_by_id($atype_id);
            if($objAtype)
            {
                $data['atype_id'] = $objAtype->id;
                $data['atype_name'] = $objAtype->name;
                $data['atype_name_en'] = $objAtype->name_en;
                $data['atype_name_cn'] = $objAtype->name_cn;
                $data['atype_code'] = $objAtype->code;
            }
        }
        if($acategory_id !== FALSE) {
            $objAcategory = Acategory::find_by_id($acategory_id);
            if($objAcategory)
            {
                $data['acategory_id'] = $objAcategory->id;
                $data['acategory_name'] = $objAcategory->name;
                $data['acategory_name_en'] = $objAcategory->name_en;
                $data['acategory_name_cn'] = $objAcategory->name_cn;
            }
        }
        if($name !== FALSE) $data['name'] = $name;
        if($name_en !== FALSE) $data['name_en'] = $name_en;
        if($name_cn !== FALSE) $data['name_cn'] = $name_cn;
        if($content !== FALSE) $data['content'] = $content;
        if($content_en !== FALSE) $data['content_en'] = $content_en;
        if($content_cn !== FALSE) $data['content_cn'] = $content_cn;
        if($image !== FALSE) $data['image'] = $image;
        if($home !== FALSE) $data['home'] = $home;
        if($enable !== FALSE) $data['enable'] = $enable;

        if(isset($data))
        {
            $objArticle = Article::create($data);
            $article = $objArticle->to_array();

            if($products)
            {
                foreach((array)$products as $i => $p)
                {
                    if(isset($p['product_id']) && $p['product_id'] && isset($p['sort']) && $p['sort'])
                    {
                        $objProduct = Product::find_by_id($p['product_id']);
                        if($objProduct)
                        {
                            $objArticleProduct = ArticleProduct::find_by_article_id_and_product_id($id, $p['product_id']);
                            if(!$objArticleProduct)
                            {
                                $objArticleProduct = ArticleProduct::create(array(
                                    'article_id' => $objArticle->id,
                                    'product_id' => $objProduct->id,
                                    'sort' => $p['sort']
                                ));
                                $article['products'][] = $objArticleProduct->to_array();
                            }
                        }
                    }
                }
            }

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('article' => $article)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $atype_id = $this->input->post('atype_id', TRUE);
        $acategory_id = $this->input->post('acategory_id', TRUE);
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $image = $this->input->post('image', TRUE);
        $home = $this->input->post('home', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $products = $this->input->post('products', TRUE);

        $objArticle = Article::find($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($atype_id !== FALSE && $objArticle->atype_id != $atype_id)
        {
            $objAtype = Atype::find_by_id($atype_id);
            if($objAtype)
            {
                $data['atype_id'] = $objAtype->id;
                $data['atype_name'] = $objAtype->name;
                $data['atype_name_en'] = $objAtype->name_en;
                $data['atype_name_cn'] = $objAtype->name_cn;
                $data['atype_code'] = $objAtype->code;
            }
        }
        if($acategory_id !== FALSE && $objArticle->acategory_id != $acategory_id) {
            $objAcategory = Acategory::find_by_id($acategory_id);
            if($objAcategory)
            {
                $data['acategory_id'] = $objAcategory->id;
                $data['acategory_name'] = $objAcategory->name;
                $data['acategory_name_en'] = $objAcategory->name_en;
                $data['acategory_name_cn'] = $objAcategory->name_cn;
            }
        }
        if($name !== FALSE && $objArticle->name != $name) $data['name'] = $name;
        if($name_en !== FALSE && $objArticle->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== FALSE && $objArticle->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($content !== FALSE && $objArticle->content != $content) $data['content'] = $content;
        if($content_en !== FALSE && $objArticle->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== FALSE && $objArticle->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($image !== FALSE && $objArticle->image != $image) $data['image'] = $image;
        if($home !== FALSE && $objArticle->home != $home) $data['home'] = $home;
        if($enable !== FALSE && $objArticle->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objArticle->update_attributes($data);
        }

        if($products !== FALSE)
        {
            $objArticleProducts = ArticleProduct::find_all_by_article_id($id);
            if(!$objArticleProducts)
            {
                $article['products'] = array();
                foreach((array)$products as $i => $p)
                {
                    if(isset($p['product_id']) && $p['product_id'] && isset($p['sort']) && $p['sort'])
                    {
                        $objProduct = Product::find_by_id($p['product_id']);
                        if($objProduct)
                        {
                            $objArticleProduct = ArticleProduct::find_by_article_id_and_product_id($id, $p['product_id']);
                            if(!$objArticleProduct)
                            {
                                $objArticleProduct = ArticleProduct::create(array(
                                    'article_id' => $objArticle->id,
                                    'product_id' => $objProduct->id,
                                    'sort' => $p['sort']
                                ));
                                $article['products'][] = $objArticleProduct->to_array();
                            }
                        }
                    }
                }
            }
            else
            {
                foreach((array)$products as $i => $p)
                {
                    $find = FALSE;
                    foreach((array)$objArticleProducts as $j => $o)
                    {
                        if($find === TRUE || $p === FALSE || isset($p['product_id']) === FALSE || !$p['product_id'] || $o === FALSE) continue;
                        if($p['product_id'] == $o->product_id)
                        {
                            if($p['sort'] != $o->sort)
                            {
                                $o->sort = $p['sort'];
                                $o->save();
                            }
                            $objArticleProducts[$j] = FALSE;
                            $find = TRUE;
                        }
                    }

                    if($find === FALSE)
                    {
                        $objArticleProduct = ArticleProduct::create(array(
                            'article_id' => $id,
                            'product_id' => $p['product_id'],
                            'sort' => $p['sort']
                        ));
                    }
                }

                foreach((array)$objArticleProducts as $i => $o)
                {
                    if($o === FALSE) continue;
                    $o->delete();
                }
            }
        }
        else
        {
            ArticleProduct::delete_all(array('conditions' => 'article_id = '.$id));
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);
        $home = $this->input->post('home', TRUE);

        $objArticle = Article::find_by_id($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objArticle->enable != $enable) $data['enable'] = $enable;
        if($home !== FALSE && $objArticle->home != $home) $data['home'] = $home;

        if(isset($data))
        {
            $objArticle->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objArticle = Article::find($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objArticle->delete = 0;
        $objArticle->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objArticle = Article::find($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objArticle->delete = 1;
        $objArticle->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objArticle = Article::find($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objArticle->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function move_up() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objArticle = Article::find_by_id($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $find = FALSE;
        $objArticles = Article::all(array(
            'conditions' => 'atype_id = '.$objArticle->atype_id.' AND acategory_id = '.$objArticle->acategory_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objArticles as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== FALSE)
        {
            $objArticle = array_splice($objArticles, $find, 1);
            array_splice($objArticles, $find-1, 0, $objArticle);
        }

        $count = count($objArticles);
        $max = 999999999;
        foreach((array)$objArticles as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $articles = to_array($objArticles, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('articles' => $articles)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objArticle = Article::find_by_id($id);
        if(!$objArticle)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $find = FALSE;
        $objArticles = Article::all(array(
            'conditions' => 'atype_id = '.$objArticle->atype_id.' AND acategory_id = '.$objArticle->acategory_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objArticles as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i < count($objArticles)-1) $find = $i;
        }
        if($find !== FALSE)
        {
            $objArticle = array_splice($objArticles, $find, 1);
            array_splice($objArticles, $find+1, 0, $objArticle);
        }

        $count = count($objArticles);
        $max = 999999999;
        foreach((array)$objArticles as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $articles = to_array($objArticles, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('articles' => $articles)
        ));
        return TRUE;
    }
}