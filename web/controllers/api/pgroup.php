<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pgroup_controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('node');
    }

    public function get() {
        if(User::is_admin() === false) return false;

        $ptype_id = $this->input->post('ptype_id', true);

        $query['conditions'] = '';
        if($ptype_id) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`ptype_id` = '.$ptype_id;

        $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`title` != "root" AND `delete` = "0"';
        $query['order'] = '`lft` ASC';
        
        $pgroups = array();
        $objPgroups = Pgroup::all($query);
        if($objPgroups) $pgroups = to_array($objPgroups, ['methods' => ['_tree_name', '_image', '_image_thumb']]);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'pgroups' => $pgroups
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;

        $ptype_id = $this->input->post('ptype_id');
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $alias = $this->input->post('alias');
        $title = $this->input->post('title');
        $title_en = $this->input->post('title_en');
        $title_cn = $this->input->post('title_cn');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $product_size = $this->input->post('product_size');
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if(!$name) {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return false;
        }

        if($ptype_id !== false) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
                $data['ptype_name_en'] = $objPtype->name_en;
                $data['ptype_name_cn'] = $objPtype->name_cn;
                $data['ptype_code'] = $objPtype->code;
            }
        }
        if($name !== false) $data['name'] = $name;
        if($name_en !== false) $data['name_en'] = $name_en;
        if($name_cn !== false) $data['name_cn'] = $name_cn;
        if($alias !== false) $data['alias'] = $alias;
        if($title !== false) $data['title'] = $title;
        if($title_en !== false) $data['title_en'] = $title_en;
        if($title_cn !== false) $data['title_cn'] = $title_cn;
        if($image !== false) $data['image'] = $image;
        if($description !== false) $data['description'] = $description;
        if($description_en !== false) $data['description_en'] = $description_en;
        if($description_cn !== false) $data['description_cn'] = $description_cn;
        if($product_size !== false) $data['product_size'] = $product_size;
        if($parent !== false) $data['parent'] = $parent;
        if($enable !== false) $data['enable'] = $enable;

        if(isset($data)) {
            $objPgroup = Pgroup::create($data);
            
            $this->node->rebuild_tree('pgroups');

            $objPgroup = Pgroup::find($objPgroup->id);
            $objPgroup->tree_name = str_repeat('　　', $objPgroup->level).$objPgroup->name;
            $objPgroup->tree_name_en = str_repeat('　　', $objPgroup->level).$objPgroup->name_en;
            $objPgroup->tree_name_cn = str_repeat('　　', $objPgroup->level).$objPgroup->name_cn;
            $objPgroup->save();
            $pgroup = $objPgroup->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '修改成功',
                'data' => array('pgroup' => $pgroup)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        $ptype_id = $this->input->post('ptype_id');
        $name = $this->input->post('name');
        $name_en = $this->input->post('name_en');
        $name_cn = $this->input->post('name_cn');
        $alias = $this->input->post('alias');
        $title = $this->input->post('title');
        $title_en = $this->input->post('title_en');
        $title_cn = $this->input->post('title_cn');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $description_en = $this->input->post('description_en');
        $description_cn = $this->input->post('description_cn');
        $product_size = $this->input->post('product_size');
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if(!$name) {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return false;
        }

        $objPgroup = Pgroup::find($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($ptype_id !== false && $objPgroup->ptype_id != $ptype_id) {
            $objPtype = Ptype::find_by_id($ptype_id);
            if($objPtype) {
                $data['ptype_id'] = $objPtype->id;
                $data['ptype_name'] = $objPtype->name;
                $data['ptype_name_en'] = $objPtype->name_en;
                $data['ptype_name_cn'] = $objPtype->name_cn;
                $data['ptype_code'] = $objPtype->code;
            }
        }
        if($name !== false && $objPgroup->name != $name) $data['name'] = $name;
        if($name_en !== false && $objPgroup->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objPgroup->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($alias !== false && $objPgroup->alias != $alias) $data['alias'] = $alias;
        if($title !== false && $objPgroup->title != $title) $data['title'] = $title;
        if($title_en !== false && $objPgroup->title_en != $title_en) $data['title_en'] = $title_en;
        if($title_cn !== false && $objPgroup->title_cn != $title_cn) $data['title_cn'] = $title_cn;
        if($image !== false && $objPgroup->image != $image) $data['image'] = $image;
        if($description !== false && $objPgroup->description != $description) $data['description'] = $description;
        if($product_size !== false && $objPgroup->product_size != $product_size) $data['product_size'] = $product_size;
        if($parent !== false && $objPgroup->parent != $parent) $data['parent'] = $parent;
        if($enable !== false && $objPgroup->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPgroup->update_attributes($data);

            $this->node->rebuild_tree('pgroups');

            $objPgroup = Pgroup::find($id);
            $objPgroup->tree_name = str_repeat('　　', $objPgroup->level).$objPgroup->name;
            $objPgroup->save();

            $objSubPcategories = Pgroup::all(array(
                'conditions' => '`lft` > '.$objPgroup->lft.' AND `rght` < '.$objPgroup->rght,
                'order' => '`lft` ASC'
            ));
            foreach ($objSubPcategories as $i => $objSubPgroup) {
                $objSubPgroup->tree_name = str_repeat('　　', $objSubPgroup->level).$objSubPgroup->name;
                $objSubPgroup->save();
            }

            $objProducts = Product::find_all_by_pgroup_id($id);
            foreach($objProducts as $i => $P) {
                $P->pgroup_name = $objPgroup->name;
                $P->pgroup_name_en = $objPgroup->name_en;
                $P->pgroup_name_cn = $objPgroup->name_cn;
                $P->save();
            }

            if(isset($data['name']) || isset($data['name_en']) || isset($data['name_cn'])) {
                Product::update_all([
                    'conditions' => 'pgroup_id = '.$id,
                    'set' => [
                        'Pgroup_name' => $data['name'],
                        'Pgroup_name_en' => $data['name_en'],
                        'Pgroup_name_cn' => $data['name_cn'],
                    ],
                ]);
            }
        }
        
        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array()
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objPgroup = Pgroup::find_by_id($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        if($enable !== false && $objPgroup->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objPgroup->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPgroup = Pgroup::find($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPgroup->delete = 0;
        $objPgroup->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPgroup = Pgroup::find($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPgroup->delete = 1;
        $objPgroup->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);
        
        $objPgroup = Pgroup::find($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objPgroup->delete();

        $this->node->rebuild_tree('pgroups');

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);

        $objPgroup = Pgroup::find_by_id($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objTargetPgroup = Pgroup::find(array(
            'conditions' => '`parent` = '.$objPgroup->parent.' AND `lft` < '.$objPgroup->lft,
            'order' => '`lft` DESC'
        ));
        if($objTargetPgroup) {
            $this->node->update_node($objPgroup->id, 'pgroups', 'before', $objTargetPgroup->id, array('name' => $objPgroup->name));
        }

        $objPgroups = Pgroup::all(array(
            'order' => '`lft` ASC'
        ));
        $pgroups = to_array($objPgroups, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('pgroups' => $pgroups)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', TRUE);

        $objPgroup = Pgroup::find_by_id($id);
        if(!$objPgroup) {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return false;
        }

        $objTargetPgroup = Pgroup::find(array(
            'conditions' => '`parent` = '.$objPgroup->parent.' AND `lft` > '.$objPgroup->lft,
            'order' => '`rght` ASC'
        ));
        if($objTargetPgroup) {
            $this->node->update_node($objPgroup->id, 'pgroups', 'after', $objTargetPgroup->id, array('name' => $objPgroup->name));
        }

        $objPgroups = Pgroup::all(array(
            'order' => '`lft` ASC'
        ));
        $pgroups = to_array($objPgroups, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('pgroups' => $pgroups)
        ));
        return TRUE;
    }
}