<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Store_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;

        $del = $this->input->post('del', TRUE);
        
        $query['conditions'] = '';
        $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = '.($del ? $del : 0);

        $stores = array();
        $objStores = Store::all($query);
        if($objStores) $stores = to_array($objStores);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'stores' => $stores
            )
        ));
        return TRUE;
    }

    public function add() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $name = $this->input->post('name', TRUE);
        $telephone = $this->input->post('telephone', TRUE);
        $time = $this->input->post('time', TRUE);
        $address = $this->input->post('address', TRUE);
        $address_eng = $this->input->post('address_eng', TRUE);
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);

        if($name) $data['name'] = $name;
        if($telephone) $data['telephone'] = $telephone;
        if($time) $data['time'] = $time;
        if($address) $data['address'] = $address;
        if($address_eng) $data['address_eng'] = $address_eng;
        if($url) $data['url'] = $url;
        if($image) $data['image'] = $image;

        if(isset($data))
        {
            $objStore = Store::create($data);
            $store = $objStore->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('store' => $store)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $name = $this->input->post('name', TRUE);
        $telephone = $this->input->post('telephone', TRUE);
        $time = $this->input->post('time', TRUE);
        $address = $this->input->post('address', TRUE);
        $address_eng = $this->input->post('address_eng', TRUE);
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objStore = Store::find($id);
        if(!$objStore)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($name !== FALSE && $objStore->name != $name) $data['name'] = $name;
        if($telephone !== FALSE && $objStore->telephone != $telephone) $data['telephone'] = $telephone;
        if($time !== FALSE && $objStore->time != $time) $data['time'] = $time;
        if($address !== FALSE && $objStore->address != $address) $data['address'] = $address;
        if($address_eng !== FALSE && $objStore->address_eng != $address_eng) $data['address_eng'] = $address_eng;
        if($url !== FALSE && $objStore->url != $url) $data['url'] = $url;
        if($image !== FALSE && $objStore->image != $image) $data['image'] = $image;
        if($enable !== FALSE && $objStore->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objStore->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array()
        ));
        return TRUE;
    }

    public function del() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objStore = Store::find($id);
        if(!$objStore)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objStore->delete = 1;
        $objStore->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function clear() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objStore = Store::find($id);
        if(!$objStore)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objStore->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
        return TRUE;
    }
}