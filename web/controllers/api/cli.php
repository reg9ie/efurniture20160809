<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cli_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function queue() {
        // trigger_error('Queue Enable, '.date('Y-m-d H:i:s'));

        // List
        // crontab -l
        // Edit
        // crontab -e
        // */5 * * * * /usr/bin/php /home/www/neocene/index.php 'api/cli/queue'

        $this->change_mod();
        $this->mail();
        $this->backup();
    }

    public function mail() {
        if(date('H:i') == '00:00') {
            $objMailAccounts = MailAccount::all(['conditions' => '`enable` = 1 AND `delete` = 0']);
            foreach($objMailAccounts as $i => $a) {
                $a->yestoday_times = $a->today_times;
                $a->today_times = 0;
                $a->save();
            }
        }

        $this->load->library('mail_library');

        // for gmail limit
        // $objMailAccounts = MailAccount::all(['conditions' => '`enable` = 1 AND `delete` = 0 AND (`limit` - 1) > `today_times`']);
        // $query['limit'] = count($objMailAccounts);
        $query['limit'] = 5;
        $query['conditions'] = '`sended` = 0 AND `sended_times` = 0';
        $total = MailQueue::count($query);
        
        if($total == 0) {
            $query['conditions'] = '`sended` = 0 AND `sended_times` < 3';
            $total = MailQueue::count($query);

            if($total == 0) return;
        }

        // trigger_error('Queue Start, total: '.$total.PHP_EOL);
        echo 'Send queue start, total: '.$total.PHP_EOL;

        $query['order'] = '`id` ASC';
        $objMails = MailQueue::all($query);
        foreach($objMails as $i => $o) {
            $o->sended_times = $o->sended_times + 1;

            $start = time();
            $result = $this->mail_library->send(0, $o->to_email, $o->reply_email, $o->subject, $o->message);
            // trigger_error($result);
            if($result === true) {
                $o->sended = 1;
                $o->sended_time = time() - $start;
                $o->sended_at = date('Y-m-d H:i:s');
            }
            else {
                $o->error_log = $result;
            }
            $o->save();
        }

        echo 'Send queue end'.PHP_EOL;
        // trigger_error('Queue End');
    }

    public function backup() {
        $objQueues = Queue::all(['conditions' => '`sys` = "backup" AND `runed` = 0']);
        if($objQueues) {
            $this->backup_library->backup(date('Ymd'));

            foreach((array)$objQueues as $i => $o) {
                $o->update_attributes(['runed' => 1]);
            }
        }
    }

    public function change_mod() {
        $ftp_connect = ftp_connect(configFtpServer);
        $ftp_login_result = ftp_login($ftp_connect, configFtpUser, configFtpPassword);
        $dirs = ['backup', 'assets/images', 'assets/files', 'web/views/tw/email', 'web/views/en/email'];
        foreach ($dirs as $i => $dir) {
            ftp_chmod($ftp_connect, 0777, $dir);
        }
        ftp_close($ftp_connect);
    }

    public function create_dir() {
        $ftp_connect = ftp_connect(configFtpServer);
        $ftp_login_result = ftp_login($ftp_connect, configFtpUser, configFtpPassword);
        $dirs = [
            'backup',
            'assets',
            'assets/images',
            'assets/files',
            'web',
            'web/views',
            'web/views/tw',
            'web/views/en',
            'web/views/tw/email',
            'web/views/en/email',
        ];
        foreach ($dirs as $i => $dir) {
            ftp_mkdir($ftp_connect, $dir);
            ftp_chmod($ftp_connect, 0777, $dir);
        }
        ftp_close($ftp_connect);
    }
}