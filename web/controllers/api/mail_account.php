<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_account_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $enable = $this->input->post('enable', TRUE);
        $delete = $this->input->post('delete', TRUE);

        $pagination = array('total' => 0, 'count' => $count, 'pages' => 1, 'page' => $page);

        $query['conditions'] = '';
        if($enable !== FALSE) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`enable` = "'.$enable.'"';
        if($delete !== FALSE) $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';

        $total = MailAccount::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $mail_accounts = array();
        $objMail_accounts = MailAccount::all($query);
        if($objMail_accounts)
        {
            $mail_accounts = to_array($objMail_accounts);
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('mail_accounts' => $mail_accounts, 'pagination' => $pagination)
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('account') === FALSE) return FALSE;
        if($this->input->post('password') === FALSE) return FALSE;
        if($this->input->post('host') === FALSE) return FALSE;
        if($this->input->post('port') === FALSE) return FALSE;
        if($this->input->post('email') === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $account = $this->input->post('account', TRUE);
        $password = $this->input->post('password', TRUE);
        $host = $this->input->post('host', TRUE);
        $port = $this->input->post('port', TRUE);
        $email = $this->input->post('email', TRUE);
        $name = $this->input->post('name', TRUE);
        $cc = $this->input->post('cc', TRUE);
        $bcc = $this->input->post('bcc', TRUE);
        $queue = $this->input->post('queue', TRUE);
        $limit = $this->input->post('limit', TRUE);
        $today_times = $this->input->post('today_times', TRUE);
        $yestoday_times = $this->input->post('yestoday_times', TRUE);
        $total_times = $this->input->post('total_times', TRUE);
        $enable = $this->input->post('enable', TRUE);
        $delete = $this->input->post('delete', TRUE);

        $objMailAccount = MailAccount::find_by_account_and_host($account, $host);
        if($objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'account',
                'message' => 'account 重複',
                'data' => ''
            ));
            return FALSE;
        }

        if($email && valid_email($email) === FALSE)
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => array()
            ));
            return FALSE;
        }

        if($account) $data['account'] = $account;
        if($password) $data['password'] = $password;
        if($host) $data['host'] = $host;
        if($port) $data['port'] = $port;
        if($email) $data['email'] = $email;
        if($name) $data['name'] = $name;
        if($cc) $data['cc'] = $cc;
        if($bcc) $data['bcc'] = $bcc;
        if($queue) $data['queue'] = $queue;
        if($limit) $data['limit'] = $limit;
        if($today_times) $data['today_times'] = $today_times;
        if($yestoday_times) $data['yestoday_times'] = $yestoday_times;
        if($total_times) $data['total_times'] = $total_times;
        if($enable) $data['enable'] = $enable;
        if($delete) $data['delete'] = $delete;
        
        if(isset($data))
        {
            $objMailAccount = MailAccount::create($data);
            $mail_account = $objMailAccount->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('mail_account' => $mail_account)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $account = $this->input->post('account', TRUE);
        $password = $this->input->post('password', TRUE);
        $host = $this->input->post('host', TRUE);
        $port = $this->input->post('port', TRUE);
        $email = $this->input->post('email', TRUE);
        $name = $this->input->post('name', TRUE);
        $cc = $this->input->post('cc', TRUE);
        $bcc = $this->input->post('bcc', TRUE);
        $queue = $this->input->post('queue', TRUE);
        $limit = $this->input->post('limit', TRUE);
        $today_times = $this->input->post('today_times', TRUE);
        $yestoday_times = $this->input->post('yestoday_times', TRUE);
        $total_times = $this->input->post('total_times', TRUE);
        $yestoday_times = $this->input->post('yestoday_times', TRUE);
        $total_times = $this->input->post('total_times', TRUE);
        $enable = $this->input->post('enable', TRUE);
        $delete = $this->input->post('delete', TRUE);
        
        $objMailAccount = MailAccount::find($id);
        if(!$objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($email && !valid_email($email))
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => array()
            ));
            return FALSE;
        }

        if($account !== FALSE && $objMailAccount->account != $account) $data['account'] = $account;
        if($password !== FALSE && $objMailAccount->password != $password) $data['password'] = $password;
        if($host !== FALSE && $objMailAccount->host != $host) $data['host'] = $host;
        if($port !== FALSE && $objMailAccount->port != $port) $data['port'] = $port;
        if($email !== FALSE && $objMailAccount->email != $email) $data['email'] = $email;
        if($name !== FALSE && $objMailAccount->name != $name) $data['name'] = $name;
        if($cc !== FALSE && $objMailAccount->cc != $cc) $data['cc'] = $cc;
        if($bcc !== FALSE && $objMailAccount->bcc != $bcc) $data['bcc'] = $bcc;
        if($queue !== FALSE && $objMailAccount->queue != $queue) $data['queue'] = $queue;
        if($limit !== FALSE && $objMailAccount->limit != $limit) $data['limit'] = $limit;
        if($today_times !== FALSE && $objMailAccount->today_times != $today_times) $data['today_times'] = $today_times;
        if($yestoday_times !== FALSE && $objMailAccount->yestoday_times != $yestoday_times) $data['yestoday_times'] = $yestoday_times;
        if($total_times !== FALSE && $objMailAccount->total_times != $total_times) $data['total_times'] = $total_times;
        if($enable !== FALSE && $objMailAccount->enable != $enable) $data['enable'] = $enable;
        if($delete !== FALSE && $objMailAccount->delete != $delete) $data['delete'] = $delete;

        if(isset($data))
        {
            $objMailAccount->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objMailAccount = MailAccount::find($id);
        if(!$objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objMailAccount->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objMailAccount->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objMailAccount = MailAccount::find($id);
        if(!$objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objMailAccount->delete = 0;
        $objMailAccount->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objMailAccount = MailAccount::find($id);
        if(!$objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objMailAccount->delete = 1;
        $objMailAccount->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objMailAccount = MailAccount::find($id);
        if(!$objMailAccount)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objMailAccount->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
        return TRUE;
    }
}