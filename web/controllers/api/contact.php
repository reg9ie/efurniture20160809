<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function add() {
        if($this->input->post() === false) return false;
        if($this->input->post('title') === false) return false;
        if($this->input->post('content') === false) return false;
        if($this->input->post('name') === false) return false;

        $title = $this->input->post('title', true);
        $content = $this->input->post('content');
        $name = $this->input->post('name', true);
        $gender = $this->input->post('gender', true);
        $email = $this->input->post('email', true);
        $cellphone = $this->input->post('cellphone', true);
        $telephone = $this->input->post('telephone', true);
        $country = $this->input->post('country', true);
        $city = $this->input->post('city', true);
        $area = $this->input->post('area', true);
        $address = $this->input->post('address', true);

        if($email && valid_email($email) === false) {
            $this->load->view('api/respone', [
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => []
            ]);
            return false;
        }

        if($title) $data['title'] = $title;
        if($content) $data['content'] = $content;
        if($name) $data['name'] = $name;
        if($gender) $data['gender'] = $gender;
        if($email) $data['email'] = $email;
        if($cellphone) $data['cellphone'] = $cellphone;
        if($telephone) $data['telephone'] = $telephone;
        if($country) $data['country'] = $country;
        if($city) $data['city'] = $city;
        if($area) $data['area'] = $area;
        if($address) $data['address'] = $address;

        if(isset($data)) {
            $objContact = Contact::create($data);
            $contact = $objContact->to_array();

            // $this->load->library('mail_library');
            // $this->mail_library->sys_contact($email, $title, $content, $name, $telephone);

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['contact' => $contact]
            ]);
            return true;
        }
    }

    public function reply() {
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        if($this->input->post('reply_content') === false) return false;

        $id = $this->input->post('id', true);
        $reply_content = $this->input->post('reply_content', true);

        $objContact = Contact::find($id);
        if(!$objContact) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objContact->reply_content = $reply_content;
        $objContact->save();

        // $this->load->library('mail_library');
        // $this->mail_library->sys_contact_reply($objContact->email, $objContact->title, $objContact->content, $objContact->name, $objContact->telephone, $objContact->reply_content);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '回應成功',
            'data' => ['contact' => $objContact->to_array()]
        ]);
        return true;
    }

    public function del() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objContact = Contact::find($id);
        if(!$objContact) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objContact->delete = 1;
        $objContact->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
        return true;
    }

    public function clear() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objContact = Contact::find($id);
        if(!$objContact) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objContact->delete();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ]);
        return true;
    }
}