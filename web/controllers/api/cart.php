<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function add() {
        if($this->input->post() === false) return false;
        if($this->input->post('product_id') === false) return false;
        if($this->input->post('quantity') === false) return false;

        $product_id = $this->input->post('product_id', true);
        $quantity = $this->input->post('quantity', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->add($product_id, $quantity);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '新增成功',
            'data' => ['cart' => $cart]
        ]);
        return true;
    }

    public function edit() {
        if($this->input->post() === false) return false;
        if($this->input->post('product_id') === false) return false;
        if($this->input->post('quantity') === false) return false;

        $product_id = $this->input->post('product_id', true);
        $quantity = $this->input->post('quantity', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->edit($product_id, $quantity);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '編輯成功',
            'data' => ['cart' => $cart]
        ]);
        return true;
    }

    public function del() {
        if($this->input->post() === false) return false;
        if($this->input->post('product_id') === false) return false;

        $product_id = $this->input->post('product_id', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->del($product_id);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ['cart' => $cart]
        ]);
        return true;
    }

    public function payment() {
        if($this->input->post() === false) return false;
        if($this->input->post('payment_id') === false) return false;

        $payment_id = $this->input->post('payment_id', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->payment($payment_id);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '編輯成功',
            'data' => ['cart' => $cart]
        ]);
        return true;
    }

    public function shipping() {
        if($this->input->post() === false) return false;
        if($this->input->post('shipping_id') === false) return false;

        $shipping_id = $this->input->post('shipping_id', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->shipping($shipping_id);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '編輯成功',
            'data' => ['cart' => $cart]
        ]);
        return true;
    }

    public function point() {
        if(User::get() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('_input_count') === false) return false;
        if($this->input->post('point_use') === false) return false;

        $_input_count = $this->input->post('_input_count', true);
        $point_use = $this->input->post('point_use', true);

        $this->load->library('cart_library');
        $cart = $this->cart_library->point($point_use);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '計算成功',
            'data' => [
                '_input_count' => $_input_count,
                'cart' => $cart
            ]
        ]);
        return true;
    }

    public function user() {
        $objUser = User::get();
        if($objUser) {
            $user['name'] = $objUser->name;
            $user['gender'] = $objUser->gender;
            $user['cellphone'] = $objUser->cellphone;
            $user['telephone'] = $objUser->telephone;
            $user['zip'] = $objUser->zip;
            $user['address'] = $objUser->address;
            $user['email'] = $objUser->email;
            $user['cvs_type'] = $objUser->cvs_type;
            $user['cvs_code'] = $objUser->cvs_code;
            $user['cvs_name'] = $objUser->cvs_name;
            $user['cvs_telephone'] = $objUser->cvs_telephone;
            $user['cvs_zip'] = $objUser->cvs_zip;
            $user['cvs_address'] = $objUser->cvs_address;

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '取得成功',
                'data' => ['user' => $user]
            ]);
            return true;
        }
    }

    public function recipient() {
        if($this->input->post() === false) return false;
        if($this->input->post('recipient_name') === false) return false;
        if($this->input->post('recipient_cellphone') === false) return false;
        if($this->input->post('recipient_address') === false) return false;
        if($this->input->post('recipient_note') === false) return false;

        $recipient_name = $this->input->post('recipient_name', true);
        $recipient_gender = $this->input->post('recipient_gender', true);
        $recipient_cellphone = $this->input->post('recipient_cellphone', true);
        $recipient_telephone = $this->input->post('recipient_telephone', true);
        $recipient_zip = $this->input->post('recipient_zip', true);
        $recipient_address = $this->input->post('recipient_address', true);
        $recipient_email = $this->input->post('recipient_email', true);
        $recipient_note = $this->input->post('recipient_note', true);

        $cart = $this->session->userdata('cart');

        if($recipient_name) {
            $cart['recipient_name'] = $recipient_name;
            $cart['buyer_name'] = $recipient_name;
        }
        if($recipient_gender) {
            $cart['recipient_gender'] = $recipient_gender;
            $cart['buyer_gender'] = $recipient_gender;
        }
        if($recipient_cellphone) {
            $cart['recipient_cellphone'] = $recipient_cellphone;
            $cart['buyer_cellphone'] = $recipient_cellphone;
        }
        if($recipient_telephone) {
            $cart['recipient_telephone'] = $recipient_telephone;
            $cart['buyer_telephone'] = $recipient_telephone;
        }
        if($recipient_zip) {
            $cart['recipient_zip'] = $recipient_zip;
            $cart['buyer_zip'] = $recipient_zip;
        }
        if($recipient_address) {
            $cart['recipient_address'] = $recipient_address;
            $cart['buyer_address'] = $recipient_address;
        }
        if($recipient_email) {
            $cart['recipient_email'] = $recipient_email;
            $cart['buyer_email'] = $recipient_email;
        }
        if($recipient_note) $cart['recipient_note'] = $recipient_note;

        $this->session->set_userdata(['cart' => $cart]);
    }

    public function cvs() {
        // $objconfig = Config::find_by_name('ezship_account');
        // $suID = $objconfig ? $objconfig->name : '';
        // $processID = '0';
        // $stCate = ''; //TFM-全家超商, TLF-萊爾富超商, TOK-OK超商, 空白-全部超商
        // $stCode = '';
        // $rtURL = site_url('api/cart/ezship');
        // $webPara = '';
        // redirect('http://map.ezship.com.tw/ezship_map_web.jsp?suID='.$suID.'&processID='.$processID.'&stCate='.$stCate.'&stCode='.$stCode.'&rtURL='.$rtURL.'&webPara='.$webPara);

        // $this->load->library('ezship_library');
        // $this->ezship_library->map_redirect();
    }

    public function ezship() {
        // if($this->input->post() === false) return false;
        // if($this->input->post('stCode') === false) return false;
        // if($this->input->post('stName') === false) return false;
        // if($this->input->post('stTel') === false) return false;
        // if($this->input->post('stAddr') === false) return false;

        // trigger_error( json_decode($this->input->post()) );

        // $stCate = $this->input->post('stCate');
        // $stCode = $this->input->post('stCode');
        // $stName = $this->input->post('stName');
        // $stTel = $this->input->post('stTel');
        // $stAddr = $this->input->post('stAddr');

        // $this->load->library('cart_library');
        // $this->cart_library->cvs($stCate, $stCode, $stName, $stTel, '', $stAddr);

        // redirect(configLangURI.'/cart/step03');
    }

    public function allpay_emap_receiver() {
        // trigger_error( json_encode($this->input->post()) );

        if($this->input->post() === false) return false;
        if($this->input->post('MerchantID') === false) return false;
        if($this->input->post('MerchantTradeNo') === false) return false;
        if($this->input->post('LogisticsSubType') === false) return false;
        if($this->input->post('CVSStoreID') === false) return false;
        if($this->input->post('CVSStoreName') === false) return false;
        if($this->input->post('CVSAddress') === false) return false;
        if($this->input->post('CVSTelephone') === false) return false;

        $MerchantID = $this->input->post('MerchantID');
        $MerchantTradeNo = $this->input->post('MerchantTradeNo');
        $LogisticsSubType = $this->input->post('LogisticsSubType');
        $CVSStoreID = $this->input->post('CVSStoreID');
        $CVSStoreName = $this->input->post('CVSStoreName');
        $CVSAddress = $this->input->post('CVSAddress');
        $CVSTelephone = $this->input->post('CVSTelephone');

        $cvs_type = '';
        if($LogisticsSubType == 'FAMI') $cvs_type = 'family';
        elseif($LogisticsSubType == 'UNIMART') $cvs_type = '711';

        $this->load->library('cart_library');
        $this->cart_library->cvs($cvs_type, $CVSStoreID, $CVSStoreName, $CVSTelephone, '', $CVSAddress);

        redirect(configLangURI.'/cart/payment');
    }
}