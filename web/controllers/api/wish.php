<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Wish_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function add() {
        if($this->input->post() === false) return false;
        if($this->input->post('product_id') === false) return false;

        $product_id = $this->input->post('product_id', true);

        $objProduct = Product::find_by_id_and_enable_and_delete($product_id, 1, 0);
        if(!$objProduct) return false;

        $objPcategory = Pcategory::find_by_id_and_enable_and_delete($objProduct->pcategory_id, 1, 0);
        if(!$objPcategory) return false;

        $objParentPcategory = Pcategory::find_by_id_and_enable_and_delete($objPcategory->parent, 1, 0);
        if(!$objParentPcategory) return false;

        $find = false;
        $wishes = json_decode($this->input->cookie('wishes'), true);
        foreach ($wishes as $i => $w) {
            if($find) continue;
            if($w['product_id'] == $product_id) $find = true;
        }
        if($find === false) {
            $wishes[] = [
                'ptype_id' => $objProduct->ptype_id,
                'ptype_name' => $objProduct->ptype_name,
                'product_id' => $objProduct->id,
                'product_no' => $objProduct->no,
                'product_name' => $objProduct->name,
                'product_title' => $objProduct->title,
                'product_image' => $objProduct->_image(),
                'pcategory_id' => $objProduct->pcategory_id,
                'pcategory_name' => $objProduct->pcategory_name,
                'pcategory_parent' => $objPcategory->parent,
                'pcategory_parent_id' => $objParentPcategory->id,
                'pcategory_parent_name' => $objParentPcategory->name,
            ];
            $this->input->set_cookie([
                'name'   => 'wishes',
                'value'  => json_encode($wishes),
                'expire' => 86400*30,
            ]);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '新增成功',
            'data' => ['wishes' => $wishes]
        ]);
        return true;
    }

    public function del() {
        if($this->input->post('product_id') === false) return false;

        $product_id = $this->input->post('product_id', true);

        $objProduct = Product::find_by_id_and_enable_and_delete($product_id, 1, 0);
        if(!$objProduct) return false;

        $wishes = [];
        if($this->input->cookie('wishes')) $wishes = json_decode($this->input->cookie('wishes'), true);
        foreach ($wishes as $i => $w) {
            if($w['product_id'] == $product_id) unset($wishes[$i]);
        }
        $wishes = array_values($wishes);
        
        $this->input->set_cookie([
            'name'   => 'wishes',
            'value'  => json_encode($wishes),
            'expire' => 86400*30,
        ]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ['wishes' => $wishes]
        ]);
        return true;
    }
}