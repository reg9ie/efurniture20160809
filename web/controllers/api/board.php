<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Board_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $page_id = $this->input->post('page_id', TRUE);
        $bcategory_id = $this->input->post('bcategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $delete = $this->input->post('delete', TRUE);

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id not exist',
                'data' => array()
            ));
            return FALSE;
        }

        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        if($page_id)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`page_id` = "'.$page_id.'"';
        }
        if($name)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`name` LIKE "%'.$name.'%"';
        }
        if($delete !== FALSE)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = "'.$delete.'"';
        }

        $total = Board::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        if($sort) $query['order'] = $sort;

        $boards = array();
        $objBoards = Board::all($query);
        if($objBoards)
        {
            $boards = to_array($objBoards, array(
                'methods' => array('_image')
            ));
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'boards' => $boards,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;
        if($this->input->post('bcategory_id') === FALSE) return FALSE;
        if($this->input->post('image') === FALSE) return FALSE;

        $page_id = $this->input->post('page_id', TRUE);
        $bcategory_id = $this->input->post('bcategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $description = $this->input->post('description', TRUE);
        $description_en = $this->input->post('description_en', TRUE);
        $description_cn = $this->input->post('description_cn', TRUE);
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);
        $enable = $this->input->post('enable', TRUE);
        
        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'page_id',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($page_id)
        {
            $objPage = Page::find_by_id($page_id);
            if($objPage)
            {
                $data['page_id'] = $objPage->id;
                $data['page_name'] = $objPage->name;
                $data['page_code'] = $objPage->code;
            }
        }
        if($bcategory_id)
        {
            $objBcategory = Bcategory::find_by_id($bcategory_id);
            if($objBcategory)
            {
                $data['bcategory_id'] = $objBcategory->id;
                $data['bcategory_name'] = $objBcategory->name;
                $data['bcategory_name_en'] = $objBcategory->name_en;
                $data['bcategory_name_cn'] = $objBcategory->name_cn;
            }
        }
        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($description) $data['description'] = $description;
        if($content) $data['content'] = $content;
        if($url) $data['url'] = $url;
        if($image) $data['image'] = $image;
        if($enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBoard = Board::create($data);
            $board = $objBoard->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('board' => $board)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;
        if($this->input->post('bcategory_id') === FALSE) return FALSE;
        if($this->input->post('image') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);
        $bcategory_id = $this->input->post('bcategory_id', TRUE);
        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $description_en = $this->input->post('description_en', TRUE);
        $description_cn = $this->input->post('description_cn', TRUE);
        $content = $this->input->post('content');
        $content_en = $this->input->post('content_en');
        $content_cn = $this->input->post('content_cn');
        $url = $this->input->post('url', TRUE);
        $image = $this->input->post('image', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objBoard = Board::find_by_id($id);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'page_id',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        if($page_id !== FALSE && $objBoard->page_id != $page_id)
        {
            $objPage = Page::find_by_id($page_id);
            if($objPage)
            {
                $data['page_id'] = $objPage->id;
                $data['page_name'] = $objPage->name;
                $data['page_name_en'] = $objPage->name_en;
                $data['page_name_cn'] = $objPage->name_cn;
                $data['page_code'] = $objPage->code;
            }
        }
        if($bcategory_id !== FALSE && $objBoard->bcategory_id != $bcategory_id)
        {
            $objBcategory = Bcategory::find_by_id($bcategory_id);
            if($objBcategory)
            {
                $data['bcategory_id'] = $objBcategory->id;
                $data['bcategory_name'] = $objBcategory->name;
                $data['bcategory_name_en'] = $objBcategory->name_en;
                $data['bcategory_name_cn'] = $objBcategory->name_cn;
            }
        }
        if($name !== FALSE && $objBoard->name != $name) $data['name'] = $name;
        if($name_en !== FALSE && $objBoard->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== FALSE && $objBoard->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($description !== FALSE && $objBoard->description != $description) $data['description'] = $description;
        if($description_en !== FALSE && $objBoard->description_en != $description_en) $data['description_en'] = $description_en;
        if($description_cn !== FALSE && $objBoard->description_cn != $description_cn) $data['description_cn'] = $description_cn;
        if($content !== FALSE && $objBoard->content != $content) $data['content'] = $content;
        if($content_en !== FALSE && $objBoard->content_en != $content_en) $data['content_en'] = $content_en;
        if($content_cn !== FALSE && $objBoard->content_cn != $content_cn) $data['content_cn'] = $content_cn;
        if($url !== FALSE && $objBoard->url != $url) $data['url'] = $url;
        if($image !== FALSE && $objBoard->image != $image) $data['image'] = $image;
        if($enable !== FALSE && $objBoard->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBoard->update_attributes($data);
            $board = $objBoard->to_array();
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objBoard = Board::find_by_id($id);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objBoard->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBoard->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objBoard = Board::find_by_id($id);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBoard->delete = 0;
        $objBoard->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objBoard = Board::find_by_id($id);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBoard->delete = 1;
        $objBoard->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objBoard = Board::find_by_id($id);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBoard->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function move_up() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);

        $objBoard = Board::find_by_id_and_delete($id, 0);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        $find = FALSE;
        $objBoards = Board::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objBoards as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== FALSE)
        {
            $objBoard = array_splice($objBoards, $find, 1);
            array_splice($objBoards, $find-1, 0, $objBoard);
        }
        
        $count = count($objBoards);
        $max = 999999999;
        foreach((array)$objBoards as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $boards = to_array($objBoards, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('boards' => $boards)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;
        if($this->input->post('page_id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $page_id = $this->input->post('page_id', TRUE);

        $objBoard = Board::find_by_id_and_delete($id, 0);
        if(!$objBoard)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPage = Page::find_by_id($page_id);
        if(!$objPage)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'page_id 不存在',
                'data' => array()
            ));
            return FALSE;
        }

        $objBoards = Board::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        $boards = to_array($objBoards, array('only' => 'id'));

        $find = FALSE;
        $objBoards = Board::all(array(
            'conditions' => 'page_id = '.$page_id.' AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        foreach((array)$objBoards as $i => $o)
        {
            if($find !== FALSE) continue;
            if($o->id == $id && $i < count($objBoards)-1) $find = $i;
        }
        if($find !== FALSE)
        {
            $objBoard = array_splice($objBoards, $find, 1);
            array_splice($objBoards, $find+1, 0, $objBoard);
        }
        
        $count = count($objBoards);
        $max = 999999999;
        foreach((array)$objBoards as $i => $o)
        {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $boards = to_array($objBoards, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('boards' => $boards)
        ));
        return TRUE;
    }
}