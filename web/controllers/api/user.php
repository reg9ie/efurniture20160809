<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('type') === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $type = $this->input->post('type', TRUE);
        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        $text = $this->input->post('text', TRUE);
        $birthday = $this->input->post('birthday', TRUE);
        $start = $this->input->post('start', TRUE);
        $end = $this->input->post('end', TRUE);

        $pagination = array('total' => 0, 'count' => $count, 'pages' => 1, 'page' => $page);

        switch($type)
        {
            default:
            case '':
                $query['conditions'] = '`delete` = 0';
                break;
            case 'member_vip_black':
                $query['conditions'] = '`delete` = 0 AND `role_id` = 0';
                break;
            case 'member':
                $query['conditions'] = '`delete` = 0 AND `role_id` = 0 AND `vip` = 0';
                break;
            case 'vip':
                $query['conditions'] = '`delete` = 0 AND `role_id` = 0 AND `vip` = 1';
                break;
            case 'black':
                $query['conditions'] = '`delete` = 0 AND `role_id` = 0 AND `black` = 1';
                break;
            case 'admin':
                $query['conditions'] = '`delete` = 0 AND `role_id` != 0';
                break;
            case 'recycle':
                $query['conditions'] = '`delete` = 1';
                break;
            case 'recycle_admin':
                $query['conditions'] = '`delete` = 1 AND `role_id` != 0';
                break;
        }

        if($text)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '(account LIKE "%'.$text.'%" OR '.
                                    'email LIKE "%'.$text.'%" OR '.
                                    'telephone LIKE "%'.$text.'%" OR '.
                                    'cellphone LIKE "%'.$text.'%" OR '.
                                    'name LIKE "%'.$text.'%")';
        }

        if($birthday)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '`birthday` LIKE "'.$birthday.'"';
        }

        if($start && $end)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '`created_at` BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        }
        elseif($start)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '`created_at` >= "'.$start.' 00:00:00"';
        }
        elseif($end)
        {
            $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').
                                   '`created_at` <= "'.$end.' 23:59:59"';
        }

        $total = User::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $users = array();
        $objUsers = User::all($query);
        if($objUsers)
        {
            $users = to_array($objUsers,array(
                'only' => array('id', 'account', 'email', 'name', 'cellphone', 'vip', 'vip_start_at', 'vip_end_at', 'black', 'point', 'role_id', 'order_times', 'order_price', 'enable', 'created_at'),
                'methods' => array('_role_name', '_birthday', '_vip_start_at', '_vip_end_at')
            ));
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ($count == 'all' ? 1 : (ceil($total / $count) > 0 ? ceil($total / $count) : 1));
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array('users' => $users, 'pagination' => $pagination)
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('account') === FALSE) return FALSE;
        if($this->input->post('password') === FALSE) return FALSE;

        $account = $this->input->post('account', TRUE);
        $password = $this->input->post('password', TRUE);
        $email = $this->input->post('email', TRUE);
        $nickname = $this->input->post('nickname', TRUE);
        $avatar = $this->input->post('avatar', TRUE);
        $firstname = $this->input->post('firstname', TRUE);
        $middlename = $this->input->post('middlename', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $name = $this->input->post('name', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $birthday = $this->input->post('birthday', TRUE);
        $cellphone = $this->input->post('cellphone', TRUE);
        $telephone = $this->input->post('telephone', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $country = $this->input->post('country', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
        $city = $this->input->post('city', TRUE);
        $area_id = $this->input->post('area_id', TRUE);
        $area = $this->input->post('area', TRUE);
        $address = $this->input->post('address', TRUE);
        $blog = $this->input->post('blog', TRUE);
        $no = $this->input->post('no', TRUE);
        $vip = $this->input->post('vip', TRUE);
        $vip_start_at = $this->input->post('vip_start_at', TRUE);
        $vip_end_at = $this->input->post('vip_end_at', TRUE);
        $black = $this->input->post('black', TRUE);
        $role_id = $this->input->post('role_id', TRUE);
        $note = $this->input->post('note', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objUser = User::find_by_account($account);
        if($objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'account',
                'message' => 'account 重複',
                'data' => ''
            ));
            return FALSE;
        }

        if($password && strlen($password) < 8)
        {
            $this->load->view('api/respone', array(
                'status' => 'password',
                'message' => 'password 小於8位數',
                'data' => ''
            ));
            return FALSE;
        }

        if($email && valid_email($email) === FALSE)
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => array()
            ));
            return FALSE;
        }

        if($account) $data['account'] = $account;
        if($password) $data['password'] = $password;
        if($email) $data['email'] = $email;
        if($nickname) $data['nickname'] = $nickname;
        if($avatar) $data['avatar'] = $avatar;
        if($firstname) $data['firstname'] = $firstname;
        if($middlename) $data['middlename'] = $middlename;
        if($lastname) $data['lastname'] = $lastname;
        if($name) $data['name'] = $name;
        if($gender) $data['gender'] = $gender;
        if($birthday) $data['birthday'] = $birthday;
        if($cellphone) $data['cellphone'] = $cellphone;
        if($telephone) $data['telephone'] = $telephone;
        if($country_id) $data['country_id'] = $country_id;
        if($country) $data['country'] = $country;
        if($city_id) $data['city_id'] = $city_id;
        if($city) $data['city'] = $city;
        if($area_id) $data['area_id'] = $area_id;
        if($area) $data['area'] = $area;
        if($address) $data['address'] = $address;
        if($blog) $data['blog'] = $blog;
        if($no) $data['no'] = $no;
        if($vip) $data['vip'] = $vip;
        if($vip_start_at) $data['vip_start_at'] = $vip_start_at;
        if($vip_end_at) $data['vip_end_at'] = $vip_end_at;
        if($black) $data['black'] = $black;
        if($role_id) $data['role_id'] = $role_id;
        if($note) $data['note'] = $note;
        if($enable) $data['enable'] = $enable;
        
        if($country_id)
        {
            $objCountry = Country::find($country_id);
            if($objCountry) $data['country'] = $objCountry->name;
        }

        if($city_id)
        {
            $objCity = City::find($city_id);
            if($objCity) $data['city'] = $objCity->name;
        }
        
        if($area_id)
        {
            $objArea = Area::find($area_id);
            if($objArea) $data['area'] = $objArea->name;
        }

        if(isset($data))
        {
            $objUser = User::create($data);
            $user = $objUser->to_array(array('except' => 'password'));

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('user' => $user)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $account = $this->input->post('account', TRUE);
        $password = $this->input->post('password', TRUE);
        $email = $this->input->post('email', TRUE);
        $nickname = $this->input->post('nickname', TRUE);
        $avatar = $this->input->post('avatar', TRUE);
        $firstname = $this->input->post('firstname', TRUE);
        $middlename = $this->input->post('middlename', TRUE);
        $lastname = $this->input->post('lastname', TRUE);
        $name = $this->input->post('name', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $birthday = $this->input->post('birthday', TRUE);
        $cellphone = $this->input->post('cellphone', TRUE);
        $telephone = $this->input->post('telephone', TRUE);
        $country_id = $this->input->post('country_id', TRUE);
        $country = $this->input->post('country', TRUE);
        $city_id = $this->input->post('city_id', TRUE);
        $city = $this->input->post('city', TRUE);
        $area_id = $this->input->post('area_id', TRUE);
        $area = $this->input->post('area', TRUE);
        $country = $this->input->post('country', TRUE);
        $city = $this->input->post('city', TRUE);
        $area = $this->input->post('area', TRUE);
        $address = $this->input->post('address', TRUE);
        $blog = $this->input->post('blog', TRUE);
        $no = $this->input->post('no', TRUE);
        $vip = $this->input->post('vip', TRUE);
        $vip_start_at = $this->input->post('vip_start_at', TRUE);
        $vip_end_at = $this->input->post('vip_end_at', TRUE);
        $black = $this->input->post('black', TRUE);
        $role_id = $this->input->post('role_id', TRUE);
        $note = $this->input->post('note', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objUser = User::find($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($account && $objUser->account != $account)
        {
            $objUser_by_account = User::find_by_account($account, array('conditions' => array('id != '.$id)));
            if($objUser_by_account)
            {
                $this->load->view('api/respone', array(
                    'status' => 'account',
                    'message' => 'account 重複',
                    'data' => ''
                ));
                return FALSE;
            }
        }

        if($password && strlen($password) < 8)
        {
            $this->load->view('api/respone', array(
                'status' => 'password',
                'message' => 'password 小於8位數',
                'data' => ''
            ));
            return FALSE;
        }

        if($email && !valid_email($email))
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => array()
            ));
            return FALSE;
        }

        if($account !== FALSE && $objUser->account != $account) $data['account'] = $account;
        if($password !== FALSE && $objUser->password != $password) $data['password'] = $password;
        if($email !== FALSE && $objUser->email != $email) $data['email'] = $email;
        if($nickname !== FALSE && $objUser->nickname != $nickname) $data['nickname'] = $nickname;
        if($avatar !== FALSE && $objUser->avatar != $avatar) $data['avatar'] = $avatar;
        if($firstname !== FALSE && $objUser->firstname != $firstname) $data['firstname'] = $firstname;
        if($middlename !== FALSE && $objUser->middlename != $middlename) $data['middlename'] = $middlename;
        if($lastname !== FALSE && $objUser->lastname != $lastname) $data['lastname'] = $lastname;
        if($name !== FALSE && $objUser->name != $name) $data['name'] = $name;
        if($gender !== FALSE && $objUser->gender != $gender) $data['gender'] = $gender;
        if($birthday !== FALSE && $objUser->birthday != $birthday) $data['birthday'] = $birthday;
        if($cellphone !== FALSE && $objUser->cellphone != $cellphone) $data['cellphone'] = $cellphone;
        if($telephone !== FALSE && $objUser->telephone != $telephone) $data['telephone'] = $telephone;
        if($country_id !== FALSE && $objUser->country_id != $country_id) $data['country_id'] = $country_id;
        if($country !== FALSE && $objUser->country != $country) $data['country'] = $country;
        if($city_id !== FALSE && $objUser->city_id != $city_id) $data['city_id'] = $city_id;
        if($city !== FALSE && $objUser->city != $city) $data['city'] = $city;
        if($area_id !== FALSE && $objUser->area_id != $area_id) $data['area_id'] = $area_id;
        if($area !== FALSE && $objUser->area != $area) $data['area'] = $area;
        if($address !== FALSE && $objUser->address != $address) $data['address'] = $address;
        if($blog !== FALSE && $objUser->blog != $blog) $data['blog'] = $blog;
        if($no !== FALSE && $objUser->no != $no) $data['no'] = $no;
        if($vip !== FALSE && $objUser->vip != $vip) $data['vip'] = $vip;
        if($vip_start_at !== FALSE && $objUser->vip_start_at != $vip_start_at) $data['vip_start_at'] = $vip_start_at;
        if($vip_end_at !== FALSE && $objUser->vip_end_at != $vip_end_at) $data['vip_end_at'] = $vip_end_at;
        if($black !== FALSE && $objUser->black != $black) $data['black'] = $black;
        if($role_id !== FALSE && $objUser->role_id != $role_id) $data['role_id'] = $role_id;
        if($note !== FALSE && $objUser->note != $note) $data['note'] = $note;
        if($enable !== FALSE && $objUser->enable != $enable) $data['enable'] = $enable;

        if(isset($user['birthday']) && $user['birthday'])
        {
            $user['birthday'] = date('Y-m-d', strtotime($user['birthday']));
        }

        if(isset($data['country_id']) && $data['country_id'])
        {
            $objCountry = Country::find($data['country_id']);
            if($objCountry && $objUser->country != $objCountry->name) $data['country'] = $objCountry->name;
        }

        if(isset($data['city_id']) && $data['city_id'])
        {
            $objCity = City::find($data['city_id']);
            if($objCity && $objUser->city != $objCity->name) $data['city'] = $objCity->name;
        }
        
        if(isset($data['area_id']) && $data['area_id'])
        {
            $objArea = Area::find($data['area_id']);
            if($objArea && $objUser->area != $objArea->name) $data['area'] = $objArea->name;
        }

        if(isset($data))
        {
            $objUser->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $vip = $this->input->post('vip', TRUE);
        $black = $this->input->post('black', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objUser = User::find_by_id($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($vip !== FALSE && $objUser->vip != $vip) $data['vip'] = $vip;
        if($black !== FALSE && $objUser->black != $black) $data['black'] = $black;
        if($enable !== FALSE && $objUser->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objUser->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objUser = User::find_by_id($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser->delete = 0;
        $objUser->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objUser = User::find_by_id($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser->delete = 1;
        $objUser->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objUser = User::find_by_id($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }

    public function register() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('account') === FALSE) return FALSE;
        if($this->input->post('password') === FALSE) return FALSE;
        if($this->input->post('confirm') === FALSE) return FALSE;
        if($this->input->post('birthday_year') === FALSE) return FALSE;
        if($this->input->post('birthday_month') === FALSE) return FALSE;
        if($this->input->post('birthday_day') === FALSE) return FALSE;
        if($this->input->post('captcha') === FALSE) return FALSE;

        $account = $this->input->post('account', TRUE);
        $password = $this->input->post('password', TRUE);
        $confirm = $this->input->post('confirm', TRUE);
        $name = $this->input->post('name', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $cellphone = $this->input->post('cellphone', TRUE);
        $birthday_year = $this->input->post('birthday_year', TRUE);
        $birthday_month = $this->input->post('birthday_month', TRUE);
        $birthday_day = $this->input->post('birthday_day', TRUE);
        $captcha = $this->input->post('captcha', TRUE);

        if($captcha != $this->session->userdata('captcha'))
        {
            $captcha = create_captcha(array(
                'img_path' => configPathImageTemp,
                'img_url' => configUrlImageTemp,
                'img_width' => 100,
                'img_height' => 40,
                'expiration' => 7200,
                'font_path' => configPathFont.'VeraBd.ttf',
                'word' => random_string('numeric', 4),
                'bg_color' => '#bbb',
                'border_color' => '#bbb',
                'text_color' => '#3a466f',
                'grid_color' => '#3a466f',
                'shadow_color' => '#3a64f6'));
            $this->session->set_userdata('captcha', $captcha['word']);

            $this->load->view('api/respone', array(
                'status' => 'captcha',
                'message' => '驗證碼錯誤',
                'data' => array('captcha_image' => $captcha['image'])
            ));
            return FALSE;
        }

        $objUser_by_account = User::find_by_account($account);
        if($objUser_by_account)
        {
            $this->load->view('api/respone', array(
                'status' => 'account',
                'message' => 'account 重複',
                'data' => ''
            ));
            return FALSE;
        }

        if($password && strlen($password) < 8)
        {
            $this->load->view('api/respone', array(
                'status' => 'password',
                'message' => 'password 小於8位數',
                'data' => ''
            ));
            return FALSE;
        }

        if($password != $confirm)
        {
            $this->load->view('api/respone', array(
                'status' => 'confirm',
                'message' => 'password 與 confirm 不同',
                'data' => ''
            ));
            return FALSE;
        }

        if($account && valid_email($account) === FALSE)
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 格式錯誤',
                'data' => array()
            ));
            return FALSE;
        }

        if($account !== FALSE)
        {
            $data['account'] = $account;
            $data['email'] = $data['account'];
        }
        if($password !== FALSE) $data['password'] = $password;
        if($name !== FALSE) $data['name'] = $name;
        if($gender !== FALSE) $data['gender'] = $gender;
        if($birthday_year !== FALSE &&
           $birthday_month !== FALSE &&
           $birthday_day !== FALSE) $data['birthday'] = date('Y-m-d', strtotime($birthday_year.'-'.$birthday_month.'-'.$birthday_day));
        if($cellphone !== FALSE) $data['cellphone'] = $cellphone;

        if(isset($data))
        {
            $data['name'] = $data['account'];
            $data['enable'] = 1;
            $objUser = User::create($data);

            $user = $objUser->to_array(array('except' => 'password'));
            $user['password'] = $password;

            User::login($account, $password);

            $this->load->library('mail_library');
            $this->mail_library->user_register($objUser->email, $user);

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '註冊成功',
                'data' => array('user' => $user)
            ));
            return TRUE;
        }
    }

    public function edit_info() {
        $objLoginUser = User::get();
        if($objLoginUser === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $name = $this->input->post('name', TRUE);
        $gender = $this->input->post('gender', TRUE);
        $birthday = $this->input->post('birthday', TRUE);
        $cellphone = $this->input->post('cellphone', TRUE);
        $telephone = $this->input->post('telephone', TRUE);
        $zip = $this->input->post('zip', TRUE);
        $address = $this->input->post('address', TRUE);
        $email = $this->input->post('email', TRUE);

        if($objLoginUser->id != $id)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不符',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser = User::find_by_id($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($name !== FALSE && $objUser->name != $name) $data['name'] = $name;
        if($gender !== FALSE && $objUser->gender != $gender) $data['gender'] = $gender;
        if($birthday !== FALSE && $objUser->birthday != $birthday) $data['birthday'] = date('Y-m-d', strtotime($birthday));
        if($cellphone !== FALSE && $objUser->cellphone != $cellphone) $data['cellphone'] = $cellphone;
        if($telephone !== FALSE && $objUser->telephone != $telephone) $data['telephone'] = $telephone;
        if($zip !== FALSE && $objUser->zip != $zip) $data['zip'] = $zip;
        if($address !== FALSE && $objUser->address != $address) $data['address'] = $address;
        if($email !== FALSE && $objUser->email != $email) $data['email'] = $email;

        if(isset($data))
        {
            $objUser->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function change_password() {
        $objLoginUser = User::get();
        if($objLoginUser === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $password = $this->input->post('password', TRUE);
        $confirm = $this->input->post('confirm', TRUE);

        if($objLoginUser->id != $id)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不符',
                'data' => ''
            ));
            return FALSE;
        }

        $objUser = User::find($id);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($password && strlen($password) < 8)
        {
            $this->load->view('api/respone', array(
                'status' => 'password',
                'message' => 'password 小於8位數',
                'data' => ''
            ));
            return FALSE;
        }

        if($password != $confirm)
        {
            $this->load->view('api/respone', array(
                'status' => 'confirm',
                'message' => 'password 與 confirm 不同',
                'data' => ''
            ));
            return FALSE;
        }

        if($password !== FALSE && $objUser->password != $password) $data['password'] = $password;

        if(isset($data))
        {
            $objUser->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function forget_password() {
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('email') === FALSE) return FALSE;

        $email = $this->input->post('email', TRUE);

        $objUser = User::find_by_account_and_enable_and_delete($email, 1, 0);
        if(!$objUser)
        {
            $this->load->view('api/respone', array(
                'status' => 'email',
                'message' => 'email 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $password = random_string('alnum', 8);
        $objUser->password = $password;
        $objUser->save();
        $user = $objUser->to_array();
        $user['password'] = $password;

        $this->load->library('mail_library');
        $this->mail_library->user_forget_password($email, $user);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '密碼重新設定成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function permission() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('role_id') === FALSE) return FALSE;
        if($this->input->post('permission_id') === FALSE) return FALSE;
        if($this->input->post('enable') === FALSE) return FALSE;

        $role_id = $this->input->post('role_id', TRUE);
        $permission_id = $this->input->post('permission_id', TRUE);
        $enable = $this->input->post('enable', TRUE);
        
        $objRole = Role::find($role_id);
        if(!$objRole)
        {
            $this->load->view('api/respone', array(
                'status' => 'role_id',
                'message' => 'role_id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objPermission = Permission::find($permission_id);
        if(!$objPermission)
        {
            $this->load->view('api/respone', array(
                'status' => 'permission_id',
                'message' => 'permission_id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($role_id !== FALSE) $data['role_id'] = $role_id;
        if($permission_id !== FALSE) $data['permission_id'] = $permission_id;
        if($enable !== FALSE) $data['enable'] = $enable;
        
        if($data)
        {
            $objRolePermission = RolePermission::find_by_role_id_and_permission_id($role_id, $permission_id);
            if(!$objRolePermission)
            {
                $objRolePermission = RolePermission::create($data);
            }
            else
            {
                $objRolePermission->enable = $enable;
                $objRolePermission->save();
            }
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '變更成功',
            'data' => array()
        ));
        return TRUE;
    }
}