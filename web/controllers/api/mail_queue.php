<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_queue_controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function get() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('count') === FALSE) return FALSE;
        if($this->input->post('page') === FALSE) return FALSE;
        if($this->input->post('sort') === FALSE) return FALSE;

        $count = $this->input->post('count', TRUE);
        $page = $this->input->post('page', TRUE);
        $sort = $this->input->post('sort', TRUE);
        
        $pagination = array(
            'total' => 0,
            'count' => $count,
            'pages' => 1,
            'page' => $page
        );

        $query['conditions'] = '';
        $total = MailQueue::count($query);

        $query['limit'] = $count;
        $query['offset'] = ($page - 1) * $count;
        $query['order'] = $sort;

        $mail_queues = array();
        $objMailQueues = MailQueue::all($query);
        if($objMailQueues)
        {
            $mail_queues = to_array($objMailQueues);
        }

        $pagination['count'] = $count;
        $pagination['total'] = $total;
        $pagination['pages'] = ceil($total / $count) > 0 ? ceil($total / $count) : 1;
        $pagination['page'] = $page;
        $pagination['first'] = 1;
        $pagination['previous'] = $page - 1 > 1 ? $page - 1 : 1;
        $pagination['start'] = ($pagination['page'] - 4 > 0 ? $pagination['page'] - 4 : 1);
        $pagination['end'] = ($pagination['start'] + 8 < $pagination['pages'] ? $pagination['start'] + 8 : $pagination['pages']);
        $pagination['next'] = $pagination['page'] + 1 < $pagination['end'] ? $pagination['page'] + 1 : $pagination['end'];
        $pagination['last'] = $pagination['pages'];

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'mail_queues' => $mail_queues,
                'pagination' => $pagination
            )
        ));
        return TRUE;
    }
    
    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $from_email = $this->input->post('from_email', TRUE);
        $from_name = $this->input->post('from_name', TRUE);
        $to_email = $this->input->post('to_email', TRUE);
        $reply_email = $this->input->post('reply_email', TRUE);
        $reply_name = $this->input->post('reply_name');
        $cc = $this->input->post('cc', TRUE);
        $bcc = $this->input->post('bcc', TRUE);
        $subject = $this->input->post('subject', TRUE);
        $message = $this->input->post('message');
        $attach = $this->input->post('attach', TRUE);
        $error_log = $this->input->post('error_log', TRUE);
        $sended = $this->input->post('sended', TRUE);
        $sended_times = $this->input->post('sended_times', TRUE);
        $sended_time = $this->input->post('sended_time', TRUE);

        $objMailQueue = MailQueue::find_by_id($id);
        if(!$objMailQueue)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($from_email !== FALSE && $objMailQueue->from_email != $from_email) $data['from_email'] = $from_email;
        if($from_name !== FALSE && $objMailQueue->from_name != $from_name) $data['from_name'] = $from_name;
        if($to_email !== FALSE && $objMailQueue->to_email != $to_email) $data['to_email'] = $to_email;
        if($reply_email !== FALSE && $objMailQueue->reply_email != $reply_email) $data['reply_email'] = $reply_email;
        if($reply_name !== FALSE && $objMailQueue->reply_name != $reply_name) $data['reply_name'] = $reply_name;
        if($cc !== FALSE && $objMailQueue->cc != $cc) $data['cc'] = $cc;
        if($bcc !== FALSE && $objMailQueue->bcc != $bcc) $data['bcc'] = $bcc;
        if($subject !== FALSE && $objMailQueue->subject != $subject) $data['subject'] = $subject;
        if($message !== FALSE && $objMailQueue->message != $message) $data['message'] = $message;
        if($attach !== FALSE && $objMailQueue->attach != $attach) $data['attach'] = $attach;
        if($error_log !== FALSE && $objMailQueue->error_log != $error_log) $data['error_log'] = $error_log;
        if($sended !== FALSE && $objMailQueue->sended != $sended) $data['sended'] = $sended;
        if($sended_times !== FALSE && $objMailQueue->sended_times != $sended_times) $data['sended_times'] = $sended_times;
        if($sended_time !== FALSE && $objMailQueue->sended_time != $sended_time) $data['sended_time'] = $sended_time;

        if(isset($data))
        {
            $objMailQueue->update_attributes($data);
            $mail_queue = $objMailQueue->to_array();
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('mail_queue' => $mail_queue)
        ));
        return TRUE;
    }

    public function delete() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $objMailQueue = MailQueue::find_by_id($id);
        if(!$objMailQueue)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objMailQueue->delete();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ));
    }
}