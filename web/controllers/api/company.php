<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function get() {
        if(User::is_admin() === false) return false;

        $delete = $this->input->post('delete', true);
        
        $query['conditions'] = '';
        $query['conditions'] = ($query['conditions'] ? $query['conditions'].' AND ' : '').'`delete` = '.($delete ? $delete : 0);

        $companyies = [];
        $objCompanies = Company::all($query);
        if($objCompanies) $companyies = to_array($objCompanies);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => ['companyies' => $companyies]
        ]);
        return true;
    }

    public function add() {
        if($this->input->post() === false) return false;
        if($this->input->post('name') === false) return false;

        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $telephone = $this->input->post('telephone', true);
        $fax = $this->input->post('fax', true);
        $address = $this->input->post('address', true);
        $address_en = $this->input->post('address_en', true);
        $address_cn = $this->input->post('address_cn', true);
        $email = $this->input->post('email', true);
        $time = $this->input->post('time', true);
        $url = $this->input->post('url', true);
        $google_map_iframe = $this->input->post('google_map_iframe');
        $image = $this->input->post('image', true);

        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($telephone) $data['telephone'] = $telephone;
        if($fax) $data['fax'] = $fax;
        if($address) $data['address'] = $address;
        if($address_en) $data['address_en'] = $address_en;
        if($address_cn) $data['address_cn'] = $address_cn;
        if($email) $data['email'] = $email;
        if($time) $data['time'] = $time;
        if($url) $data['url'] = $url;
        if($google_map_iframe) $data['google_map_iframe'] = $google_map_iframe;
        if($image) $data['image'] = $image;

        if(isset($data)) {
            $objCompany = Company::create($data);
            $company = $objCompany->to_array();

            $this->load->view('api/respone', [
                'status' => 'ok',
                'message' => '新增成功',
                'data' => ['company' => $company]
            ]);
            return true;
        }
    }

    public function edit() {
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;
        if($this->input->post('name') === false) return false;

        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $name_en = $this->input->post('name_en', true);
        $name_cn = $this->input->post('name_cn', true);
        $telephone = $this->input->post('telephone', true);
        $fax = $this->input->post('fax', true);
        $address = $this->input->post('address', true);
        $address_en = $this->input->post('address_en', true);
        $address_cn = $this->input->post('address_cn', true);
        $email = $this->input->post('email', true);
        $time = $this->input->post('time', true);
        $url = $this->input->post('url', true);
        $google_map_iframe = $this->input->post('google_map_iframe');
        $image = $this->input->post('image', true);
        $enable = $this->input->post('enable', true);

        $objCompany = Company::find($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => []
            ]);
            return false;
        }

        if($name !== false && $objCompany->name != $name) $data['name'] = $name;
        if($name_en !== false && $objCompany->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== false && $objCompany->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($telephone !== false && $objCompany->telephone != $telephone) $data['telephone'] = $telephone;
        if($fax !== false && $objCompany->fax != $fax) $data['fax'] = $fax;
        if($address !== false && $objCompany->address != $address) $data['address'] = $address;
        if($address_en !== false && $objCompany->address_en != $address_en) $data['address_en'] = $address_en;
        if($address_cn !== false && $objCompany->address_cn != $address_cn) $data['address_cn'] = $address_cn;
        if($email !== false && $objCompany->email != $email) $data['email'] = $email;
        if($time !== false && $objCompany->time != $time) $data['time'] = $time;
        if($url !== false && $objCompany->url != $url) $data['url'] = $url;
        if($google_map_iframe !== false && $objCompany->google_map_iframe != $google_map_iframe) $data['google_map_iframe'] = $google_map_iframe;
        if($image !== false && $objCompany->image != $image) $data['image'] = $image;
        if($enable !== false && $objCompany->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objCompany->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => []
        ]);
        return true;
    }

    public function edit_attribute() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $enable = $this->input->post('enable', true);

        $objCompany = Company::find_by_id($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($enable !== false && $objCompany->enable != $enable) $data['enable'] = $enable;

        if(isset($data)) {
            $objCompany->update_attributes($data);
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_restore() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objCompany = Company::find($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objCompany->delete = 0;
        $objCompany->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        $objCompany = Company::find($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objCompany->delete = 1;
        $objCompany->save();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
        return true;
    }

    public function delete_force() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        $objCompany = Company::find($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $objCompany->delete();

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '清除成功',
            'data' => ''
        ]);
    }

    public function deletes_soft() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);
        
        if($id && !is_array($id)) $id = array($id);

        $query['conditions'] = '`id` IN ('.implode(',', $id).')';

        $objCompanies = Company::all($query);
        if(!$objCompanies) {
            $this->load->view('api/respone', [
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        if($objCompanies) {
            foreach((array)$objCompanies as $i => $p) {
                $p->delete = 1;
                $p->save();
            }
        }

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ]);
        return true;
    }

    public function move_up() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objCompany = Company::find_by_id($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objCompanies = Company::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objCompanies as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i > 0) $find = $i;
        }
        if($find !== false) {
            $objCompany = array_splice($objCompanies, $find, 1);
            array_splice($objCompanies, $find-1, 0, $objCompany);
        }

        foreach((array)$objCompanies as $i => $o) {
            $o->sort = $i+1;
            $o->save();
        }
        $companies = to_array($objCompanies, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', [
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ['companies' => $companies]
        ]);
        return true;
    }

    public function move_down() {
        if(User::is_admin() === false) return false;
        if($this->input->post() === false) return false;
        if($this->input->post('id') === false) return false;

        $id = $this->input->post('id', true);

        $objCompany = Company::find_by_id($id);
        if(!$objCompany) {
            $this->load->view('api/respone', [
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ]);
            return false;
        }

        $find = false;
        $objCompanies = Company::all(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
        foreach((array)$objCompanies as $i => $o) {
            if($find !== false) continue;
            if($o->id == $id && $i < count($objCompanies)-1) $find = $i;
        }
        if($find !== false) {
            $objCompany = array_splice($objCompanies, $find, 1);
            array_splice($objCompanies, $find+1, 0, $objCompany);
        }

        foreach((array)$objCompanies as $i => $o) {
            $o->sort = $max-($count-$i-1);
            $o->save();
        }
        $companies = to_array($objCompanies, ['only' => ['id', 'sort']]);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('companies' => $companies)
        ));
        return true;
    }
}