<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Bcategory_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->library('node');
    }

    public function get() {
        if(User::is_admin() === FALSE) return FALSE;

        $query['conditions'] = '`title` != "root" AND `delete` = "0"';
        $query['order'] = '`lft` ASC';

        $bcategories = array();
        $objBcategories = Bcategory::all($query);
        if($objBcategories) $bcategories = to_array($objBcategories);

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '查詢成功',
            'data' => array(
                'bcategories' => $bcategories
            )
        ));
        return TRUE;
    }

    public function add() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('name') === FALSE) return FALSE;

        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        if(!$name)
        {
            $this->load->view('api/respone', array(
                'status' => 'name',
                'message' => 'name 為空字串',
                'data' => ''
            ));
            return FALSE;
        }

        if($name) $data['name'] = $name;
        if($name_en) $data['name_en'] = $name_en;
        if($name_cn) $data['name_cn'] = $name_cn;
        if($parent) $data['parent'] = $parent;
        if($enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBcategory = Bcategory::create($data);
            
            $this->node->rebuild_tree('bcategories');

            $objBcategory = Bcategory::find($objBcategory->id);
            $objBcategory->tree_name = str_repeat('　　', $objBcategory->level).$objBcategory->name;
            $objBcategory->tree_name_en = str_repeat('　　', $objBcategory->level).$objBcategory->name_en;
            $objBcategory->tree_name_cn = str_repeat('　　', $objBcategory->level).$objBcategory->name_cn;
            $objBcategory->save();
            $bcategory = $objBcategory->to_array();

            $this->load->view('api/respone', array(
                'status' => 'ok',
                'message' => '新增成功',
                'data' => array('bcategory' => $bcategory)
            ));
            return TRUE;
        }
    }

    public function edit() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $name = $this->input->post('name', TRUE);
        $name_en = $this->input->post('name_en', TRUE);
        $name_cn = $this->input->post('name_cn', TRUE);
        $parent = $this->input->post('parent', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objBcategory = Bcategory::find($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($name !== FALSE && $objBcategory->name != $name) $data['name'] = $name;
        if($name_en !== FALSE && $objBcategory->name_en != $name_en) $data['name_en'] = $name_en;
        if($name_cn !== FALSE && $objBcategory->name_cn != $name_cn) $data['name_cn'] = $name_cn;
        if($parent !== FALSE && $objBcategory->parent != $parent) $data['parent'] = $parent;
        if($enable !== FALSE && $objBcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBcategory->update_attributes($data);

            $this->node->rebuild_tree('bcategories');

            $objBcategory = Bcategory::find($id);
            $objBcategory->tree_name = str_repeat('　　', $objBcategory->level).$objBcategory->name;
            $objBcategory->save();
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function edit_attribute() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        $enable = $this->input->post('enable', TRUE);

        $objBcategory = Bcategory::find_by_id($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        if($enable !== FALSE && $objBcategory->enable != $enable) $data['enable'] = $enable;

        if(isset($data))
        {
            $objBcategory->update_attributes($data);
        }

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_restore() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objBcategory = Bcategory::find($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBcategory->delete = 0;
        $objBcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '還原成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_soft() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objBcategory = Bcategory::find($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'error',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBcategory->delete = 1;
        $objBcategory->save();

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function delete_force() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);
        
        $objBcategory = Bcategory::find($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objBcategory->delete();
        
        $this->node->rebuild_tree('bcategories');

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '刪除成功',
            'data' => ''
        ));
        return TRUE;
    }

    public function move_up() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objBcategory = Bcategory::find_by_id($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objTargetBcategory = Bcategory::find(array(
            'conditions' => '`parent` = '.$objBcategory->parent.' AND `lft` < '.$objBcategory->lft,
            'order' => '`lft` DESC'
        ));
        if($objTargetBcategory)
        {
            $this->node->update_node($objBcategory->id, 'bcategories', 'before', $objTargetBcategory->id, array('name' => $objBcategory->name));
        }

        $objBcategories = Bcategory::all(array(
            'order' => '`lft` ASC'
        ));
        $bcategories = to_array($objBcategories, array('only' => array('id', 'sort')));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('bcategories' => $bcategories)
        ));
        return TRUE;
    }

    public function move_down() {
        if(User::is_admin() === FALSE) return FALSE;
        if($this->input->post() === FALSE) return FALSE;
        if($this->input->post('id') === FALSE) return FALSE;

        $id = $this->input->post('id', TRUE);

        $objBcategory = Bcategory::find_by_id($id);
        if(!$objBcategory)
        {
            $this->load->view('api/respone', array(
                'status' => 'id',
                'message' => 'id 不存在',
                'data' => ''
            ));
            return FALSE;
        }

        $objTargetBcategory = Bcategory::find(array(
            'conditions' => '`parent` = '.$objBcategory->parent.' AND `lft` > '.$objBcategory->lft,
            'order' => '`rght` ASC'
        ));
        if($objTargetBcategory)
        {
            $this->node->update_node($objBcategory->id, 'bcategories', 'after', $objTargetBcategory->id, array('name' => $objBcategory->name));
        }

        $objBcategories = Bcategory::all(array(
            'order' => '`lft` ASC'
        ));
        $bcategories = to_array($objBcategories, array('only' => 'id', 'sort'));

        $this->load->view('api/respone', array(
            'status' => 'ok',
            'message' => '修改成功',
            'data' => array('bcategories' => $bcategories)
        ));
        return TRUE;
    }
}