<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Support_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $objPage = Page::find(['conditions' => '`code` = "support" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $objDownloads = Download::all(['conditions' => '`enable` = 1 AND `delete` = 0']);
        $downloads = to_array($objDownloads, ['methods' => ['_file', '_created_at']]);

        $this->template_library->frontend([
            'name' => $page['title'],
            'html' => 'support',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => ['page' => $page, 'downloads' => $downloads]
        ]);
    }
}