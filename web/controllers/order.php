<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Order_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        redirect(configLangURI.'/order/search');
    }

    public function search() {
        $objPage = Page::find(['conditions' => '`code` = "order" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => '訂單查詢',
            'html' => 'order_search',
            'css' => [],
            'js' => [],
            'json' => '',
            'data' => ['page' => $page]
        ]);
    }

    public function lists() {
        $recipient_email = $this->input->get('recipient_email');
        if(!$recipient_email) redirect(configLangURI.'/order/search');

        $objOrders = Order::all([
            'conditions' => '`delete` = 0 AND `enable` = 1 AND '.
                            '`recipient_email` = "'.$recipient_email.'"'
        ]);
        if(!$objOrders) redirect(configLangURI.'/order/search');
        $orders = to_array($objOrders, [
            'methods' => ['_created_at', '_payment', '_status_remit', '_status_pay', '_status_ship', '_status_return', '_status_refund'],
            'include' => ['order_products']
        ]);
        
        $objPage = Page::find(['conditions' => '`code` = "order" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => '訂單列表',
            'html' => 'order_lists',
            'css' => [],
            'js' => ['assets/js/order.js'],
            'json' => 'var json_orders = '.json_encode($orders).';',
            'data' => ['page' => $page, 'orders' => $orders, 'recipient_email' => $recipient_email]
        ]);
    }

    public function detail() {
        $order_no = $this->input->post('order_no');
        $recipient_email = $this->input->post('recipient_email');
        if(!$order_no || !$recipient_email) redirect(configLangURI.'/order/search');

        $objOrder = Order::find([
            'conditions' => '`delete` = 0 AND `enable` = 1 AND '.
                            '`no` = "'.$order_no.'" AND '.
                            '`recipient_email` = "'.$recipient_email.'"'
        ]);
        if(!$objOrder) redirect('order/search');
        $order = $objOrder->to_array([
            'methods' => ['_created_at', '_payment', '_status_remit', '_status_pay', '_status_ship', '_status_return', '_status_refund'],
            'include' => ['order_products']
        ]);

        $objPage = Page::find(['conditions' => '`code` = "order" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $this->template_library->frontend([
            'name' => '訂單明細',
            'html' => 'order_detail',
            'css' => [],
            'js' => ['assets/js/order.js'],
            'json' => 'var json_order = '.json_encode($order).';',
            'data' => ['page' => $page, 'order' => $order, 'order_no' => $order_no, 'recipient_email' => $recipient_email]
        ]);
    }
}