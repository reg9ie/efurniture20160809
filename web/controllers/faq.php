<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Faq_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index($acategory_id = false, $article_id = false) {
        if($acategory_id && $article_id) {
            $this->detail($acategory_id, $article_id);
            return false;
        }
        elseif($acategory_id) {
            $this->category($acategory_id);
            return false;
        }

        $objPage = Page::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $objAtype = Atype::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);
        
        $objAcategories = Acategory::all([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $acategories = to_array($objAcategories);
        foreach ($acategories as $i => $a) {
            $objArticles = Article::all([
                'conditions' => '`atype_id` = '.$objAtype->id.' AND `acategory_id` = '.$a['id'].' AND `enable` = 1 AND `delete` = 0',
                'limit' => 4,
                'order' => '`sort` ASC',
            ]);
            $acategories[$i]['articles'] = to_array($objArticles);
        }

        $this->template_library->frontend([
            'name' => $page['title'],
            'html' => 'faq',
            'css' => '',
            'js' => '',
            'json' => '',
            'data' => ['acategories' => $acategories]
        ]);
    }

    public function category($acategory_id = false) {
        $objPage = Page::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $objAtype = Atype::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);

        $objAcategories = Acategory::all([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $acategories = to_array($objAcategories);

        $objAcategory = Acategory::find([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `id` = '.$acategory_id.' AND `enable` = 1 AND `delete` = 0',
        ]);
        if(!$objAcategory) redirect(configLangURI.'/faq');
        if($objAcategory->title == 'root') redirect(configLangURI.'/faq');

        $acategory = $objAcategory->to_array();

        $articles = [];
        $objArticles = Article::all([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `acategory_id` = '.$acategory_id.' AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ]);
        if($objArticles) $articles = to_array($objArticles);

        $this->template_library->frontend([
            'name' => $pcategory['name'].' / '.$page['title'],
            'html' => 'faq_category',
            'css' => '',
            'js' => '',
            'json' => '',
            'data' => [
                'page' => $page,
                'acategories' => $acategories,
                'acategory' => $acategory,
                'articles' => $articles,
            ]
        ]);
    }

    public function detail($acategory_id = false, $article_id = false) {
        $objPage = Page::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);
        $objPage->view_times++;
        $objPage->save();
        $page = $objPage->to_array(['methods' => ['images']]);

        $objAtype = Atype::find(['conditions' => '`code` = "faq" AND `enable` = 1 AND `delete` = 0']);

        $objAcategories = Acategory::all([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `level` > -1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $acategories = to_array($objAcategories);

        $objAcategory = Acategory::find([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `id` = '.$acategory_id.' AND `enable` = 1 AND `delete` = 0',
        ]);
        if(!$objAcategory) redirect(configLangURI.'/faq');
        if($objAcategory->title == 'root') redirect(configLangURI.'/faq');
        $acategory = $objAcategory->to_array();

        $objArticle = Article::find([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `id` = '.$article_id.' AND `enable` = 1 AND `delete` = 0'
        ]);
        if(!$objArticle) redirect(configLangURI.'/faq');
        $article = $objArticle->to_array();

        $objArticle->view_times++;
        $objArticle->save();

        $objBeforArticle = Article::first([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `acategory_id` = '.$acategory_id.' AND `sort` < '.$objArticle->sort.' AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` DESC'
        ]);
        $article['befor'] = $objBeforArticle ? $objBeforArticle->to_array() : false;

        $objAfterArticle = Article::first([
            'conditions' => '`atype_id` = '.$objAtype->id.' AND `acategory_id` = '.$acategory_id.' AND `sort` > '.$objArticle->sort.' AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ]);
        $article['after'] = $objAfterArticle ? $objAfterArticle->to_array() : false;

        $this->template_library->frontend(array(
            'name' => $article['name'].' / '.$pcategory['name'].' / '.$page['title'],
            'html' => 'faq_detail',
            'css' => '',
            'js' => '',
            'json' => '',
            'data' => [
                'page' => $page,
                'acategories' => $acategories,
                'acategory' => $acategory,
                'article' => $article,
            ]
        ));
    }
}