<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->helper('captcha');
    }
    
    public function index() {
        $error_message = '';
        if($this->input->post() !== false) {
            if($this->input->post('account') === false) return false;
            if($this->input->post('password') === false) return false;
            if($this->input->post('captcha') === false) return false;

            if($this->input->post('captcha', true) == $this->session->userdata('captcha')) {
                $account = $this->input->post('account', true);
                $password = $this->input->post('password', true);

                $objUser = User::login($account, $password);
                if($objUser) redirect('/');

                $error_message = '<div><strong>錯誤！</strong></div><div>帳號密碼錯誤，請確認帳號密碼。</div>';
            }
            else {
                $error_message = '<div><strong>錯誤！</strong></div><div>驗證碼錯誤，請重新輸入驗證碼。</div>';
            }
        }

        $captcha = create_captcha([
            'img_path' => configPathImageTemp,
            'img_url' => configUrlImageTemp,
            'img_width' => 80,
            'img_height' => 25,
            'expiration' => 7200,
            'font_path' => configPathFont.'VeraBd.ttf',
            'word' => random_string('numeric', 4),
            'bg_color' => '#fff',
            'border_color' => '#fff',
            'text_color' => '#682121',
            'grid_color' => '#78466f',
            'shadow_color' => '#3a64f6'
        ]);
        $this->session->set_userdata('captcha', $captcha['word']);

        $user = [
            'account' => '',
            'password' => '',
            'confirm' => '',
            'birthday_year' => date('Y'),
            'birthday_month' => date('m'),
            'birthday_day' => date('d'),
            'readed' => false,
            'captcha' => ''
        ];

        $this->template_library->frontend([
            'name' => '會員登入',
            'html' => 'login',
            'css' => '',
            'js' => ['assets/js/member.js'],
            'json' => 'var json_user = '.json_encode($user).';',
            'data' => ['captcha_image' => $captcha['image'], 'error_message' => $error_message]
        ]);
    }
}