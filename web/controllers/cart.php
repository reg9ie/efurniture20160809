<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        redirect(configLangURI.'/cart/lists');
    }

    public function lists() {
        $cart = $this->session->userdata('cart');

        $shippings = [];
        $objShippings = Shipping::all(['conditions' => '`enable` = 1', 'order' => '`sort` ASC']);
        if($objShippings) $shippings = to_array($objShippings);

        $payments = [];
        $objPayments = Payment::all(['conditions' => '`enable` = 1', 'order' => '`sort` ASC']);
        if($objPayments) $payments = to_array($objPayments);

        $this->template_library->frontend([
            'name' => '確認購物清單',
            'html' => 'cart_lists',
            'css' => [],
            'js' => [],
            'json' => 'var json_shippings = '.json_encode($shippings).';'.
                      'var json_payments = '.json_encode($payments).';',
            'data' => ['payments' => $payments, 'shippings' => $shippings]
        ]);
    }

    public function payment() {
        $cart = $this->session->userdata('cart');
        if(isset($cart['products']) == false || !$cart['products']) redirect(configLangURI.'/cart/lists');

        $shippings = [];
        $objShippings = Shipping::all(['conditions' => '`enable` = 1', 'order' => '`sort` ASC']);
        if($objShippings) $shippings = to_array($objShippings);

        $payments = [];
        $objPayments = Payment::all(['conditions' => '`enable` = 1', 'order' => '`sort` ASC']);
        if($objPayments) $payments = to_array($objPayments);

        $this->template_library->frontend([
            'name' => '填寫付款與運送資料',
            'html' => 'cart_payment',
            'css' => [],
            'js' => [],
            'json' => 'var json_shippings = '.json_encode($shippings).';'.
                      'var json_payments = '.json_encode($payments).';',
            'data' => ['payments' => $payments, 'shippings' => $shippings]
        ]);
    }

    public function to_emap() {
    //     $cart = $this->session->userdata('cart');
    //     if(isset($cart['products']) == false || !$cart['products']) redirect(configLangURI.'/cart/lists');
        
    //     $this->load->library('allpay_library');
    //     $this->allpay_library->emap($cart);
    //     return true;
    }

    public function to_payment() {
        $order_id = $this->session->userdata('order_id');
        if(!$order_id) redirect(configLangURI.'/');

        $objOrder = Order::find_by_id_and_enable_and_delete($order_id, 1, 0);
        if(!$objOrder) redirect(configLangURI.'/');
        $order = $objOrder->to_array(['include' => ['order_products']]);

        $objPayment = Payment::find_by_id_and_enable_and_delete($objOrder->payment_id, 1, 0);
        if(!$objPayment) redirect(configLangURI.'/');
        if($objPayment->code == 'credit' || $objPayment->code == 'atm' || $objPayment->code == 'cvs') {
            $this->load->library('allpay_library');
            $this->allpay_library->checkout($order);
            return true;
        }
        elseif($objPayment->code == 'paypal') {
            $this->load->library('paypal_library');
            $this->paypal_library->checkout($order);
            return true;
        }
        elseif($objPayment->code == 'hncb') {
            $this->load->library('hncb_library');
            $this->hncb_library->checkout($order);
            return true;
        }

        redirect(configLangURI.'/cart/done');
    }

    public function cancel() {
        $order_id = $this->session->userdata('order_id');
        $this->session->unset_userdata('order_id');
        if(!$order_id) redirect(configLangURI.'/');

        $cart = $this->session->userdata('cart');
        $this->session->unset_userdata('cart');
        if(!$cart) redirect(configLangURI.'/');

        $objOrder = Order::find_by_id_and_enable_and_delete($order_id, 1, 0);
        if(!$objOrder) redirect(configLangURI.'/');
        $order = $objOrder->to_array(['include' => ['order_products']]);

        $this->template_library->frontend([
            'name' => '訂單取消',
            'html' => 'cart_cancel',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => ['order' => $order],
        ]);
    }

    public function done() {
        $order_id = $this->session->userdata('order_id');
        $this->session->unset_userdata('order_id');
        if(!$order_id) redirect(configLangURI.'/');

        $cart = $this->session->userdata('cart');
        $this->session->unset_userdata('cart');
        if(!$cart) redirect(configLangURI.'/');

        $objOrder = Order::find_by_id_and_enable_and_delete($order_id, 1, 0);
        if(!$objOrder) redirect(configLangURI.'/');
        $order = $objOrder->to_array(['include' => ['order_products']]);

        $this->template_library->frontend([
            'name' => '購物完成',
            'html' => 'cart_done',
            'css' => '',
            'js' => [],
            'json' => '',
            'data' => ['order' => $order],
        ]);
    }
}