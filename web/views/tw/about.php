<article id="pageBanner" class="mobileHide">
    <?if($data['page']['images']):?>
    <img src="<?=$data['page']['images'][0]['_filename']?>">
    <?endif?>
</article>
<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li class="last"><a href="about">關於e+</a></li>
        </ul>
    </div>
</article>

<?=$data['page']['content']?>