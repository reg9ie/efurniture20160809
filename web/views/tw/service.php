
	<div class="carousel_wrapper">
		<div class="main_carousel">
			<div><img src="assets/images/primary/service_carousel.jpg" alt="" /></div>
		</div>
		<a class="carousel_next"><img src="assets/images/primary/main_carousel_next.png" alt="" /></a>
		<a class="carousel_prev"><img src="assets/images/primary/main_carousel_prev.png" alt="" /></a>
	</div>

	<div class="content_wrapper" id="se01">
		<div class="container">
			<div class="page_content_tit"> 交易方式 </div>
			<div class="page_content_text">
				<img src="assets/images/primary/service_img.png" alt="">
			</div>
		</div>
	</div>

	<div class="content_wrapper service_01" id="se02">
		<div class="container">
			<div class="page_content_tit"> 交易程序 </div>
			<div class="page_content_text">
				<img src="assets/images/primary/service_img-02.png" alt="">
			</div>
		</div>
	</div>

	<div class="content_wrapper service_02">
		<div class="container">
			<div class="page_content_text">
				<img src="assets/images/primary/service_padding.png" alt="">
				<img src="assets/images/primary/service_img-03.png" alt="">
			</div>
		</div>
	</div>

	<div class="content_wrapper" id="se03">
		<div class="container">
			<div class="page_content_tit"> 交易時間 </div>
			<div class="page_content_text">
				<table class="service_time text-left">
					<tr>
						<td><span>旺季：</span>5月至12月</td>
						<td><span>交易時間：</span>早上8點-中午12點，下午16點-晚上19點</td>
					</tr>
						<td><span>淡季：</span>1月至5月</td>
						<td><span>交易時間：</span>早上8點-下午17點</td>
				</table>
			</div>
		</div>
	</div>

	<div class="content_wrapper gray_bg" id="se04">
		<div class="container">
			<div class="page_content_text">
				<img src="assets/images/primary/service_img-04.png" alt="">
			</div>
		</div>
	</div>

	<div class="content_wrapper" id="se05">
		<div class="container">
			<div class="page_content_tit"> 冷藏庫承租作業規定 </div>
			<div class="page_content_text">
				<ul class="service_list">
					<li>1、果菜市場冷藏庫作業時間為每日<span>早上七點至晚上十點整</span>（休假日將提前公佈於冷藏庫公佈欄）</li>
					<li>2、本庫主要功能為調節水果批發產銷使用，因應本會增加自用坪數，故每年承租坪數將減少，現<span>承租戶如冷藏目的為自行宅配調整，或自行直銷通路調整，</span>請至本會推廣部申請興建冷藏庫。另果菜市場有議價收購高接梨，可接洽並解決冷藏問題，明年度承租坪數減少時，上述承租戶將<span>優先不予續租</span>。</li>
					<li>3、經與本庫訂立冷藏契約後，因貨品產量減少欲改坪數者，應於契約起日起<span>七日</span>內至本冷藏庫辦理。若逾時未辦理，則依原契約內容實行。</li>
					<li>4、本庫與承租戶所訂承租坪數係指立體空間，非一般民間所謂地坪，故<span>進貨數量是以個人不分規格如下表計算方式計算，庫房件數額滿，即不能再進貨</span>，或增加坪數。</li>
				</ul>
			</div>
			<div class="page_content_text">
				<img src="assets/images/primary/service_img-05.png" alt="">
			</div>
			<div class="page_content_text">
				<ul class="service_list">
					<li>5、承租戶分租（須於訂契約時告知本庫並記載於契約書上）太多人，致每人使用坪數低於五坪，或承租戶貨品分規格入貨者，入庫數量不得以上表計算。為利進出貨作業，<span>無法配合者請自行出貨</span>，出入工費用將僅收入貨工資（總工資之6成）。</li>
					<li>6、入庫貨品請依規定卸貨，並至本庫辦公室<span>領取入貨單據</span>；出貨請先至本庫辦公室<span>登打出貨單據</span>，依出貨單據時間排定出貨順序，<span>並於出貨單據上簽名</span>。</li>
					<li>7、貨物由冷藏庫出貨至市場販售，一律都要打市場交易計算單。</li>
					<li>8、承租人如在冷藏庫整理貨品者，貨品庫存件數本庫不予負責。</li>
					<li>9、承租期間不遵守本庫合約及未能配合本庫作業者，日後將永不續租。</li>
				</ul>
			</div>
			<div class="page_content_text">
				<img src="assets/images/primary/service_img-06.png" alt="">
			</div>
		</div>
	</div>