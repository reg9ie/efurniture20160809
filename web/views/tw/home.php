<article id="carousel"  class="mobileHide">
    <div id="owl-carousel-slider" class="owl-carousel owl-theme">
        <?foreach($data['page']['images'] as $m):?>
            <div class="item">
                <img src="<?=$m['_filename']?>">
                <div class="inbox">
                    <div class="carouselContBox clearfix">
                        <div class="carouselContBoxItem1"><?=$m['content']?></div>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
</article>

<article id="cont">
    <div class="inbox">
        <h2 class="idxTit01">推薦商品</h2>
        <div id="filters" class="button-group inner_wrapper line_box clearfix">
            <a class="button sele" data-filter="*" href="#"><span><strong>所有</strong></span></a>
            <? foreach($data['popular']['pgroups'] as $i => $g): ?>
            <a class="button" data-filter=".group_<?=$g['id']?>" href="#"><span><strong><?=$g['name']?></strong></span></a>
            <? endforeach ?>
            <span class="line"></span>
        </div>
        <div id="container" class="grid clearfix">
            <? foreach($data['popular']['products'] as $i => $p): ?>
            <figure class="element-item effect-winston group_<?=$p['pgroup_id']?>">
                <a href="product/<?=$p['pcategory_id']?>/<?=$p['id']?>">
                    <img src="<?=$p['_image']?>" alt="">
                </a>
                <figcaption>
                    <h4><?=$p['no']?></h4>
                    <h4><?=$p['name']?></h4>
                    <p>
                        <a class="product_wish_add" href="#" data-type="home" data-product_id="<?=$p['id']?>" style="<?=($p['wish'] ? 'color:#3085a3' : '')?>">
                            <i class="fa fa-fw fa-heart"></i>
                        </a>
                    </p>
                </figcaption>
            </figure>
            <? endforeach ?>
        </div>
    </div>
</article>

<article id="cont" class="bg-f0">
    <div class="inbox">
        <h2 class="idxTit02" >客製案例</h2>
        <div id="owl-carousel-ctm" class="owl-carousel-ctm owl-carousel owl-theme-ctm">
            <? foreach($data['custom']['products'] as $i => $p): ?>
            <div class="item">
                <a href="custom/<?=$p['pcategory_id']?>/<?=$p['id']?>">
                    <div class="ctmItem">
                        <img src="<?=$p['_image']?>">
                        <span class="ctmH4"><h4><?=$p['name']?></h4></span>
                        <span class="ctmH5"><h5><?=$p['title']?></h5></span>
                        <span class="ctmCont">
                        <p><?=nl2br($p['intro'])?></p>
                        </span>
                    </div>
                </a>
            </div>
            <? endforeach ?>
        </div>
    </div>
</article>

<article id="cont">
    <div class="inbox">
        <h2 class="idxTit03" >類別總覽</h2>
        <div id="owl-carousel-brd" class="owl-carousel-brd owl-carousel owl-theme-brd">
            <div class="item">
                <div class="brdItem">
                    <a href="products_class.html"><img src="http://placehold.it/250x105"></a>
                </div>
            </div>
            <div class="item">
                <div class="brdItem">
                    <a href="products_class.html"><img src="http://placehold.it/250x105"></a>
                </div>
            </div>
            <div class="item">
                <div class="brdItem">
                    <a href="products_class.html"><img src="http://placehold.it/250x105"></a>
                </div>
            </div>
            <div class="item">
                <div class="brdItem">
                    <a href="products_class.html"><img src="http://placehold.it/250x105"></a>
                </div>
            </div>
        </div>
    </div>
</article>

<article id="cont">
    <div class="inbox">
        <h2 class="idxTit04" >新增文字區塊</h2>
        <span class="newCont">
            <p>永進木器廠在五零年代末，擴廠奠基於台中市南屯區的犁頭店，60多年前以製作嫁妝的五斗櫃、鏡台起家。隨著台灣社會經濟的成長，曾自創收音機品牌 Sun Star，也曾製作電視木製拉門、縫紉機等等；那段歲月正是陪伴著四、五年級生成長的生命記憶，而永進的木作一直在我們生活中的每個時代中出現。</p>
        </span>
    </div>
</article>

<article id="cont" class="contVideo">
    <video id="video" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
        <source src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/953/ladybug_ladybird-SD.mp4" type="video/mp4">
        <source src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/953/ladybug_ladybird-SD.webm" type="video/webm">
    </video>
</article>

<article id="cont" class="mapCont">
    <div class="inbox">
        <h2 class="idxTit05" >找到我們</h2>
    </div>
    <div id="googleMap" style="width:100%;"></div>
</article>