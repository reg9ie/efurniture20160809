<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li><a href="product/<?=$data['pcategory']['parent_pcategory']['id']?>"><?=$data['pcategory']['parent_pcategory']['name']?></a></li>
            <li><a href="product/<?=$data['pcategory']['id']?>"><?=$data['pcategory']['name']?></a></li>
            <li class="last"><a href="product/<?=$data['product']['pcategory_id']?>/<?=$data['product']['id']?>"><?=$data['product']['name']?></a></li>
        </ul>
        <div class="searchbox">
            <input id="search_text" type="text" placeholder="搜尋此類別" value="<?=$data['search_text']?>">
            <a href="#"><i class="fa fa-search" id="search_btn" aeia-hidden="true"></i></a>
        </div>
    </div>
</article>

<div class="productsDetails">
    <article id="cont" class="proHead">
        <div class="inbox clearfix">
            <div id="slider" class="box2 proDetails">
                <ul class="bxslider">
                    <? foreach($data['product']['images'] as $i => $image): ?>
                    <li><img src="<?=$image['_image']?>"/></li>
                    <? endforeach ?>
                </ul>
            </div>
            <div  class="box2 proDetails">
                <h3><?=$data['product']['no']?> <?=$data['product']['name']?></h3>
                <br>
                <h4><?=$data['product']['title']?></h4>
                <h4><?=$data['product']['intro']?></h4>
                <br>
                <p><?=$data['product']['description']?></p>
                <div class="share clearfix">
                    <a class="product_wish_add" href="#" data-type="product_detail" data-product_id="<?=$data['product']['id']?>" style="<?=$data['product']['wish'] ? 'opacity:1;' : ''?>">
                        <span></span>
                        <h4>收藏</h4>
                    </a>
                    <a class="shareFb" href="#"><span></span><h4>分享</h4></a>
                </div>
            </div>
        </div>
    </article>

    <article id="cont">
        <div class="inbox proDetails2"><?=$data['product']['content']?></div>
    </article>

    <article id="pageBanner" class="proDetails3">
        <div class="inbox">
            <div class="box3">
                <?=$data['product']['custom1']?>
            </div>
            <div class="box3">
                <?=$data['product']['custom2']?>
            </div>
            <div id="pageBox" class="box3">
                <img src="<?=$data['product']['_custom_image']?>" alt="">
            </div>
        </div>
    </article>

    <article id="cont">
        <div class="inbox">
            <h2><a class="idxTit06" href="">相關商品</a></h2>
            <div id="owl-carousel-brd" class="owl-carousel-brd owl-carousel owl-theme-brd">
                <? foreach($data['products'] as $i => $p): ?>
                <div class="item">
                    <div class="brdItem">
                        <a href="product/<?=$p['pcategory_id']?>/<?=$p['id']?>">
                            <img src="<?=$p['_image']?>">
                        </a>
                    </div>
                </div>
                <? endforeach ?>
            </div>
        </div>
    </article>

    <article id="cont" class="bg-e8eef1">
        <div class="inbox">
            <h2 class="idxTit07"></h2>
            <div class="ctt_form clearfix" ng-controller="contact" ng-init="init()">
                <div class="tt"><input type="text" placeholder="姓名*" ng-model="contact.name"></div>
                <div class="tt"><input type="text" placeholder="電話" ng-model="contact.telephone"></div>
                <div class="tt"><input type="text" placeholder="其他聯絡方式" ng-model="contact.cellphone"></div>
                <div class="tt2"><input type="text" placeholder="標題*" ng-model="contact.title"></div>
                <div class="tt_area clearfix">
                    <textarea placeholder="輸入您的訊息*" ng-model="contact.content"></textarea>
                </div>
                <div class="sumt"><input type="submit" value="送出" ng-click="send(contact)"></div>
            </div>
        </div>
    </article>
</div>