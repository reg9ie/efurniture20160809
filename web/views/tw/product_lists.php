<article id="pageBanner" class="mobileHide">
    <img src="assets/images/main/bxslider-01.png">
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li><a href="<?=$data['pcategory']['parent']?>"><?=$data['pcategory']['parent_name']?></a></li>
            <li class="last"><a href="<?=$data['pcategory']['id']?>"><?=$data['pcategory']['name']?></a></li>
        </ul>
        <div class="searchbox">
            <input id="search_text" type="text" placeholder="搜尋此類別" value="<?=$data['search_text']?>">
            <a href="#"><i class="fa fa-search" id="search_btn" aeia-hidden="true"></i></a>
        </div>
    </div>
</article>

<article id="cont">
    <div class="inbox">
        <div id="filters" class="button-group inner_wrapper line_box clearfix">
            <a class="button sele" data-filter="*" href="#"><span><strong>所有</strong></span></a>
            <? foreach ($data['pgroups'] as $i => $pg): ?>
            <a class="button" data-filter=".group_<?=$pg['id']?>" href="#"><span><strong><?=$pg['name']?></strong></span></a>
            <? endforeach ?>
            <span class="line"></span>
        </div>
        <div id="container" class="grid clearfix">
            <? foreach ($data['products'] as $i => $p): ?>
            <figure class="element-item  effect-winston group_<?=$p['pgroup_id']?>">
                <a href="product/<?=$p['pcategory_id']?>/<?=$p['id']?>">
                    <img src="<?=$p['_image']?>" alt="">
                </a>
                 <figcaption>
                    <h4><?=$p['no']?></h4>
                    <h4><?=$p['name']?></h4>
                    <p>
                        <a class="product_wish_add" href="#" data-type="home" data-product_id="<?=$p['id']?>" style="<?=($p['wish'] ? 'color:#3085a3' : '')?>">
                            <i class="fa fa-fw fa-heart"></i>
                        </a>
                    </p>
                </figcaption>
            </figure>
            <? endforeach ?>
        </div>
        <div class="inner_wrapper clearfix">
            <a class="button" href="product/<?=$data['pcategory']['id']?>-<?=$data['pagination']['previous']?>"><span><strong><</strong></span></a>
            <? for ($page = $data['pagination']['start']; $page <= $data['pagination']['end']; $page++): ?>
            <a class="button" href="product/<?=$data['pcategory']['id']?>-<?=$page?>"><span><strong><?=$page?></strong></span></a>
            <? endfor ?>
            <a class="button" href="product/<?=$data['pcategory']['id']?>-<?=$data['pagination']['next']?>"><span><strong>></strong></span></a>
            <span class="line"></span>
        </div>
    </div>
</article>