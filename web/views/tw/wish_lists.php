<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li class="last"><a href="wish">追蹤清單</a></li>
        </ul>
    </div>
</article>

<div class="list">
    <article id="cont">
    <div class="inbox">
        <div id="container" class="grid clearfix">
            <? foreach ($data['wishes'] as $i => $w): ?>
            <div class="element-item">
                <div class="ctmItem">
                    <a href="product/<?=$w['pcategory_id']?>/<?=$w['product_id']?>">
                        <img src="<?=$w['product_image']?>" alt="">
                        <span class="ctmH4"><h4><?=$w['product_no']?></h4></span>
                        <span class="ctmH4"><h4><?=$w['product_name']?></h4></span>
                        <span class="ctmH5"><h5><?=$w['product_title']?></h5></span>
                        <span class="ctmCont">
                            <p>
                                <a href="product/<?=$w['pcategory_parent']?>"><?=$w['pcategory_parent_name']?></a>
                                <span> / </span>
                                <a href="product/<?=$w['pcategory_id']?>"><?=$w['pcategory_name']?></a>
                            </p>
                        </span>
                    </a>
                    <a class="product_wish_del" href="#" data-product_id="<?=$w['product_id']?>"><div class="close"></div></a>
                </div>
            </div>
            <? endforeach ?>
        </div>
    </div>
</article>
</div>