<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li><a href="custom">客製訂做</a></li>
            <li><a href="custom/<?=$data['pcategory']['id']?>"><?=$data['pcategory']['name']?></a></li>
            <li class="last"><a href="product/<?=$data['product']['pcategory_id']?>/<?=$data['product']['id']?>"><?=$data['product']['name']?></a></li>
        </ul>
        <div class="searchbox">
            <input id="search_text" type="text" placeholder="搜尋此類別" value="<?=$data['search_text']?>">
            <a href="#"><i class="fa fa-search" id="search_btn" aeia-hidden="true"></i></a>
        </div>
    </div>
</article>

<div class="productsDetails">
    <article id="cont">
        <div class="inbox proDetails2"><?=$data['product']['content']?></div>
    </article>

    <article id="cont">
        <div class="inbox">
            <h2><a class="idxTit06" href="">相關商品</a></h2>
            <div id="owl-carousel-brd" class="owl-carousel-brd owl-carousel owl-theme-brd">
                <? foreach($data['products'] as $i => $p): ?>
                <div class="item">
                    <div class="brdItem">
                        <a href="product/<?=$p['pcategory_id']?>/<?=$p['id']?>">
                            <img src="<?=$p['_image']?>">
                        </a>
                    </div>
                </div>
                <? endforeach ?>
            </div>
        </div>
    </article>

    <article id="cont" class="bg-e8eef1">
        <div class="inbox">
            <h2 class="idxTit07"></h2>
            <div class="ctt_form clearfix" ng-controller="contact" ng-init="init()">
                <div class="tt"><input type="text" placeholder="姓名*" ng-model="contact.name"></div>
                <div class="tt"><input type="text" placeholder="電話" ng-model="contact.telephone"></div>
                <div class="tt"><input type="text" placeholder="其他聯絡方式" ng-model="contact.cellphone"></div>
                <div class="tt2"><input type="text" placeholder="標題*" ng-model="contact.title"></div>
                <div class="tt_area clearfix">
                    <textarea placeholder="輸入您的訊息*" ng-model="contact.content"></textarea>
                </div>
                <div class="sumt"><input type="submit" value="送出" ng-click="send(contact)"></div>
            </div>
        </div>
    </article>
</div>