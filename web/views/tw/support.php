<div class="carousel_wrapper">
    <div class="main_carousel">
        <?foreach($data['page']['images'] as $m):?>
        <div><img  src="<?=$m['_filename']?>"/></div>
        <?endforeach;?>
    </div>
    <a class="carousel_next"><img src="assets/images/primary/main_carousel_next.png" alt="" /></a>
    <a class="carousel_prev"><img src="assets/images/primary/main_carousel_prev.png" alt="" /></a>
</div>
<div class="content_wrapper">
    <div class="container">
        <div class="content_title main_color"> <img src="assets/images/primary/section_title_icon.png" alt=""> 簡介下載 </div>
        <div class="table_list">
            <div class="table_head">DOWNLOAD</div>
            <?foreach($data['downloads'] as $i => $d):?>
            <a class="list_row" href="<?=$d['_file']?>" target="_blank">
                <div class="table_date"><?=$d['_created_at']?></div>
                <div class="table_type"> <span> PDF </span> </div>
                <div class="table_title"><?=$d['name']?></div>
            </a>
            <?endforeach?>
        </div>
    </div>
</div>