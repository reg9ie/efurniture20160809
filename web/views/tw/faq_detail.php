<article id="pageBanner" class="mobileHide">
    <img src="assets/images/main/bxslider-01.png">
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li><a href="faq">常見問題</a></li>
            <li class="last"><a href="faq/<?=$data['acategory']['id']?>"><?=$data['acategory']['name']?></a></li>
        </ul>
    </div>
</article>

<article id="cont" class="faq">
    <div class="inbox">
        <span class="pageTitle03 clearfix">
                <span class="circle">
                <div class="circleIcon">
                </div>
                </span>
                <h3><?=$data['acategory']['name']?></h3>
        </span>
        <div class="qCont3 bg-Gradient-GRAY">
            <h4><?=$data['article']['name']?></h4>
            <br>
            <p><?=$data['article']['content']?></p>
        </div>
        <div class="qCont4 clearfix">
            <? if($data['article']['befor']): ?><span class="qPre"><h4><a href="faq/<?=$data['article']['befor']['acategory_id']?>/<?=$data['article']['befor']['id']?>"><?=$data['article']['befor']['name']?></a></h4></span><? endif ?>
            <? if($data['article']['after']): ?><span class="qNext"><h4><a href="faq/<?=$data['article']['after']['acategory_id']?>/<?=$data['article']['after']['id']?>"><?=$data['article']['after']['name']?></a></h4></span><? endif ?>
        </div>
    </div>
</article>