<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="app" class="js csstransitions ng-scope">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <meta property='fb:app_id' content="<?=$config['facebook_api_id']?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <meta name="keywords" content="<?=$config['site_keywords']?>">
    <meta name="description" content="<?=$config['site_description']?>">
    <meta name="apple-mobile-web-app-title" content="<?=$title?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <base href="<?=base_url()?>"/>
    <link type="image/icon" rel="bookmark" href="assets/image/favicon.ico">
    <link type="image/icon" rel="shortcut icon" href="assets/images/favicon.ico">
    <link type="image/icon" rel="apple-touch-icon" href="assets/images/icon@2.png">
    <?foreach((array)$css as $asset):?>
    <link type="text/css" rel="stylesheet" href="<?=$asset?>"/>
    <?endforeach;?>
    <title><?=$title?></title>
</head>
<body id="top" class="fw idx">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <header id="header">
        <div id="nav_box" class="megaMenu">
            <div class="innerMenu">
                <h1 class="logoB"><a href="<?=site_url()?>"><img src="assets/images/main/logoB2.png" alt="福華傢俱"  width="190" height="155"></a></h1>
                <h1 class="logo"><a href="<?=site_url()?>"><img src="assets/images/main/logo2.png" alt="e+logo"></a></h1>
                <div class="menu_container clearfix">
                    <ul class="menu-ul">
                        <? foreach($pcategories as $i => $pcategory): ?>
                        <li class="has-dropdown full-width">
                            <a href="product/<?=$pcategory['id']?>"><?=$pcategory['name']?></a>
                            <div class="menu-dropdown">
                                <div class="columns">
                                    <div class="span3">
                                        <div class="column-content">
                                            <img src="<?=$pcategory['_image']?>" alt="">
                                        </div>
                                    </div>
                                    <? foreach($pcategory['childs'] as $j => $cp): ?>
                                    <div class="span3">
                                        <div class="column-content">
                                            <h4 class="ttl"><a href="product/<?=$cp['id']?>"><?=$cp['name']?></a></h4>
                                            <ul>
                                                <? foreach($cp['products'] as $k => $pd): ?>
                                                <li><a href="product/<?=$cp['id']?>/<?=$pd['id']?>"><?=$pd['name']?></a></li>
                                                <? endforeach ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <? endforeach ?>
                                </div>
                            </div>
                        </li>
                        <? endforeach ?>
                        <li><a href="custom">客製訂做</a></li>
                        <li><a href="about">關於e+</a></li>
                        <li><a href="faq">常見問題</a></li>
                        <li><a href="contact">聯絡我們</a></li>
                    </ul>
                </div>
                <div class="headerButtons">
                    <ul class="clearfix">
                        <li><a class="fb" href="<?=$config['facebook_url']?>"></a></li>
                        <li><a class="yahoo" href="<?=$config['yahoo_bid_url']?>"></a></li>
                        <li><a class="wishlists" href="wish"></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="hambuger">
            <article id="humContainer">
                <header id="main-nav">
                    <h1 class="ham-logo"><a href="<?=site_url()?>"><img src="assets/images/main/logoHam.png" alt="e+福華傢俱"></a></h1>
                </header>
                <div id="bun">
                    <div class="mmm-burger"></div>
                </div>
            </article>
            <aside id='sidebar'>
                <nav id='mobile-nav'>
                    <ul class="mobile-nav-ul">
                        <? foreach($pcategories as $i => $pcategory): ?>
                        <li class="nav-item"><a href="product/<?=$pcategory['id']?>"><h3><?=$pcategory['name']?></h3></a></li>
                        <? endforeach ?>
                        <li class="nav-item"><a href="custom"><h3>客製訂作</h3></a></li>
                        <li class="nav-item"><a href="about"><h3>關於e+</h3></a></li>
                        <li class="nav-item"><a href="faq"><h3>常見問題</h3></a></li>
                        <li class="nav-item"><a href="contact"><h3>聯絡我們</h3></a></li>
                        <li class="nav-item"><a href="wish"><h3>追蹤清單</h3></a></li>
                        <?if($config['facebook_url']):?><li class="nav-item"><a href="<?=$config['facebook_url']?>"><h3>facebook</h3></a></li><?endif;?>
                        <?if($config['yahoo_bid_url']):?><li class="nav-item"><a href="<?=$config['yahoo_bid_url']?>"><h3>Yahoo賣場</h3></a></li><?endif;?>
                    </ul>
              </nav>
            </aside>
        </div>
    </header>
    <div><?=$container?></div>
    <footer id="footer">
        <div class="inbox clearfix">
            <div class="box3">
                <h4>關於e+</h4>
                <p><?=$config['company_description']?></p>
            </div>
            <div class="box3">
                <h4>類別總覽</h4>
                <ul>
                    <? foreach($pcategories as $i => $pcategory): ?>
                    <li><a href="product/<?=$pcategory['id']?>"><?=$pcategory['name']?></a></li>
                    <? endforeach ?>
                </ul>
            </div>
            <div class="box3">
                <h4>聯絡我們</h4>
                <ul>
                    <li><?=$config['company_email']?></li>
                    <li><?=$config['company_address']?></li>
                    <li>Tel：<?=$config['company_telephone']?></li>
                    <li>Fax：<?=$config['company_fax']?></li>
                </ul>
            </div>
            <div class="cmmtyIcon clearfix">
                <?if($config['yahoo_bid_url']):?><a href="<?=$config['yahoo_bid_url']?>" target="_blank">yahoo</a><?endif;?>
                <?if($config['facebook_url']):?><a class="fb" href="<?=$config['facebook_url']?>" target="_blank">facebook</a><?endif;?>
                <?if($config['youtube_url']):?><a class="yt" href="<?=$config['youtube_url']?>" target="_blank">youtube</a><?endif;?>
                <?if($config['instagram_url']):?><a class="ig" href="<?=$config['instagram_url']?>" target="_blank">instagram</a><?endif;?>
            </div>
            <div class="copyright">
                <h5>Copyright©<?=$config['site_name']?>. Designed by <a href="http://fishsaut.com/">Fishsaut</a></h5>
            </div>
        </div>
    </footer>
</body>
<?if(isset($json)):?>
<script type="text/javascript"><?=$json?></script>
<?endif;?>
<?foreach((array)$js as $asset):?>
<script type="text/javascript" src="<?=$asset?>"></script>
<?endforeach;?>
<!--[if lt IE 9]>
<?foreach((array)$js_lt_ie9 as $asset):?>
<script type="text/javascript" src="<?=$asset?>"></script>
<?endforeach;?>
<![endif]-->
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', '<?=$config['google_analysis_tracking_id']?>', 'auto');ga('send', 'pageview');</script>