<div style="text-align: center;background: #0071b9;width: 100%;height: 50px;padding: 10px 0;margin: 0 auto;">
    <a href="<?=site_url('tw')?>">
        <img src="<?=site_url('assets/images/primary/main_logo.jpg')?>" alt="Logo">
    </a>
</div>
<hr>
<table border="0" cellpadding="0" cellspacing="0" style="width:800px;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">
    <tr>
        <td style="color:#000000;font-size:15px;font-weight:bold;background-color:#f3f3f3;margin:15px 5px 15px 5px;">
            訂單編號  : <?=$order['no']?>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">購買人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['buyer_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['buyer_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['buyer_telephone']?></th>
    </tr>
    <tr>
        <th width="15%">地址</th>
        <th><?=$order['buyer_zip'].$order['buyer_address']?></th>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">收件人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['recipient_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['recipient_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['recipient_telephone']?></th>
    </tr>
    <?if($order['recipient_address']):?>
    <tr>
        <th width="15%">地址</th>
        <th>
            <?=$order['recipient_zip'].$order['recipient_address']?>
        </th>
    </tr>
    <?endif;?>
    <?if($order['cvs_name']):?>
    <tr>
        <th width="15%">超商</th>
        <th>
            <?=($order['cvs_code'] ? $order['cvs_code'].'<br>' : '')?>
            <?=($order['cvs_name'] ? $order['cvs_name'].'<br>' : '')?>
            <?=($order['cvs_telephone'] ? $order['cvs_telephone'].'<br>' : '')?>
            <?=($order['cvs_address'] ? $order['cvs_zip'].$order['cvs_address'] : '')?>
        </th>
    </tr>
    <?endif;?>
    <tr>
        <th width="15%">訂購備註</th>
        <th><?=$order['recipient_note']?></th>
    </tr>
</table>
<?=$order['buyer_name']?> 您好!<br>
您訂單已為您取消<br>
謝謝<br>