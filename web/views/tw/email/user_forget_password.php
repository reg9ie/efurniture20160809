<div style="text-align: center;background: #0071b9;width: 100%;height: 50px;padding: 10px 0;margin: 0 auto;">
    <a href="<?=site_url('tw')?>">
        <img src="<?=site_url('assets/images/primary/main_logo.jpg')?>" alt="Logo">
    </a>
</div>
<hr>
<table border="0" cellpadding="0" cellspacing="0" style="width:800px;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">
    <tr align="left">
        <td style="color:#000000;font-size:15px;font-weight:bold;background-color:#f3f3f3;margin:15px 5px 15px 5px;">
            重新設定新密碼
        </td>
    </tr>
</table>
<?=$user['name']?> 您好!<br>
您的會員資料如下<br>
帳號: <?=$user['account']?><br>
新密碼: <?=$user['password']?><br>
<br>
請妥善保管你的帳號及密碼<br>
建議您立即登入會員系統，重新修改您想要的密碼<br>
<a href="<?=site_url('tw')?>" target="_blank">立即前往官網 <?=site_url('tw')?></a>