<div style="text-align: center;background: #0071b9;width: 100%;height: 50px;padding: 10px 0;margin: 0 auto;">
    <a href="<?=site_url('tw')?>">
        <img src="<?=site_url('assets/images/primary/main_logo.jpg')?>" alt="Logo">
    </a>
</div>
<hr>
<table border="0" cellpadding="0" cellspacing="0" style="width:800px;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">
    <tr>
        <td style="color:#000000;font-size:15px;font-weight:bold;background-color:#f3f3f3;margin:15px 5px 15px 5px;">
            訂單編號  : <?=$order['no']?>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">購買人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['buyer_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['buyer_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['buyer_telephone']?></th>
    </tr>
    <tr>
        <th width="15%">地址</th>
        <th><?=$order['buyer_zip'].$order['buyer_address']?></th>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">收件人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['recipient_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['recipient_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['recipient_telephone']?></th>
    </tr>
    <?if($order['recipient_address']):?>
    <tr>
        <th width="15%">地址</th>
        <th><?=$order['recipient_zip'].$order['recipient_address']?></th>
    </tr>
    <?endif;?>
    <?if($order['cvs_name']):?>
    <tr>
        <th width="15%">超商</th>
        <th>
            <?=($order['cvs_code'] ? $order['cvs_code'].'<br>' : '')?>
            <?=($order['cvs_name'] ? $order['cvs_name'].'<br>' : '')?>
            <?=($order['cvs_telephone'] ? $order['cvs_telephone'].'<br>' : '')?>
            <?=($order['cvs_address'] ? $order['cvs_address'] : '')?>
        </th>
    </tr>
    <?endif;?>
    <tr>
        <th width="15%">訂購備註</th>
        <th><?=$order['recipient_note']?></th>
    </tr>
</table>
<br>
<?=$order['buyer_name']?> 您好<br>
您的訂單已於今日出貨<br>
您可透過到官網 查詢您的訂單。<br>
您的訂單商品，將於2日內送達(除了宜蘭、花蓮、台東、偏遠地區及外島外)。<br>
請注意： 您的收件地址必須有人查收<br>
<br>
若是超商取貨請務必於5日內攜帶證件前往取貨<br>
<br>
<br>
歡迎您再次光臨，使用我們所提供的服務。<br>
再次感謝您的購買。<br>
<br>
敬啟<br>