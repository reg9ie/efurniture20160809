<div style="text-align: center;background: #0071b9;width: 100%;height: 50px;padding: 10px 0;margin: 0 auto;">
    <a href="<?=site_url('tw')?>">
        <img src="<?=site_url('assets/images/primary/main_logo.jpg')?>" alt="Logo">
    </a>
</div>
<hr>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf">
    <tr>
        <td style="color:#000000;font-size:15px;font-weight:bold;background-color:#f3f3f3;margin:15px 5px 15px 5px;">
            訂單編號  : <?=$order['no']?>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">購買人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['buyer_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['buyer_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['buyer_telephone']?></th>
    </tr>
    <tr>
        <th width="15%">地址</th>
        <th><?=$order['buyer_zip'].$order['buyer_address']?></th>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">收件人資料</th>
    </tr>
    <tr>
        <th width="15%">姓名</th>
        <th><?=$order['recipient_name']?></th>
    </tr>
    <tr>
        <th width="15%">行動電話</th>
        <th><?=$order['recipient_cellphone']?></th>
    </tr>
    <tr>
        <th width="15%">室內電話</th>
        <th><?=$order['recipient_telephone']?></th>
    </tr>
    <?if($order['recipient_address']):?>
    <tr>
        <th width="15%">地址</th>
        <th><?=$order['recipient_zip'].$order['recipient_address']?></th>
    </tr>
    <?endif;?>
    <?if($order['cvs_name']):?>
    <tr>
        <th width="15%">超商</th>
        <th>
            <?=($order['cvs_code'] ? $order['cvs_code'].'<br>' : '')?>
            <?=($order['cvs_name'] ? $order['cvs_name'].'<br>' : '')?>
            <?=($order['cvs_telephone'] ? $order['cvs_telephone'].'<br>' : '')?>
            <?=($order['cvs_address'] ? $order['cvs_zip'].$order['cvs_address'] : '')?>
        </th>
    </tr>
    <?endif;?>
    <tr>
        <th width="15%">訂購備註</th>
        <th><?=$order['recipient_note']?></th>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <th colspan="2">購物說明</th>
    </tr>
    <tr>
        <th width="15%">付款說明</th>
        <th>
            <div>銀行匯款資訊，帳號如下所示</div>
            <br/>
            <div>銀行: <?=$config['company_bank_code']?><?=$config['company_bank_name']?></div>
            <div>賬戶: <?=$config['company_bank_account']?></div>
            <div>戶名: <?=$config['company_bank_account_name']?></div>
            <br/>
            <div>付款完成後 <a href="mailto:<?=$config['email_from']?>"><?=$config['email_from']?></a> </div>
            <div>請登入會員專區利用訂單查詢輸入您的匯款訊息，待確認您的款項入帳後，將立即為您追加。</div>
        </th>
    </tr>
    <tr>
        <th width="15%">注意事項</th>
        <th>
            <div>感謝您的支持與愛護，為加速我們為您服務速度，請您盡快完成付款流程！</div>
            <div>若有任何訂單上處理的問題，歡迎您來信 <a href="mailto:<?=$config['email_from']?>"><?=$config['email_from']?></a></div>
            <div>我們會盡快回覆您！謝謝</div>
            <br/>
            <div>近期由於系統更新，如果遇到線上購物未成功的狀況，請勿再試</div>
            <div>建議可先 Email 或 FB 私訊我們確認是否有刷成功</div>
        </th>
    </tr>
</table>