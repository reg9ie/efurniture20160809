<div style="text-align: center;background: #0071b9;width: 100%;height: 50px;padding: 10px 0;margin: 0 auto;">
    <a href="<?=site_url('tw')?>">
        <img src="<?=site_url('assets/images/primary/main_logo.jpg')?>" alt="Logo">
    </a>
</div>
<hr>
<div style="padding:10px;border:1px solid #eee;border-radius: 3px;border-left:5px solid #5bc0de;">
    日期: <?=$date?><br>
    聯絡人: <?=$name?><br>
    Email: <?=$email?><br>
    聯絡電話: <?=$telephone?><br>
    名稱: <?=$title?><br>
    內容: <?=$content?>
</div>
<div style="padding:10px;border:1px solid #eee;border-radius: 3px;margin-top:20px;">
    回應: <?=$reply_content?>
</div>