<article id="cont">
    <div id="googleMap" style="width:100%;"></div>
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li class="last"><a href="contact">聯絡我們</a></li>
        </ul>
    </div>
</article>

<article id="cont">
    <div class="inbox cttBox">
        <h2 class="idxTit08">如何找到我們</h2>
        <div class="box3">
            <span class="pageTitle03 clearfix">
                <span class="circle">
                <div class="circleIcon circleIcon10">
                </div>
                </span>
                <h3>地址</h3>
            </span>
            <div class="cBox">
                <h5><?=$config['company_address']?></h5>
                <br>
                <h5><?=nl2br($config['company_address_description'])?></h5>
            </div>
        </div>
        <div class="box3">
            <span class="pageTitle03 clearfix">
                <span class="circle">
                <div class="circleIcon circleIcon11">
                </div>
                </span>
                <h3>電話、傳真</h3>
            </span>
            <div class="cBox">
                <br>
                <h5>Tel <?=$config['company_telephone']?></h5>
                <h5>Fax <?=$config['company_fax']?></h5>
                <br>
            </div>
        </div>
        <div class="box3">
            <span class="pageTitle03 clearfix">
                <span class="circle">
                <div class="circleIcon circleIcon12">
                </div>
                </span>
                <h3>營業時間</h3>
            </span>
            <div class="cBox">
                <br>
                <h5><?=nl2br($config['company_time'])?></h5>
                <br>
            </div>
        </div>
    </div>
</article>

<article id="cont" class="bg-e8eef1">
    <div class="inbox">
        <div class="ctt_form clearfix" ng-controller="contact" ng-init="init()">
            <div class="tt"><input type="text" placeholder="姓名*" ng-model="contact.name"></div>
            <div class="tt"><input type="text" placeholder="電話" ng-model="contact.telephone"></div>
            <div class="tt"><input type="text" placeholder="其他聯絡方式" ng-model="contact.cellphone"></div>
            <div class="tt2"><input type="text" placeholder="標題*" ng-model="contact.title"></div>
            <div class="tt_area clearfix">
                <textarea placeholder="輸入您的訊息*" ng-model="contact.content"></textarea>
            </div>
            <div class="sumt"><input type="submit" value="送出" ng-click="send(contact)"></div>
        </div>
    </div>
</article>