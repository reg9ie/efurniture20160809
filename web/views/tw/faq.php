<article id="pageBanner" class="mobileHide">
    <img src="assets/images/main/bxslider-01.png">
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li class="last"><a href="faq">常見問題</a></li>
        </ul>
    </div>
</article>

<article id="cont" class="faq">
    <div class="inbox faqbox">
        <? foreach($data['acategories'] as $i => $acategory): ?>
        <div class="box3">
            <span class="pageTitle03 clearfix">
                <a href="faq/<?=$acategory['id']?>">
                    <span class="circle"><div class="circleIcon <?=( $i > 0 ? 'circleIcon'.($i+1) : '')?>"></div></span>
                    <h3><?=$acategory['name']?></h3>
                </a>
            </span>
            <div class="qBox">
                <? foreach($acategory['articles'] as $j => $article): ?>
                <a href="faq/<?=$acategory['id']?>/<?=$article['id']?>"><?=$article['name']?></a>
                <? endforeach ?>
            </div>
            <span class="qMore clearfix">
                <a href="faq/<?=$acategory['id']?>">查看所有問題<span>➔</span></a>
            </span>
        </div>
        <? endforeach ?>
    </div>
</article>