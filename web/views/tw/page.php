<div class="carousel_wrapper">
    <div class="main_carousel">
        <?foreach($data['page']['images'] as $m):?>
        <div><img  src="<?=$m['_filename']?>"/></div>
        <?endforeach;?>
    </div>
    <a class="carousel_next"><img src="assets/images/primary/main_carousel_next.png" alt="" /></a>
    <a class="carousel_prev"><img src="assets/images/primary/main_carousel_prev.png" alt="" /></a>
</div>
<div class="content_wrapper" id="about">
    <div class="container"><?=$data['page']['content']?></div>
</div>