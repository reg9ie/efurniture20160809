<article id="pageBanner" class="mobileHide">
    <img src="assets/images/main/bxslider-01.png">
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li><a href="faq">常見問題</a></li>
            <li class="last"><a href="faq/<?=$data['acategory']['id']?>"><?=$data['acategory']['name']?></a></li>
        </ul>
    </div>
</article>

<article id="cont" class="faq">
    <div class="inbox">
        <span class="pageTitle03 clearfix">
                <span class="circle">
                <div class="circleIcon">
                </div>
                </span>
                <h3><?=$data['acategory']['name']?></h3>
        </span>
        <div class="qCont">
            <ul>
                <? foreach($data['articles'] as $i => $article): ?>
                <li><a href="faq/<?=$data['acategory']['id']?>/<?=$article['id']?>"><h4><?=$article['name']?></h4></a></li>
                <? endforeach ?>
            </ul>
        </div>
    </div>
</article>

<article id="cont" class="qCont2">
    <div class="inbox">
        <? foreach($data['acategories'] as $i => $acategory): ?>
            <span class="pageTitle03 clearfix">
                <a href="faq/<?=$acategory['id']?>">
                    <span class="circle"><div class="circleIcon <?=( $i > 0 ? 'circleIcon'.($i+1) : '')?>"></div></span>
                    <h3><?=$acategory['name']?></h3>
                </a>
            </span>
        <? endforeach ?>
    </div>
</article>