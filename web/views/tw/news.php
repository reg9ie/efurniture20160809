<div class="carousel_wrapper">
    <div class="main_carousel">
        <?foreach($data['page']['images'] as $m):?>
        <div><img  src="<?=$m['_filename']?>"/></div>
        <?endforeach;?>
    </div>
    <a class="carousel_next"><img src="assets/images/primary/main_carousel_next.png" alt="" /></a>
    <a class="carousel_prev"><img src="assets/images/primary/main_carousel_prev.png" alt="" /></a>
</div>
<div class="content_wrapper">
    <div class="container">
        <div class="content_title"> <img src="assets/images/primary/news_icon.png" alt=""> 最新消息 </div>
        <? if($data['news']): ?>
        <div class="news_content">
            <div class="news_title">
                <div class="news_name"><?=$data['news']['name']?></div>
                <div class="news_type"><?=$data['news']['ntype_name']?></div>
                <div class="news_date"><?=$data['news']['_created_at']?></div>
            </div>
            <div class="news_text"><?=$data['news']['content']?></div>
        </div>
        <? endif ?>
        <div class="news_sidelist">
            <div class="table_list">
                <!-- <div class="news_side_tit"><?=date('Y')?></div> -->
                <?foreach($data['newses'] as $i => $n):?>
                <a href="news/<?=$n['id']?>" class="list_row">
                    <div class="table_date"><?=$n['_created_at']?></div>
                    <div class="table_type"> <span><?=$n['ntype_name']?></span> </div>
                    <div class="table_title"><?=$n['name']?></div>
                </a>
                <?endforeach?>
            </div>
        </div>
    </div>
</div>