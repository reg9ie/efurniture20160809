<article id="pageBanner" class="mobileHide">
    <img src="assets/images/main/bxslider-01.png">
</article>

<article id="breadcrumbs">
    <div class="inbox">
        <ul class="crumbs clearfix">
            <li><a href="<?=site_url()?>">首頁</a></li>
            <li class="last"><a href="custom">客製訂做</a></li>
        </ul>
    </div>
</article>

<div class="products">
    <article id="cont" class="pdtBox">
        <div class="inbox columns">
            <? foreach($data['pcategories'] as $i => $p): ?>
            <div class="col4 col-sm-12 col-xs-12 progroup">
                <a href="custom/<?=$p['id']?>">
                    <div class="col12 col-sm-5 col-xs-12 ">
                        <img src="<?=$p['_image']?>" alt="">
                    </div>
                    <div class="col12 col-sm-7 col-xs-12 progroup-item">
                        <h5><?=$p['name']?></h5>
                        <p><?=nl2br($p['description'])?></p>
                    </div>
                </a>
            </div>
            <? endforeach ?>
        </div>
    </article>
</div>