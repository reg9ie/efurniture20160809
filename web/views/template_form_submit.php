<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <form id="form" action="<?=$data['postURL']?>" method="post" name="main">
        <?foreach((array)$data['input'] as $k => $v):?>
        <input type="hidden" name="<?=$k?>" value="<?=$v?>">
        <?endforeach;?>
    </form>
</body>
</html>
<script type="text/javascript">
function submit() {
    document.getElementById('form').submit();
}
window.onload = submit;
</script>