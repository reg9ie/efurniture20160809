<div>
    <div class="toolbar"></div>
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="col-md-2">名稱</td>
            <td class="col-md-8">路徑</td>
            <td class="col-md-1 text-right">中文</td>
            <td class="col-md-1">英文</td>
        </tr>
        <?foreach((array)$data['emails'] as $i => $e):?>
        <tr>
            <td><?=$i+1?></td>
            <td><?=$e['name']?></td>
            <td><?=$e['filename']?></td>
            <td>
                <p class="pull-right">
                    <a class="btn btn-primary" href="admin/template/email_edit/tw/<?=$e['filename']?>"><i class="fa fa-pencil-square-o"></i></a>
                </p>
            </td>
            <td>
                <a class="btn btn-primary" href="admin/template/email_edit/en/<?=$e['filename']?>"><i class="fa fa-pencil-square-o"></i></a>
            </td>
        </tr>
        <?endforeach;?>
    </table>
</div>