<div ng-controller="template" ng-init="init_edit()">
    <div class="toolbar"></div>
    <form class="form-horizontal well" method="post" action="<?=current_url()?>">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4"><?=$data['email']['name']?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputUri">檔名</label>
            <div class="col-md-4"><?=$data['email']['filename']?></div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputHTML">HTML</label>
            <div class="col-md-10">
                <textarea name="html" placeholder="HTML" rows="40"><?=$data['email']['html']?></textarea>
                <div class="form-control" id="inputHTML"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <input type="submit" class="btn btn-primary" value="確定">
                <a class="btn btn-default" href="admin/template/lists">取消</a>
            </div>
        </div>
    </form>
</div>