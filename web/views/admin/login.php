<div class="row" id="login">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <form class="login-form" method="post" action="<?=current_url()?>" autocomplete="off">
            <div class="form-group">
                <input class="form-control login-field" type="text" name="account" placeholder="帳號">
                <label class="login-field-icon fui-user" for="login-name"></label>
            </div>
            <div class="form-group">
                <input class="form-control login-field" type="password" name="password" placeholder="密碼">
                <label class="login-field-icon fui-lock"></label>
            </div>
            <div class="text-center">
                <button class="btn btn-block btn-primary" type="submit">登入</button>
            </div>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>