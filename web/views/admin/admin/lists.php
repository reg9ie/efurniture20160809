<div class="ng-cloak" ng-cloak ng-controller="admin" ng-init="init_lists()">
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#user" data-toggle="tab">帳號</a></li>
        <li><a href="#role" data-toggle="tab">權限</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="user">
            <div class="row">
                <div class="col-md-2">
                    <input type="text" class="form-control" id="inputSearchText" placeholder="名稱" ng-model="search.text" ng-change="user_get(search, 1)" >
                </div>
                <div class="col-md-1 col-md-offset-9">
                    <a class="btn btn-info pull-right" href="admin/admin/user_add">新增</a>
                </div>
            </div>
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td class="">#</td>
                    <td class="col-md-2">姓名</td>
                    <td class="col-md-2">帳號</td>
                    <td class="col-md-2">角色</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-2">註冊</td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in users">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="e.name"></td>
                    <td ng-bind="e.account"></td>
                    <td ng-bind="e._role_name"></td>
                    <td></td>
                    <td ng-bind="e.created_at"></td>
                    <td>
                        <label class="checkbox" for="checkboxEnable">
                            <input type="checkbox" id="checkboxEnable{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="user_edit(e)" ng-checkbox>
                        </label>
                        <div class="pull-right">
                            <a class="btn btn-primary" ng-href="admin/admin/user_edit/{{e.id}}">編輯</a>
                            <button type="button" class="btn btn-danger" ng-click="user_del(e.id)" ng-show="users.length > 1">刪除</button>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="text-center">
                <div class="pull-left">
                    <small>Total: <span ng-bind="pagination.total"></span></small>
                </div>
                <ul class="pagination">
                    <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="user_get(search, pagination.previous)">&lsaquo;</a></li>
                    <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="user_get(search, pagination.first)">1</a></li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
                    <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                        <a href="#" ng-bind="p" ng-click="user_get(search, p)"></a>
                    </li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="user_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="user_get(search, pagination.next)">&rsaquo;</a></li>
                </ul>
                <div class="pull-right" style="width: 90px;">
                    <input class="form-control" id="paginationPage" type="text" placeholder="頁數" ng-model="pagination.page" ng-change="user_get(search, pagination.page)" >
                </div>
            </div>
        </div>
        <div class="tab-pane" id="role">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td class="">#</td>
                    <td class="col-md-2">名稱</td>
                    <td class="col-md-2">代號</td>
                    <td class="col-md-7">權限</td>
                    <td class="col-md-1"></td>
                </tr>
                <tr ng-repeat="r in roles">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="r.name"></td>
                    <td ng-bind="r.code"></td>
                    <td>
                        <div ng-repeat="p in r.permissions">
                            <label class="checkbox" for="checkboxEnable">
                                <input type="checkbox" id="checkboxPermission{{r.id}}_{{p.id}}" data-toggle="checkbox" ng-model="p.enable" ng-change="permission_save(p)" ng-checkbox>
                                <span ng-bind="p.name"></span>
                            </label>
                        </div>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>