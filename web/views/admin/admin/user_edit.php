<div class="ng-cloak" ng-cloak ng-controller="admin" ng-init="init_edit()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" href="admin/admin/user_add">新增</a>
    </div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputAccount">帳號</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputAccount" placeholder="帳號" ng-model="user.account">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputPassword">密碼</label>
            <div class="col-md-4">
                <input type="password" class="form-control" id="inputPassword" placeholder="密碼" ng-model="user.password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">姓名</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="姓名" ng-model="user.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEmail">Email</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputEmail" placeholder="Email" ng-model="user.email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputRole">角色</label>
            <div class="col-md-4">
                <select class="selectpicker" id="selectRole" ng-model="user.role_id" ng-select>
                    <option value="{{r.id}}" title="{{r.name}}" ng-repeat="r in roles" ng-bind="r.name"></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="checkboxEnable">啟用</label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxEnable"><input type="checkbox" id="checkboxEnable" data-toggle="checkbox" ng-model="user.enable" ng-checkbox></label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="user_edit(user)">確定</button>
                <a class="btn btn-default" href="admin/admin#user">取消</a>
            </div>
        </div>
    </form>
</div>