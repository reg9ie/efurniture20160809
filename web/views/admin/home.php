<div ng-controller="home" ng-init="init()">
    <div class="well" id="analysis">
        <div class="page-header">
            <strong>Google Analysis 流量分析</strong>
        </div>
        <div id="chart" style="padding-bottom:20px;"></div>
        <div class="well" style="margin-bottom:0;">
            <div class="row">
                <div class="col-md-3">
                    <br/>
                    <div class="text-center muted type"><small>拜訪者</small></div>
                    <div class="text-center muted type"><small>瀏覽頁面</small></div>
                </div>
                <div class="col-md-3">
                    <div class="text-center day"><small>今天</small></div>
                    <div class="text-center"><strong><?=$data['analysis']['today']['visits']?></strong></div>
                    <div class="text-center"><strong><?=$data['analysis']['today']['pageviews']?></strong></div>
                </div>
                <div class="col-md-3">
                    <div class="text-center day"><small>昨天</small></div>
                    <div class="text-center"><strong><?=$data['analysis']['yesterday']['visits']?></strong></div>
                    <div class="text-center"><strong><?=$data['analysis']['yesterday']['pageviews']?></strong></div>
                </div>
                <div class="col-md-3">
                    <div class="text-center day"><small>30天累計</small></div>
                    <div class="text-center"><strong><?=$data['analysis']['all']['visits']?></strong></div>
                    <div class="text-center"><strong><?=$data['analysis']['all']['pageviews']?></strong></div>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-md-2">
                    <div>
                        <small>Mail Queue</small>
                        <small class="pull-right"><?=$data['sysinfo']['mail_queue_count']?></small>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <small>ONLINE</small>
                        <small class="pull-right"><?=$data['sysinfo']['online_number']?></small>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?=$data['sysinfo']['online_percent']?>%"><?=$data['sysinfo']['online_percent']?>%</div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <small>CPU</small>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?=$data['sysinfo']['cpu_percent']?>%"><?=$data['sysinfo']['cpu_percent']?>%</div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <small>RAM</small>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?=$data['sysinfo']['memory_percent']?>%"><?=$data['sysinfo']['memory_percent']?>%</div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <small>SWAP</small>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?=$data['sysinfo']['swap_percent']?>%"><?=$data['sysinfo']['swap_percent']?>%</div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div>
                        <small>Disk</small>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:<?=$data['sysinfo']['disk_percent']?>%"><?=$data['sysinfo']['disk_percent']?>%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var analysis_results = [
<?foreach((array)$data['analysis']['results'] as $result):?>
["<?=date('m/d', strtotime($result->getDate()))?>", <?=$result->getVisits()?> ,<?=$result->getPageviews()?>],
<?endforeach;?>
];
</script>