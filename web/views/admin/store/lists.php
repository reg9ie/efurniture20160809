<div ng-controller="store" ng-init="init_lists()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" id="add_btn" href="admin/store/add">新增</a>
    </div>
    <div class="tab-pane active" id="store">
        <table class="table table-striped table-condensed table-hover">
            <tr>
                <td>#</td>
                <td class="col-md-1"></td>
                <td class="col-md-3">名稱</td>
                <td class="col-md-2">電話</td>
                <td class="col-md-2">營業時間</td>
                <td class="col-md-1"></td>
                <td class="col-md-4"></td>
            </tr>
            <tr ng-repeat="s in stores">
                <td><label ng-bind="$index+1"></label></td>
                <td ><img ng-src="{{s._image_thumb}}" style="max-width:200px;max-height:50px"></td>
                <td><label ng-bind="s.name"></label></td>
                <td><label ng-bind="s.telephone"></label></td>
                <td><label ng-bind="s.time"></label></td>
                <td></td>
                <td>
                    <label class="checkbox pull-left" for="checkboxImageEnable{{s.id}}">
                        <input type="checkbox" id="checkboxImageEnable{{s.id}}" data-toggle="checkbox" ng-model="s.enable" ng-change="enable_store(i)" ng-checkbox>
                    </label>
                    <button type="button" class="btn" ng-click="move_up_store(i)"><i class="fa fa-chevron-up"></i></button>
                    <button type="button" class="btn" ng-click="move_down_store(i)"><i class="fa fa-chevron-down"></i></button>
                    <p class="pull-right">
                        <a class="btn btn-info" ng-href="admin/store/edit/{{s.id}}"><i class="fa fa-pencil-square-o"></i></a>
                        <button type="button" class="btn btn-danger" ng-click="del_store(i)"><i class="fa fa-trash-o"></i></button>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</div>