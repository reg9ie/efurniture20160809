<div ng-controller="store" ng-init="init_edit()">
    <div class="toolbar"></div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputImage">圖片</label>
            <div class="col-md-4">
                <div><img ng-show="store.image" ng-src="{{store._image}}" style="max-width:400px;max-height:100px;"></div>
                <div>
                    <input class="btn" type="button" value="上傳圖片" ng-click="upload_store_image(store)">
                    <input class="btn" type="button" value="刪除圖片" ng-click="del_store_image(store)">
                </div>
                <label>建議尺寸 181x181</label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="store.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTelephone">電話</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputTelephone" placeholder="電話" ng-model="store.telephone">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTime">時間</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputTime" placeholder="時間" ng-model="store.time">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputAddress">地址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputAddress" placeholder="地址" ng-model="store.address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputAddress_eng">英文地址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputAddress_eng" placeholder="英文地址" ng-model="store.address_eng">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputUrl">網址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputUrl" placeholder="網址" ng-model="store.url">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEnable"></label>
            <div class="col-md-4">
                <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="store.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="edit_store(store)">確定</button>
                <a class="btn btn-danger" href="admin/store">取消</a>
            </div>
        </div>
        <input type="file" name="file" id="file" style="display:none;">
    </form>
</div>