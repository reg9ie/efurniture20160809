<div class="ng-cloak" ng-cloak ng-controller="download" ng-init="init_download()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" href="admin/download/download_add">新增</a>
    </div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="download.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputFile">檔案</label>
            <div class="col-md-4">
                <div ng-show="download.file" ng-bind="download.file"></div>
                <div>
                    <input class="btn" type="button" value="上傳檔案" ng-click="download_file_upload(download)">
                    <input class="btn" type="button" value="刪除檔案" ng-click="download_file_delete(download)">
                </div>
                <label>檔案限制: PDF 20MB</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4 col-md-offset-2">
                <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="download.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="download_edit(download)">確定</button>
                <a class="btn btn-danger" href="admin/download">取消</a>
            </div>
        </div>
    </form>
</div>