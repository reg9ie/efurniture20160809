<div ng-controller="point" ng-init="init_edit()">
    <div class="toolbar"></div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2">使用者</label>
            <div class="col-md-4">
                <label ng-bind="point.user_name"></label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="point.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputPoint">購物金</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputPoint" placeholder="購物金" ng-model="point.point">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="add_point(point)">確定</button>
                <a class="btn btn-default" href="admin/point">取消</a>
            </div>
        </div>
    </form>
</div>