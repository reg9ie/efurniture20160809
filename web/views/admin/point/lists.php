<div ng-controller="point" ng-init="init_lists()">
    <div class="row">
        <div class="col-md-2">
            <input type="text" class="form-control" id="inputSearchText" placeholder="名稱" ng-model="search.text" ng-change="get_point(search, 1)" >
        </div>
    </div>
    <div class="tab-pane" id="point">
        <table class="table table-striped table-condensed table-hover">
            <tr>
                <td>#</td>
                <td class="col-md-2">姓名</td>
                <td class="col-md-2">名稱</td>
                <td class="col-md-1">購物金</td>
                <td class="col-md-2">日期</td>
                <td class="col-md-3"></td>
                <td class="col-md-2"></td>
            </tr>
            <tr ng-repeat="p in points">
                <td ng-bind="$index+1"></td>
                <td ng-bind="p.user_name"></td>
                <td ng-bind="p.name"></td>
                <td ng-bind="p.point"></td>
                <td ng-bind="p.created_at"></td>
                <td></td>
                <td>
                    <div class="pull-right">
                        <a class="btn btn-primary" ng-href="admin/point/edit/{{p.id}}">編輯</a>
                        <button type="button" class="btn btn-danger" ng-click="del_point(p)">刪除</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="text-center">
        <div class="pull-left">
            <small>Total: <span ng-bind="pagination.total"></span></small>
        </div>
        <ul class="pagination">
            <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="get_point(search, pagination.previous)">&lsaquo;</a></li>
            <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="get_point(search, pagination.first)">1</a></li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
            <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                <a href="#" ng-bind="p" ng-click="get_point(search, p)"></a>
            </li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="get_point(search, pagination.last)" ng-bind="pagination.last"></a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="get_point(search, pagination.next)">&rsaquo;</a></li>
        </ul>
        <div class="pull-right" style="width: 90px;">
            <input class="form-control" id="paginationPage" type="text" placeholder="頁數" ng-model="pagination.page" ng-change="get_point(search, pagination.page)" >
        </div>
    </div>
</div>