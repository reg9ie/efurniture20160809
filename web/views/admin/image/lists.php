<div class="ng-cloak" ng-cloak ng-controller="image" ng-init="init_lists()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" id="add_btn" href="admin/image/image_add">新增</a>
    </div>
    <div class="tab-pane active" id="image">
        <table class="table table-striped table-condensed table-hover">
            <tr>
                <td>#</td>
                <td class="col-md-1"></td>
                <td class="col-md-5">頁面</td>
                <td class="col-md-2">日期</td>
                <td class="col-md-1"></td>
                <td class="col-md-3"></td>
            </tr>
            <tr ng-repeat="i in images">
                <td><label ng-bind="$index+1"></label></td>
                <td ><img ng-src="{{i._image}}" style="max-width:200px;max-height:50px"></td>
                <td><label ng-bind="i.name"></label></td>
                <td><label ng-bind="i.updated_at"></label></td>
                <td></td>
                <td>
                    <label class="checkbox pull-left" for="checkboxImageEnable{{i.id}}">
                        <input type="checkbox" id="checkboxImageEnable{{i.id}}" data-toggle="checkbox" ng-model="i.enable" ng-change="image_enable(i)" ng-checkbox>
                    </label>
                    <button type="button" class="btn" ng-click="image_move_up(i)"><i class="fa fa-chevron-up"></i></button>
                    <button type="button" class="btn" ng-click="image_move_down(i)"><i class="fa fa-chevron-down"></i></button>
                    <p class="pull-right">
                        <a class="btn btn-info" ng-href="admin/image/image_edit/{{i.id}}"><i class="fa fa-pencil-square-o"></i></a>
                        <button type="button" class="btn btn-danger" ng-click="image_del(i)"><i class="fa fa-trash-o"></i></button>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</div>