<div class="ng-cloak" ng-cloak ng-controller="image" ng-init="init_image()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" id="add_btn" href="admin/image/image_add">新增</a>
    </div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputImage">圖片</label>
            <div class="col-md-4">
                <div><img ng-show="image.image" ng-src="{{image._image}}" style="max-width:400px;max-height:100px;"></div>
                <div>
                    <input class="btn" type="button" value="上傳圖片" ng-click="image_upload_image(image)">
                    <input class="btn" type="button" value="刪除圖片" ng-click="image_del_image(image)">
                    <label>建議尺寸 首頁:1920x770, 內頁:1920x480</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="image.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputUrl">網址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputUrl" placeholder="網址" ng-model="image.url">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEnable"></label>
            <div class="col-md-4">
                <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="image.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="image_edit(image)">確定</button>
                <a class="btn btn-danger" href="admin/image">取消</a>
            </div>
        </div>
    </form>
</div>