<form class="ng-cloak" ng-cloak ng-controller="config" ng-init="init_edit()">
    <div class="pull-right">
        <button class="btn btn-info" id="emails_add_btn" type="button" ng-click="mail_account_add()">新增</button>
    </div>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#site" data-toggle="tab">網站</a></li>
        <li><a href="#companies" data-toggle="tab">公司</a></li>
        <li><a href="#emails" data-toggle="tab">郵件</a></li>
        <li><a href="#socials" data-toggle="tab">社群</a></li>
        <li><a href="#google" data-toggle="tab">Google</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="site">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputSite_name">網站名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputSite_name" placeholder="網站名稱" ng-model="config.site_name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputSite_keywords">網站關鍵字</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputSite_keywords" placeholder="網站關鍵字" ng-model="config.site_keywords">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputSite_description">網站描述</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputSite_description" placeholder="網站描述" ng-model="config.site_description">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputAdmin_email">管理員 Email</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputAdmin_email" placeholder="管理員 Email" ng-model="config.admin_email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="edit_config()">確定</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="companies">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_name">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputCompany_name" placeholder="名稱" ng-model="config.company_name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_address">地址</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputCompany_address" placeholder="地址" ng-model="config.company_address">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_address_description">地址方位描述</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputCompany_address_description" placeholder="地址方位描述" rows="4" ng-model="config.company_address_description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_telephone">電話</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputCompany_telephone" placeholder="電話" ng-model="config.company_telephone">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_fax">傳真</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputCompany_fax" placeholder="傳真" ng-model="config.company_fax">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_email">Email</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputCompany_email" placeholder="Email" ng-model="config.company_email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_time">營業時間</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputCompany_time" placeholder="營業時間" rows="2" ng-model="config.company_time"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCompany_description">公司描述</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputCompany_description" placeholder="公司描述" rows="4" ng-model="config.company_description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="edit_config()">確定</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="emails">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-3">帳號</td>
                    <td class="col-md-3">主機</td>
                    <td class="col-md-1 text-right">每日限制</td>
                    <td class="col-md-1 text-right">今日發信</td>
                    <td class="col-md-1 text-right">昨日發信</td>
                    <td class="col-md-1 text-right">發信總計</td>
                    <td class="col-md-2"></td>
                </tr>
                <tr ng-repeat="e in mail_accounts">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="e.account"></td>
                    <td ng-bind="e.host"></td>
                    <td class="text-right" ng-bind="e.limit"></td>
                    <td class="text-right" ng-bind="e.today_times"></td>
                    <td class="text-right" ng-bind="e.yestoday_times"></td>
                    <td class="text-right" ng-bind="e.total_times"></td>
                    <td>
                        <label class="checkbox" for="checkboxEmailsEnable{{e.id}}">
                            <input type="checkbox" id="checkboxEmailsEnable{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="mail_account_enable(e)" ng-checkbox>
                        </label>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" ng-click="mail_account_edit(e)"><i class="fa fa-pencil-square-o"></i></button>
                            <button type="button" class="btn btn-danger" ng-click="mail_account_delete(e)"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="tab-pane" id="google">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputGoogle_account">Google 開發者帳號</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputGoogle_analysis_account" placeholder="Google 開發者帳號" ng-model="config.google_analysis_account">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputGoogle_analysis_key_file_name">Google P12憑證</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputGoogle_analysis_key_file_name" placeholder="Google P12憑證" ng-model="config.google_analysis_key_file_name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputGoogle_analysisProfileId">Analysis Profile Id</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputGoogle_analysisProfileId" placeholder="Google Analysis Profile Id" ng-model="config.google_analysis_profile_id">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputGoogle_analysisTrackingId">Analysis Tracking Id</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputGoogle_analysisTrackingId" placeholder="Google Analysis Tracking Id" ng-model="config.google_analysis_tracking_id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="edit_config()">確定</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="socials">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputFacebookUrl">Facebook</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputFacebookUrl" placeholder="Facebook 粉絲專頁" ng-model="config.facebook_url">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputYoutubeUrl">Youtube</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputYoutubeUrl" placeholder="Youtube 頻道" ng-model="config.youtube_url">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputInstagramUrl">Instagram</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputInstagramUrl" placeholder="Instagram" ng-model="config.instagram_url">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputYahooBidUrl">Yahoo 拍賣</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputYahooBidUrl" placeholder="Yahoo 拍賣" ng-model="config.yahoo_bid_url">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="edit_config()">確定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="email_detail" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="form-horizontal" style="margin-top:30px;">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_account">SMTP 帳號</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_account" placeholder="SMTP 帳號" ng-model="mail_account.account">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_password">SMTP 密碼</label>
                            <div class="col-md-4">
                                <input type="password" class="form-control" id="inputEmail_password" placeholder="SMTP 密碼" ng-model="mail_account.password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_host">SMTP 主機</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_host" placeholder="SMTP 主機" ng-model="mail_account.host">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_port">SMTP Port</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_port" placeholder="SMTP Port" ng-model="mail_account.port">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_email">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_email" placeholder="Email" ng-model="mail_account.email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_name">寄件人名稱</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_name" placeholder="寄件人名稱" ng-model="mail_account.name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_cc">副本</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_cc" placeholder="副本" ng-model="mail_account.cc">
                            </div>
                            <div class="col-md-6"><p class="help-block">多筆email同時接收，使用逗號(,)隔開每個email</p></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_bcc">密件副本</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_bcc" placeholder="密件副本" ng-model="mail_account.bcc">
                            </div>
                            <div class="col-md-6"><p class="help-block">多筆email同時接收，使用逗號(,)隔開每個email</p></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_limit">每日限制</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="inputEmail_limit" placeholder="每日限制" ng-model="mail_account.limit">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputEmail_enable">啟用</label>
                            <div class="col-md-4">
                                <label class="checkbox">
                                    <input type="checkbox" data-toggle="checkbox" ng-model="mail_account.enable" ng-checkbox>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-4">
                                <button type="button" class="btn btn-primary" ng-click="mail_account_save(mail_account)">確定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>