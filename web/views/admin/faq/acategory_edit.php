<div class="ng-cloak" ng-cloak ng-controller="faq" ng-init="init_acategory()">
    <a class="btn btn-info pull-right" href="admin/faq/acategory_add">新增</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="acategory.name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">
                        <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="acategory.enable" ng-checkbox>啟用</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="acategory_edit(acategory)">確定</button>
                        <a class="btn btn-danger" href="admin/faq/lists#acategory">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>