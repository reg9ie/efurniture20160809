<div class="ng-cloak" ng-cloak ng-controller="faq" ng-init="init_lists()">
    <a class="btn btn-info pull-right" id="add_btn" href="admin/faq/article_add">新增</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#article" data-toggle="tab">文章</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="article">
            <div class="row">
                <div class="col-md-2">
                    <select class="selectpicker" id="selectAcategory" data-live-search="true" ng-model="search.acategory_id" ng-change="article_get(search, 1)" ng-select>
                        <option value="">全部分類</option>
                        <option value="{{e.id}}" title="{{e.name}}" ng-repeat="e in acategories" ng-bind="e.tree_name"></option>
                    </select>
                </div>
            </div>
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-2">目錄</td>
                    <td class="col-md-4">標題</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-2">日期</td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in articles">
                    <td><label ng-bind="$index+1"></label></td>
                    <td><label ng-bind="e.acategory_name"></label></td>
                    <td><label ng-bind="e.name"></label></td>
                    <td></td>
                    <td><label ng-bind="e.created_at"></label></td>
                    <td>
                        <label class="checkbox pull-left" for="checkboxArticleEnable{{e.id}}">
                            <input type="checkbox" id="checkboxArticleEnable{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="article_enable(e)" ng-checkbox>
                        </label>
                        <button type="button" class="btn" ng-click="article_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="article_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-info" ng-href="admin/faq/article_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="article_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
            <div class="text-center">
                <div class="pull-left">
                    <small>Total: <span ng-bind="pagination.total"></span></small>
                </div>
                <ul class="pagination">
                    <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="article_get(search, pagination.previous)">&lsaquo;</a></li>
                    <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="article_get(search, pagination.first)">1</a></li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
                    <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                        <a href="#" ng-bind="p" ng-click="article_get(search, p)"></a>
                    </li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="article_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="article_get(search, pagination.next)">&rsaquo;</a></li>
                </ul>
                <div class="pull-right" style="width: 90px;">
                    <input class="form-control" id="paginationPage" type="text" placeholder="頁數" ng-model="pagination.page" ng-change="article_get(search, pagination.page)" >
                </div>
            </div>
        </div>

        <div class="tab-pane" id="acategory">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-6">名稱</td>
                    <td class="col-md-3"></td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in acategories">
                    <td ng-bind="$index+1"></td>
                    <td>
                        <small ng-bind="e.tree_name"></small><br>
                    </td>
                    <td></td>
                    <td>
                        <p class="pull-left">
                            <label class="checkbox" for="checkboxEnableAc{{e.id}}">
                                <input type="checkbox" id="checkboxEnableAc{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="acategory_enable(e)" ng-checkbox>
                            </label>
                        </p>
                        <button type="button" class="btn" ng-click="acategory_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="acategory_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-primary" ng-href="admin/faq/acategory_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="acategory_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>