<div class="ng-cloak" ng-cloak ng-controller="faq" ng-init="init_article()">
    <a class="btn btn-danger pull-right" href="admin/faq">取消</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2" for="selectAcategory">目錄</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectAcategory" title="請選擇目錄" data-live-search="true" ng-model="article.acategory_id" ng-select>
                            <option value="{{e.id}}" title="{{e.name}}" ng-bind="e.tree_name" ng-repeat="e in acategories"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="article.name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputContent">內容</label>
                    <div class="col-md-10">
                        <textarea class="form-control" id="inputContent" placeholder="內容" rows="40" ng-model="article.content" ng-editor></textarea>
                        <div>
                            <input class="btn" type="button" value="上傳圖片" ng-click="article_upload_content_images(article)">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">
                        <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="article.enable" ng-checkbox>啟用</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="article_add(article)">確定</button>
                        <a class="btn btn-danger" href="admin/qa/lists#article">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>