<div class="ng-cloak" ng-cloak ng-controller="product" ng-init="init_pcategory()">
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group" ng-hide="pcategory.ptype_id == 2">
                    <label class="control-label col-md-2" for="selectPcategory">上層分類</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectPcategory" title="請選擇分類" ng-model="pcategory.parent" ng-select>
                            <option value="{{e.id}}" title="{{e.name}}" ng-bind="e.tree_name" ng-disabled="e.level > 0" ng-repeat="e in pcategories"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="pcategory.name">
                    </div>
                </div>
                <div class="form-group" ng-hide="pcategory.parent == 1 || pcategory.parent == 2">
                    <label class="control-label col-md-2" for="inputDescription">描述</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputDescription" placeholder="描述" rows="4" ng-model="pcategory.description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputImage">圖片</label>
                    <div class="col-md-4">
                        <div><img ng-show="pcategory.image" ng-src="{{pcategory._image}}" style="max-width:400px;max-height:100px;"></div>
                        <div>
                            <input class="btn" type="button" value="上傳圖片" ng-click="pcategory_image_upload(pcategory)">
                            <input class="btn" type="button" value="刪除圖片" ng-click="pcategory_image_del(pcategory)">
                            <label ng-show="pcategory.parent == 1" ng-hide="pcategory.parent == 2">建議尺寸 250x250</label>
                            <label ng-show="pcategory.parent == 2" ng-hide="pcategory.parent == 1">建議尺寸 450x300</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputEnable"></label>
                    <div class="col-md-4">
                        <label class="checkbox" for="checkboxShop"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="pcategory.enable" ng-checkbox>啟用</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="pcategory_add(pcategory)">確定</button>
                        <a class="btn btn-danger" href="admin/product/lists?ptype_id={{pcategory.ptype_id}}#pcategory">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>