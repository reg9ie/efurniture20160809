<div class="ng-cloak" ng-cloak ng-controller="product" ng-init="init_ptype()">
    <div class="toolbar"></div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="ptype.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTable">商品尺寸</label>
            <div class="col-md-10">
                <div><textarea class="form-control" id="inputTable" placeholder="商品尺寸" rows="20" ng-model="ptype.table" ng-editor></textarea></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEnable"></label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxShop"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="ptype.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="ptype_add(ptype)">確定</button>
                <a class="btn btn-danger" href="admin/product/lists#ptype">取消</a>
            </div>
        </div>
    </form>
</div>