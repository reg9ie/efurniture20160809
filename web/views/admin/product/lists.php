<div class="ng-cloak" ng-cloak ng-controller="product" ng-init="init_lists()">
    <a class="btn btn-info pull-right" id="add_btn" ng-href="admin/product/product_add?ptype_id={{search.ptype_id}}">新增</a>
    <div class="col-md-2 pull-right">
        <select class="selectpicker" id="selectPtype" ng-model="search.ptype_id" ng-change="product_get(search, 1);pcategory_get(search);pgroup_get(search);" ng-select>
            <option value="{{e.id}}" title="{{e.name}}" ng-repeat="e in ptypes" ng-bind="e.name"></option>
        </select>
    </div>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#product" data-toggle="tab">商品</a></li>
        <li><a href="#pcategory" data-toggle="tab">目錄</a></li>
        <li><a href="#pgroup" data-toggle="tab">分類</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="product">
            <div class="row">
                <div class="col-md-2">
                    <select class="selectpicker" id="selectPcategory" ng-model="search.pcategory_id" ng-change="product_get(search, 1)" ng-select>
                        <option value="">全部目錄</option>
                        <option value="{{e.id}}" title="{{e.name}}" ng-repeat="e in pcategories" ng-bind="e.tree_name"></option>
                    </select>
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" id="inputSearchName" placeholder="名稱 / No" ng-model="search.name" ng-change="product_get(search, 1)" >
                </div>
            </div>
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td><input type="checkbox" name="select_all" ng-model="products._select" ng-click="product_select_all()"></td>
                    <td class="col-md-1">圖片</td>
                    <td class="col-md-2">名稱</td>
                    <td class="col-md-3">目錄</td>
                    <td class="col-md-2"></td>
                    <td class="col-md-1">首頁</td>
                    <td class="col-md-3"></td>
                </tr>
                <tbody id="sortable" ng-model="products" ng-change="product_sortable(products)" ng-sortable>
                    <tr ng-repeat="e in products">
                        <td>
                            <input type="checkbox" name="select_{{e.id}}" ng-model="e._select" ng-click="product_select()">
                            <div class="pull-right">
                                <i class="sortable fa fa-bars"></i>
                            </div>
                        </td>
                        <td><img ng-src="{{e._image_thumb}}" style="max-width:200px;max-height:50px"></td>
                        <td>
                            <small ng-bind="e.name"></small><br>
                            <small ng-bind="e.name_en"></small>
                        </td>
                        <td>
                            <small ng-bind="e.pcategory_name"></small><br>
                            <small ng-bind="e.pcategory_name_en"></small>
                        </td>
                        <td></td>
                        <td>
                            <label class="checkbox" for="checkboxHomeP{{e.id}}">
                                <input type="checkbox" id="checkboxHomeP{{e.id}}" data-toggle="checkbox" ng-model="e.home" ng-change="product_home(e)" ng-checkbox>
                            </label>
                        </td>
                        <td style="padding:0">
                            <div class="pull-left">
                                <label class="checkbox" for="checkboxEnableP{{e.id}}">
                                    <input type="checkbox" id="checkboxEnableP{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="product_enable(e)" ng-checkbox>
                                </label>
                            </div>
                            <button type="button" class="btn" ng-click="product_move_up(e)"><i class="fa fa fa-chevron-up"></i></button>
                            <button type="button" class="btn" ng-click="product_move_down(e)"><i class="fa ffa fa-chevron-down"></i></button>
                            <div class="pull-right">
                                <a class="btn btn-primary" ng-href="admin/product/product_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                                <button type="button" class="btn btn-danger" ng-click="product_del(e)"><i class="fa fa-trash-o"></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="pull-right" ng-show="show.status">
                            <button type="button" class="btn btn-danger" ng-click="products_del()"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="text-center">
                <div class="pull-left">
                    <small>Total: <span ng-bind="pagination.total"></span></small>
                </div>
                <ul class="pagination">
                    <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="product_get(search, pagination.previous)">&lsaquo;</a></li>
                    <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="product_get(search, pagination.first)">1</a></li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
                    <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                        <a href="#" ng-bind="p" ng-click="product_get(search, p)"></a>
                    </li>
                    <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="product_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
                    <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="product_get(search, pagination.next)">&rsaquo;</a></li>
                </ul>
                <div class="pull-right" style="width: 90px;">
                    <select class="selectpicker" id="selectCount" title="每頁顯示數量" ng-model="pagination.count" ng-change="product_count(pagination.count)">
                        <option value="10">10</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                        <option value="all">全部</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="pcategory">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-6">名稱</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-2"></td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in pcategories">
                    <td ng-bind="$index+1"></td>
                    <td><small ng-bind="e.tree_name"></small></td>
                    <td><img ng-src="{{e._image_thumb}}" style="max-width:200px;max-height:50px"></td>
                    <td></td>
                    <td>
                        <p class="pull-left">
                            <label class="checkbox" for="checkboxEnablePc{{e.id}}">
                                <input type="checkbox" id="checkboxEnablePc{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="pcategory_enable(e)" ng-checkbox>
                            </label>
                        </p>
                        <button type="button" class="btn" ng-click="pcategory_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="pcategory_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-primary" ng-href="admin/product/pcategory_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="pcategory_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div class="tab-pane" id="pgroup">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-6">名稱</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-2"></td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in pgroups">
                    <td ng-bind="$index+1"></td>
                    <td><small ng-bind="e.tree_name"></small></td>
                    <td><img ng-src="{{e._image_thumb}}" style="max-width:200px;max-height:50px"></td>
                    <td></td>
                    <td>
                        <p class="pull-left">
                            <label class="checkbox" for="checkboxEnablePg{{e.id}}">
                                <input type="checkbox" id="checkboxEnablePg{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="pgroup_enable(e)" ng-checkbox>
                            </label>
                        </p>
                        <button type="button" class="btn" ng-click="pgroup_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="pgroup_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-primary" ng-href="admin/product/pgroup_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="pgroup_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>