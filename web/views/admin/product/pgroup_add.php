<div class="ng-cloak" ng-cloak ng-controller="product" ng-init="init_pgroup()">
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="pgroup.name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputEnable"></label>
                    <div class="col-md-4">
                        <label class="checkbox" for="checkboxShop"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="pgroup.enable" ng-checkbox>啟用</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="pgroup_add(pgroup)">確定</button>
                        <a class="btn btn-danger" href="admin/product/lists?ptype_id={{product.ptype_id}}#pgroup">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>