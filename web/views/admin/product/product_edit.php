<div class="ng-cloak" ng-cloak ng-controller="product" ng-init="init_product()">
    <a class="btn btn-info pull-right" href="admin/product/product_add">新增</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2" for="selectPcategory">目錄</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectPcategory" title="請選擇目錄" ng-model="product.pcategory_id" ng-select>
                            <option value="{{e.id}}" title="{{e.name}}" ng-bind="e.tree_name" ng-disabled="e.level == 0" ng-repeat="e in pcategories"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="selectPgroup">分類</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectPgroup" title="請選擇分類" ng-model="product.pgroup_id" ng-select>
                            <option value="{{e.id}}" title="{{e.name}}" ng-bind="e.tree_name" ng-repeat="e in pgroups"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group" ng-hide="product.ptype_id == 2">
                    <label class="control-label col-md-2" for="inputNo">編號</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputNo" placeholder="編號" ng-model="product.no">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="product.name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputTitle">標題</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputTitle" placeholder="標題" ng-model="product.title">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputIntro">簡介</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputIntro" placeholder="簡介" rows="4" ng-model="product.intro"></textarea>
                    </div>
                </div>
                <div class="form-group" ng-hide="product.ptype_id == 2">
                    <label class="control-label col-md-2" for="inputDescription">描述</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="inputDescription" placeholder="描述" rows="4" ng-model="product.description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputImages">圖片</label>
                    <div class="col-md-10">
                        <div>
                            <input class="btn" type="button" value="上傳圖片" ng-click="product_images_upload(product)">
                            <label>建議尺寸 480x480</label>
                        </div>
                        <div class="clearfix" style="margin-top:10px;">
                            <div ng-repeat="image in product.images" style="margin:0 10px 10px 0;float:left;">
                                <div class="text-center" style="position:relative;">
                                    <button class="close text-danger" type="button" ng-click="product_images_del(product, $index)" style="position:absolute;top:10px;right:10px;opacity:1;">
                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                    </button>
                                    <button class="btn btn-link text-primary" type="button" ng-click="product_images_move_up(product, $index)" style="position:absolute;top:50%;left:0;margin-top:-10px;">
                                        <i class="fa fa-angle-left"></i>
                                    </button>
                                    <button class="btn btn-link text-primary" type="button" ng-click="product_images_move_down(product, $index)" style="position:absolute;top:50%;right:0;margin-top:-10px;">
                                        <i class="fa fa-angle-right"></i>
                                    </button>
                                    <img class="img-thumbnail" ng-src="{{image._thumbname}}" style="height:100px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputContent">內容</label>
                    <div class="col-md-10">
                        <div><textarea class="form-control" id="inputContent" placeholder="內容" rows="40" ng-model="product.content" editor-bodyclass="product_page_pro_detail" ng-editor></textarea></div>
                        <div><input type="button" value="上傳圖片" class="alt_btn" ng-click="product_content_images_upload(product)"></div>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCustom1">訂做範圍</label>
                    <div class="col-md-5">
                        <div><textarea class="form-control" id="inputCustom1" placeholder="訂做範圍1" rows="8" ng-model="product.custom1" editor-bodyclass="product_page_custom" ng-editor></textarea></div>
                    </div>
                    <div class="col-md-5">
                        <div><textarea class="form-control" id="inputCustom2" placeholder="訂做範圍2" rows="8" ng-model="product.custom2" editor-bodyclass="product_page_custom" ng-editor></textarea></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputCustomImage">圖片</label>
                    <div class="col-md-4">
                        <div><img ng-show="product.custom_image" ng-src="{{product._custom_image}}" style="max-width:180px;max-height:90px;"></div>
                        <div>
                            <input class="btn" type="button" value="上傳圖片" ng-click="product_custom_image_upload(product)">
                            <input class="btn" type="button" value="刪除圖片" ng-click="product_custom_image_del(product)">
                        </div>
                        <label>建議尺寸 360x180</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputHome"></label>
                    <div class="col-md-4">
                        <label class="checkbox" for="inputHome">
                            <input type="checkbox" id="inputHome" data-toggle="checkbox" ng-model="product.home" ng-checkbox>首頁
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputEnable"></label>
                    <div class="col-md-4">
                        <label class="checkbox" for="inputEnable">
                            <input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="product.enable" ng-checkbox>啟用
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="product_edit(product)">確定</button>
                        <a class="btn btn-danger" ng-href="admin/product/lists?ptype_id={{product.ptype_id}}#product">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>