<div class="ng-cloak" ng-cloak ng-controller="board" ng-init="init_board()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" id="add_btn" href="admin/board/board_add">新增</a>
    </div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="selectBcategory">分類</label>
            <div class="col-md-4">
                <select class="selectpicker" id="selectBcategory" title="請選擇分類" data-live-search="true" ng-model="board.bcategory_id" ng-change="bcategory_change(board)" ng-select>
                    <option value="{{e.id}}" title="{{e.name+' / '+e.alias}}" ng-bind="e.tree_name+' / '+e.alias" ng-repeat="e in bcategories"></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputImage">圖片</label>
            <div class="col-md-4">
                <div><img ng-show="board.image" ng-src="{{board._image}}" style="max-width:400px;max-height:100px;"></div>
                <div>
                    <input class="btn" type="button" value="上傳圖片" ng-click="board_upload_image(board)">
                    <input class="btn" type="button" value="刪除圖片" ng-click="board_del_image(board)">
                </div>
                <label>建議尺寸 480x430</label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="board.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputUrl">網址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputUrl" placeholder="網址" ng-model="board.url">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEnable"></label>
            <div class="col-md-4">
                <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="board.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="board_edit(board)">確定</button>
                <a class="btn btn-danger" href="admin/board">取消</a>
            </div>
        </div>
        <input type="file" name="file" id="file" style="display:none;">
        <input type="file" name="files[]" id="files" style="display:none;" multiple>
    </form>
</div>