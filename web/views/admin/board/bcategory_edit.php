<div class="ng-cloak" ng-cloak ng-controller="board" ng-init="init_bcategory()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" href="admin/board/bcategory_add">新增</a>
    </div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">名稱</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="bcategory.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputAlias">別名</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputAlias" placeholder="別名" ng-model="bcategory.alias">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEnable"></label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxShop"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="bcategory.enable" ng-checkbox>啟用</label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="bcategory_edit(bcategory)">確定</button>
                <a class="btn btn-danger" href="admin/board/lists#bcategory">取消</a>
            </div>
        </div>
        <input type="file" name="file" id="file" style="display:none;">
        <input type="file" name="files[]" id="files" style="display:none;" multiple>
    </form>
</div>