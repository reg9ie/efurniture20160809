<div class="ng-cloak" ng-cloak ng-controller="board" ng-init="init_lists()">
    <a class="btn btn-info pull-right" id="add_btn" href="admin/board/board_add">新增</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#board" data-toggle="tab">廣告</a></li>
        <li><a href="#bcategory" data-toggle="tab">分類</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="board">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-2">名稱</td>
                    <td class="col-md-3">分類</td>
                    <td class="col-md-2">日期</td>
                    <td class="col-md-1"></td>
                    <td class="col-md-4"></td>
                </tr>
                <tr ng-repeat="e in boards">
                    <td><label ng-bind="$index+1"></label></td>
                    <td ><img ng-src="{{e._image}}" style="max-width:200px;max-height:50px"></td>
                    <td><label ng-bind="e.name"></label></td>
                    <td><label ng-bind="e.bcategory_name"></label></td>
                    <td><label ng-bind="e.updated_at"></label></td>
                    <td></td>
                    <td>
                        <label class="checkbox pull-left" for="checkboxBoardEnable{{e.id}}">
                            <input type="checkbox" id="checkboxBoardEnable{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="board_enable(e)" ng-checkbox>
                        </label>
                        <button type="button" class="btn" ng-click="board_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="board_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-info" ng-href="admin/board/board_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="board_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div class="tab-pane" id="bcategory">
            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>#</td>
                    <td class="col-md-7">名稱</td>
                    <td class="col-md-2">別名</td>
                    <td class="col-md-3"></td>
                </tr>
                <tr ng-repeat="e in bcategories">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="e._tree_name"></td>
                    <td ng-bind="e.alias"></td>
                    <td>
                        <p class="pull-left">
                            <label class="checkbox" for="checkboxEnableBc{{e.id}}">
                                <input type="checkbox" id="checkboxEnableBc{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="bcategory_enable(e)" ng-checkbox>
                            </label>
                        </p>
                        <button type="button" class="btn" ng-click="bcategory_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                        <button type="button" class="btn" ng-click="bcategory_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                        <p class="pull-right">
                            <a class="btn btn-primary" ng-href="admin/board/bcategory_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                            <button type="button" class="btn btn-danger" ng-click="bcategory_del(e)"><i class="fa fa-trash-o"></i></button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>