<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="zh-TW" xmlns:fb="http://ogp.me/ns/fb#" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="app">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <meta property='fb:app_id' content="<?=$config['facebook_api_id']?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?=$config['site_keywords']?>">
    <meta name="description" content="<?=$config['site_description']?>">
    <title><?=$title?></title>
    <base href="<?=base_url()?>"/>
    <link type="image/png" rel="shortcut icon" href="favicon.png" />
    <?foreach((array)$css as $asset):?>
    <link type="text/css" rel="stylesheet" href="<?=$asset?>"/>
    <?endforeach;?>
</head>
<body>
    <div class="container" id="wrapper">
        <div id="container"><?=$container?></div>
    </div>
    <div class="navbar navbar-fixed-bottom" id="footer">
        <div class="container">
            <div class="copyright text-right">
                <div>Copyright © & Created by <strong><a href="http://pgs.cc" target="_blank">PGS.cc</a></strong> All rights reserved.&nbsp;&nbsp;<a href="http://pgs.cc" target="_blank">網頁設計</a></div>
            </div>
        </div>
    </div>
</body>
</html>
<?if(isset($json)):?>
<script type="text/javascript"><?=$json?></script>
<?endif;?>
<?foreach((array)$js as $asset):?>
<script type="text/javascript" src="<?=$asset?>"></script>
<?endforeach;?>