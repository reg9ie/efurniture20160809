<div ng-controller="user" ng-init="init_edit()">
    <div class="toolbar"></div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputAccount">帳號</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputAccount" placeholder="帳號" ng-model="user.account">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputPassword">密碼</label>
            <div class="col-md-4">
                <input type="password" class="form-control" id="inputPassword" placeholder="密碼" ng-model="user.password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputEmail">Email</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputEmail" placeholder="Email" ng-model="user.email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">姓名</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputName" placeholder="姓名" ng-model="user.name">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputGender">性別</label>
            <div class="col-md-4">
                <label class="radio">
                    <input type="radio" name="checkboxGender" data-toggle="radio" value="1" ng-model="user.gender" ng-radio>先生
                </label>
                <label class="radio">
                    <input type="radio" name="checkboxGender" data-toggle="radio" value="2" ng-model="user.gender" ng-radio>小姐
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputBirthday">生日</label>
            <div class="col-md-4">
                <input type="text" class="form-control datepicker" id="inputBirthday" placeholder="生日" ng-model="user.birthday">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTelephone">聯絡電話</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputTelephone" placeholder="聯絡電話" ng-model="user.telephone">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputCellphone">行動電話</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputCellphone" placeholder="行動電話" ng-model="user.cellphone">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputZip">地址</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="inputZip" placeholder="郵遞區號" ng-model="user.zip">
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control" id="inputAddress" placeholder="地址" ng-model="user.address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="checkboxVip">VIP</label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxVip">
                    <input type="checkbox" id="checkboxVip" data-toggle="checkbox" ng-model="user.vip" ng-checkbox>
                </label>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control datepicker" id="inputVipStart" placeholder="VIP 開始日期" ng-model="user.vip_start_at">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control datepicker" id="inputVipEnd" placeholder="VIP 到期日期" ng-model="user.vip_end_at">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="checkboxBlack">黑名單</label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxBlack">
                    <input type="checkbox" id="checkboxBlack" data-toggle="checkbox" ng-model="user.black" ng-checkbox>
                </label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="checkboxEnable">啟用</label>
            <div class="col-md-4">
                <label class="checkbox" for="checkboxEnable"><input type="checkbox" id="checkboxEnable" data-toggle="checkbox" ng-model="user.enable" ng-checkbox></label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <button type="button" class="btn btn-primary" ng-click="user_add(user)">確定</button>
                <a class="btn btn-default" href="admin/member">取消</a>
            </div>
        </div>
    </form>
</div>