<div class="ng-cloak" ng-cloak ng-controller="user" ng-init="init_lists()">
    <div class="row">
        <div class="col-md-2">
            <select class="selectpicker" id="selectSearchType" data-live-search="true" ng-model="search.type" ng-change="user_get(search, 1)">
                <option value="member_vip_black">全部會員</option>
                <option value="member">一般會員</option>
                <option value="vip">VIP會員</option>
                <option value="black">黑名單</option>
            </select>
        </div>
        <div class="col-md-2">
            <input type="text" class="form-control datepicker" id="inputSearchStart" placeholder="註冊時間-開始" ng-model="search.start" ng-change="user_get(search, 1)">
        </div>
        <div class="col-md-2">
            <input type="text" class="form-control datepicker" id="inputSearchEnd" placeholder="註冊時間-結束" ng-model="search.end" ng-change="user_get(search, 1)">
        </div>
        <div class="col-md-2">
            <input type="text" class="form-control" id="inputSearchText" placeholder="名稱" ng-model="search.text" ng-change="user_get(search, 1)" >
        </div>
        <div class="col-md-1 col-md-offset-3">
            <a class="btn btn-info pull-right" href="admin/member/user_add">新增</a>
        </div>
    </div>
    <div class="tab-pane" id="member">
        <table class="table table-striped table-condensed table-hover">
            <tr>
                <th><input type="checkbox" name="select_all" ng-model="users._select" ng-click="user_select_all()"></th>
                <td class="col-md-1">姓名</td>
                <td class="col-md-2">Email</td>
                <td class="">VIP</td>
                <td class="col-md-1">VIP 開始</td>
                <td class="col-md-1">VIP 到期</td>
                <td class="">黑名單</td>
                <td class="col-md-2">註冊</td>
                <td class=""><a href="#" ng-click="user_add_point()">新增</a> 購物金</td>
                <td class=""></td>
            </tr>
            <tr ng-repeat="e in users">
                <td><input type="checkbox" name="select_{{e.id}}" ng-model="e._select" ng-click="user_select()"></td>
                <td ng-bind="e.name"></td>
                <td ng-bind="e.email"></td>
                <td>
                    <label class="checkbox">
                        <input type="checkbox" data-toggle="checkbox" ng-model="e.vip" ng-change="user_vip(e)" ng-checkbox>
                    </label>
                </td>
                <td ng-bind="e._vip_start_at"></td>
                <td ng-bind="e._vip_end_at"></td>
                <td>
                    <label class="checkbox">
                        <input type="checkbox" data-toggle="checkbox" ng-model="e.black" ng-change="user_black(e)" ng-checkbox>
                    </label>
                </td>
                <td ng-bind="e.created_at"></td>
                <td>
                    <a ng-href="admin/point/add/{{e.id}}">新增</a>
                    <label ng-bind="e.point" style="padding-left:10px"></label>
                </td>
                <td>
                    <div class="pull-left">
                        <label class="checkbox">
                            <input type="checkbox" data-toggle="checkbox" ng-model="e.enable" ng-change="user_enable(e)" ng-checkbox>
                        </label>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" ng-href="admin/member/user_edit/{{e.id}}">編輯</a>
                        <button type="button" class="btn btn-danger" ng-click="del_user(e)" ng-show="users.length > 1">刪除</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="text-center">
        <div class="pull-left">
            <small>Total: <span ng-bind="pagination.total"></span></small>
        </div>
        <ul class="pagination">
            <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="user_get(search, pagination.previous)">&lsaquo;</a></li>
            <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="user_get(search, pagination.first)">1</a></li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
            <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                <a href="#" ng-bind="p" ng-click="user_get(search, p)"></a>
            </li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="user_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="user_get(search, pagination.next)">&rsaquo;</a></li>
        </ul>
        <div class="pull-right" style="width: 90px;">
            <select class="selectpicker" id="selectCount" title="每頁顯示數量" ng-model="pagination.count" ng-change="user_count(pagination.count)">
                <option value="10">10</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="500">500</option>
                <option value="1000">1000</option>
                <option value="all">全部</option>
            </select>
        </div>
    </div>
    <div id="user_add_point" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <form class="form-horizontal well">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputUser">會員</label>
                            <div class="col-md-10">
                                <label ng-repeat="un in point.user_name" ng-bind="un+($last?'':',　')"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputName">名稱</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="point.name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="inputPoint">購物金</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputPoint" placeholder="購物金" ng-model="point.point">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="button" class="btn btn-primary" ng-click="point_add(point)">確定</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>