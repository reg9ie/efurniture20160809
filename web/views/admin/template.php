<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="zh-TW" xmlns:fb="http://ogp.me/ns/fb#" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="app">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <meta property='fb:app_id' content="<?=$config['facebook_api_id']?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?=$config['site_keywords']?>">
    <meta name="description" content="<?=$config['site_description']?>">
    <title><?=$title?></title>
    <base href="<?=base_url()?>"/>
    <link type="image/icon" rel="shortcut icon" href="assets/images/favicon.ico">
    <?foreach((array)$css as $asset):?>
    <link type="text/css" rel="stylesheet" href="<?=$asset?>"/>
    <?endforeach;?>
</head>
<body>
    <div id="alert" role="alert">
        <div class="container">
            <button class="close" type="button" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="content">
                <span class="icon fa fa-lightbulb-o" aria-hidden="true"></span>
                <strong class="title"></strong>
                <small class="message"></small>
            </div>
        </div>
    </div>
    <div class="container" id="wrapper">
        <div class="row">
            <div class="col-md-2 text-center" id="menu">
                <ul class="nav nav-pills nav-stacked" id="lists">
                    <li class="<?=(isset($uris[2]) && $uris[2] == 'home'?'active':'')?>"><a href="admin/home" class="home"><strong>控制中心</strong></a></li>
                    <?if(User::is_permission('contact_manage')):?><li class="<?=(isset($uri[2]) && $uri[2] == 'contact'?'active':'')?>"><a href="admin/contact" class="contact"><strong>留言管理</strong></a></li><?endif;?>
                    <?if(User::is_permission('product_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'product'?'active':'')?>"><a href="admin/product" class="product"><strong>商品管理</strong></a></li><?endif;?>
                    <?if(User::is_permission('article_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'faq'?'active':'')?>"><a href="admin/faq" class="faq"><strong>常見問題</strong></a></li><?endif;?>
                    <?if(User::is_permission('article_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'page'?'active':'')?>"><a href="admin/page" class="page"><strong>頁面管理</strong></a></li><?endif;?>
                    <?if(User::is_permission('right_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'admin'?'active':'')?>"><a href="admin/admin" class="admin"><strong>權限管理</strong></a></li><?endif;?>
                    <?if(User::is_permission('template_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'template'?'active':'')?>"><a href="admin/template" class="template"><strong>郵件樣板</strong></a></li><?endif;?>
                    <?if(User::is_permission('config_manage')):?><li class="<?=(isset($uris[2]) && $uris[2] == 'config'?'active':'')?>"><a href="admin/config" class="config"><strong>網站設定</strong></a></li><?endif;?>
                </ul>
            </div>
            <div class="col-md-10">
                <div id="header">
                    <div class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <span class="navbar-brand"><?=$name?></span>
                            </div>
                            <div class="navbar-collapse collapse">
                                <div class="nav navbar-nav navbar-right">
                                    <div class="dropdown" id="control">
                                        <button class="btn btn-primary navbar-btn dropdown-toggle" data-toggle="dropdown">
                                            <span class="fui-list"></span>
                                        </button>
                                        <span class="dropdown-arrow dropdown-arrow-inverse"></span>
                                        <ul class="dropdown-menu dropdown-inverse">
                                            <li><a href="<?=base_url()?>" target="_blank"><i class="glyphicon glyphicon-home"></i> <span class="pull-right">前台</span></a></li>
                                            <li><a href="<?=site_url('/admin/logout/')?>"><i class="fa fa-sign-out"></i> <span class="pull-right">登出</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="container"><?=$container?></div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-fixed-bottom" id="footer">
        <div class="container">
            <div class="copyright text-right">
                <div>Copyright © & Created by <strong><a href="" target="_blank">Fish</a></strong> All rights reserved.&nbsp;&nbsp;<a href="" target="_blank">網頁設計</a></div>
            </div>
        </div>
    </div>
    <div id="message" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <strong class="title modal-title"></strong>
                </div>
                <div class="modal-body">
                    <p class="message"></p>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="lightbox" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img class="image" src="" />
                    <div class="controller">
                         <a class="left pull-left text-center" href="#previous"><div class="glyphicon glyphicon-chevron-left"></div></a>
                         <a class="right pull-right text-center" href="#next"><div class="glyphicon glyphicon-chevron-right"></div></a>
                    </div>
                    <div class="container">
                        <div class="title"></div>
                        <div class="content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="loading" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="container"></div>
        </div>
    </div>
    <form id="fileupload" name="fileupload" method="post" action="api/file/upload" enctype="multipart/form-data" style="display:none;">
        <input type="text" name="type" value="image">
        <input type="text" name="path" value="image">
        <input type="file" name="file" id="file">
        <input type="file" name="files[]" id="files" multiple>
        <input type="submit" value="submit">
    </form>
</body>
</html>
<?if(isset($json)):?>
<script type="text/javascript"><?=$json?></script>
<?endif;?>
<?foreach((array)$js as $asset):?>
<script type="text/javascript" src="<?=$asset?>"></script>
<?endforeach;?>