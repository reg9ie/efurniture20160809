<div class="ng-cloak" ng-controller="error_log" ng-init="init_lists()" ng-cloak>
    <div class="toolbar">
        <div class="col-md-1">
            <input class="form-control" type="text" ng-model="auto_reload_sec" ng-change="error_log_auto_reload(auto_reload, auto_reload_sec)" />
        </div>
        <div class="col-md-2">
            <div class="checkbox"><input type="checkbox" ng-model="auto_reload" ng-change="error_log_auto_reload(auto_reload, auto_reload_sec)" ng-checkbox>Auto Reload</div>
        </div>
        <button class="btn btn-danger pull-right" type="button" ng-click="error_log_delete_force_all()"><i class="fa fa-trash-o"></i></button>
    </div>
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="text-right" style="width:4%">id</td>
            <td class="text-right" style="width:4%">no</td>
            <td class="text-right" style="width:4%">type</td>
            <td>file</td>
            <td class="text-right" style="width:5%">line</td>
            <td style="width:12%">time</td>
            <td style="width:10%"></td>
        </tr>
        <tr ng-repeat="e in error_logs">
            <td ng-bind="$index+1"></td>
            <td class="text-right"><small ng-bind="e.id"></small></td>
            <td class="text-right"><small ng-bind="e.no"></small></td>
            <td class="text-right"><small ng-bind="e.type"></small></td>
            <td><small ng-bind="e.file"></small></td>
            <td class="text-right"><small ng-bind="e.line"></small></td>
            <td><small ng-bind="e.time"></small></td>
            <td>
                <i class="btn fa fa-file-text-o" ng-click="error_log_show_detail(e)"></i>
                <button type="button" class="btn btn-danger" ng-click="error_log_delete_force(e)"><i class="fa fa-trash-o"></i></button>
            </td>
        </tr>
    </table>
    <div class="text-center">
        <div class="pull-left">
            <small>Total: <span ng-bind="pagination.total"></span></small>
        </div>
        <ul class="pagination">
            <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="error_log_get(search, pagination.previous)">&lsaquo;</a></li>
            <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="error_log_get(search, pagination.first)">1</a></li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
            <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                <a href="#" ng-bind="p" ng-click="error_log_get(search, p)"></a>
            </li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="error_log_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="error_log_get(search, pagination.next)">&rsaquo;</a></li>
        </ul>
        <div class="pull-right" style="width:120px;">
            <input class="form-control pull-left" id="paginationPage" type="text" placeholder="頁數" style="width:70px;" ng-model="pagination.page" ng-change="error_log_get(search, pagination.page)" >
            <button class="btn btn-primary pull-right" type="button" ng-click="error_log_get(search, pagination.page)"><i class="fa fa-refresh"></i></button>
        </div>
    </div>
    <div id="error_log_detail" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <form class="form-horizontal well">
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">time</label>
                            <div class="col-md-4">
                                <label ng-bind="error_log.time"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">id</label>
                            <div class="col-md-4">
                                <label ng-bind="error_log.id"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">no</label>
                            <div class="col-md-4">
                                <label ng-bind="error_log.no"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">type</label>
                            <div class="col-md-4">
                                <label ng-bind="error_log.type"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">file</label>
                            <div class="col-md-10">
                                <label ng-bind="error_log.file"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">line</label>
                            <div class="col-md-4">
                                <label ng-bind="error_log.line"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">message</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="8" ng-model="error_log.message"></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">url</label>
                            <div class="col-md-10">
                                <label ng-bind="error_log.url"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">User Agent</label>
                            <div class="col-md-10">
                                <label ng-bind="error_log.user_agent"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">post</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="8" ng-model="error_log.post"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>