<div class="ng-cloak" ng-cloak ng-controller="news" ng-init="init_lists()">
    <div class="toolbar">
        <a class="btn btn-info pull-right" id="add_btn" href="admin/news/news_add">新增</a>
    </div>
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="col-md-2">分類</td>
            <td class="col-md-5">標題</td>
            <td class="col-md-2">日期</td>
            <td class="col-md-3"></td>
        </tr>
        <tr ng-repeat="e in newses">
            <td><label ng-bind="$index+1"></label></td>
            <td><label ng-bind="e.ntype_name"></label></td>
            <td><label ng-bind="e.name.substring(0, 25)"></label></td>
            <td><label ng-bind="e.created_at"></label></td>
            <td>
                <label class="checkbox pull-left" for="checkboxNewsEnable{{e.id}}">
                    <input type="checkbox" id="checkboxNewsEnable{{e.id}}" data-toggle="checkbox" ng-model="e.enable" ng-change="news_enable(e)" ng-checkbox>
                </label>
                <button type="button" class="btn" ng-click="news_move_up(e)"><i class="fa fa-chevron-up"></i></button>
                <button type="button" class="btn" ng-click="news_move_down(e)"><i class="fa fa-chevron-down"></i></button>
                <p class="pull-right">
                    <a class="btn btn-info" ng-href="admin/news/news_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                    <button type="button" class="btn btn-danger" ng-click="news_del(e)"><i class="fa fa-trash-o"></i></button>
                </p>
            </td>
        </tr>
    </table>
    <div class="text-center">
        <div class="pull-left">
            <small>Total: <span ng-bind="pagination.total"></span></small>
        </div>
        <ul class="pagination">
            <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="news_get(search, pagination.previous)">&lsaquo;</a></li>
            <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="news_get(search, pagination.first)">1</a></li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
            <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                <a href="#" ng-bind="p" ng-click="news_get(search, p)"></a>
            </li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="news_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="news_get(search, pagination.next)">&rsaquo;</a></li>
        </ul>
        <div class="pull-right" style="width: 90px;">
            <input class="form-control" id="paginationPage" type="text" placeholder="頁數" ng-model="pagination.page" ng-change="news_get(search, pagination.page)" >
        </div>
    </div>
</div>