<div class="ng-cloak" ng-cloak ng-controller="news" ng-init="init_news()">
    <a class="btn btn-danger pull-right" href="admin/news">取消</a>
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
        <li><a href="#en" data-toggle="tab">英文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2" for="selectNtype">分類</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectNtype" title="請選擇分類" ng-model="news.ntype_id" ng-select>
                            <option value="{{e.id}}" title="{{e.name}}" ng-bind="e.name" ng-repeat="e in ntypes"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputName">名稱</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputName" placeholder="名稱" ng-model="news.name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputContent">內容</label>
                    <div class="col-md-10">
                        <textarea id="inputContent" placeholder="內容" rows="40" ng-model="news.content" ng-editor></textarea>
                        <div><input class="alt_btn" type="button" value="上傳圖片" ng-click="news_upload_content_images(news)"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">
                        <label class="checkbox" for="inputEnable"><input type="checkbox" id="inputEnable" data-toggle="checkbox" ng-model="news.enable" ng-checkbox>啟用</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="news_add(news)">確定</button>
                        <a class="btn btn-danger" href="admin/news">取消</a>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="en">
                <div class="form-group">
                    <label class="control-label col-md-2" for="selectNtype">Type</label>
                    <div class="col-md-4">
                        <select class="selectpicker" id="selectNtype" title="Select Type" ng-model="news.ntype_id" ng-select>
                            <option value="{{e.id}}" title="{{e.name_en}}" ng-bind="e.name_en" ng-repeat="e in ntypes"></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputNameEn">Name</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="inputNameEn" placeholder="Name" ng-model="news.name_en">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2" for="inputContentEn">Content</label>
                    <div class="col-md-10">
                        <textarea id="inputContentEn" placeholder="Content" rows="40" ng-model="news.content_en" ng-editor></textarea>
                        <div><input class="alt_btn" type="button" value="Upload Images" ng-click="news_upload_content_en_images(news)"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-2">
                        <label class="checkbox" for="inputEnableEn">
                            <input type="checkbox" id="inputEnableEn" data-toggle="checkbox" ng-model="news.enable" ng-checkbox>Enable
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="news_add(news)">Confirm</button>
                        <a class="btn btn-danger" href="admin/news">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>