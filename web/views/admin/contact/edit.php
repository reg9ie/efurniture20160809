<div ng-controller="contact" ng-init="init_edit()">
    <div class="toolbar"></div>
    <form class="form-horizontal well">
        <div class="form-group">
            <label class="control-label col-md-2" for="inputCreated_at">留言時間</label>
            <div class="col-md-4">
                <p class="form-control-static" ng-bind="contact.created_at"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputName">聯絡人</label>
            <div class="col-md-4">
                <p class="form-control-static" ng-bind="contact.name"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTelephone">聯絡電話</label>
            <div class="col-md-4">
                <p class="form-control-static" ng-bind="contact.telephone"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputCellphone">其他聯絡方式</label>
            <div class="col-md-4">
                <p class="form-control-static" ng-bind="contact.cellphone"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputTitle">標題</label>
            <div class="col-md-4">
                <p class="form-control-static" ng-bind="contact.title"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="inputContent">內容</label>
            <div class="col-md-6">
                <p class="form-control-static" ng-bind="contact.content"></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-4">
                <a class="btn btn-danger" href="admin/contact">取消</a>
            </div>
        </div>
        <input type="file" name="file" id="file" style="display:none;">
    </form>
</div>