<div ng-controller="contact" ng-init="init_lists()">
    <div class="toolbar"></div>
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="col-md-2">日期</td>
            <td class="col-md-1">聯絡人</td>
            <td class="col-md-3">標題</td>
            <td class="col-md-4"></td>
            <td></td>
            <td class="col-md-2"></td>
        </tr>
        <tr ng-repeat="c in contacts">
            <td ng-bind="$index+1"></td>
            <td ng-bind="c.created_at"></td>
            <td ng-bind="c.name"></td>
            <td ng-bind="c.title"></td>
            <td></td>
            <td ng-bind="c.replied_at"></td>
            <td>
                <p class="pull-right">
                    <a class="btn btn-primary" ng-href="admin/contact/edit/{{c.id}}"><i class="fa fa-pencil-square-o"></i></a>
                    <button type="button" class="btn btn-danger" ng-click="del_contact(c)"><i class="fa fa-trash-o"></i></button>
                </p>
            </td>
        </tr>
    </table>
</div>