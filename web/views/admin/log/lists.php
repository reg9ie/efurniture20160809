<div ng-controller="log">
    <div class="toolbar">
        <button type="button" class="btn btn-danger pull-right" ng-click="del_errorLogs()">刪除</button>
    </div>
    <table class="table table-striped table-condensed table-hover row" id="errorLogs">
        <tr>
            <th class="col-md-1 text-right">No</th>
            <th class="col-md-5">檔案</th>
            <th class="col-md-1 text-right">行號</th>
            <th class="col-md-2">時間</th>
            <th class="col-md-3"></th>
        </tr>
        <?foreach((array)$data['errorLogs'] as $i => $errorLog):?>
        <tr class="errorLog id<?=$errorLog['id']?>">
            <td rowspan="2" class="text-right"><?=$errorLog['type']?></td>
            <td><?=$errorLog['file']?></td>
            <td rowspan="2" class="text-right"><?=$errorLog['line']?></td>
            <td rowspan="2"><?=$errorLog['time']?></td>
            <td>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" ng-click="show_errorLog(<?=$errorLog['id']?>)">細節</button>
                    <button type="button" class="btn btn-danger" ng-click="del_errorLog(<?=$errorLog['id']?>)">刪除</button>
                </div>
            </td>
        </tr>
        <tr class="errorLog id<?=$errorLog['id']?>">
            <td class="message hide" colspan="4">
                <pre class="prettyprint linenums"><?=$errorLog['message']?></pre>
            </td>
        </tr>
        <?endforeach;?>
    </table>
</div>