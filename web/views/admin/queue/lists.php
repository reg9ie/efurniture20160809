<div class="ng-cloak" ng-cloak ng-controller="queue" ng-init="init_lists()">
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="col-md-1">收件人</td>
            <td class="col-md-2">主旨</td>
            <td class="col-md-1 text-right">次數</td>
            <td class="col-md-1 text-right">時間</td>
            <td class="col-md-1"></td>
            <td class="col-md-2">寄件於</td>
            <td class="col-md-2">建立於</td>
            <td class="col-md-2"></td>
        </tr>
        <tr ng-repeat="e in mail_queues">
            <td ng-bind="pagination.count*(pagination.page-1)+$index+1"></td>
            <td><small ng-bind="e.to_email"></small></td>
            <td><small ng-bind="e.subject"></small></td>
            <td class="text-right" ng-bind="e.sended_times"></td>
            <td class="text-right" ng-bind="e.sended_time+'秒'"></td>
            <td></td>
            <td ng-bind="e.sended_at"></td>
            <td ng-bind="e.created_at"></td>
            <td class="text-right" style="padding:0">
                <p class="pull-left">
                    <small ng-bind="e.sended==1?'成功':''"></small>
                </p>
                <i class="btn fa fa-file-text-o" ng-click="mail_queue_show(e)"></i>
                <button type="button" class="btn btn-danger" ng-click="mail_queue_delete(e)"><i class="fa fa-trash-o"></i></button>
            </td>
        </tr>
    </table>
    <div class="text-center">
        <div class="pull-left">
            <small>Total: <span ng-bind="pagination.total"></span></small>
        </div>
        <ul class="pagination">
            <li ng-class="{'disabled': pagination.page == 1}"><a href="#" ng-click="mail_queue_get(search, pagination.previous)">&lsaquo;</a></li>
            <li ng-class="{'disabled': pagination.page == 1}" ng-hide="pagination.pages < 10 || pagination.start < 2"><a href="#" ng-click="mail_queue_get(search, pagination.first)">1</a></li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.start < 3"><a href="#">...</a></li>
            <li ng-class="{'active': pagination.page == p}" ng-repeat="p in [pagination.start, pagination.end] | range">
                <a href="#" ng-bind="p" ng-click="mail_queue_get(search, p)"></a>
            </li>
            <li class="disabled" ng-hide="pagination.pages < 10 || pagination.end > pagination.pages-2"><a href="#">...</a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}" ng-hide="pagination.pages < 10 || pagination.end == pagination.pages"><a href="#" ng-click="mail_queue_get(search, pagination.last)" ng-bind="pagination.last"></a></li>
            <li ng-class="{'disabled': pagination.page == pagination.pages}"><a href="#" ng-click="mail_queue_get(search, pagination.next)">&rsaquo;</a></li>
        </ul>
        <div class="pull-right" style="width: 90px;">
            <input class="form-control" id="paginationPage" type="text" placeholder="頁數" ng-model="pagination.page" ng-change="mail_queue_get(search, pagination.page)" >
        </div>
    </div>
    <div id="mail_queue_detail" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <form class="form-horizontal well">
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">建立於</label>
                            <div class="col-md-4">
                                <label ng-bind="mail_queue.created_at"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">id</label>
                            <div class="col-md-4">
                                <label ng-bind="mail_queue.id"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">收件人</label>
                            <div class="col-md-4">
                                <label ng-bind="mail_queue.to_email"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">主旨</label>
                            <div class="col-md-4">
                                <label ng-bind="mail_queue.subject"></label>
                            </div>
                        </div>
                        <div class="form-group" style="margin:0">
                            <label class="col-md-2 text-right">內容</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="16" ng-model="mail_queue.message"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>