<div class="ng-cloak" ng-cloak ng-controller="page" ng-init="init_lists()">
    <div class="toolbar"></div>
    <table class="table table-striped table-condensed table-hover">
        <tr>
            <td>#</td>
            <td class="col-md-5">名稱</td>
            <td class="col-md-1"></td>
            <td class="col-md-1">瀏覽</td>
            <td class="col-md-2">日期</td>
            <td class="col-md-3"></td>
        </tr>
        <tr ng-repeat="e in pages">
            <td><label ng-bind="$index+1"></label></td>
            <td><label ng-bind="e.name"></label></td>
            <td><label ng-bind="e.code"></label></td>
            <td><label ng-bind="e.view_times"></label></td>
            <td><label ng-bind="e.updated_at"></label></td>
            <td>
                <p class="pull-right">
                    <a class="btn btn-info" ng-href="admin/page/page_edit/{{e.id}}"><i class="fa fa-pencil-square-o"></i></a>
                </p>
            </td>
        </tr>
    </table>
</div>