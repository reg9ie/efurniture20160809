<div class="ng-cloak" ng-cloak ng-controller="page" ng-init="init_page()">
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="#tw" data-toggle="tab">中文</a></li>
    </ul>
    <form class="form-horizontal">
        <div class="tab-content">
            <div class="tab-pane active" id="tw">
                <div class="form-group">
                    <label class="control-label col-md-2">代號</label>
                    <label class="col-md-4" ng-bind="page.code"></label>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">名稱</label>
                    <div class="col-md-4">
                        <input class="form-control" type="text" placeholder="名稱" ng-model="page.name" />
                    </div>
                </div>
                <div class="form-group" ng-show="page.code == 'home'">
                    <label class="control-label col-md-2" for="inputImages">形象圖</label>
                    <div class="col-md-10">
                        <input class="btn" type="button" value="上傳形象圖" ng-click="page_images_upload(page)">
                        <label>尺寸 首頁:1920x500</label>
                    </div>
                    <div class="col-md-10 col-md-offset-2" ng-repeat="image in page.images">
                        <div class="col-md-4" style="padding-left:0">
                            <img class="img-thumbnail" ng-src="{{image._filename}}" style="max-width:220px;max-height:100px;">
                        </div>
                        <div class="col-md-5" style="padding-left:0;">
                            <div class="input-group">
                                <div class="input-group-addon" style="padding:10px 2px;">內容</div>
                                <textarea class="form-control" placeholder="內容" rows="2" ng-model="image.content" style="padding:8px 2px;"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-left:0;padding-right:2px;">
                            <button class="btn" type="button" ng-click="page_images_move_up(page, $index)">
                                <i class="fa fa-angle-up"></i>
                            </button>
                            <button class="btn" type="button" ng-click="page_images_move_down(page, $index)">
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <i class="btn btn-danger fa fa-trash-o" ng-click="page_images_del(page, $index)"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group" ng-hide="page.code == 'home'">
                    <label class="control-label col-md-2" for="inputImage">形象圖</label>
                    <div class="col-md-10">
                        <input class="btn" type="button" value="上傳形象圖" ng-click="page_image_upload(page)">
                        <label>尺寸 內頁:1920x500</label>
                        <div>
                            <div class="clearfix" style="margin-top:10px;">
                                <div ng-repeat="image in page.images" style="margin:0 10px 10px 0;float:left;">
                                    <div class="text-center" style="position:relative;">
                                        <button class="close text-danger" type="button" ng-click="page_images_del(page, $index)" style="position:absolute;top:10px;right:10px;opacity:1;">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        <img class="img-thumbnail" ng-src="{{image._filename}}" style="height:80px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" ng-show="page.code == 'about' || page.code == 'privacy' || page.code == 'copyright' || page.code == 'terms'">
                    <label class="control-label col-md-2" for="inputContent">內容</label>
                    <div class="col-md-10">
                        <div><textarea class="form-control" id="inputContent" placeholder="內容" rows="40" ng-model="page.content" ng-editor></textarea></div>
                        <div><input type="button" value="上傳圖片" class="alt_btn" ng-click="page_content_images_upload(page)"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <button type="button" class="btn btn-primary" ng-click="page_edit(page)">確定</button>
                        <a class="btn btn-danger" href="admin/page">取消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>