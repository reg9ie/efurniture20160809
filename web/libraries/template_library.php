<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Template_library {
    var $CI = false;
    var $config = false;
    var $user = false;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->spark('php-activerecord/0.0.2');

        $this->config = Config::all_to_array();
        $this->config['environment']         = configEnvironment;
        $this->config['timezone']            = configTimezone;
        $this->config['url_domain']          = configUrlDomain;
        $this->config['url_admin']           = configUrlAdmin;
        $this->config['url_base']            = configUrlBase;
        $this->config['url_image']           = configUrlImage;
        $this->config['path_base']           = configPathBase;
        $this->config['path_image']          = configPathImage;
        $this->config['lang']                = configLang;

        $this->user = '';

        if(!$this->CI->backup_library->check(date('Ymd'))) Queue::create(array('sys' => 'backup'));
    }

    public function frontend($data = false) {
        $default_css = [
            'assets/normalize/3.0.3/normalize.css',
            'assets/Animate.css/3.5.1/animate.css',
            'assets/font-awesome/4.5.0/css/font-awesome.min.css',
            'assets/jquery.bxslider/4.1.2/jquery.bxslider.css',
            'assets/owl.carousel/2.0.0-beta.3/assets/owl.carousel.css',
            'assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.default.css',
            'assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.ctm.css',
            'assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.brd.css',
            'assets/pure-css3-dropdown-megamenu/css/css3menu.css',
            'assets/css/base.css',
            'assets/css/main.css',
            'assets/css/main_add.css',
            'assets/css/common.css',
        ];
        $default_js = [
            'http://maps.googleapis.com/maps/api/js?key=AIzaSyD8A-kMTzFh5c0puWtd9_JYNo6lcbYoe9o&sensor=false',
            'assets/jquery/jquery-1.8.2.min.js',
            'assets/jquery/jquery.easing.js',
            'assets/jquery.bxslider/4.1.2/jquery.bxslider.js',
            'assets/owl.carousel/2.0.0-beta.3/owl.carousel.min.js',
            'assets/masonry/3.3.2/masonry.pkgd.js',
            'assets/masonry/3.3.2/imagesloaded.pkgd.min.js',
            'assets/isotope/2.2.2/isotope.pkgd.min.js',
            'assets/angular.js/1.2.12/angular.min.js',
            'assets/angular.js/1.2.12/angular-sanitize.min.js',
            'assets/moment/2.1.0/min/moment.min.js',
            'assets/moment/2.1.0/min/lang/zh-tw.js',
            'assets/URI/1.14.1/URI.min.js',
            'assets/purl/2.3.1/purl.js',
            'assets/js/common.js',
            'assets/js/map.js',
            'assets/js/navigation.js',
            'assets/js/hambuger.js',
            'assets/js/main.js',
        ];
        $js_lt_ie9 = [];

        if(isset($data['css']) === false || empty($data['css']) === true) $data['css'] = $default_css;
        else $data['css'] = array_merge((array)$default_css, (array)$data['css']);

        if(isset($data['js']) === false || empty($data['js']) === true) $data['js'] = $default_js;
        else $data['js'] = array_merge((array)$default_js, (array)$data['js']);

        $data['js_lt_ie9'] = $js_lt_ie9;

        $var['css'] = $data['css'];
        $var['js'] = $data['js'];
        $var['js_lt_ie9'] = $data['js_lt_ie9'];
        $var['config'] = $this->config;
        $var['user'] = $this->user;

        if(isset($data['json'])) $var['json'] = $data['json'];
        if(isset($data['data'])) $var['data'] = $data['data'];

        $var['uris'] = $this->CI->uri->segment_array();
        $var['uri'] = $this->CI->uri->uri_string();
        $var['name'] = (isset($data['name']) ? $data['name'] : '');
        $var['title'] = $this->config['site_name'].($var['name'] ? ' | '.$var['name'] : '');

        $var['pcategories'] = [];
        $objPtype = Ptype::find(array(
            'conditions' => '`code` = "popular" AND  `enable` = 1 AND `delete` = 0'
        ));
        $objPcategories = Pcategory::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `level` > -1 AND `enable` = 1 AND `delete` = 0',
            'order' => '`lft` ASC'
        ]);
        $pcategories = to_array($objPcategories, ['methods' => ['_tree_name', '_image']]);
        $objProducts = Product::all([
            'conditions' => '`ptype_id` = '.$objPtype->id.' AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ]);
        $products = to_array($objProducts, ['methods' => ['_image', 'images']]);
        foreach ($pcategories as $i => $p) {
            $p['products'] = [];
            foreach ($products as $j => $pd) {
                if($pd['pcategory_id'] == $p['id']) {
                    $p['products'][] = $pd;
                    unset($products[$j]);
                }
            }
            
            $p['childs'] = [];
            if($p['level'] == 0) $var['pcategories'][] = $p;
            else {
                foreach ($var['pcategories'] as $j => $vp) {
                    if($p['parent'] == $vp['id']) {
                        $var['pcategories'][$j]['childs'][] = $p;
                    }
                }
            }
        }

        $var['search_text'] = '';
        if(isset($var['uris'][1], $var['uris'][2], $var['uris'][3]) &&
           $var['uris'][1] == 'product' && $var['uris'][2] == 'search' && $var['uris'][3]) $var['search_text'] = urldecode($var['uris'][3]);

        $this->CI->load->library('cart_library');
        $cart = $this->CI->cart_library->get_cart();
        $var['json'] .= 'var cart = '.json_encode($cart).';';
        $var['json'] .= 'var config_lang = "'.configLang.'";';

        if(isset($data['html'])) $var['container'] = $this->CI->load->view(configLang.'/'.$data['html'], $var, true);

        $this->CI->load->view(configLang.'/'.(isset($data['template']) ? $data['template'] : 'template'), $var);
    }

    public function backend($data = false) {
        $default_css = array(
            'assets/jquery-ui/1.10.4/themes/base/minified/jquery-ui.min.css',
            'assets/bootstrap/3.2.0/css/bootstrap.min.css',
            'assets/bootstrap-select/1.5.4/bootstrap-select.min.css',
            'assets/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css',
            'assets/Flat-UI/2.1.3/css/flat-ui.css',
            'assets/Flat-UI/2.1.3/css/fix.css',
            'assets/font-awesome/4.5.0/css/font-awesome.min.css',
            'assets/css/admin/common.css',
            'assets/css/admin/primary.css'
            // 'css/google-code-prettify/20130304/prettify.css',
        );
        $default_js = array(
            'assets/jquery/jquery-1.10.2.min.js',
            'assets/jquery-ui/1.10.4/ui/minified/jquery-ui.min.js',
            'assets/moment/2.10.6/min/moment.min.js',
            'assets/bootstrap/3.2.0/js/bootstrap.min.js',
            'assets/bootstrap-select/1.5.4/bootstrap-select.min.js',
            'assets/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js',
            'assets/Flat-UI/2.1.3/js/bootstrap-switch.js',
            'assets/Flat-UI/2.1.3/js/jquery.tagsinput.js',
            'assets/Flat-UI/2.1.3/js/jquery.placeholder.js',
            'assets/Flat-UI/2.1.3/js/flatui-checkbox.js',
            'assets/Flat-UI/2.1.3/js/flatui-radio.js',
            'assets/Flat-UI/2.1.3/js/application.js',
            'assets/jquery.ajaxfileupload.js/jquery.ajaxfileupload.js',
            'assets/spin/spin.min.js',
            'assets/angular.js/1.2.12/angular.min.js',
            'assets/purl/2.3.1/purl.js',
            'assets/js/admin/common.js'
            // 'js/underscore/1.7.0/underscore-min.js',
            // 'js/bootstrap-gridline/1.0.0/gridline.min.js',
            
        );

        if(isset($data['css']) === false || empty($data['css']) === true) $data['css'] = $default_css;
        else $data['css'] = array_merge((array)$default_css, (array)$data['css']);

        if(isset($data['js']) === false || empty($data['js']) === true) $data['js'] = $default_js;
        else $data['js'] = array_merge((array)$default_js, (array)$data['js']);

        $var['css'] = $data['css'];
        $var['js'] = $data['js'];
        $var['config'] = $this->config;
        $var['user'] = $this->user;

        if(isset($data['json'])) $var['json'] = $data['json'];
        if(isset($data['data'])) $var['data'] = $data['data'];

        $var['uris'] = $this->CI->uri->segment_array();
        $var['uri'] = $this->CI->uri->uri_string();
        $var['name'] = (isset($data['name']) ? $data['name'] : '');
        $var['title'] = ($var['name'] ? $data['name'].'-' : '').$this->config['site_name'];

        if(isset($data['html'])) $var['container'] = $this->CI->load->view('/admin/'.$data['html'], $var, true);

        $this->CI->load->view('admin/'.(isset($data['template']) ? $data['template'] : 'template'), $var);

        if($this->config['environment'] == 'development') $this->CI->output->enable_profiler(true);
    }

    public function form_submit($data = false) {
        $var = [];
        
        if(isset($data['data'])) $var['data'] = $data['data'];

        $this->CI->load->view('template_form_submit', $var);
    }
}