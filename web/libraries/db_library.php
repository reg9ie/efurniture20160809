<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Db_library {
    var $CI;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->dbforge();

        if($this->CI->db->table_exists('configs')) return false;

        $this->error_logs();
        $this->logs();
        $this->configs();
        $this->queues();
        $this->render_queues();
        $this->mail_queues();
        $this->mail_accounts();
        $this->permissions_and_roles();
        $this->users();
        $this->contacts();
        $this->pages();
        $this->images();
        $this->boards();
        $this->products();
        $this->newses();
        $this->articles();
        $this->downloads();
        $this->companies();
        $this->payments();
        $this->shippings();
        $this->orders();
        $this->points();
    }

    public function error_logs() {
        if(!$this->CI->db->table_exists('error_logs')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'no' => array('type' => 'INT', 'unsigned' => true),
                'type' => array('type' => 'VARCHAR', 'constraint' => '32'),
                'message' => array('type' => 'MEDIUMTEXT'),
                'post' => array('type' => 'MEDIUMTEXT'),
                'file' => array('type' => 'VARCHAR', 'constraint' => '255'),
                'line' => array('type' => 'INT', 'unsigned' => true),
                'url' => array('type' => 'VARCHAR', 'constraint' => '255'),
                'user_agent' => array('type' => 'VARCHAR', 'constraint' => '255'),
                'time' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('error_logs');
        }
    }

    public function logs() {
        if(!$this->CI->db->table_exists('logs')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'user_id' => array('type' => 'INT', 'unsigned' => true),
                'user_account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'user_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sys' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sys_id' => array('type' => 'INT', 'unsigned' => true),
                'message' => array('type' => 'MEDIUMTEXT'),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('logs');
            $this->CI->db->query('CREATE INDEX `logs_user_id` ON `logs` (`user_id`)');
            $this->CI->db->query('CREATE INDEX `logs_sys_id` ON `logs` (`sys_id`)');
            $this->CI->db->query('CREATE INDEX `logs_user_id_sys_id` ON `logs` (`user_id`, `sys_id`)');
        }
    }

    public function configs() {
        if(!$this->CI->db->table_exists('configs')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'value' => array('type' => 'VARCHAR', 'constraint' => '512')
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('configs');
            $this->CI->db->query('ALTER TABLE `configs` ADD CONSTRAINT UNIQUE (`name`)');

            $this->CI->db->insert('configs', array('name' => 'site_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'site_keywords', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'site_description', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'site_logo', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'admin_email', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_email', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_telephone', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_fax', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_address', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_address_description', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_time', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_description', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_blog', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_bank_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_bank_code', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_bank_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'company_bank_account_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_smtp_host', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_smtp_port', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_smtp_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_smtp_password', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_from', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_from_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_cc', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_bcc', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'email_queue', 'value' => '1'));
            $this->CI->db->insert('configs', array('name' => 'facebook_url', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'facebook_id', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'facebook_api_id', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'facebook_api_secret', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'google_map_iframe', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'google_analysis_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'google_analysis_key_file_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'google_analysis_profile_id', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'google_analysis_tracking_id', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'instagram_url', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'youtube_url', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'yahoo_bid_url', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_product', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_news', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_faq', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_home_board', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_home_product', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'per_page_by_member_order', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'product_vip_discount', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'product_vip_discount_enable', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'product_countdown_date', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'product_countdown_enable', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'cart_user', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'cart_discount', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'cart_discount_enable', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'cart_tax', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'point_feedback_formula', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'point_feedback_formula_enable', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'ezship_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'ezship_password', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_test', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_password', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_merchant_id', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_hash_key', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_hash_iv', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'allpay_item_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'paypal_test', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'paypal_account', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'paypal_item_name', 'value' => ''));
            $this->CI->db->insert('configs', array('name' => 'exchange_usdcad', 'value' => ''));
        }
    }

    public function queues() {
        if(!$this->CI->db->table_exists('queues')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'sys' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sys_id' => array('type' => 'INT', 'unsigned' => true),
                'parameter' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'runed' => array('type' => 'TINYINT', 'unsigned' => true),
                'runed_time' => array('type' => 'SMALLINT', 'unsigned' => true),
                'runed_at' => array('type' => 'DATETIME', 'null' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('queues');
            $this->CI->db->query('CREATE INDEX `queues_runed` ON `queues` (`runed`)');
        }
    }

    public function render_queues() {
        if(!$this->CI->db->table_exists('render_queues')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'page' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'parameter' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'rendered' => array('type' => 'TINYINT', 'unsigned' => true),
                'rendered_time' => array('type' => 'SMALLINT', 'unsigned' => true),
                'rendered_at' => array('type' => 'DATETIME', 'null' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('render_queues');
            $this->CI->db->query('CREATE INDEX `render_queues_rendered` ON `render_queues` (`rendered`)');
        }
    }
    
    public function mail_queues() {
        if(!$this->CI->db->table_exists('mail_queues')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'from_email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'from_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'to_email' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'reply_email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'reply_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cc' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'bcc' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'subject' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'message' => array('type' => 'MEDIUMTEXT'),
                'attach' => array('type' => 'VARCHAR', 'constraint' => '4096'),
                'error_log' => array('type' => 'MEDIUMTEXT'),
                'sended' => array('type' => 'TINYINT', 'unsigned' => true),
                'sended_times' => array('type' => 'SMALLINT', 'unsigned' => true),
                'sended_time' => array('type' => 'SMALLINT', 'unsigned' => true),
                'sended_at' => array('type' => 'DATETIME', 'null' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('mail_queues');
            $this->CI->db->query('CREATE INDEX `mail_queues_sended` ON `mail_queues` (`sended`)');
            $this->CI->db->query('CREATE INDEX `mail_queues_sended_times` ON `mail_queues` (`sended_times`)');
            $this->CI->db->query('CREATE INDEX `mail_queues_sended_sended_times` ON `mail_queues` (`sended`, `sended_times`)');
        }
    }
    
    public function mail_accounts() {
        if(!$this->CI->db->table_exists('mail_accounts')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'password' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'host' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'port' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cc' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'bcc' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'queue' => array('type' => 'TINYINT', 'unsigned' => true),
                'limit' => array('type' => 'SMALLINT', 'unsigned' => true),
                'today_times' => array('type' => 'SMALLINT', 'unsigned' => true),
                'yestoday_times' => array('type' => 'SMALLINT', 'unsigned' => true),
                'total_times' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('mail_accounts');
            $this->CI->db->query('CREATE INDEX `mail_accounts_queue` ON `mail_accounts` (`queue`)');
            $this->CI->db->query('CREATE INDEX `mail_accounts_enable` ON `mail_accounts` (`enable`)');
            $this->CI->db->query('CREATE INDEX `mail_accounts_delete` ON `mail_accounts` (`delete`)');
            $this->CI->db->query('CREATE INDEX `mail_accounts_enable_delete` ON `mail_accounts` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `mail_accounts_queue_enable_delete` ON `mail_accounts` (`queue`, `enable`, `delete`)');
        }
    }

    public function permissions_and_roles() {
        if(!$this->CI->db->table_exists('roles')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('roles');
            $this->CI->db->query('ALTER TABLE `roles` ADD CONSTRAINT UNIQUE (`name`)');
            $this->CI->db->query('ALTER TABLE `roles` ADD CONSTRAINT UNIQUE (`code`)');

            $this->CI->db->insert('roles', array('id' => 1, 'name' => '總管理', 'code' => 'superadmin', 'enable' => 1));
            $this->CI->db->insert('roles', array('id' => 2, 'name' => '管理', 'code' => 'manage', 'enable' => 1));
            $this->CI->db->insert('roles', array('id' => 3, 'name' => '行銷', 'code' => 'marketing', 'enable' => 1));
            $this->CI->db->insert('roles', array('id' => 4, 'name' => '產品', 'code' => 'product', 'enable' => 1));
            $this->CI->db->insert('roles', array('id' => 5, 'name' => '客服', 'code' => 'service', 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('permissions')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('permissions');
            $this->CI->db->query('ALTER TABLE `permissions` ADD CONSTRAINT UNIQUE (`name`)');
            $this->CI->db->query('ALTER TABLE `permissions` ADD CONSTRAINT UNIQUE (`code`)');

            $this->CI->db->insert('permissions', array('id' => 1, 'name' => '網站設定', 'code' => 'config_manage', 'enable' => 1));
            $this->CI->db->insert('permissions', array('id' => 2, 'name' => '權限管理', 'code' => 'right_manage', 'enable' => 1));
            $this->CI->db->insert('permissions', array('id' => 3, 'name' => '樣板管理', 'code' => 'template_manage', 'enable' => 1));
            $this->CI->db->insert('permissions', array('id' => 4, 'name' => '產品管理', 'code' => 'product_manage', 'enable' => 1));
            $this->CI->db->insert('permissions', array('id' => 5, 'name' => '訂單管理', 'code' => 'order_manage', 'enable' => 1));
            $this->CI->db->insert('permissions', array('id' => 6, 'name' => '文章管理', 'code' => 'article_manage', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 8, 'name' => '形象管理', 'code' => 'image_manage', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 9, 'name' => '廣告管理', 'code' => 'board_manage', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 10, 'name' => '下載管理', 'code' => 'download_manage', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 11, 'name' => '郵件序列', 'code' => 'mail_queue', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 12, 'name' => '備份管理', 'code' => 'backup_manage', 'enable' => 1));
            // $this->CI->db->insert('permissions', array('id' => 6, 'name' => '留言管理', 'code' => 'contact_manage', 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('role_permissions')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'role_id' => array('type' => 'INT', 'unsigned' => true),
                'permission_id' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('role_permissions');
            $this->CI->db->query('ALTER TABLE `role_permissions` ADD CONSTRAINT UNIQUE (`role_id`, `permission_id`)');

            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 1, 'enable' => 1));
            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 2, 'enable' => 1));
            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 3, 'enable' => 1));
            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 4, 'enable' => 1));
            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 5, 'enable' => 1));
            $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 6, 'enable' => 1));
            // $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 7, 'enable' => 1));
            // $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 8, 'enable' => 1));
            // $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 9, 'enable' => 1));
            // $this->CI->db->insert('role_permissions', array('role_id' => 1, 'permission_id' => 10, 'enable' => 1));
        }
    }

    public function users() {
        if(!$this->CI->db->table_exists('users')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'no' => array('type' => 'VARCHAR', 'constraint' => '128'),
                'account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'password' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'nickname' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'avatar' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'firstname' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'middlename' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'lastname' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'gender' => array('type' => 'TINYINT', 'unsigned' => true),
                'birthday' => array('type' => 'DATE', 'null' => true),
                'cellphone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'line_id' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'wechat_id' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'facebook_id' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'facebook_url' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'cvs_type' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'order_times' => array('type' => 'INT', 'unsigned' => true),
                'order_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'order_price_this_month' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'point' => array('type' => 'INT'),
                'vip' => array('type' => 'INT', 'unsigned' => true),
                'vip_start_at' => array('type' => 'DATETIME', 'null' => true),
                'vip_end_at' => array('type' => 'DATETIME', 'null' => true),
                'black' => array('type' => 'INT', 'unsigned' => true),
                'subscribe' => array('type' => 'INT', 'unsigned' => true),
                'role_id' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('users');
            $this->CI->db->query('ALTER TABLE `users` ADD CONSTRAINT UNIQUE (`account`)');
            $this->CI->db->query('CREATE INDEX `users_role_id` ON `users` (`role_id`)');
            $this->CI->db->query('CREATE INDEX `users_enable` ON `users` (`enable`)');
            $this->CI->db->query('CREATE INDEX `users_delete` ON `users` (`delete`)');
            $this->CI->db->query('CREATE INDEX `users_subscribe` ON `users` (`subscribe`)');
            $this->CI->db->query('CREATE INDEX `users_enable_delete` ON `users` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `users_subscribe_enable_delete` ON `users` (`subscribe`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `users_role_id_enable_delete` ON `users` (`role_id`, `enable`, `delete`)');
        }
    }

    public function contacts() {
        if(!$this->CI->db->table_exists('contacts')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'title' => array('type' => 'VARCHAR', 'constraint' => '128'),
                'content' => array('type' => 'TEXT'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cellphone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'country_id' => array('type' => 'INT', 'unsigned' => true),
                'province_id' => array('type' => 'INT', 'unsigned' => true),
                'city_id' => array('type' => 'INT', 'unsigned' => true),
                'district_id' => array('type' => 'INT', 'unsigned' => true),
                'country' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'province' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'city' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'district' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'addreass' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'reply' => array('type' => 'TINYINT', 'unsigned' => true),
                'reply_content' => array('type' => 'TEXT'),
                'repled_at' => array('type' => 'DATETIME', 'null' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('contacts');
            $this->CI->db->query('CREATE INDEX `contacts_reply` ON `contacts` (`reply`)');
            $this->CI->db->query('CREATE INDEX `contacts_delete` ON `contacts` (`delete`)');
            $this->CI->db->query('CREATE INDEX `contacts_reply_delete` ON `contacts` (`reply`, `delete`)');
        }
    }

    public function pages() {
        if(!$this->CI->db->table_exists('pages')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'images' => array('type' => 'TEXT'),
                'image_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'images_en' => array('type' => 'TEXT'),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('pages');
            $this->CI->db->query('ALTER TABLE `pages` ADD CONSTRAINT UNIQUE (`name`)');
            $this->CI->db->query('ALTER TABLE `pages` ADD CONSTRAINT UNIQUE (`code`)');
            $this->CI->db->insert('pages', array('name' => '首頁', 'code' => 'home', 'title' => 'HOME', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '產品專區', 'code' => 'product', 'title' => 'PRODUCT', 'sort' => 2, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '最新消息', 'code' => 'news', 'title' => 'NEWS', 'sort' => 3, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '關於我們', 'code' => 'about', 'title' => 'ABOUT', 'sort' => 4, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '聯絡我們', 'code' => 'contact', 'title' => 'CONTACT', 'sort' => 5, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '技術服務', 'code' => 'support', 'title' => 'SUPPORT', 'sort' => 6, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '購物說明', 'code' => 'faq', 'title' => '購物說明', 'sort' => 7, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '訂單查詢', 'code' => 'order', 'title' => '訂單查詢', 'sort' => 8, 'enable' => 1));
            $this->CI->db->insert('pages', array('name' => '隱私權政策', 'code' => 'privacy', 'title' => '隱私權政策', 'sort' => 9, 'enable' => 0));
            $this->CI->db->insert('pages', array('name' => '著作權聲明', 'code' => 'copyright', 'title' => '著作權聲明', 'sort' => 10, 'enable' => 0));
            $this->CI->db->insert('pages', array('name' => '會員條款', 'code' => 'terms', 'title' => '會員條款', 'sort' => 11, 'enable' => 0));
        }
    }

    public function images() {
        if(!$this->CI->db->table_exists('images')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'page_id' => array('type' => 'INT', 'unsigned' => true),
                'page_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'page_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'url' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'click_times' => array('type' => 'INT', 'unsigned' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('images');
            $this->CI->db->query('CREATE INDEX `images_page_id` ON `images` (`page_id`)');
            $this->CI->db->query('CREATE INDEX `images_enable` ON `images` (`enable`)');
            $this->CI->db->query('CREATE INDEX `images_delete` ON `images` (`delete`)');
            $this->CI->db->query('CREATE INDEX `images_enable_delete` ON `images` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `images_page_id_enable_delete` ON `images` (`page_id`, `enable`, `delete`)');
        }
    }

    public function boards() {
        if(!$this->CI->db->table_exists('bcategories')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'lft' => array('type' => 'INT', 'unsigned' => true),
                'rght' => array('type' => 'INT', 'unsigned' => true),
                'parent' => array('type' => 'INT', 'unsigned' => true),
                'level' => array('type' => 'SMALLINT'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('bcategories');
            $this->CI->db->query('CREATE INDEX `bcategories_lft` ON `bcategories` (`lft`)');
            $this->CI->db->query('CREATE INDEX `bcategories_rght` ON `bcategories` (`rght`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent` ON `bcategories` (`parent`)');
            $this->CI->db->query('CREATE INDEX `bcategories_level` ON `bcategories` (`level`)');
            $this->CI->db->query('CREATE INDEX `bcategories_enable` ON `bcategories` (`enable`)');
            $this->CI->db->query('CREATE INDEX `bcategories_delete` ON `bcategories` (`delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_enable_delete` ON `bcategories` (`parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_lft_enable_delete` ON `bcategories` (`parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_rght_enable_delete` ON `bcategories` (`parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_lft_rght_enable_delete` ON `bcategories` (`parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_level_enable_delete` ON `bcategories` (`parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_parent_lft_rght_level_enable_delete` ON `bcategories` (`parent`, `lft`, `rght`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `bcategories_enable_delete` ON `bcategories` (`enable`, `delete`)');

            $this->CI->db->insert('bcategories', array('name' => '\\', 'tree_name' => '\\', 'title' => 'root', 'lft' => 1, 'rght' => 2, 'parent' => 0, 'level' => -1, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('boards')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'page_id' => array('type' => 'INT', 'unsigned' => true),
                'page_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'page_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'page_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'page_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'bcategory_id' => array('type' => 'INT', 'unsigned' => true),
                'bcategory_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'bcategory_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'bcategory_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'url' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'click_times' => array('type' => 'INT', 'unsigned' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('boards');
            $this->CI->db->query('CREATE INDEX `boards_page_id` ON `boards` (`page_id`)');
            $this->CI->db->query('CREATE INDEX `boards_enable` ON `boards` (`enable`)');
            $this->CI->db->query('CREATE INDEX `boards_delete` ON `boards` (`delete`)');
            $this->CI->db->query('CREATE INDEX `boards_enable_delete` ON `boards` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `boards_page_id_enable_delete` ON `boards` (`page_id`, `enable`, `delete`)');
        }
    }

    public function products() {
        if(!$this->CI->db->table_exists('ptypes')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'alias' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'MEDIUMTEXT'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'table' => array('type' => 'MEDIUMTEXT'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('ptypes');
            $this->CI->db->query('ALTER TABLE `ptypes` ADD CONSTRAINT UNIQUE (`code`)');
            // $this->CI->db->query('ALTER TABLE `ptypes` ADD CONSTRAINT UNIQUE (`name`)');
        }

        if(!$this->CI->db->table_exists('pcategories')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'ptype_id' => array('type' => 'INT', 'unsigned' => true),
                'ptype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'alias' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'TEXT'),
                'description_en' => array('type' => 'TEXT'),
                'description_cn' => array('type' => 'TEXT'),
                'product_size' => array('type' => 'TEXT'),
                'lft' => array('type' => 'INT', 'unsigned' => true),
                'rght' => array('type' => 'INT', 'unsigned' => true),
                'parent' => array('type' => 'INT', 'unsigned' => true),
                'level' => array('type' => 'SMALLINT'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('pcategories');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id` ON `pcategories` (`ptype_id`)');
            $this->CI->db->query('CREATE INDEX `pcategories_lft` ON `pcategories` (`lft`)');
            $this->CI->db->query('CREATE INDEX `pcategories_rght` ON `pcategories` (`rght`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent` ON `pcategories` (`parent`)');
            $this->CI->db->query('CREATE INDEX `pcategories_level` ON `pcategories` (`level`)');
            $this->CI->db->query('CREATE INDEX `pcategories_enable` ON `pcategories` (`enable`)');
            $this->CI->db->query('CREATE INDEX `pcategories_delete` ON `pcategories` (`delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_enable_delete` ON `pcategories` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_enable_delete` ON `pcategories` (`parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_lft_enable_delete` ON `pcategories` (`parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_rght_enable_delete` ON `pcategories` (`parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_lft_rght_enable_delete` ON `pcategories` (`parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_level_enable_delete` ON `pcategories` (`parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_parent_lft_rght_level_enable_delete` ON `pcategories` (`parent`, `lft`, `rght`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_enable_delete` ON `pcategories` (`ptype_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_lft_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_rght_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_lft_rght_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_level_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pcategories_ptype_id_parent_lft_rght_level_enable_delete` ON `pcategories` (`ptype_id`, `parent`, `lft`, `rght`, `level`, `enable`, `delete`)');

            $this->CI->db->insert('pcategories', array('name' => '\\', 'tree_name' => '\\', 'title' => 'root', 'lft' => 1, 'rght' => 2, 'parent' => 0, 'level' => -1, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('pgroups')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'ptype_id' => array('type' => 'INT', 'unsigned' => true),
                'ptype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'alias' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'TEXT'),
                'description_en' => array('type' => 'TEXT'),
                'description_cn' => array('type' => 'TEXT'),
                'product_size' => array('type' => 'TEXT'),
                'lft' => array('type' => 'INT', 'unsigned' => true),
                'rght' => array('type' => 'INT', 'unsigned' => true),
                'parent' => array('type' => 'INT', 'unsigned' => true),
                'level' => array('type' => 'SMALLINT'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('pgroups');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id` ON `pgroups` (`ptype_id`)');
            $this->CI->db->query('CREATE INDEX `pgroups_lft` ON `pgroups` (`lft`)');
            $this->CI->db->query('CREATE INDEX `pgroups_rght` ON `pgroups` (`rght`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent` ON `pgroups` (`parent`)');
            $this->CI->db->query('CREATE INDEX `pgroups_level` ON `pgroups` (`level`)');
            $this->CI->db->query('CREATE INDEX `pgroups_enable` ON `pgroups` (`enable`)');
            $this->CI->db->query('CREATE INDEX `pgroups_delete` ON `pgroups` (`delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_enable_delete` ON `pgroups` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_enable_delete` ON `pgroups` (`parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_lft_enable_delete` ON `pgroups` (`parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_rght_enable_delete` ON `pgroups` (`parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_lft_rght_enable_delete` ON `pgroups` (`parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_level_enable_delete` ON `pgroups` (`parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_parent_lft_rght_level_enable_delete` ON `pgroups` (`parent`, `lft`, `rght`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_enable_delete` ON `pgroups` (`ptype_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_lft_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_rght_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_lft_rght_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_level_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `pgroups_ptype_id_parent_lft_rght_level_enable_delete` ON `pgroups` (`ptype_id`, `parent`, `lft`, `rght`, `level`, `enable`, `delete`)');

            $this->CI->db->insert('pgroups', array('name' => '\\', 'tree_name' => '\\', 'title' => 'root', 'lft' => 1, 'rght' => 2, 'parent' => 0, 'level' => -1, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('products')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'ptype_id' => array('type' => 'INT', 'unsigned' => true),
                'ptype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pcategory_id' => array('type' => 'INT', 'unsigned' => true),
                'pcategory_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pcategory_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pcategory_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pgroup_id' => array('type' => 'INT', 'unsigned' => true),
                'pgroup_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pgroup_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pgroup_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'title_en' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'title_cn' => array('type' => 'VARCHAR', 'constraint' => '1024'),
                'intro' => array('type' => 'MEDIUMTEXT'),
                'intro_en' => array('type' => 'MEDIUMTEXT'),
                'intro_cn' => array('type' => 'MEDIUMTEXT'),
                'description' => array('type' => 'MEDIUMTEXT'),
                'description_en' => array('type' => 'MEDIUMTEXT'),
                'description_cn' => array('type' => 'MEDIUMTEXT'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'custom1' => array('type' => 'MEDIUMTEXT'),
                'custom1_en' => array('type' => 'MEDIUMTEXT'),
                'custom1_cn' => array('type' => 'MEDIUMTEXT'),
                'custom2' => array('type' => 'MEDIUMTEXT'),
                'custom2_en' => array('type' => 'MEDIUMTEXT'),
                'custom2_cn' => array('type' => 'MEDIUMTEXT'),
                'custom_image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'images' => array('type' => 'MEDIUMTEXT'),
                'stock' => array('type' => 'SMALLINT', 'unsigned' => true),
                'preorder' => array('type' => 'SMALLINT', 'unsigned' => true),
                'ordered' => array('type' => 'SMALLINT', 'unsigned' => true),
                'price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special_start' => array('type' => 'DATETIME', 'null' => true),
                'price_special_end' => array('type' => 'DATETIME', 'null' => true),
                'price_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_member_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_vip_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special_start_usd' => array('type' => 'DATETIME', 'null' => true),
                'price_special_end_usd' => array('type' => 'DATETIME', 'null' => true),
                'price_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_member_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_vip_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_special_start_cad' => array('type' => 'DATETIME', 'null' => true),
                'price_special_end_cad' => array('type' => 'DATETIME', 'null' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'relateds' => array('type' => 'TEXT'),
                'home' => array('type' => 'TINYINT', 'unsigned' => true),
                'in_stock' => array('type' => 'TINYINT', 'unsigned' => true),
                'model_list' => array('type' => 'TINYINT', 'unsigned' => true),
                'new' => array('type' => 'TINYINT', 'unsigned' => true),
                'top' => array('type' => 'TINYINT', 'unsigned' => true),
                'best' => array('type' => 'TINYINT', 'unsigned' => true),
                'hot' => array('type' => 'TINYINT', 'unsigned' => true),
                'countdown' => array('type' => 'TINYINT', 'unsigned' => true),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'click_times' => array('type' => 'INT', 'unsigned' => true),
                'cart_times' => array('type' => 'INT', 'unsigned' => true),
                'sold_times' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('products');
            $this->CI->db->query('CREATE INDEX `products_ptype_id` ON `products` (`ptype_id`)');
            $this->CI->db->query('CREATE INDEX `products_pcategory_id` ON `products` (`pcategory_id`)');
            $this->CI->db->query('CREATE INDEX `products_pgroup_id` ON `products` (`pgroup_id`)');
            $this->CI->db->query('CREATE INDEX `products_home` ON `products` (`home`)');
            $this->CI->db->query('CREATE INDEX `products_top` ON `products` (`top`)');
            $this->CI->db->query('CREATE INDEX `products_best` ON `products` (`best`)');
            $this->CI->db->query('CREATE INDEX `products_enable` ON `products` (`enable`)');
            $this->CI->db->query('CREATE INDEX `products_delete` ON `products` (`delete`)');
            $this->CI->db->query('CREATE INDEX `products_enable_delete` ON `products` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_home_enable_delete` ON `products` (`home`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_new_enable_delete` ON `products` (`new`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_top_enable_delete` ON `products` (`top`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_best_enable_delete` ON `products` (`best`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_hot_enable_delete` ON `products` (`hot`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_enable_delete` ON `products` (`ptype_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_pcategory_id_enable_delete` ON `products` (`ptype_id`, `pcategory_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_pgroup_id_enable_delete` ON `products` (`ptype_id`, `pgroup_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_home_enable_delete` ON `products` (`ptype_id`, `home`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_pcategory_id_home_enable_delete` ON `products` (`ptype_id`, `pcategory_id`, `home`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `products_ptype_id_pgroup_id_home_enable_delete` ON `products` (`ptype_id`, `pgroup_id`, `home`, `enable`, `delete`)');
        }

        if(!$this->CI->db->table_exists('product_relateds')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'product_id' => array('type' => 'INT', 'unsigned' => true),
                'related_id' => array('type' => 'INT', 'unsigned' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('product_relateds');
            $this->CI->db->query('CREATE INDEX `product_relateds_product_id` ON `product_relateds` (`product_id`)');
            $this->CI->db->query('CREATE INDEX `product_relateds_related_id` ON `product_relateds` (`related_id`)');
            $this->CI->db->query('CREATE INDEX `product_relateds_product_id_related_id` ON `product_relateds` (`product_id`, `related_id`)');
        }

        // if(!$this->CI->db->table_exists('product_specs')) {
        //     $this->CI->dbforge->add_field(array(
        //         'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
        //         'product_id' => array('type' => 'INT', 'unsigned' => true),
        //         'no' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'description' => array('type' => 'MEDIUMTEXT'),
        //         'content' => array('type' => 'MEDIUMTEXT'),
        //         'stock' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'preorder' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'ordered' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'special_start' => array('type' => 'DATETIME', 'null' => true),
        //         'special_end' => array('type' => 'DATETIME', 'null' => true),
        //         'sort' => array('type' => 'INT', 'unsigned' => true),
        //         'view_times' => array('type' => 'INT', 'unsigned' => true),
        //         'click_times' => array('type' => 'INT', 'unsigned' => true),
        //         'cart_times' => array('type' => 'INT', 'unsigned' => true),
        //         'sold_times' => array('type' => 'INT', 'unsigned' => true),
        //         'enable' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'delete' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'created_at' => array('type' => 'DATETIME', 'null' => true),
        //         'updated_at' => array('type' => 'DATETIME', 'null' => true)
        //     ));
        //     $this->CI->dbforge->add_key('id', true);
        //     $this->CI->dbforge->create_table('product_specs');
        //     $this->CI->db->query('CREATE INDEX `product_specs_product_id` ON `product_specs` (`product_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_specs_enable` ON `product_specs` (`enable`)');
        //     $this->CI->db->query('CREATE INDEX `product_specs_delete` ON `product_specs` (`delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_specs_enable_delete` ON `product_specs` (`enable`, `delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_specs_product_id_enable_delete` ON `product_specs` (`product_id`, `enable`, `delete`)');
        // }

        // if(!$this->CI->db->table_exists('product_colors')) {
        //     $this->CI->dbforge->add_field(array(
        //         'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
        //         'product_id' => array('type' => 'INT', 'unsigned' => true),
        //         'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'description' => array('type' => 'MEDIUMTEXT'),
        //         'content' => array('type' => 'MEDIUMTEXT'),
        //         'color' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'images' => array('type' => 'TEXT'),
        //         'stock' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'preorder' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'ordered' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
        //         'special_start' => array('type' => 'DATETIME', 'null' => true),
        //         'special_end' => array('type' => 'DATETIME', 'null' => true),
        //         'sort' => array('type' => 'INT', 'unsigned' => true),
        //         'view_times' => array('type' => 'INT', 'unsigned' => true),
        //         'click_times' => array('type' => 'INT', 'unsigned' => true),
        //         'cart_times' => array('type' => 'INT', 'unsigned' => true),
        //         'sold_times' => array('type' => 'INT', 'unsigned' => true),
        //         'enable' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'delete' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'created_at' => array('type' => 'DATETIME', 'null' => true),
        //         'updated_at' => array('type' => 'DATETIME', 'null' => true)
        //     ));
        //     $this->CI->dbforge->add_key('id', true);
        //     $this->CI->dbforge->create_table('product_colors');
        //     $this->CI->db->query('CREATE INDEX `product_colors_product_id` ON `product_colors` (`product_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_colors_enable` ON `product_colors` (`enable`)');
        //     $this->CI->db->query('CREATE INDEX `product_colors_delete` ON `product_colors` (`delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_colors_enable_delete` ON `product_colors` (`enable`, `delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_colors_product_id_enable_delete` ON `product_colors` (`product_id`, `enable`, `delete`)');
        // }

        // if(!$this->CI->db->table_exists('product_stocks')) {
        //     $this->CI->dbforge->add_field(array(
        //         'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
        //         'product_id' => array('type' => 'INT', 'unsigned' => true),
        //         'product_spec_id' => array('type' => 'INT', 'unsigned' => true),
        //         'product_color_id' => array('type' => 'INT', 'unsigned' => true),
        //         'stock' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'preorder' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'ordered' => array('type' => 'SMALLINT', 'unsigned' => true),
        //         'view_times' => array('type' => 'INT', 'unsigned' => true),
        //         'click_times' => array('type' => 'INT', 'unsigned' => true),
        //         'cart_times' => array('type' => 'INT', 'unsigned' => true),
        //         'sold_times' => array('type' => 'INT', 'unsigned' => true),
        //         'created_at' => array('type' => 'DATETIME', 'null' => true),
        //         'updated_at' => array('type' => 'DATETIME', 'null' => true)
        //     ));
        //     $this->CI->dbforge->add_key('id', true);
        //     $this->CI->dbforge->create_table('product_stocks');
        //     $this->CI->db->query('CREATE INDEX `product_stocks_product_id` ON `product_stocks` (`product_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_stocks_product_spec_id` ON `product_stocks` (`product_spec_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_stocks_product_color_id` ON `product_stocks` (`product_color_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_stocks_product_spec_id_product_color_id` ON `product_stocks` (`product_spec_id`, `product_color_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_stocks_product_id_product_spec_id_product_color_id` ON `product_stocks` (`product_id`, `product_spec_id`, `product_color_id`)');
        // }

        // if(!$this->CI->db->table_exists('product_images')) {
        //     $this->CI->dbforge->add_field(array(
        //         'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
        //         'product_id' => array('type' => 'INT', 'unsigned' => true),
        //         'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'description' => array('type' => 'MEDIUMTEXT'),
        //         'content' => array('type' => 'MEDIUMTEXT'),
        //         'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
        //         'sort' => array('type' => 'INT', 'unsigned' => true),
        //         'view_times' => array('type' => 'INT', 'unsigned' => true),
        //         'click_times' => array('type' => 'INT', 'unsigned' => true),
        //         'enable' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'delete' => array('type' => 'TINYINT', 'unsigned' => true),
        //         'created_at' => array('type' => 'DATETIME', 'null' => true),
        //         'updated_at' => array('type' => 'DATETIME', 'null' => true)
        //     ));
        //     $this->CI->dbforge->add_key('id', true);
        //     $this->CI->dbforge->create_table('product_images');
        //     $this->CI->db->query('CREATE INDEX `product_images_product_id` ON `product_images` (`product_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_images_enable` ON `product_images` (`enable`)');
        //     $this->CI->db->query('CREATE INDEX `product_images_delete` ON `product_images` (`delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_images_enable_delete` ON `product_images` (`enable`, `delete`)');
        //     $this->CI->db->query('CREATE INDEX `product_images_product_id_enable_delete` ON `product_images` (`product_id`, `enable`, `delete`)');
        // }

        // if(!$this->CI->db->table_exists('product_pcategories'))
        // {
        //     $this->CI->dbforge->add_field(array(
        //         'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
        //         'product_id' => array('type' => 'INT', 'unsigned' => true),
        //         'pcategory_id' => array('type' => 'INT', 'unsigned' => true)
        //     ));
        //     $this->CI->dbforge->add_key('id', true);
        //     $this->CI->dbforge->create_table('product_pcategories');
        //     $this->CI->db->query('CREATE INDEX `product_pcategories_product_id` ON `product_pcategories` (`product_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_pcategories_pcategory_id` ON `product_pcategories` (`pcategory_id`)');
        //     $this->CI->db->query('CREATE INDEX `product_pcategories_product_id_pcategory_id` ON `product_pcategories` (`product_id`, `pcategory_id`)');
        // }
    }

    public function newses() {
        if(!$this->CI->db->table_exists('ntypes')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('ntypes');
            $this->CI->db->query('ALTER TABLE `ntypes` ADD CONSTRAINT UNIQUE (`name`)');
            $this->CI->db->query('ALTER TABLE `ntypes` ADD CONSTRAINT UNIQUE (`code`)');
            
            $this->CI->db->insert('ntypes', array('id' => 1, 'name' => 'NEWS', 'name_en' => 'NEWS', 'name_cn' => 'NEWS', 'code' => 'news', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('ntypes', array('id' => 2, 'name' => 'EVENT', 'name_en' => 'EVENT', 'name_cn' => 'EVENT', 'code' => 'event', 'sort' => 2, 'enable' => 1));
            $this->CI->db->insert('ntypes', array('id' => 3, 'name' => 'PRODUCT', 'name_en' => 'PRODUCT', 'name_cn' => 'PRODUCT', 'code' => 'product', 'sort' => 3, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('newses')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'ntype_id' => array('type' => 'INT', 'unsigned' => true),
                'ntype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ntype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ntype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ntype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'MEDIUMTEXT'),
                'description_en' => array('type' => 'MEDIUMTEXT'),
                'description_cn' => array('type' => 'MEDIUMTEXT'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'home' => array('type' => 'INT', 'unsigned' => true),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('newses');
            $this->CI->db->query('CREATE INDEX `newses_ntype_id` ON `newses` (`ntype_id`)');
            $this->CI->db->query('CREATE INDEX `newses_enable` ON `newses` (`enable`)');
            $this->CI->db->query('CREATE INDEX `newses_delete` ON `newses` (`delete`)');
            $this->CI->db->query('CREATE INDEX `newses_enable_delete` ON `newses` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `newses_ntype_id_enable_delete` ON `newses` (`ntype_id`, `enable`, `delete`)');
        }
    }

    public function articles() {
        if(!$this->CI->db->table_exists('atypes')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('atypes');
            $this->CI->db->query('ALTER TABLE `atypes` ADD CONSTRAINT UNIQUE (`name`)');
            $this->CI->db->query('ALTER TABLE `atypes` ADD CONSTRAINT UNIQUE (`code`)');
            
            $this->CI->db->insert('atypes', array('id' => 1, 'name' => '最新消息', 'name_en' => 'News', 'name_cn' => '最新消息', 'code' => 'news', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('atypes', array('id' => 2, 'name' => '常見問題', 'name_en' => 'FAQ', 'name_cn' => '常見問題', 'code' => 'faq', 'sort' => 2, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('acategories')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'atype_id' => array('type' => 'INT', 'unsigned' => true),
                'atype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'tree_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'lft' => array('type' => 'INT', 'unsigned' => true),
                'rght' => array('type' => 'INT', 'unsigned' => true),
                'parent' => array('type' => 'INT', 'unsigned' => true),
                'level' => array('type' => 'SMALLINT'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('acategories');
            $this->CI->db->query('CREATE INDEX `acategories_lft` ON `acategories` (`lft`)');
            $this->CI->db->query('CREATE INDEX `acategories_rght` ON `acategories` (`rght`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent` ON `acategories` (`parent`)');
            $this->CI->db->query('CREATE INDEX `acategories_level` ON `acategories` (`level`)');
            $this->CI->db->query('CREATE INDEX `acategories_enable` ON `acategories` (`enable`)');
            $this->CI->db->query('CREATE INDEX `acategories_delete` ON `acategories` (`delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_enable_delete` ON `acategories` (`parent`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_lft_enable_delete` ON `acategories` (`parent`, `lft`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_rght_enable_delete` ON `acategories` (`parent`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_lft_rght_enable_delete` ON `acategories` (`parent`, `lft`, `rght`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_level_enable_delete` ON `acategories` (`parent`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_parent_lft_rght_level_enable_delete` ON `acategories` (`parent`, `lft`, `rght`, `level`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `acategories_enable_delete` ON `acategories` (`enable`, `delete`)');

            $this->CI->db->insert('acategories', array('atype_id' => 2, 'atype_code' => 'faq', 'atype_name' => '常見問題', 'name' => '\\', 'tree_name' => '\\', 'title' => 'root', 'lft' => 1, 'rght' => 2, 'parent' => 0, 'level' => -1, 'enable' => 1));
        }

        if(!$this->CI->db->table_exists('articles')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'atype_id' => array('type' => 'INT', 'unsigned' => true),
                'atype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'atype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'acategory_id' => array('type' => 'INT', 'unsigned' => true),
                'acategory_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'acategory_name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'acategory_name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'MEDIUMTEXT'),
                'description_en' => array('type' => 'MEDIUMTEXT'),
                'description_cn' => array('type' => 'MEDIUMTEXT'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'home' => array('type' => 'INT', 'unsigned' => true),
                'view_times' => array('type' => 'INT', 'unsigned' => true),
                'click_times' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('articles');
            $this->CI->db->query('CREATE INDEX `articles_atype_id` ON `articles` (`atype_id`)');
            $this->CI->db->query('CREATE INDEX `articles_acategory_id` ON `articles` (`acategory_id`)');
            $this->CI->db->query('CREATE INDEX `articles_enable` ON `articles` (`enable`)');
            $this->CI->db->query('CREATE INDEX `articles_delete` ON `articles` (`delete`)');
            $this->CI->db->query('CREATE INDEX `articles_enable_delete` ON `articles` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `articles_atype_id_enable_delete` ON `articles` (`atype_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `articles_atype_id_acategory_id_enable_delete` ON `articles` (`atype_id`, `acategory_id`, `enable`, `delete`)');
        }

        if(!$this->CI->db->table_exists('article_products')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'article_id' => array('type' => 'INT', 'unsigned' => true),
                'product_id' => array('type' => 'INT', 'unsigned' => true),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('article_products');
            $this->CI->db->query('CREATE INDEX `article_products_article_id` ON `article_products` (`article_id`)');
            $this->CI->db->query('CREATE INDEX `article_products_product_id` ON `article_products` (`product_id`)');
            $this->CI->db->query('CREATE INDEX `article_products_article_id_product_id` ON `article_products` (`article_id`, `product_id`)');
        }
    }

    public function downloads() {
        if(!$this->CI->db->table_exists('downloads')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'description_en' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'description_cn' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'file' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('downloads');
            $this->CI->db->query('CREATE INDEX `downloads_enable` ON `downloads` (`enable`)');
            $this->CI->db->query('CREATE INDEX `downloads_delete` ON `downloads` (`delete`)');
            $this->CI->db->query('CREATE INDEX `downloads_enable_delete` ON `downloads` (`enable`, `delete`)');
        }
    }

    public function companies() {
        if(!$this->CI->db->table_exists('companies')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_en' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name_cn' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'description_en' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'description_cn' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'content_en' => array('type' => 'MEDIUMTEXT'),
                'content_cn' => array('type' => 'MEDIUMTEXT'),
                'telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'fax' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'address_en' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'address_cn' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'time' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'url' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'google_map_iframe' => array('type' => 'TEXT'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('companies');
            $this->CI->db->query('CREATE INDEX `companies_enable` ON `companies` (`enable`)');
            $this->CI->db->query('CREATE INDEX `companies_delete` ON `companies` (`delete`)');
            $this->CI->db->query('CREATE INDEX `companies_enable_delete` ON `companies` (`enable`, `delete`)');
        }
    }

    public function payments() {
        if(!$this->CI->db->table_exists('payments')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'use' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'shippings' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('payments');
            $this->CI->db->query('CREATE INDEX `payments_enable` ON `payments` (`enable`)');
            $this->CI->db->query('CREATE INDEX `payments_delete` ON `payments` (`delete`)');
            $this->CI->db->query('CREATE INDEX `payments_enable_delete` ON `payments` (`enable`, `delete`)');
            $this->CI->db->insert('payments', array('id' => 1, 'code' => 'atm', 'name' => '實體ATM轉帳付款', 'description' => '請依照提供的匯款帳號資料付款，訂單成立後請在三天內付款。付款完成後請至【訂單查詢】填寫匯款單。', 'shippings' => 'delivery', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('payments', array('id' => 2, 'code' => 'cash_on_delivery', 'name' => '貨到付款', 'description' => '貨到付款', 'shippings' => 'delivery', 'sort' => 2, 'enable' => 1));
            $this->CI->db->insert('payments', array('id' => 3, 'code' => 'credit', 'name' => '線上刷卡(一次付清)', 'description' => '線上刷卡', 'shippings' => 'delivery,family,711', 'sort' => 2, 'enable' => 1));
            $this->CI->db->insert('payments', array('id' => 4, 'code' => 'cvs', 'name' => '超商付款', 'description' => '超商付款', 'shippings' => 'delivery,family,711', 'sort' => 3, 'enable' => 1));
            $this->CI->db->insert('payments', array('id' => 5, 'code' => 'paypal', 'name' => 'Paypal付款', 'description' => 'Paypal付款', 'shippings' => 'delivery,family,711', 'sort' => 4, 'enable' => 1));
        }
    }

    public function shippings() {
        if(!$this->CI->db->table_exists('shippings')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'free' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'payments' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'sort' => array('type' => 'INT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('shippings');
            $this->CI->db->query('CREATE INDEX `shippings_enable` ON `shippings` (`enable`)');
            $this->CI->db->query('CREATE INDEX `shippings_delete` ON `shippings` (`delete`)');
            $this->CI->db->query('CREATE INDEX `shippings_enable_delete` ON `shippings` (`enable`, `delete`)');
            $this->CI->db->insert('shippings', array('id' => 2, 'code' => 'delivery', 'name' => '貨運宅配', 'description' => '購物滿3500元免運費!!', 'payments' => 'atm,cash_on_delivery', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('shippings', array('id' => 1, 'code' => 'post', 'name' => '郵寄運送', 'description' => '郵寄掛號免運費!!', 'sort' => 1, 'enable' => 1));
            $this->CI->db->insert('shippings', array('id' => 3, 'code' => 'family', 'name' => '全家超商取貨', 'description' => '購物滿3500元免運費。', 'payments' => 'atm,credit,cvs', 'sort' => 3, 'enable' => 1));
            $this->CI->db->insert('shippings', array('id' => 4, 'code' => '711', 'name' => '7-Eleven超商取貨', 'description' => 'ibon交貨便超商取貨，此選項需先行付款，無法於取貨時才付款。購物滿3500元免運費。 注意! 交貨便約需3個工作天!', 'payments' => 'atm,credit,cvs', 'sort' => 4, 'enable' => 1));
            $this->CI->db->insert('shippings', array('id' => 5, 'code' => 'outlying_island', 'name' => '離島配送', 'description' => '澎湖、金門、馬祖等離島地區，請選擇此運送方式。', 'sort' => 5, 'enable' => 0));
            $this->CI->db->insert('shippings', array('id' => 6, 'code' => 'hk', 'name' => '港澳配送', 'description' => '港澳大陸地區請選擇此運送方式，購物滿NT$10,000免運費!!', 'sort' => 6, 'enable' => 0));
            $this->CI->db->insert('shippings', array('id' => 7, 'code' => 'international', 'name' => '國際配送', 'description' => '歐洲、美洲、澳洲等地區請選擇此運送方式，會以郵局掛號寄出。', 'sort' => 7, 'enable' => 0));
        }
    }

    public function orders() {
        if(!$this->CI->db->table_exists('orders')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'user_id' => array('type' => 'INT', 'unsigned' => true),
                'user_account' => array('type' => 'INT', 'unsigned' => true),
                'user_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'user_gender' => array('type' => 'TINYINT', 'unsigned' => true),
                'user_vip' => array('type' => 'TINYINT', 'unsigned' => true),
                'user_black' => array('type' => 'TINYINT', 'unsigned' => true),
                'buyer_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'buyer_gender' => array('type' => 'TINYINT', 'unsigned' => true),
                'buyer_cellphone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'buyer_telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'buyer_zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'buyer_address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'buyer_email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_gender' => array('type' => 'TINYINT', 'unsigned' => true),
                'recipient_cellphone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'recipient_email' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'recipient_note' => array('type' => 'TEXT'),
                'payment_id' => array('type' => 'INT', 'unsigned' => true),
                'payment_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'payment_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'payment_use' => array('type' => 'INT', 'unsigned' => true),
                'shipping_id' => array('type' => 'INT', 'unsigned' => true),
                'shipping_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'shipping_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'shipping_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'shipping_free' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'shipping_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'shipping_time' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'shipping_at' => array('type' => 'DATETIME', 'null' => true),
                'cvs_type' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_telephone' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'cvs_address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'count' => array('type' => 'SMALLINT', 'unsigned' => true),
                'point_use' => array('type' => 'INT'),
                'point_feedback' => array('type' => 'INT'),
                'price_all' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_discount' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_tax' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_shipping' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price_total' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'paysystem_test' => array('type' => 'SMALLINT', 'unsigned' => true),
                'paysystem_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_type' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cavalue' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'paysystem_storeid' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_ordernumber' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_amount' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'paysystem_authstatus' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_authcode' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_authtime' => array('type' => 'DATETIME', 'null' => true),
                'paysystem_authmsg' => array('type' => 'VARCHAR', 'constraint' => '128'),
                'paysystem_fee' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'paysystem_message' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'paysystem_status' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_check' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'paysystem_return_result' => array('type' => 'MEDIUMTEXT'),
                'paysystem_bank_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_bank_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_bank_account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_bank_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'paysystem_bank_user' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_bank_expire_at' => array('type' => 'DATETIME', 'null' => true),
                'paysystem_bank_at' => array('type' => 'DATETIME', 'null' => true),
                'paysystem_cvs_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cvs_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cvs_barcode1' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cvs_barcode2' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cvs_barcode3' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'paysystem_cvs_expire_at' => array('type' => 'DATETIME', 'null' => true),
                'paysystem_at' => array('type' => 'DATETIME', 'null' => true),
                'logistics_send_data' => array('type' => 'TEXT'),
                'logistics_send_data_at' => array('type' => 'DATETIME', 'null' => true),
                'logistics_receive_data' => array('type' => 'TEXT'),
                'logistics_receive_data_at' => array('type' => 'DATETIME', 'null' => true),
                'invoice_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'invoice_type' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'invoice_business_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'invoice_business_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'invoice_business_zip' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'invoice_business_address' => array('type' => 'VARCHAR', 'constraint' => '512'),
                'invoice_note' => array('type' => 'TEXT'),
                'invoice_date' => array('type' => 'DATE', 'null' => true),
                'invoice_donate' => array('type' => 'TINYINT', 'unsigned' => true),
                'config_product_vip_discount' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'config_product_vip_discount_enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'config_cart_user' => array('type' => 'TINYINT', 'unsigned' => true),
                'config_cart_discount' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'config_cart_discount_enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'config_cart_tax' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'status_remit' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_pay' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_ship' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_return' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_refund' => array('type' => 'TINYINT', 'unsigned' => true),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('orders');
            $this->CI->db->query('CREATE INDEX `orders_enable` ON `orders` (`enable`)');
            $this->CI->db->query('CREATE INDEX `orders_delete` ON `orders` (`delete`)');
            $this->CI->db->query('CREATE INDEX `orders_enable_delete` ON `orders` (`enable`, `delete`)');
        }

        if(!$this->CI->db->table_exists('order_products')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'order_id' => array('type' => 'INT', 'unsigned' => true),
                'ptype_id' => array('type' => 'INT', 'unsigned' => true),
                'ptype_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'ptype_code' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'pcategory_id' => array('type' => 'INT', 'unsigned' => true),
                'pcategory_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_id' => array('type' => 'INT', 'unsigned' => true),
                'product_no' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_title' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special_start' => array('type' => 'DATETIME', 'null' => true),
                'product_price_special_end' => array('type' => 'DATETIME', 'null' => true),
                'product_price_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_member_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_vip_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special_usd' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special_start_usd' => array('type' => 'DATETIME', 'null' => true),
                'product_price_special_end_usd' => array('type' => 'DATETIME', 'null' => true),
                'product_price_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_member_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_vip_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special_cad' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_price_special_start_cad' => array('type' => 'DATETIME', 'null' => true),
                'product_price_special_end_cad' => array('type' => 'DATETIME', 'null' => true),
                'product_special' => array('type' => 'TINYINT', 'unsigned' => true),
                'product_best' => array('type' => 'TINYINT', 'unsigned' => true),
                'product_top' => array('type' => 'TINYINT', 'unsigned' => true),
                'product_spec_id' => array('type' => 'INT', 'unsigned' => true),
                'product_spec_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_spec_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_spec_price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_spec_price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_spec_price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_color_id' => array('type' => 'INT', 'unsigned' => true),
                'product_color_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_color_color' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_color_image' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'product_color_price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_color_price_member' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_color_price_vip' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'product_color_price_special' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'price' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'quantity' => array('type' => 'INT', 'unsigned' => true),
                'discount' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'subtotal' => array('type' => 'DECIMAL', 'constraint' => '8,2', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('order_products');
            $this->CI->db->query('CREATE INDEX `order_products_order_id` ON `order_products` (`order_id`)');
            $this->CI->db->query('CREATE INDEX `order_products_product_id` ON `order_products` (`product_id`)');
        }

        if(!$this->CI->db->table_exists('order_statuses')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'order_id' => array('type' => 'INT', 'unsigned' => true),
                'status_remit' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_pay' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_ship' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_return' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_refund' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'status_delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('order_statuses');
            $this->CI->db->query('CREATE INDEX `order_statuses_order_id` ON `order_statuses` (`order_id`)');
        }
    }

    public function points() {
        if(!$this->CI->db->table_exists('points')) {
            $this->CI->dbforge->add_field(array(
                'id' => array('type' => 'INT', 'unsigned' => true, 'auto_increment' => true),
                'user_id' => array('type' => 'INT', 'unsigned' => true),
                'user_account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'user_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'order_id' => array('type' => 'INT', 'unsigned' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'description' => array('type' => 'VARCHAR', 'constraint' => '256'),
                'content' => array('type' => 'MEDIUMTEXT'),
                'point' => array('type' => 'INT'),
                'used' => array('type' => 'TINYINT', 'unsigned' => true),
                'used_at' => array('type' => 'DATETIME', 'null' => true),
                'total' => array('type' => 'INT'),
                'create_by_user_id' => array('type' => 'INT', 'unsigned' => true),
                'create_by_user_account' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'create_by_user_name' => array('type' => 'VARCHAR', 'constraint' => '64'),
                'enable' => array('type' => 'TINYINT', 'unsigned' => true),
                'delete' => array('type' => 'TINYINT', 'unsigned' => true),
                'created_at' => array('type' => 'DATETIME', 'null' => true),
                'updated_at' => array('type' => 'DATETIME', 'null' => true)
            ));
            $this->CI->dbforge->add_key('id', true);
            $this->CI->dbforge->create_table('points');
            $this->CI->db->query('CREATE INDEX `points_user_id` ON `points` (`user_id`)');
            $this->CI->db->query('CREATE INDEX `points_order_id` ON `points` (`order_id`)');
            $this->CI->db->query('CREATE INDEX `points_create_by_user_id` ON `points` (`create_by_user_id`)');
            $this->CI->db->query('CREATE INDEX `points_enable` ON `points` (`enable`)');
            $this->CI->db->query('CREATE INDEX `points_delete` ON `points` (`delete`)');
            $this->CI->db->query('CREATE INDEX `points_enable_delete` ON `points` (`enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `points_user_id_enable_delete` ON `points` (`user_id`, `enable`, `delete`)');
            $this->CI->db->query('CREATE INDEX `points_user_id_used_enable_delete` ON `points` (`user_id`, `used`, `enable`, `delete`)');
        }
    }
}