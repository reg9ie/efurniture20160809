<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ezship_library {
    // Document
    //      https://www.ezship.com.tw/file/ezship_WebOrder_XML.pdf

    var $CI;
    var $config;

    function __construct() {
        $this->CI = & get_instance();
        $this->config = Config::all_to_array();
    }

    public function map_redirect() {
        $ezship_account = $this->config['ezship_account'];
        
        $suID = $ezship_account ? $ezship_account : '';
        $processID = '0';
        $stCate = ''; //TFM-全家超商, TLF-萊爾富超商, TOK-OK超商, 空白-全部超商
        $stCode = '';
        $rtURL = site_url('api/cart/ezship');
        $webPara = '';

        redirect('http://map.ezship.com.tw/ezship_map_web.jsp?suID='.$suID.'&processID='.$processID.'&stCate='.$stCate.'&stCode='.$stCode.'&rtURL='.$rtURL.'&webPara='.$webPara);
    }

    public function xml_order_send($order = FALSE) {
        if($order === FALSE) return FALSE;
        if(!isset($order['no'])) return FALSE;
        if(!isset($order['price_total'])) return FALSE;
        if(!isset($order['recipient_name']) || empty($order['recipient_name'])) return FALSE;
        if(!isset($order['recipient_cellphone']) || empty($order['recipient_cellphone'])) return FALSE;
        if(!isset($order['recipient_email']) || empty($order['recipient_email'])) return FALSE;
        if(!isset($order['shipping_code']) || empty($order['shipping_code'])) return FALSE;
        if(!isset($order['products'])) return FALSE;
        if(!isset($this->config['ezship_account']) || empty($this->config['ezship_account'])) return FALSE;

        $postURL = 'https://www.ezship.com.tw/emap/ezship_xml_order_api.jsp';

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><ORDER/>');
        $xml->addChild('suID', $this->config['ezship_account']);
        $xml->addChild('orderID', $order['no']);
        $xml->addChild('orderStatus', 'A02'); // A01超商取貨，不需確認 A02超商取貨，須確認
                                              // A03超商取貨，輕鬆袋/迷你袋，不需確認 A04超商取貨，輕鬆袋/迷你袋，需確認
                                              // A05宅配，不需確認 A06宅配，須確認
        $xml->addChild('orderType', '3'); // 1取貨付款 3取貨不付款
        $xml->addChild('orderAmount', '2000'); //$order['price_total'] 1取貨付款 10~6000 3取貨不付款0~2000
        $xml->addChild('rvName', $order['recipient_name']);
        $xml->addChild('rvEmail', $order['recipient_email']);
        $xml->addChild('rvMobile', $order['recipient_cellphone']);
        if($order['shipping_code'] == 'cvs')
        {
            $xml->addChild('stCode', $order['cvs_type'].$order['cvs_code']);
        }
        $xml->addChild('rtURL', site_url('api/order/ezship_xml_order_return'));
        $xml->addChild('webPara', 'PGScc');

        foreach((array)$order['products'] as $i => $p)
        {
            $xml_detail = $xml->addChild('Detail');
            $xml_detail->addChild('prodItem', $i+1);
            $xml_detail->addChild('prodNo', $p['product_no']);
            $xml_detail->addChild('prodName', $p['product_name']);
            $xml_detail->addChild('prodPrice', $p['subtotal']);
            $xml_detail->addChild('prodQty', $p['quantity']);
            $xml_detail->addChild('prodSpec', '顏色: '.$p['product_color_name'].', 規格:'.$p['product_spec_name']);
        }

        $post = 'web_map_xml='.$xml->asXML();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POST, count($post));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $result = curl_exec($ch);
        curl_close ($ch);

        // trigger_error('xml_order_send, order_no: '.$order['no']);
        $objOrder = Order::find_by_no($order['no']);
        if($objOrder)
        {
            $objOrder->logistics_send_data = $xml->asXML();
            $objOrder->logistics_send_data_at = date('Y-m-d H:i:s');
            $objOrder->save();
        }

        return TRUE;
    }

    public function xml_order_return() {
        // trigger_error('xml_order_return');
        // trigger_error( json_encode($this->CI->input->post()) );
        // order_id
        //      訂單編號
        // sn_id
        //      店到店編號
        //      若<orderStatus> = A01 與 A02 一般店到店寄件 會回傳值 (8碼數字)
        //      若<orderStatus> = A03 輕鬆袋或迷你袋寄件 為空值
        //      若<orderStatus> = A05 與 A06 宅配寄件 會回傳值 (10碼數字)
        //      若訂單無法建立，會回傳八個零，錯誤狀態詳<orderStatus>
        // order_status
        //      訂單狀態
        //      S01 訂單新增成功
        //      E00 XML內容有誤或欄位短缺
        //      E01 <suID>帳號不存在
        //      E02 <suID>帳號無建立取貨付款權限 或 無網站串接權限 或 無ezShip宅配權限
        //      E03 <suID>帳號無可用之 輕鬆袋 或 迷你袋
        //      E04 <stCode>取件門市有誤
        //      E05 <orderAmount>金額有誤
        //      E06 <rvEmail>格式有誤
        //      E07 <rvMobile>格式有誤
        //      E08 <orderStatus>內容有誤 或 為空值
        //      E09 <orderType>內容有誤 或 為空值
        //      E10 <rvName>內容有誤 或 為空值
        //      E11 <rvAddr>內容有誤 或 為空值
        //      E98 XML系統發生錯誤無法載入
        //      E99 系統錯誤
        // webPara
        //      網站所需額外判別資料
        // date('Y-m-d H:i:s')

        //'logistics_receive_data' => array('type' => 'TEXT'),
        //'logistics_receive_data_at' => array('type' => 'DATETIME', 'null' => TRUE),
    }
}