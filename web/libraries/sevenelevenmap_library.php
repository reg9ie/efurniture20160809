<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sevenelevenmap_library {
    public $CI;
    public $STOREID = '310';

    function __construct() {
        $this->CI = & get_instance();
    }

    public function to_map($returnURL = FALSE, $test = FALSE) {
        if($returnURL === FALSE) return FALSE;

        if($test) $postURL = 'http://202.168.204.210/EC3G829/emap/eServiceMap.php';
        else $postURL = 'http://202.168.204.216/EC3G829/emap/eServiceMap.php';
        
        $input = array(
            'eshopid' => $this->STOREID,
            'servicetype' => 3,
            'url' => $returnURL,
            'tempvar' => '',
            'display' => 'page',
            'charset' => 'utf8'
        );

        $this->CI->template_library->form_submit(array(
            'data' => array('postURL' => $postURL, 'input' => $input)
        ));
    }
}