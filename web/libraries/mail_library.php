<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_library {
    var $CI;
    var $config;
    var $queue;

    function __construct() {
        $this->CI = & get_instance();
        $this->config = Config::all_to_array();
        $this->queue = filter_var($this->config['email_queue'], FILTER_VALIDATE_BOOLEAN);
    }

    public function test($email = false) {
        if($email === false) $email = $this->config['admin_email'];
        if(valid_email($email) === false) return false;
        
        $data['config'] = $this->config;
        $data['email'] = $email;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '測試',
            $this->CI->load->view(configLang.'/email/test', $data, true));
    }

    public function user_new_web($email = false) {
        if($email === false || valid_email($email) === false) return false;
        
        $data['config'] = $this->config;
        $data['email'] = $email;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '網站轉移',
            $this->CI->load->view(configLang.'/email/user_new_web', $data, true));
    }

    public function user_register($email = false, $user = false) {
        if($email === false || valid_email($email) === false) return false;
        if($user === false) return false;
        
        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['user'] = $user;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '會員註冊',
            $this->CI->load->view(configLang.'/email/user_register', $data, true));
    }

    public function user_forget_password($email = false, $user = false) {
        if($email === false || valid_email($email) === false) return false;
        if($user === false) return false;
        
        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['user'] = $user;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '密碼重新設定',
            $this->CI->load->view(configLang.'/email/user_forget_password', $data, true));
    }

    public function user_order_buy($email = false, $order = false) {
        if($email === false || valid_email($email) === false) return false;
        if($order === false) return false;

        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '訂單成立',
            $this->CI->load->view(configLang.'/email/user_order_buy', $data, true));
    }

    public function user_order_paid($email = false, $order = false) {
        if($email === false || valid_email($email) === false) return false;
        if($order === false) return false;
        
        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '訂單已付款',
            $this->CI->load->view(configLang.'/email/user_order_paid', $data, true));
    }

    public function user_order_shipping($email = false, $order = false) {
        if($email === false || valid_email($email) === false) return false;
        if($order === false) return false;

        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '訂單出貨中',
            $this->CI->load->view(configLang.'/email/user_order_shipping', $data, true));
    }

    public function user_order_canceled($email = false, $order = false) {
        if($email === false || valid_email($email) === false) return false;
        if($order === false) return false;

        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '訂單取消',
            $this->CI->load->view(configLang.'/email/user_order_canceled', $data, true));
    }

    public function sys_contact($email = false, $title = false, $content = false, $name = false, $telephone = false) {
        if($email === false) return false;
        if($title === false) return false;
        if($content === false) return false;
        if($name === false) return false;
        
        if(!$this->config['admin_email'] || valid_email($this->config['admin_email']) === false) return false;
        if(valid_email($email) === false) return false;

        $date = date('Y-m-d H:i:s');
        $data['config'] = $this->config;
        $data['date'] = $date;
        $data['email'] = $email;
        $data['name'] = $name;
        $data['telephone'] = ($telephone ? $telephone : '');
        $data['title'] = $title;
        $data['content'] = nl2br($content);

        $this->send(
            $this->queue,
            $this->config['admin_email'],
            $email,
            '聯絡我們',
            $this->CI->load->view(configLang.'/email/sys_contact', $data, true));
    }

    public function sys_contact_reply($email = false, $title = false, $content = false, $name = false, $telephone = false, $reply_content = false) {
        if($email === false) return false;
        if($title === false) return false;
        if($content === false) return false;
        if($name === false) return false;
        if($telephone === false) return false;
        if($reply_content === false) return false;
        
        if(!$this->config['admin_email'] || valid_email($this->config['admin_email']) === false) return false;
        if(valid_email($email) === false) return false;
        $date = date('Y-m-d H:i:s');

        $data['config'] = $this->config;
        $data['date'] = $date;
        $data['name'] = $name;
        $data['email'] = $email;
        $data['telephone'] = $telephone;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['reply_content'] = $reply_content;

        $this->send(
            $this->queue,
            $email,
            $this->config['admin_email'],
            '聯絡我們: '.$title,
            $this->CI->load->view(configLang.'/email/sys_contact_reply', $data, true));
    }

    public function sys_order_buy($email = false, $order = false) {
        if($email === false) return false;
        if($order === false) return false;

        if(!$this->config['admin_email'] || valid_email($this->config['admin_email']) === false) return false;
        if(valid_email($email) === false) return false;

        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $this->config['admin_email'],
            $this->config['admin_email'],
            '新訂單',
            $this->CI->load->view(configLang.'/email/sys_order_buy', $data, true));
    }

    public function sys_order_paid($email = false, $order = false) {
        if(!$this->config['admin_email'] || valid_email($this->config['admin_email']) === false) return false;
        if($email === false || valid_email($email) === false) return false;
        if($order === false) return false;

        $data['config'] = $this->config;
        $data['email'] = $email;
        $data['order'] = $order;

        $this->send(
            $this->queue,
            $this->config['admin_email'],
            $this->config['admin_email'],
            '訂單已付款',
            $this->CI->load->view(configLang.'/email/sys_order_paid', $data, true));
    }

    public function send($queue = false, $to_email = false, $reply_email = false, $subject = false, $message = false) {
        if($to_email === false) return false;
        if($reply_email === false) return false;
        if($subject === false) return false;
        if($message === false) return false;

        if($queue)
        {
            MailQueue::create(array(
                'to_email' => $to_email,
                'reply_email' => $reply_email,
                'subject' => $subject,
                'message' => $message
            ));
            return true;
        }

        $objMailAccount = MailAccount::find(array(
            'conditions' => '`enable` = 1 AND `delete` = 0 AND (`limit`-1) > `today_times`',
            'order' => '`today_times` ASC'
        ));
        if($objMailAccount) {
            $this->CI->load->library('email');
            $this->CI->email->initialize([
                'protocol' => 'smtp',
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n",
                'smtp_host' => $objMailAccount->host,
                'smtp_port' => $objMailAccount->port,
                'smtp_user' => $objMailAccount->account,
                'smtp_pass' => $objMailAccount->password,
                'smtp_timeout' => 60
            ]);
            $this->CI->email->from($objMailAccount->email, $objMailAccount->name);
            $this->CI->email->to($to_email);
            $this->CI->email->reply_to($reply_email);
            $this->CI->email->cc($objMailAccount->cc);
            $this->CI->email->bcc($objMailAccount->bcc);
            $this->CI->email->subject($this->config['site_name'].$subject);
            $this->CI->email->message($message);

            if($this->CI->email->send()) {
                $objMailAccount->update_attributes(['today_times' => $objMailAccount->today_times + 1]);
                return true;
            }
            
            return $this->CI->email->print_debugger();
        }
    }
}