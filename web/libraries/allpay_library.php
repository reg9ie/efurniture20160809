<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Allpay_library {
    var $CI;
    var $config;

    function __construct() {
        $this->CI = & get_instance();
        $this->config = Config::all_to_array();
    }

    public function emap($cart = false) {
        if($cart === false) return false;
        if(!isset($cart['shipping_id'])) return false;
        if(!isset($cart['shipping_code'])) return false;
        if(!isset($cart['payment_id'])) return false;
        if(!isset($cart['payment_code'])) return false;

        $MerchantID = $this->config['allpay_merchant_id']; // 廠商編號 varchar(10)
        $MerchantTradeNo = date('YmdHis'); // 廠商交易編號 varchar(20)
        $LogisticsType = 'CVS'; // 物流類型 varchar(20), CVS
        $LogisticsSubType = ''; // 物流子類型 varchar(20)
                                    // 全家 FAMI
                                    // 全家店到店 FAMIC2C
                                    // 統一超商 UNIMART
                                    // 統一超商交貨便 UNIMARTC2C
        $IsCollection = ($cart['payment_code'] == 'cvs' ? 'Y' : 'N'); // 是否代收貨款 varchar(1)
                                                                        // 不代收貨款 N
                                                                        // 代收貨款 Y
        $ServerReplyURL = site_url('api/cart/allpay_emap_receiver/'); // Server 端回覆網址 varchar(200)
        $ExtraData = ''; // 額外資訊 varchar(20)
        $Device = ''; // 使用設備 Int, 0
                        // 電腦 0
                        // 行動裝置 1

        if($cart['shipping_code'] == 'family') { // 全家
            $LogisticsSubType = 'FAMI';
        }
        elseif($cart['shipping_code'] == 'family_c2c') { // 全家店到店
            $LogisticsSubType = 'FAMIC2C';
        }
        elseif($cart['shipping_code'] == '711') { // 統一超商
            $LogisticsSubType = 'UNIMART';
        }
        elseif($cart['shipping_code'] == '711_c2c') { // 統一超商交貨便
            $LogisticsSubType = 'UNIMARTC2C';
        }
        else return false;

        $input = [
            'MerchantID' => $MerchantID,
            'MerchantTradeNo' => $MerchantTradeNo,
            'LogisticsType' => $LogisticsType,
            'LogisticsSubType' => $LogisticsSubType,
            'IsCollection' => $IsCollection,
            'ServerReplyURL' => $ServerReplyURL,
            'ExtraData' => $ExtraData,
            'Device' => $Device,
        ];

        $postURL = 'https://logistics.allpay.com.tw/Express/map';
        if($this->config['allpay_test']) $postURL = 'http://logistics-stage.allpay.com.tw/Express/map';

        $this->CI->template_library->form_submit([
            'data' => ['postURL' => $postURL, 'input' => $input]
        ]);
        return true;
    }

    public function checkout($order = false) {
        if($order === false) return false;
        if(!isset($order['no'])) return false;
        if(!isset($order['payment_id'])) return false;
        if(!isset($order['payment_code'])) return false;
        if(!isset($order['price_total'])) return false;
        if(!isset($order['created_at'])) return false;

        $MerchantID = $this->config['allpay_merchant_id']; // 廠商編號 varchar(10)
        $MerchantTradeNo = $order['no']; // 廠商交易編號 varchar(20), 自訂 不可重複 英數字大小寫
        $MerchantTradeDate = date('Y/m/d H:i:s', strtotime($order['created_at']));  // 廠商交易時間 varchar(20), yyyy/MM/dd HH:mm:ss
        $PaymentType = 'aio'; // 交易類型 varchar(20), aio
        $TotalAmount = $order['price_total']; // 交易金額 Money, 整數, 新台幣, 不可為 0, CVS & BARCODE 30 以上
        $TradeDesc = $this->config['site_name']; // 交易描述 varchar(200)
        $ItemName = $this->config['allpay_item_name']; // 商品名稱 varchar(200), 以 # 分隔不同商品
        $ReturnURL = site_url('api/order/allpay_receiver/'.$order['payment_code'].'/'.$order['no']); // 付款完成通知回傳網址 varchar(200)
        $ChoosePayment = ''; // 付款方式 varchar(20)
                                // 信用卡 Credit
                                // 網路 ATM WebATM
                                // 自動櫃員機 ATM
                                // 超商代碼 CVS
                                // 超商條碼 BARCODE
                                // 支付寶 Alipay
                                // 財付通 Tenpay
                                // 儲值消費 TopUpUsed
                                // 不指定付款方式 ALL
        $PaymentInfoURL = '';
        $CreditInstallment = '';
        $InstallmentAmount = '';
        if($order['payment_code'] == 'credit') { // 信用卡
            $ChoosePayment = 'Credit';
        }
        elseif($order['payment_code'] == 'credit3') { // 信用卡 分3期
            $ChoosePayment = 'Credit';
            $CreditInstallment = 3; // 刷卡分期期數 int
            $InstallmentAmount = $TotalAmount;
        }
        elseif($order['payment_code'] == 'credit6') { // 信用卡 分6期
            $ChoosePayment = 'Credit';
            $CreditInstallment = 6; // 刷卡分期期數 int
            $InstallmentAmount = $TotalAmount;
        }
        elseif($order['payment_code'] == 'credit9') { // 信用卡 分9期
            $ChoosePayment = 'Credit';
            $CreditInstallment = 9; // 刷卡分期期數 int
            $InstallmentAmount = $TotalAmount;
        }
        elseif($order['payment_code'] == 'credit12') { // 信用卡 分12期
            $ChoosePayment = 'Credit';
            $CreditInstallment = 12; // 刷卡分期期數 int
            $InstallmentAmount = $TotalAmount;
        }
        elseif($order['payment_code'] == 'atm') { // 自動櫃員機
            $ChoosePayment = 'ATM';
            $PaymentInfoURL = $ReturnURL;
        }
        elseif($order['payment_code'] == 'cvs') { // 超商代碼
            $ChoosePayment = 'CVS';
            $StoreExpireDate = ''; // 超商繳費截止時間 int, 預設 7天
            $PaymentInfoURL = $ReturnURL;
        }
        elseif($order['payment_code'] == 'barcode') { // 超商條碼
            $ChoosePayment = 'BARCODE';
            $StoreExpireDate = ''; // 超商繳費截止時間 int, 預設 7天
            $PaymentInfoURL = $ReturnURL;
        }
        $ClientBackURL = site_url('api/order/allpay_client_back/'.$order['payment_code'].'/'.$order['no']); // 返回廠商網址 varchar(200)
        $OrderResultURL = site_url('api/order/allpay_client_back/'.$order['payment_code'].'/'.$order['no']); // 回傳付款結果網址 varchar(200)
        $HashKey = $this->config['allpay_hash_key'];
        $HashIV = $this->config['allpay_hash_iv'];
        if($this->config['allpay_test']) { // 測試帳號
            $MerchantID = '2000132';
            $TradeDesc = $this->config['site_name'].' 測試付款';
            $HashKey = '5294y06JbISpM5x9';
            $HashIV = 'v77hoKGq4kWxNNIS';
        }
        // 檢查碼
        $CheckMacValue = 'HashKey='.$HashKey.'&ChoosePayment='.$ChoosePayment.'&ClientBackURL='.$ClientBackURL.
                         ($CreditInstallment ? '&CreditInstallment='.$CreditInstallment : '').
                         ($InstallmentAmount ? '&InstallmentAmount='.$InstallmentAmount : '').
                         '&ItemName='.$ItemName.'&MerchantID='.$MerchantID.'&MerchantTradeDate='.$MerchantTradeDate.
                         '&MerchantTradeNo='.$MerchantTradeNo.'&OrderResultURL='.$OrderResultURL.
                         ($PaymentInfoURL ? '&PaymentInfoURL='.$PaymentInfoURL : '').
                         '&PaymentType=aio&ReturnURL='.$ReturnURL.
                         '&TotalAmount='.$TotalAmount.'&TradeDesc='.$TradeDesc.'&HashIV='.$HashIV;
        $CheckMacValue = urlencode($CheckMacValue);
        $CheckMacValue = strtolower($CheckMacValue);
        $CheckMacValue = md5($CheckMacValue);

        $input = [
            'MerchantID' => $MerchantID,
            'MerchantTradeNo' => $MerchantTradeNo,
            'MerchantTradeDate' => $MerchantTradeDate,
            'PaymentType' => $PaymentType,
            'TotalAmount' => $TotalAmount,
            'TradeDesc' => $TradeDesc,
            'ItemName' => $ItemName,
            'ReturnURL' => $ReturnURL,
            'ClientBackURL' => $ClientBackURL,
            'OrderResultURL' => $OrderResultURL,
            'ChoosePayment' => $ChoosePayment,
            'CheckMacValue' => $CheckMacValue
        ];
        if($PaymentInfoURL) $input['PaymentInfoURL'] = $PaymentInfoURL;
        if($CreditInstallment) $input['CreditInstallment'] = $CreditInstallment;
        if($InstallmentAmount) $input['InstallmentAmount'] = $InstallmentAmount;

        $postURL = 'https://payment.allpay.com.tw/Cashier/AioCheckOut';
        if($this->config['allpay_test']) $postURL = 'http://payment-stage.allpay.com.tw/Cashier/AioCheckOut';

        $this->CI->template_library->form_submit([
            'data' => ['postURL' => $postURL, 'input' => $input]
        ]);
        return true;
    }

    public function logistics($order = false) {}

    public function payment_name($payment_type = false) {
        if($payment_type === false) return false;

        $types = [
            'WebATM_TAISHIN' => '台新銀行 WebATM',
            'WebATM_ESUN' => '玉山銀行 WebATM',
            'WebATM_HUANAN' => '華南銀行 WebATM',
            'WebATM_BOT' => '台灣銀行 WebATM',
            'WebATM_FUBON' => '台北富邦 WebATM',
            'WebATM_CHINATRUST' => '中國信託 WebATM',
            'WebATM_FIRST' => '第一銀行 WebATM',
            'WebATM_CATHAY' => '國泰世華 WebATM',
            'WebATM_MEGA' => '兆豐銀行 WebATM',
            'WebATM_YUANTA' => '元大銀行 WebATM',
            'WebATM_LAND' => '土地銀行 WebATM',
            'ATM_TAISHIN' => '台新銀行 ATM',
            'ATM_ESUN' => '玉山銀行 ATM',
            'ATM_HUANAN' => '華南銀行 ATM',
            'ATM_BOT' => '台灣銀行 ATM',
            'ATM_FUBON' => '台北富邦 ATM',
            'ATM_CHINATRUST' => '中國信託 ATM',
            'ATM_FIRST' => '第一銀行 ATM',
            'CVS_CVS' => '超商代碼繳款',
            'CVS_OK' => 'OK 超商代碼繳款',
            'CVS_FAMILY' => '全家超商代碼繳款',
            'CVS_HILIFE' => '萊爾富超商代碼繳款',
            'CVS_IBON' => '7-11 ibon 代碼繳款',
            'BARCODE_BARCODE' => '超商條碼繳款',
            'Alipay_Alipay' => '支付寶',
            'Tenpay_Tenpay' => '財付通',
            'Credit_CreditCard' => '信用卡',
            'TopUpUsed_AllPay' => '儲值/餘額消費_歐付寶',
            'TopUpUsed_ESUN' => '儲值/餘額消費_玉山銀行',
        ];

        if(array_key_exists($payment_type, $types)) return $types[$payment_type];
        
        return false;
    }
}