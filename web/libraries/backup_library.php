<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Backup_library {
    var $CI = false;
    
    function __construct() {
        $this->CI =& get_instance();
    }
    
    public function check($date = false) {
        if($date === false) return false;
        if(!file_exists(configPathBackup)) @mkdir(configPathBackup, 0777);

        $dest = configPathBackup.$date.'.zip';
        if(file_exists($dest)) return true;

        return false;
    }

    public function backup($date = false) {
        if($date === false) return false;
        if(!file_exists(configPathBackup)) return false;
        
        $files = glob(configPathBackup.'*.zip');
        if(count($files) > 1) unlink($files[0]);
        
        $source = configPathBackup.$date.'/';
        $dest = configPathBackup.$date.'.zip';
        
        if(file_exists($source)) return false;
        if(file_exists($dest)) return false;

        $dirs = glob(configPathBackup.'*', GLOB_ONLYDIR);
        foreach((array)$dirs as $i => $d) shell_exec('rm -rf '.$d);

        @mkdir($source, 0777);
        @mkdir($source.'assets', 0777);
        @mkdir($source.'assets/images', 0777);
        @mkdir($source.'assets/files', 0777);

        $this->CI->load->dbutil();
        $backup = $this->CI->dbutil->backup(['ignore' => ['error_logs', 'tracking_logs'], 'format' => 'txt']);
        write_file($source.'db.sql', $backup);
        @chmod($source.'db.sql', 0777);

        shell_exec('cp -r '.configPathBase.'assets/images/. '.$source.'assets/images');
        shell_exec('cp -r '.configPathBase.'assets/files/. '.$source.'assets/files');
        shell_exec('zip -r '.$dest.' '.$source);
        shell_exec('rm -rf '.$source);
        @chmod($dest, 0777);
    }
}