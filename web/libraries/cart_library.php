<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_library {
    var $CI;
    var $config;

    function __construct() {
        $this->CI = & get_instance();
        $this->config = Config::all_to_array();
    }

    public function get_cart() {
        $cart = $this->CI->session->userdata('cart');
        if(!$cart) {
            $cart = [
                'buyer_name' => '',
                'buyer_gender' => '',
                'buyer_cellphone' => '',
                'buyer_telephone' => '',
                'buyer_zip' => '',
                'buyer_address' => '',
                'buyer_email' => '',
                'recipient_name' => '',
                'recipient_gender' => '',
                'recipient_cellphone' => '',
                'recipient_telephone' => '',
                'recipient_zip' => '',
                'recipient_address' => '',
                'recipient_email' => '',
                'recipient_note' => '',
                'payment_id' => '',
                'payment_name' => '',
                'payment_code' => '',
                'payment_use' => 0,
                'shipping_id' => '',
                'shipping_name' => '',
                'shipping_code' => '',
                'shipping_price' => 0,
                'shipping_free' => 0,
                'cvs_type' => '',
                'cvs_code' => '',
                'cvs_name' => '',
                'cvs_telephone' => '',
                'cvs_zip' => '',
                'cvs_address' => '',
                'count' => 0,
                'point_use' => 0,
                'point_feedback' => 0,
                'price_all' => 0,
                'price_discount' => 0,
                'price_shipping' => 0,
                'price_total' => 0,
                'invoice_no' => '',
                'invoice_type' => '',
                'invoice_business_no' => '',
                'invoice_business_name' => '',
                'invoice_business_zip' => '',
                'invoice_business_address' => '',
                'invoice_note' => '',
                'invoice_date' => '',
                'invoice_donate' => '',
                'products' => [],
            ];

            $objPayment = Payment::find([
                'conditions' => '`enable` = 1 AND `delete` = 0',
                'order' => '`sort` ASC',
            ]);
            if($objPayment) {
                $cart['payment_id'] = $objPayment->id;
                $cart['payment_name'] = strip_tags($objPayment->name);
                $cart['payment_code'] = $objPayment->code;
                $cart['payment_use'] = $objPayment->use;
            }

            $objShipping = Shipping::find([
                'conditions' => '`enable` = 1 AND `delete` = 0',
                'order' => '`sort` ASC',
            ]);
            if($objShipping) {
                $cart['shipping_id'] = $objShipping->id;
                $cart['shipping_name'] = $objShipping->name;
                $cart['shipping_code'] = $objShipping->code;
                $cart['shipping_price'] = $objShipping->price;
                $cart['shipping_free'] = $objShipping->free;
            }
        }

        $objConfig_product_vip_discount = Config::find_by_name('product_vip_discount');
        $objConfig_product_vip_discount_enable = Config::find_by_name('product_vip_discount_enable');
        $objConfig_cart_user = Config::find_by_name('cart_user');
        $objConfig_cart_discount = Config::find_by_name('cart_discount');
        $objConfig_cart_discount_enable = Config::find_by_name('cart_discount_enable');
        $cart['config_product_vip_discount'] = $objConfig_product_vip_discount ? $objConfig_product_vip_discount->value : false;
        $cart['config_product_vip_discount_enable'] = $objConfig_product_vip_discount_enable ? filter_var($objConfig_product_vip_discount_enable->value, FILTER_VALIDATE_BOOLEAN) : false;
        $cart['config_cart_user'] = $objConfig_cart_user ? $objConfig_cart_user->value : false;
        $cart['config_cart_discount'] = $objConfig_cart_discount ? $objConfig_cart_discount->value : false;
        $cart['config_cart_discount_enable'] = $objConfig_cart_discount_enable ? filter_var($objConfig_cart_discount_enable->value, FILTER_VALIDATE_BOOLEAN) : false;

        $objUser = User::get();
        if($cart['config_cart_user'] && $objUser) {
            $cart['buyer_name'] = $objUser->name;
            $cart['buyer_gender'] = $objUser->gender;
            $cart['buyer_cellphone'] = $objUser->cellphone;
            $cart['buyer_telephone'] = $objUser->telephone;
            $cart['buyer_zip'] = $objUser->zip;
            $cart['buyer_address'] = $objUser->address;
            $cart['buyer_email'] = $objUser->email;
            $cart['recipient_name'] = $cart['recipient_name'] ? $cart['recipient_name'] : $objUser->name;
            $cart['recipient_gender'] = $cart['recipient_gender'] ? $cart['recipient_gender'] : $objUser->gender;
            $cart['recipient_cellphone'] = $cart['recipient_cellphone'] ? $cart['recipient_cellphone'] : $objUser->cellphone;
            $cart['recipient_telephone'] = $cart['recipient_telephone'] ? $cart['recipient_telephone'] : $objUser->telephone;
            $cart['recipient_zip'] = $cart['recipient_zip'] ? $cart['recipient_zip'] : $objUser->zip;
            $cart['recipient_address'] = $cart['recipient_address'] ? $cart['recipient_address'] : $objUser->address;
            $cart['recipient_email'] = $cart['recipient_email'] ? $cart['recipient_email'] : $objUser->email;
            $cart['cvs_type'] = $cart['cvs_type'] ? $cart['cvs_type'] : $objUser->cvs_type;
            $cart['cvs_code'] = $cart['cvs_code'] ? $cart['cvs_code'] : $objUser->cvs_code;
            $cart['cvs_name'] = $cart['cvs_name'] ? $cart['cvs_name'] : $objUser->cvs_name;
            $cart['cvs_telephone'] = $cart['cvs_telephone'] ? $cart['cvs_telephone'] : $objUser->cvs_telephone;
            $cart['cvs_zip'] = $cart['cvs_zip'] ? $cart['cvs_zip'] : $objUser->cvs_zip;
            $cart['cvs_address'] = $cart['cvs_address'] ? $cart['cvs_address'] : $objUser->cvs_address;

            if($objUser->vip && $cart['config_product_vip_discount_enable']) $cart['user_vip'] = true;
        }

        $cart = $this->set_cart($cart);

        return $cart;
    }

    public function add($product_id = false, $quantity = false) {
        if($product_id === false) return false;
        if($quantity === false) return false;

        $cart = $this->get_cart();
        $product = $this->get_product($product_id);
        $product['quantity'] = $product['quantity'] + $quantity;

        $find = false;
        foreach((array)$cart['products'] as $i => $p) {
            if($find !== false) continue;
            if($p['product_id'] == $product_id) $find = $i;
        }
        if($find === false) {
            $cart['products'][] = $product;
        }
        else {
            $cart['products'][$find]['price'] = $product['price'];
            $cart['products'][$find]['quantity'] = $cart['products'][$find]['quantity'] + $product['quantity'];
        }

        return $this->set_cart($cart);
    }

    public function edit($product_id = false, $quantity = false) {
        if($product_id === false) return false;
        if($quantity === false) return false;

        $cart = $this->get_cart();
        $product = $this->get_product($product_id);
        $product['quantity'] = (int) $quantity;

        $find = false;
        foreach((array)$cart['products'] as $i => $p) {
            if($find !== false) continue;
            if($p['product_id'] == $product_id) $find = $i;
        }
        if($find === false) {
            $cart['products'][] = $product;
        }
        else {
            $cart['products'][$find]['price'] = $product['price'];
            $cart['products'][$find]['quantity'] = (int) $product['quantity'];
        }

        return $this->set_cart($cart);
    }

    public function del($product_id = false) {
        if($product_id === false) return false;

        $cart = $this->get_cart();

        $find = false;
        foreach((array)$cart['products'] as $i => $p) {
            if($find !== false) continue;
            if($p['product_id'] == $product_id) $find = $i;
        }

        if($find !== false) {
            array_splice($cart['products'], $find, 1);
        }
        
        return $this->set_cart($cart);
    }

    public function payment($payment_id = false) {
        if($payment_id === false) return false;

        $objPayment = Payment::find_by_id_and_enable($payment_id, 1);
        if(!$objPayment) return false;

        $cart = $this->get_cart();
        $cart['payment_id'] = $objPayment->id;
        $cart['payment_name'] = strip_tags($objPayment->name);
        $cart['payment_code'] = $objPayment->code;
        $cart['payment_use'] = $objPayment->use;

        return $this->set_cart($cart);
    }

    public function shipping($shipping_id = false) {
        if($shipping_id === false) return false;

        $objShipping = Shipping::find_by_id_and_enable($shipping_id, 1);
        if(!$objShipping) return false;

        $cart = $this->get_cart();
        $cart['shipping_id'] = $objShipping->id;
        $cart['shipping_name'] = $objShipping->name;
        $cart['shipping_code'] = $objShipping->code;
        $cart['shipping_price'] = $objShipping->price;
        $cart['shipping_free'] = $objShipping->free;

        if(($objShipping->code == 'family' && $cart['cvs_type'] != 'family') ||
           ($objShipping->code == '711' && $cart['cvs_type'] != '711')) {
            $cart['cvs_type'] = $objShipping->code;
            $cart['cvs_code'] = '';
            $cart['cvs_name'] = '';
            $cart['cvs_telephone'] = '';
            $cart['cvs_zip'] = '';
            $cart['cvs_address'] = '';
        }

        return $this->set_cart($cart);
    }

    public function cvs($cvs_type = false, $cvs_code = false, $cvs_name = false, $cvs_telephone = false, $cvs_zip = false, $cvs_address = false) {
        if($cvs_name === false) return false;
        if($cvs_address === false) return false;

        $cart = $this->get_cart();
        $cart['cvs_type'] = $cvs_type ? $cvs_type : '';
        $cart['cvs_code'] = $cvs_code ? $cvs_code : '';
        $cart['cvs_name'] = $cvs_name ? $cvs_name : '';
        $cart['cvs_telephone'] = $cvs_telephone ? $cvs_telephone : '';
        $cart['cvs_zip'] = $cvs_zip ? $cvs_zip : '';
        $cart['cvs_address'] = $cvs_address ? $cvs_address : '';

        return $this->set_cart($cart);
    }

    public function point($point_use = false) {
        $objUser = User::get();
        if($objUser === false) return false;
        if($point_use === false) return false;

        $cart = $this->get_cart();

        $price = $cart['price_all'] + $cart['price_shipping'] - $cart['price_discount'];
        
        if($point_use > $objUser->point) $point_use = $objUser->point;
        if($point_use > $price) $point_use = $price;

        $cart['point_use'] = (int)$point_use;

        return $this->set_cart($cart);
    }

    private function get_product($product_id = false) {
        if($product_id === false) return false;

        $objProduct = Product::find_by_id_and_enable_and_delete($product_id, 1, 0);
        if(!$objProduct) return false;

        $price = $objProduct->price;
        if($objProduct->price_special) {
            $special = true;
            $price = $objProduct->price_special;
        }
        
        $product = array(
            'ptype_id' => $objProduct->ptype_id,
            'ptype_name' => $objProduct->ptype_name,
            'ptype_code' => $objProduct->ptype_code,
            'pcategory_id' => $objProduct->pcategory_id,
            'pcategory_name' => $objProduct->pcategory_name,
            'product_id' => $objProduct->id,
            'product_no' => $objProduct->no,
            'product_name' => $objProduct->name,
            'product_title' => $objProduct->title,
            'product_image' => $objProduct->image,
            'product_image_thumb' => thumbname($objProduct->image),
            'product_price' => $objProduct->price,
            'product_price_special' => $objProduct->price_special,
            'product_price_special_start' => '',
            'product_price_special_end' => '',
            'product_special' => $special,
            'product_price_usd' => $objProduct->price_usd,
            'product_price_special_usd' => $objProduct->price_special_usd,
            'product_price_special_start_usd' => '',
            'product_price_special_end_usd' => '',
            'product_price_cad' => $objProduct->price_cad,
            'product_price_special_cad' => $objProduct->price_special_cad,
            'product_price_special_start_cad' => '',
            'product_price_special_end_cad' => '',
            'price' => $price,
            'quantity' => 0,
            'discount' => 0,
            'subtotal' => 0,
            'stock' => $objProduct->stock
        );

        return $product;
    }

    private function set_cart($cart = false) {
        if($cart === false) return false;

        $cart['count'] = 0;
        $cart['price_all'] = 0;
        foreach((array)$cart['products'] as $i => $p) {
            $objProduct = Product::find_by_id_and_enable_and_delete($p['product_id'], 1, 0);
            if($objProduct && $p['quantity'] > $objProduct->stock) {
                $p['quantity'] = $objProduct->stock;
                $cart['products'][$i]['quantity'] = $p['quantity'];
            }

            $p['subtotal'] = $p['price'] * $p['quantity'] - $p['discount'];
            $cart['price_all'] = $cart['price_all'] + $p['subtotal'];
            $cart['products'][$i]['subtotal'] = $p['subtotal'];

            $cart['count'] = $cart['count'] + $p['quantity'];

            $cart['products'][$i]['price'] = number_format($p['price'], configNumberOfDecimalPoints);
            $cart['products'][$i]['price_special'] = number_format($p['price_special'], configNumberOfDecimalPoints);
            $cart['products'][$i]['price_cad'] = number_format($p['price_cad'], configNumberOfDecimalPoints);
            $cart['products'][$i]['price_special_cad'] = number_format($p['price_special_cad'], configNumberOfDecimalPoints);
            $cart['products'][$i]['subtotal'] = number_format($p['subtotal'], configNumberOfDecimalPoints);
        }

        $cart['price_discount'] = 0;

        if($cart['shipping_free'] != 0 && $cart['price_all'] >= $cart['shipping_free']) $cart['price_shipping'] = 0;
        else $cart['price_shipping'] = $cart['shipping_price'];

        $cart['price_total'] = $cart['price_all'] - $cart['price_discount'] + $cart['price_shipping'];

        $cart['price_all'] = number_format($cart['price_all'], configNumberOfDecimalPoints);
        $cart['price_discount'] = number_format($cart['price_discount'], configNumberOfDecimalPoints);
        $cart['price_shipping'] = number_format($cart['price_shipping'], configNumberOfDecimalPoints);
        $cart['price_total'] = number_format($cart['price_total'], configNumberOfDecimalPoints);

        $this->CI->session->set_userdata(array('cart' => $cart));

        return $cart;
    }
}