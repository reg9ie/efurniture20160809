<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Statistics_library {
    var $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    public function order_calc($date = FALSE) {
        if($date === FALSE) $date = date('Y-m-1');
        
        $date = date('Y-m-1 00:00:00', strtotime($date));
        $end = date('Y-m-t 23:59:59', strtotime($date));
        $data['price_all'] = 0;
        $data['price_shipping'] = 0;
        $data['price_discount'] = 0;
        $data['price_total'] = 0;
        $data['point_use'] = 0;
        $data['point_feedback'] = 0;
        $data['count_order'] = 0;
        $data['count_product'] = 0;
        $data['count_user_all'] = 0;
        $data['count_user_member'] = 0;
        $data['count_user_vip'] = 0;
        $data['count_user_black'] = 0;
        $data['count_status_remit'] = 0;
        $data['count_status_pay'] = 0;
        $data['count_status_ship'] = 0;
        $data['count_status_return'] = 0;
        $data['count_status_refund'] = 0;
        $data['count_status_enable'] = 0;
        $data['count_status_cancel'] = 0;
        $data['count_status_delete'] = 0;

        $objOrders = Order::all(array(
            'conditions' => '`delete` = 0 AND `created_at` >= "'.$date.'" AND `created_at` <= "'.$end.'"'
        ));
        foreach((array)$objOrders as $i => $e)
        {
            $data['price_all'] += ($e->enable == 1 ? $e->price_all : 0);
            $data['price_shipping'] += ($e->enable == 1 ? $e->price_shipping : 0);
            $data['price_discount'] += ($e->enable == 1 ? $e->price_discount : 0);
            $data['price_total'] += ($e->enable == 1 ? $e->price_total : 0);
            $data['point_use'] += ($e->enable == 1 ? $e->point_use : 0);
            $data['point_feedback'] += ($e->enable == 1 ? $e->point_feedback : 0);
            $data['count_order'] += 1;
            $data['count_product'] += ($e->enable == 1 ? $e->count : 0);
            $data['count_user_all'] += 1;
            $data['count_user_member'] += ($e->user_vip ? 0 : 1);
            $data['count_user_vip'] += ($e->user_vip ? 1 : 0);
            $data['count_user_black'] += ($e->user_black ? 1 : 0);
            $data['count_status_remit'] += ($e->enable == 1 && $e->status_remit ? 1 : 0);
            $data['count_status_pay'] += ($e->enable == 1 && $e->status_pay ? 1 : 0);
            $data['count_status_ship'] += ($e->enable == 1 && $e->status_ship ? 1 : 0);
            $data['count_status_return'] += ($e->enable == 1 && $e->status_return ? 1 : 0);
            $data['count_status_refund'] += ($e->enable == 1 && $e->status_refund ? 1 : 0);
            $data['count_status_enable'] += ($e->enable == 1 && $e->enable ? 1 : 0);
            $data['count_status_cancel'] += ($e->enable == 1 && $e->enable ? 0 : 1);
            $data['count_status_delete'] += ($e->delete ? 1 : 0);
        }

        $objStatisticsorder = Statisticsorder::find_by_date($date);
        if(!$objStatisticsorder)
        {
            $data['date'] = $date;
            $objStatisticsorder = Statisticsorder::create($data);
        }
        else
        {
            $objStatisticsorder->update_attributes($data);
        }

        $statisticsorder = $objStatisticsorder->to_array();

        return $statisticsorder;
    }
}