<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Paypal_library {
    var $CI;
    var $config;

    function __construct() {
        $this->CI = & get_instance();
        $this->config = Config::all_to_array();
    }

    public function checkout($order = false) {
        if($order === false) return false;
        if(!isset($order['no'])) return false;
        if(!isset($order['payment_id'])) return false;
        if(!isset($order['payment_code'])) return false;
        if(!isset($order['price_total'])) return false;
        if(!isset($order['created_at'])) return false;

        // 參考 Buy Now: Sample HTML button code
        // https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/html_example_buy_now

        $input = [];
        /*
        if(count($order['order_products']) == 1) {
            $input['item_name'] = $order['order_products'][0]['product_name'];                  // 商品名稱, $this->config['paypal_item_name'];
            // $input['item_number'] = $order['order_products'][0]['product_no'];               // 商品編號
            // $input['quantity'] = $order['order_products'][0]['quantity'];                    // 商品數量
        }
        else {
            foreach ($order['order_products'] as $i => $p) {
                $input['item_name_'.($i+1)] = $p['product_name'];                               // 商品名稱
                // $input['item_number_'.($i+1)] = $p['product_no'];                            // 商品編號
                // $input['quantity_'.($i+1)] = $p['quantity'];                                 // 商品數量
            }
        }
        */
        $input['item_name'] = $this->config['paypal_item_name'];                // 商品名稱
        $input['amount'] = $order['price_total'];                               // 總金額
        // $input['shipping'] = '';                                             // 運費
        // $input['tax'] = '';                                                  // 稅
        // $input['undefined_quantity'] = '1';                                  // 數量
        $input['currency_code'] = 'USD';                                        // 幣別: USD, CAD, EUR, GBP, TWD
        $input['custom'] = $order['no'];                                        // 自訂，訂單編號
        // $input["<first_name>"] = '';
        // $input["<last_name>"] = '';
        // $input["<email>"] = '';
        // $input["<night_phone_a>"] = '';
        // $input["<night_phone_b>"] = '';
        // $input["<night_phone_c>"] = '';
        // $input["<zip>"] = '';
        // $input["<country>"] = '';
        // $input["<state>"] = '';
        // $input["<city>"] = '';
        // $input["<address1>"] = '';
        // $input["<address2>"] = '';
        $input['charset'] = 'utf-8';                                            // 編碼
        $input['business'] = $this->config['paypal_account'];                   // 帳號
        $input['rm'] = '2';                                                     // 0: GET, 1: GET, 2: POST
        $input['cmd'] = '_xclick';                                              // 直接購買
        $input['notify_url'] = site_url('api/order/paypal_receiver/'.$order['payment_code'].'/'.$order['no']);          // 付款完成訊息返回網址
        $input['return'] = site_url('api/order/paypal_receiver/'.$order['payment_code'].'/'.$order['no']);              // 付款完成返回網址
        $input['cancel_return'] = site_url('api/order/paypal_cancel/'.$order['payment_code'].'/'.$order['no']);         // 交易取消返回網址
        // 幣別參考: https://www.paypal.com/cgi-bin/webscr?cmd=_batch-payment-format-outside

        $postURL = 'https://www.paypal.com/cgi-bin/webscr';
        if($this->config['paypal_test']) $postURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

        $this->CI->template_library->form_submit([
            'data' => ['postURL' => $postURL, 'input' => $input]
        ]);
        return true;
    }
}