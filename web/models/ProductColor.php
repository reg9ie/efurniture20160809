<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductColor extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product')
    );

    static $after_save = array('update_product_image');

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');

        if($this->images) {
            $images = explode(',', $this->images);
            $this->image = $images[0];
        }
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objProductColor = ProductColor::first(array(
                'conditions' => '`product_id` = "'.$this->product_id.'" AND `delete` = "0"',
                'order' => '`sort` DESC'
            ));
            if($objProductColor) $this->sort = $objProductColor->sort+1;
        }
    }

    function update_product_image() {
        $objProduct = Product::find(['conditions' => '`id` = '.$this->product_id]);
        $objProductColor = ProductColor::find([
            'conditions' => '`product_id` = '.$this->product_id.' AND `image` != ""',
            'order' => '`sort` ASC',
        ]);
        if($objProduct && $objProductColor) {
            $objProduct->image = $objProductColor->image;
            $objProduct->save();
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/product/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/product/'.thumbname($this->image) : '';
    }

    public function images() {
        if($this->images) return explode(',', $this->images);

        return array();
    }

    public function _images() {
        if($this->images)
        {
            $images = explode(',', $this->images);
            foreach($images as $i => $image)
            {
                $images[$i] = 'assets/images/product/'.$images[$i];
            }
            return $images;
        }

        return array();
    }

    public function _images_thumb() {
        if($this->images)
        {
            $images = explode(',', $this->images);
            foreach($images as $i => $image)
            {
                $images[$i] = 'assets/images/product/'.thumbname($images[$i]);
            }
            return $images;
        }

        return array();
    }
}