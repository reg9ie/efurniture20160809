<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ArticleProduct extends ActiveRecord\Model {
    static $belongs_to = array(
        array('article'),
        array('product')
    );

    public function _image() {
        if($this->product_id)
        {
            $objProduct = Product::find_by_id($this->product_id);
            return $objProduct ? 'assets/images/product/'.$objProduct->image : '';
        }
        return false;
    }
}