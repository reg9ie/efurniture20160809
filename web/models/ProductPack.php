<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductPack extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objProductPack = ProductPack::first(array(
                'conditions' => '`delete` = 0',
                'order' => '`sort` DESC'
            ));
            if($objProductPack) $this->sort = $objProductPack->sort+1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}