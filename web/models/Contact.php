<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends ActiveRecord\Model {
    function before_save() {
        if($this->attribute_is_dirty('reply_content') &&
           $this->reply_content)
        {
            $this->reply = 1;
            $this->replied_at = date('Y-m-d H:i:s');;
        }

        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
    }
}