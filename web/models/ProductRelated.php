<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductRelated extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product')
    );

    public function _image() {
        if($this->related_id) {
            $objProduct = Product::find_by_id($this->related_id);
            return $objProduct ? 'assets/images/product/'.$objProduct->image : '';
        }
        return false;
    }
}