<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Acategory extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->lft) {
            $this->lft = 1;
            $objAcategory = Acategory::first(array(
                'conditions' => 'atype_id = "'.$this->atype_id.'" AND `delete` = 0',
                'order' => '`lft` DESC'
            ));
            if($objAcategory) $this->lft = $objAcategory->lft + 1;
        }
    }

    public function _tree_name() {
        return str_repeat('　', $this->level+1).($this->tree_name);
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}