<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends ActiveRecord\Model {
    public function _shippings() {
        return explode(',', $this->shippings);
    }
}