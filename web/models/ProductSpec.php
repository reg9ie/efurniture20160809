<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductSpec extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product')
    );

    static $after_save = array('product_edit_price');

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');

        // if($this->price && $this->attribute_is_dirty('price'))
        // {
        //     $objConfig_product_vip_discount = Config::find_by_name('product_vip_discount');
        //     if($objConfig_product_vip_discount && $objConfig_product_vip_discount->value)
        //     {
        //         $this->price_vip = round($this->price * ($objConfig_product_vip_discount->value / 10));
        //     }
        // }
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objProductSpec = ProductSpec::first(array(
                'conditions' => '`product_id` = "'.$this->product_id.'" AND `delete` = "0"',
                'order' => '`sort` DESC'
            ));
            if($objProductSpec) $this->sort = $objProductSpec->sort+1;
        }

        if(!$this->no)
        {
            $objProduct = Product::find($this->product_id);
            if($objProduct)
            {
                $number = $this->sort;
                if($number < 1000) $number = sprintf('%1$03u', $number);
                $this->no = $objProduct->no.'-S'.$number;
            }
        }
    }

    function product_edit_price() {
        // if($this->sort == 1)
        // {
        //     $objProduct = Product::find($this->product_id);
        //     if($objProduct)
        //     {
        //         $objProduct->price = $this->price;
        //         $objProduct->price_vip = $this->price_vip;
        //         $objProduct->price_member = $this->price_member;
        //         $objProduct->price_special = $this->price_special;
        //         $objProduct->save();
        //     }
        // }
        $objProduct = Product::find_by_id($this->product_id);
        $objProductSpec = ProductSpec::first(array(
            'conditions' => 'product_id = '.$this->product_id.'  AND `enable` = 1 AND `delete` = 0',
            'order' => '`sort` ASC'
        ));
        $objProduct->price = $objProductSpec->price;
        $objProduct->price_vip = $objProductSpec->price_vip;
        $objProduct->price_member = $objProductSpec->price_member;
        $objProduct->price_special = $objProductSpec->price_special;
        $objProduct->save();
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    // public function _vip_price() {
    //     $objConfigProduct_vip_discount = Config::find_by_name('product_vip_discount');
    //     $objConfigProduct_vip_discount_enable = Config::find_by_name('product_vip_discount_enable');
        
    //     if($objConfigProduct_vip_discount && $objConfigProduct_vip_discount->value &&
    //        $objConfigProduct_vip_discount_enable && $objConfigProduct_vip_discount_enable->value == 1 &&
    //        $this->__is_special() === FALSE)
    //     {
    //         return round($this->price * ($objConfigProduct_vip_discount->value/10));
    //     }

    //     return '';
    // }

    public function _special() {
        return $this->__is_special();
    }

    public function _special_time() {
        if($this->__is_special())
        {
            $objProduct = Product::find_by_id($this->product_id);
            if($objProduct)
            {
                $time = time();
                $special_start = strtotime($objProduct->special_start);
                $special_end = strtotime($objProduct->special_end);

                return timespan($time, $special_end, ' ');
            }
        }
        
        return '';
    }

    private function __is_special() {
        $objProduct = Product::find_by_id($this->product_id);
        if($objProduct)
        {
            $time = time();
            $special_start = strtotime($objProduct->special_start);
            $special_end = strtotime($objProduct->special_end);

            if($this->price_special &&
               $time >= $special_start &&
               $time <= $special_end) return TRUE;
        }

        return FALSE;
    }
}