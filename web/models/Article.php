<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Article extends ActiveRecord\Model {
    static $has_many = array(
        array('article_products')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort)
        {
            $this->sort = 999999999;
            $objArticle = Article::first(array(
                'conditions' => 'atype_id = "'.$this->atype_id.'" AND acategory_id = "'.$this->acategory_id.'" AND `delete` = 0',
                'order' => '`sort` ASC'
            ));
            if($objArticle) $this->sort = $objArticle->sort - 1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        if($this->atype_id)
        {
            $objAtype = Atype::find($this->atype_id);
            return ($objAtype && $this->image ? 'assets/images/'.$objAtype->code.'/'.$this->image : '');
        }
        return '';
    }

    public function _image_thumb() {
        if($this->atype_id)
        {
            $objAtype = Atype::find($this->atype_id);
            return ($objAtype && $this->image ? 'assets/images/'.$objAtype->code.'/'.thumbname($this->image) : '');
        }
        return '';
    }

    public function _content() {
        return mb_substr(strip_tags($this->content), 0, 100);
    }
}