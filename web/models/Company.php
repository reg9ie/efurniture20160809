<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort) {
            $this->sort = 1;
            $objCompany = Company::first([
                'conditions' => '`delete` = 0',
                'order' => '`sort` DESC'
            ]);
            if($objCompany) $this->sort = $objCompany->sort + 1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/company/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/company/'.thumbname($this->image) : '';
    }
}