<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ptype extends ActiveRecord\Model {
    static $has_many = array(
        array('product_ptypes'),
        array('ptypes', 'through' => 'product_ptypes')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objPtype = Ptype::first(array(
                'order' => '`sort` DESC'
            ));
            if($objPtype) $this->sort = $objPtype->sort+1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}