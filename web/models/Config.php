<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Config extends ActiveRecord\Model {

    public static function all_to_array()
    {
        $key_array = array();
        $objConfig = Config::all();
        foreach((array)$objConfig as $i => $c)
        {
            $key_array[$c->name] = $c->value;
        }
        return $key_array;
    }
}