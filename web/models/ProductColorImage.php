<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductColorImage extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product'),
        array('product_color')
    );

    static $after_save = array('product_edit_image');

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objProductColorImage = ProductColorImage::first(array(
                'conditions' => '`delete` = 0',
                'order' => '`sort` DESC'
            ));
            if($objProductColorImage) $this->sort = $objProductColorImage->sort+1;
        }
    }

    function product_edit_image() {
        if($this->sort == 1)
        {
            $objProduct = Product::find($this->product_id);
            if($objProduct && $this->image && $this->image != $objProduct->image)
            {
                $objProduct->image = $this->image;
                $objProduct->save();
            }
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/product/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/product/'.thumbname($this->image) : '';
    }
}