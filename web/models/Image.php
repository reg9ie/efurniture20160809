<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Image extends ActiveRecord\Model {
    static $belongs_to = array(
        array('page')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort)
        {
            $this->sort = 999999999;
            $objImage = Image::first(array(
                'conditions' => 'page_id = "'.$this->page_id.'" AND `delete` = 0',
                'order' => '`sort` ASC'
            ));
            if($objImage) $this->sort = $objImage->sort - 1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/image/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/image/'.thumbname($this->image) : '';
    }
}