<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Shipping extends ActiveRecord\Model {
    public function _payments() {
        return explode(',', $this->payments);
    }
}