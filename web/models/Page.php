<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends ActiveRecord\Model {
    static $has_many = array(
        array('images')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort)
        {
            $this->sort = 1;
            $objPage = Page::first(array(
                'order' => '`sort` DESC'
            ));
            if($objPage) $this->sort = $objPage->sort+1;
        }
    }

    public function _name() {
        return strip_tags($this->name);
    }

    public function _title() {
        return strip_tags($this->title);
    }

    public function images() {
        return ($this->images ? json_decode($this->images, true) : []);
    }

    public function images_en() {
        return ($this->images_en ? json_decode($this->images_en, true) : []);
    }

    public function _content() {
        return strip_tags($this->content);
    }
}