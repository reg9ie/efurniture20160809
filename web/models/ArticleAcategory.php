<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ArticleAcategory extends ActiveRecord\Model {
    static $belongs_to = array(
        array('article'),
        array('acategory')
    );
}