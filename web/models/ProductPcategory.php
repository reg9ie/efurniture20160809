<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductPcategory extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product'),
        array('pcategory')
    );
}