<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ProductPtype extends ActiveRecord\Model {
    static $belongs_to = array(
        array('product'),
        array('ptype')
    );
}