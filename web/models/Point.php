<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Point extends ActiveRecord\Model {
    static $belongs_to = array(
        array('user'),
        array('create_user', 'class_name' => 'User')
    );

    static $after_save = array('user_edit_point');
    static $after_destroy = array('user_edit_point');

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');

        $conditions = '`user_id` = '.$this->user_id.' AND `enable` = 1 AND `delete` = 0';
        if($this->id) $conditions .= ' AND `id` <> '.$this->id;
        $objTotal = Point::find(array(
                    'select' => 'SUM(`point`) AS point',
                    'conditions' => $conditions));
        $this->total = ($objTotal ? $objTotal->point : 0) + $this->point;
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        $this->enable = 1;

        $objUser = User::get();
        if($objUser)
        {
            if(!$this->create_by_user_id) $this->create_by_user_id = $objUser->id;
            if(!$this->create_by_user_account) $this->create_by_user_account = $objUser->account;
            if(!$this->create_by_user_name) $this->create_by_user_name = $objUser->name;
        }
    }

    function user_edit_point() {
        $objUser = User::find_by_id($this->user_id);
        if($objUser)
        {
            $objPoint = Point::find(array(
                'select' => 'SUM(`point`) AS point',
                'conditions' => '`user_id` = "'.$this->user_id.'" AND `enable` = "1" AND `delete` = "0"'));
            $objUser->point = ($objPoint ? $objPoint->point : 0);
            $objUser->save();
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}