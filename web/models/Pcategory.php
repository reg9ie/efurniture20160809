<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pcategory extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->lft) {
            $this->lft = 1;
            $objPcategory = Pcategory::first(['conditions' => '`delete` = 0', 'order' => '`lft` DESC']);
            if($objPcategory) $this->lft = $objPcategory->lft + 1;
        }
    }

    public function _tree_name() {
        return str_repeat('　', $this->level+1).($this->tree_name);
    }

    public function _image() {
        return $this->image ? 'assets/images/pcategory/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/pcategory/'.thumbname($this->image) : '';
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}