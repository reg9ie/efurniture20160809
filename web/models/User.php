<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends ActiveRecord\Model {
    static $belongs_to = array(
        array('role')
    );

    function before_save() {
        if($this->attribute_is_dirty('password')) $this->password = $this->_hash_password($this->password);

        // if($this->attribute_is_dirty('lastname') ||
        //    $this->attribute_is_dirty('middlename') ||
        //    $this->attribute_is_dirty('firstname'))
        // {
        //     $this->name = $this->lastname.$this->middlename.$this->firstname;
        // }
        
        if($this->attribute_is_dirty('vip') && $this->vip)
        {
            if(!$this->vip_start_at || !$this->attribute_is_dirty('vip_start_at')) $this->vip_start_at = date('Y-m-d 00:00:00');
            if(!$this->vip_end_at || !$this->attribute_is_dirty('vip_end_at')) $this->vip_end_at = date('Y-m-d 23:59:59', strtotime('+1 years'));
        }

        if($this->vip_start_at && $this->attribute_is_dirty('vip_start_at')) $this->vip_start_at = date('Y-m-d 00:00:00', strtotime($this->vip_start_at));
        if($this->vip_end_at && $this->attribute_is_dirty('vip_end_at')) $this->vip_end_at = date('Y-m-d 23:59:59', strtotime($this->vip_end_at));

        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
    }
    
    public function check_password($password = false) {
        return $this->_check_password($password, $this->password);
    }

    public function _avatar() {
        return configUrlImageAvatar.$this->avatar;
    }

    public function _role_name() {
        if($this->role_id)
        {
            $objRole = Role::find($this->role_id);
            return ($objRole ? $objRole->name : '');
        }
        return '';
    }

    public function _role_code() {
        $objRole = Role::find($this->role_id);
        return ($objRole ? $objRole->code : '');
    }

    public function _birthday() {
        return date('Y-m-d', strtotime($this->birthday));
    }

    public function _vip_start_at() {
        
        return $this->vip_start_at ? date('Y-m-d', strtotime($this->vip_start_at)) : '';
    }

    public function _vip_end_at() {
        return $this->vip_end_at ? date('Y-m-d', strtotime($this->vip_end_at)) : '';
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public static function login($account = false, $password = false, $remember = false) {
        $CI =& get_instance();
        if($account === false) return false;
        if($password === false) return false;

        $objUser = User::find_by_account($account);
        if($objUser && $objUser->enable && $objUser->_check_password($password, $objUser->password))
        {
            User::set_session($objUser->id);
            if($remember) User::set_cookie($objUser->id);
            
            return $objUser;
        }
        else
        {
            User::clear_cookie();
            User::clear_session();

            return false;
        }
    }

    public static function logout() {
        User::clear_cookie();
        User::clear_session();
    }

    public static function get() {
        $user_id = User::get_session();
        if($user_id) {
            $objUser = User::find_by_id($user_id);
            if($objUser) {
                User::set_session($objUser->id);
                return $objUser;
            }
        }

        $user_id = User::get_cookie();
        if($user_id) {
            $objUser = User::find_by_id($user_id);
            if($objUser) {
                User::set_session($objUser->id);
                return $objUser;
            }
        }
        
        User::clear_cookie();
        User::clear_session();

        return false;
    }

    public static function is_login() {
        return (User::get() ? true : false);
    }

    public static function is_vip() {
        $objUser = User::get();
        return ($objUser && $objUser->vip ? true : false);
    }

    public static function is_admin() {
        $objUser = User::get();
        return ($objUser && $objUser->role_id ? true : false);
    }

    public static function is_superadmin() {
        $objUser = User::get();
        return ($objUser && $objUser->role_id == 1 ? true : false);
    }

    public static function is_permission($permission_code = false) {
        $objUser = User::get();
        if(!$objUser || !$objUser->role || !$objUser->role->permissions || !$permission_code) return false;

        $result = false;
        foreach($objUser->role->permissions as $i => $p)
        {
            if($permission_code == $p->code && $p->enable == 1)
            {
                $objRolePermissions = RolePermission::find_by_role_id_and_permission_id_and_enable($objUser->role->id, $p->id, 1);
                if($objRolePermissions) $result = true;
            }
        }
        return $result;
    }

    public static function user_id() {
        $objUser = User::get();
        return ($objUser ? $objUser->id : false);
    }

    private static function _hash_password($password) {
        $CI =& get_instance();
        return $CI->phpass->hash($password);
    }

    private static function _check_password($password, $hashed_password) {
        $CI =& get_instance();
        return $CI->phpass->check($password, $hashed_password);
    }

    private static function set_cookie($user_id = false) {
        if($user_id === false) return false;

        $CI =& get_instance();
        $CI->input->set_cookie('user', json_encode(array('id' => $user_id)), 2592000);
    }

    private static function set_session($user_id = false) {
        if($user_id === false) return false;

        $CI =& get_instance();
        $CI->session->set_userdata('user', json_encode(array('id' => $user_id)));
    }

    private static function get_cookie() {
        $CI =& get_instance();
        $user = json_decode($CI->input->cookie('user'), true);
        
        return ($user && isset($user['id']) && $user['id'] ? $user['id'] : false);
    }

    private static function get_session() {
        $CI =& get_instance();
        $user = json_decode($CI->session->userdata('user'), true);

        return ($user && isset($user['id']) && $user['id'] ? $user['id'] : false);
    }

    private static function clear_cookie() {
        $CI =& get_instance();
        $CI->input->set_cookie('user', '', -2592000);
    }

    private static function clear_session() {
        $CI =& get_instance();
        $CI->session->unset_userdata('user');
    }

    // public static function is_permission() {}
    // public static function email_available() {}
    // public static function account_available() {}
    // public static function in_group() {}

    public static function calc_order_price($user_id = false) {
        if($user_id === false) return false;

        $objUser = User::find_by_id($user_id);
        if(!$objUser)  return false;

        $order_price = 0;
        $order_price_this_month = 0;

        $objOrders = Order::all([
            'conditions' => '`user_id` = '.$user_id.' AND '.
                            '`status_pay` = '.Order::status('paid').' AND '.
                            '`status_return` = 0 AND '.
                            '`status_refund` = 0 AND '.
                            '`enable` = 1 AND '.
                            '`delete` = 0'
        ]);
        foreach ($objOrders as $i => $o) {
            $order_price += $o->price_total;
            if(date('Ym') == date('Ym', strtotime($o->created_at))) $order_price_this_month += $o->price_total;
        }

        $objUser->order_price = $order_price;
        $objUser->order_price_this_month = $order_price_this_month;
        $objUser->save();

        return true;
    }
}