<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Download extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort) {
            $this->sort = 999999999;
            $objDownload = Download::first(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
            if($objDownload) $this->sort = $objDownload->sort - 1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _content() {
        return mb_substr(strip_tags($this->content), 0, 100);
    }

    public function _image() {
        return ($this->image ? 'assets/images/download/'.$this->image : '');
    }

    public function _image_thumb() {
        return ($this->image ? 'assets/images/download/'.thumbname($this->image) : '');
    }

    public function _file() {
        return ($this->file ? 'assets/files/download/'.$this->file : '');
    }
}