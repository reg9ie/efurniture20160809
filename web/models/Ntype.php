<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ntype extends ActiveRecord\Model {
    static $has_many = [
        ['newses', 'class_name' => 'News'],
    ];

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
        
        if(!$this->sort) {
            $this->sort = 1;
            $objNtype = Ntype::first([
                'order' => '`sort` DESC'
            ]);
            if($objNtype) $this->sort = $objNtype->sort+1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}