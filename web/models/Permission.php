<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends ActiveRecord\Model {
    static $has_many = array(
        array('role_permissions'),
        array('roles', 'through' => 'role_permissions')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
    }
}