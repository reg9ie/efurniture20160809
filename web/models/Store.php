<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Store extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort)
        {
            $this->sort = 1;
            $objStore = Store::first(array(
                'conditions' => '`delete` = 0',
                'order' => '`sort` DESC'
            ));
            if($objStore) $this->sort = $objStore->sort+1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/store/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/store/'.thumbname($this->image) : '';
    }
}