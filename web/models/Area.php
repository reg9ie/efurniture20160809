<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Area extends ActiveRecord\Model {
    static $belongs_to = array(
        array('country'),
        array('city')
    );

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }
}