<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends ActiveRecord\Model {
    static $has_many = [];

    // static $after_create = array('edit_no');

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
        
        if($this->images) {
            $images = json_decode($this->images, true);
            if($images) $this->image = $images[0]['image'];
        }
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort) {
            $this->sort = 999999999;
            $objProduct = Product::first(array(
                'conditions' => '`products`.`delete` = 0',
                'order' => '`products`.`sort` ASC'
            ));
            if($objProduct) $this->sort = $objProduct->sort - 1;
        }
    }

    // function edit_no() {
    //     if(!$this->no) {
    //         $number = $this->id;
    //         if($number < 1000000) $number = sprintf('%1$06u', $number);
    //         $this->no = 'P'.date('ymd', strtotime($this->created_at)).$number;
    //         $this->save();
    //     }
    // }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return $this->image ? 'assets/images/product/'.$this->image : '';
    }

    public function _image_thumb() {
        return $this->image ? 'assets/images/product/'.thumbname($this->image) : '';
    }

    public function images() {
        return ($this->images ? json_decode($this->images, true) : []);
    }

    public function _custom_image() {
        return $this->custom_image ? 'assets/images/product/'.$this->custom_image : '';
    }

    public function _custom_image_thumb() {
        return $this->custom_image ? 'assets/images/product/'.thumbname($this->custom_image) : '';
    }

    public function price() {
        return number_format($this->price, configNumberOfDecimalPoints);
    }

    public function price_special() {
        return number_format($this->price_special, configNumberOfDecimalPoints);
    }

    public function price_cad() {
        return number_format($this->price_cad, configNumberOfDecimalPoints);
    }

    public function price_special_cad() {
        return number_format($this->price_special_cad, configNumberOfDecimalPoints);
    }
}