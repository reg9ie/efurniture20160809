<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MailAccount extends ActiveRecord\Model {
    function before_save() {
        if($this->attribute_is_dirty('today_times'))
        {
            if(date('H:i') == '00:00')
            {
                $this->yestoday_times = $this->today_times;
                $this->today_times = 0;
            }

            $this->total_times += 1;
        }
    }
}