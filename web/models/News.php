<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News extends ActiveRecord\Model {
    static $table_name = 'newses';

    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');

        if(!$this->sort) {
            $this->sort = 999999999;
            $objNews = News::first(['conditions' => '`delete` = 0', 'order' => '`sort` ASC']);
            if($objNews) $this->sort = $objNews->sort - 1;
        }
    }

    public function _created_at() {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function _updated_at() {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    public function _image() {
        return ($this->image ? 'assets/images/news/'.$this->image : '');
    }

    public function _image_thumb() {
        return ($this->image ? 'assets/images/news/'.thumbname($this->image) : '');
    }

    public function _content() {
        return mb_substr(strip_tags($this->content), 0, 100);
    }
}