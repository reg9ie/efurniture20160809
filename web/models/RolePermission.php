<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class RolePermission extends ActiveRecord\Model {
    static $belongs_to = array(
        array('role'),
        array('permission')
    );
}