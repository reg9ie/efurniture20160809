<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MailQueue extends ActiveRecord\Model {
    function before_save() {
        $this->updated_at = date('Y-m-d H:i:s');
    }

    function before_create() {
        $this->created_at = date('Y-m-d H:i:s');
    }
}