app.controller('user', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.user_select_all = function() {
        var result;
        $timeout(function() {
            result = $scope.users._select ? true : false;
            $.each($scope.users, function(i, o) {
                o._select = result;
            });
        });
    }

    $scope.user_select = function() {
        var select_all = $scope.users._select;
        $timeout(function() {
            $.each($scope.users, function(i, o) {
                select_all = (o._select ? select_all : false);
            });
            $scope.users._select = select_all;
        });
    }

    $scope.user_count = function(count) {
        $scope.pagination.count = count;
        $scope.user_get($scope.search, 1);
    }

    $scope.user_get = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = 'id DESC';
        $scope.ajax('user/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.users = result.data.users;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.user_add = function(user) {
        if(!user.account || user.account.length == 0) {
            alert('請輸入帳號');
            return false;
        }
        if(!user.password || user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }

        $scope.ajax('user/add', user, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/member/user_edit/'+result.data.user.id+'/add';
            }
        });
    }

    $scope.user_edit = function(user) {
        if(!user.account || user.account.length == 0) {
            alert('請輸入帳號');
            return false;
        }
        if(user.password && user.password.length > 0 && user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }

        $scope.ajax('user/edit', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.user_vip = function(user) {
        $scope.ajax('user/edit_attribute', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.user_black = function(user) {
        $scope.ajax('user/edit_attribute', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.user_enable = function(user) {
        $scope.ajax('user/edit_attribute', user, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('使用者編輯完成');
                $('#message').modal();

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.user_del = function(user) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('user/delete_soft', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.user_add_point = function() {
        $scope.point = {
            user_id: [],
            user_name: [],
            name: '',
            point: 0
        };
        $.each($scope.users, function(i, u) {
            if(u._select) {
                $scope.point.user_id.push(u.id);
                $scope.point.user_name.push(u.name);
            }
        });
        if($scope.point.user_id.length) $('#user_add_point').modal();
        else alert('請選取會員');
    }

    $scope.point_add = function(point) {
        if(!point.name || point.name.length == 0) {
            alert('請輸入名稱');
            return false;
        }
        if(!point.point || point.point.length == 0) {
            alert('請輸入購物金');
            return false;
        }
        $scope.ajax('point/adds', point, function(result) {
            if(result.status == 'ok') {
                $.each($scope.users, function(i, u) {
                    if(u._select) {
                        u.point = parseInt(u.point) + parseInt(point.point);
                    }
                });
                $('#user_add_point').modal('hide');
                alert_message('success', '訊息', '購物金新增完成');
            }
        });
    }

    $scope.init_lists = function() {
        $scope.users = json_users;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_edit = function() {
        $scope.user = json_user;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});