app.controller('product', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(angular.copy(data)),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        var client = new XMLHttpRequest();
        var formData = new FormData();
        formData.append('file', document.getElementById('file').files[0]);
        formData.append('type', type);
        formData.append('path', path);
        client.open('POST', 'api/'+url, true);
        client.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
                console.log(percentComplete + '% uploaded');
            }
        };
        client.onload = function(){
            console.log(client.status);
            console.log(client.statusText);
            console.log(client.response);
            if(client.statusText == 'OK') {
                if(typeof(callback) !== 'undefined') callback((client.response ? JSON.parse(client.response) : ''));
            }
        }
        client.send(formData);
    }

    $scope.product_image_upload = function(product) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'product', function(result) {
                if(result.status == 'ok') {
                    product.image = result.data.filename;
                    product._image = 'assets/images/product/'+product.image;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.product_image_del = function(product) {
        product.image = '';
        product._image = '';
    }

    $scope.product_images_upload = function(product) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'product');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        product.images.push({
                            image: result.data.filenames[i],
                            thumbname: result.data.thumbnames[i],
                            _image: 'assets/images/product/'+result.data.filenames[i],
                            _thumbname: 'assets/images/product/'+result.data.thumbnames[i]
                        });
                    });
                    $scope.$apply();
                }
            });
        });
    }

    $scope.product_images_move_up = function(product, index) {
        if(index == 0) return false;

        var image = product.images[index];
        product.images.splice(index, 1);
        product.images.splice(index-1, 0, image);
    }

    $scope.product_images_move_down = function(product, index) {
        if(index == product.images.length-1) return false;

        var image = product.images[index];
        product.images.splice(index, 1);
        product.images.splice(index+1, 0, image);
    }

    $scope.product_images_del = function(product, index) {
        if(confirm('確認刪除') === false) return false;

        product.images.splice(index, 1);
    }

    $scope.product_content_image_upload = function(product) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'product', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filename+'" /></div>');
                    product.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.product_content_en_image_upload = function(product) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'product', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filename+'" /></div>');
                    product.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.product_content_images_upload = function(product) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'product');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filenames[i]+'" /></div>');
                    });
                    product.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.product_content_en_images_upload = function(product) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'product');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filenames[i]+'" /></div>');
                    });
                    product.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.product_description_images_upload = function(product) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'product');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputDescription.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filenames[i]+'" /></div>');
                    });
                    product.description = tinymce.editors.inputDescription.getContent();
                }
            });
        });
    }

    $scope.product_description_en_images_upload = function(product) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'product');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputDescriptionEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/product/'+result.data.filenames[i]+'" /></div>');
                    });
                    product.description = tinymce.editors.inputDescriptionEn.getContent();
                }
            });
        });
    }

    $scope.product_custom_image_upload = function(product) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'product', function(result) {
                if(result.status == 'ok') {
                    product.custom_image = result.data.filename;
                    product._custom_image = 'assets/images/product/'+product.custom_image;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.product_custom_image_del = function(product) {
        product.custom_image = '';
        product._custom_image = '';
    }

    $scope.product_price_set_price_cad = function(product) {
        if($scope.configs.exchange_usdcad) product.price_cad = Math.round(product.price * parseFloat($scope.configs.exchange_usdcad) * 10)/10;
    }

    $scope.product_price_special_set_price_special_cad = function(product) {
        if($scope.configs.exchange_usdcad) product.price_special_cad = Math.round(product.price_special * parseFloat($scope.configs.exchange_usdcad) * 10)/10;
    }

    $scope.product_get = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;

        $scope.ajax('product/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.products = result.data.products;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.product_count = function(count) {
        $scope.pagination.count = count;
        $scope.product_get($scope.search, 1);
    }

    $scope.product_add = function(product) {
        if(typeof(product.pcategory_id) == 'undefined' || product.pcategory_id == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請選擇分類');
            $('#message').modal();
            return false;
        }
        if(typeof(product.name) == 'undefined' || product.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }
        if(typeof(product.title) == 'undefined' || product.title == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }

        $scope.ajax('product/add', product, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/product/product_edit/'+result.data.product.id+'/add?ptype_id='+product.ptype_id;
            }
        });
    }

    $scope.product_edit = function(product) {
        if(typeof(product.pcategory_id) == 'undefined' || product.pcategory_id == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請選擇分類');
            $('#message').modal();
            return false;
        }
        if(typeof(product.name) == 'undefined' || product.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }
        if(typeof(product.title) == 'undefined' || product.title == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }

        $scope.ajax('product/edit', product, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.product_del = function(product) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('product/delete_soft', product, function(result) {
            if(result.status == 'ok') {
                $scope.product_get($scope.search, $scope.pagination.page);
                $('#message .title').text('訊息');
                $('#message .message').text('刪除完成');
                $('#message').modal();
                return false;
            }
        });
    }

    $scope.product_model_list = function(product) {
        $scope.ajax('product/edit_attribute', product, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.product_home = function(product) {
        $scope.ajax('product/edit_attribute', product, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.product_enable = function(product) {
        $scope.ajax('product/edit_attribute', product, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.product_move_up = function(product) {
        $scope.ajax('product/move_up', product, function(result) {
            if(result.status == 'ok') {
                $scope.product_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.product_move_down = function(product) {
        $scope.ajax('product/move_down', product, function(result) {
            if(result.status == 'ok') {
                $scope.product_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.product_sortable = function(products) {
        var start, stop, moveA, moveB;
        $.each(products, function(i, p) {
            if(p._sortable_stop != undefined) {
                start = i;
                stop = p._sortable_stop;

                delete p._sortable_stop;
            }
        });
        if(start == stop) return;

        moveA = products[start];
        moveB = products[stop];

        $scope.ajax('product/sortable', {a: moveA.id, b: moveB.id}, function(result) {
            if(result.status == 'ok') {
                $scope.product_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.product_select = function() {
        var select_all = $scope.products._select;
        var status_show = false;
        $timeout(function() {
            $.each($scope.products, function(i, o) {
                select_all = (o._select ? select_all : false);
                status_show = (o._select ? true : status_show);
            });
            $scope.products._select = select_all;
            $scope.show.status = status_show;
        });
    }

    $scope.product_select_all = function() {
        var result;
        $timeout(function() {
            result = $scope.products._select ? true : false;
            $scope.show.status = result;
            $.each($scope.products, function(i, o) {
                o._select = result;
            });
        });
    }

    $scope.products_del = function() {
        if(confirm('確認刪除') === false) return false;
        
        var data = {id: []};
        $.each($scope.products, function(i, o) {
            if(o._select) data.id.push(o.id);
        });
        $scope.ajax('product/deletes_soft', data, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');

                $scope.product_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.pcategory_get = function(search) {
        $scope.ajax('pcategory/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.pcategories = result.data.pcategories;
            }
        });
    }

    $scope.pcategory_add = function(pcategory) {
        if(typeof(pcategory.name) == 'undefined' || pcategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('pcategory/add', pcategory, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/product/pcategory_edit/'+result.data.pcategory.id+'/add?ptype_id='+pcategory.ptype_id;
            }
        });
    }

    $scope.pcategory_edit = function(pcategory) {
        pcategory.enable = pcategory.enable ? 1 : 0;
        if(typeof(pcategory.name) == 'undefined' || pcategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('pcategory/edit', pcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.pcategory_image_upload = function(pcategory) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'pcategory', function(result) {
                if(result.status == 'ok') {
                    pcategory.image = result.data.filename;
                    pcategory._image = 'assets/images/pcategory/'+pcategory.image;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.pcategory_image_del = function(pcategory) {
        pcategory.image = '';
        pcategory._image = '';
    }

    $scope.pcategory_del = function(pcategory) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('pcategory/delete_soft', pcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.pcategory_get();
                return false;
            }
        });
    }

    $scope.pcategory_enable = function(pcategory) {
        $scope.ajax('pcategory/edit_attribute', pcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.pcategory_move_up = function(pcategory) {
        var index = $scope.pcategories.indexOf(pcategory);
        if(index == 0) return false;
        
        $scope.ajax('pcategory/move_up', pcategory, function(result) {
            if(result.status == 'ok') {
                $scope.pcategory_get();
            }
        });
    }

    $scope.pcategory_move_down = function(pcategory) {
        var index = $scope.pcategories.indexOf(pcategory);
        if(index == $scope.pcategories.length-1) return false;

        $scope.ajax('pcategory/move_down', pcategory, function(result) {
            if(result.status == 'ok') {
                $scope.pcategory_get();
            }
        });
    }

    $scope.pgroup_get = function(search) {
        $scope.ajax('pgroup/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.pgroups = result.data.pgroups;
            }
        });
    }

    $scope.pgroup_add = function(pgroup) {
        if(typeof(pgroup.name) == 'undefined' || pgroup.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('pgroup/add', pgroup, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/product/pgroup_edit/'+result.data.pgroup.id+'/add?ptype='+pgroup.ptype_id;
            }
        });
    }

    $scope.pgroup_edit = function(pgroup) {
        pgroup.enable = pgroup.enable ? 1 : 0;
        if(typeof(pgroup.name) == 'undefined' || pgroup.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('pgroup/edit', pgroup, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.pgroup_del = function(pgroup) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('pgroup/delete_soft', pgroup, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.pgroup_get();
                return false;
            }
        });
    }

    $scope.pgroup_enable = function(pgroup) {
        $scope.ajax('pgroup/edit_attribute', pgroup, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.pgroup_move_up = function(pgroup) {
        var index = $scope.pgroups.indexOf(pgroup);
        if(index == 0) return false;
        
        $scope.ajax('pgroup/move_up', pgroup, function(result) {
            if(result.status == 'ok') {
                $scope.pgroup_get();
            }
        });
    }

    $scope.pgroup_move_down = function(pgroup) {
        var index = $scope.pgroups.indexOf(pgroup);
        if(index == $scope.pgroups.length-1) return false;

        $scope.ajax('pgroup/move_down', pgroup, function(result) {
            if(result.status == 'ok') {
                $scope.pgroup_get();
            }
        });
    }

    $scope.init_lists = function() {
        $scope.ptypes = json_ptypes;
        $scope.pcategories = json_pcategories;
        $scope.pgroups = json_pgroups;
        $scope.products = json_products;
        $scope.products._select = false;
        $scope.search = json_search;
        $scope.pagination = json_pagination;
        $scope.show = {status: false};

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.split('?')[0].replace('#', '');

                href = $('#add_btn').attr('href').split('?')[0].reverse();
                href = href.replace(related.reverse(), target.reverse()).reverse();
                if($('#add_btn').attr('href').split('?')[1]) href = href + '?' + $('#add_btn').attr('href').split('?')[1];
                $('#add_btn').attr('href', href);

                index = $('#tabs a').index($('#tabs a[href="#'+target+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_product = function() {
        $scope.product = json_product;
        $scope.pcategories = json_pcategories;
        $scope.pgroups = json_pgroups;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }

    $scope.init_pcategory = function() {
        $scope.pcategories = json_pcategories;
        $scope.pcategory = json_pcategory;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }

    $scope.init_pgroup = function() {
        $scope.pgroup = json_pgroup;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});