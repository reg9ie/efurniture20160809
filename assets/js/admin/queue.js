app.controller('queue', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.mail_queue_get = function(search, page) {
        search.page = page;
        $scope.ajax('mail_queue/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.mail_queues = result.data.mail_queues;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.mail_queue_show = function(mail_queue) {
        $scope.mail_queue = mail_queue;
        $('#mail_queue_detail').modal();
    }

    $scope.mail_queue_edit = function(mail_queue) {
        $scope.ajax('mail_queue/edit', mail_queue, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.mail_queue_delete = function(mail_queue) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('mail_queue/delete', mail_queue, function(result) {
            if(result.status == 'ok') {
                $scope.mail_queue_get($scope.search, $scope.pagination.page);
                alert_message('success', '訊息', '刪除完成');
                return false;
            }
        });
    }

    $scope.init_lists = function() {
        $scope.mail_queues = json_mail_queues;
        $scope.search = json_search;
        $scope.pagination = json_pagination;
    }
});