app.controller('admin', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.user_get = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = 'id DESC';
        $scope.ajax('user/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.users = result.data.users;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.user_add = function(user) {
        if(!user.account || user.account.length == 0) {
            alert('請輸入帳號');
            return false;
        }
        if(!user.password || user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }

        $scope.ajax('user/add', user, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/admin/user_edit/'+result.data.user.id+'/add';
            }
        });
    }

    $scope.user_edit = function(user) {
        if(!user.account || user.account.length == 0) {
            alert('請輸入帳號');
            return false;
        }
        if(user.password && user.password.length > 0 && user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }

        $scope.ajax('user/edit', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.user_enable = function(user) {
        $scope.ajax('user/edit_attribute', user, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('使用者編輯完成');
                $('#message').modal();

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.user_del = function(user) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('user/delete_soft', user, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');

                $scope.user_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.permission_save = function(permission) {
        $scope.ajax('user/permission', permission, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.init_lists = function() {
        $scope.roles = json_roles;
        $scope.users = json_users;
        $scope.search = json_search;
        $scope.pagination = json_pagination;
        $scope.permissions = json_permissions;

        $(function() {
            if(location.hash) {
                var index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                var related = $(e.relatedTarget).attr('href').replace('#', '');
                var target = $(e.target).attr('href').replace('#', '');
                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_edit = function() {
        $scope.roles = json_roles;
        $scope.user = json_user;
    }
});