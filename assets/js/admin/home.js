angular.module('app', [])
.controller('home', function ($scope, $http, $timeout, $filter) {
    $scope.init = function() {
    }
});

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);

function drawChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', 'Visits');
    data.addColumn('number', 'Pageviews');
    data.addRows(analysis_results);
    var chart = new google.visualization.AreaChart(document.getElementById('chart'));
    chart.draw(data, {
        width: 930,
        height: 200,
        title: analysis_title,
        colors:['#058dc7','#058dc7'],
        areaOpacity: 0.1,
        hAxis: {textPosition: 'in', showTextEvery: 5, slantedText: false, textStyle: {color: '#058dc7', fontSize: 10}},
        pointSize: 5,
        legend: 'none',
        chartArea:{left:0,top:30,width:"100%",height:"100%"}
    });
}