app.controller('template', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.init_lists = function() {
        
    }

    $scope.init_edit = function() {
        $(document).ready(function() {
            var textarea = $('textarea[name="html"]').hide();
            var editor = ace.edit('inputHTML');
            editor.setTheme('ace/theme/monokai');
            editor.getSession().setValue(textarea.val());
            editor.getSession().on('change', function(){
                textarea.val(editor.getSession().getValue());
            });
            editor.getSession().setMode('ace/mode/php');
            editor.setOptions({
                maxLines: Infinity
            });
        });
    }
});