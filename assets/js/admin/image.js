app.controller('image', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        var client = new XMLHttpRequest();
        var formData = new FormData();
        formData.append('file', document.getElementById('file').files[0]);
        formData.append('type', type);
        formData.append('path', path);
        client.open('POST', 'api/'+url, true);
        client.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
                console.log(percentComplete + '% uploaded');
            }
        };
        client.onload = function(){
            console.log(client.status);
            console.log(client.statusText);
            console.log(client.response);
            if(client.statusText == 'OK') {
                if(typeof(callback) !== 'undefined') callback((client.response ? JSON.parse(client.response) : ''));
            }
        }
        client.send(formData);
    }

    $scope.image_upload_image = function(image) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'image', function(result) {
                if(result.status == 'ok') {
                    image.image = result.data.filename;
                    image._image = 'assets/images/image/'+image.image;
                    $scope.$apply();
                }
                $('#file').val('');
            });
        });
    }

    $scope.image_del_image = function(image) {
        image.image = '';
        image._image = '';
    }

    $scope.image_get = function(search, page) {
        search.page = page;
        $scope.ajax('image/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.images = result.data.images;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.image_add = function(image) {
        if(typeof(image.name) == 'undefined' || image.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(image.image) == 'undefined' || image.image == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳圖片');
            $('#message').modal();
            return false;
        }

        $scope.ajax('image/add', image, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/image/image_edit/'+result.data.image.id+'/add';
            }
        });
    }

    $scope.image_edit = function(image) {
        if(typeof(image.name) == 'undefined' || image.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(image.image) == 'undefined' || image.image == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳圖片');
            $('#message').modal();
            return false;
        }

        $scope.ajax('image/edit', image, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.image_del = function(image) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('image/delete_soft', image, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.image_get($scope.search, $scope.pagination.page);
                return false;
            }
        });
    }

    $scope.image_enable = function(image) {
        $scope.ajax('image/edit_attribute', image, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.image_move_up = function(image) {
        var index = $scope.images.indexOf(image);
        if(index == 0) return false;
        
        $scope.ajax('image/move_up', image, function(result) {
            if(result.status == 'ok') {
                $scope.image_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.image_move_down = function(image) {
        var index = $scope.images.indexOf(image);
        if(index == $scope.images.length-1) return false;
        $scope.ajax('image/move_down', image, function(result) {
            if(result.status == 'ok') {
                $scope.image_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.init_lists = function() {
        $scope.images = json_images;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_image = function() {
        $scope.image = json_image;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});