app.controller('download', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.download_get = function(search, page) {
        search.page = page;
        $scope.ajax('download/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.downloads = result.data.downloads;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.download_file_upload = function(download) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'file', 'download', function(result) {
                if(result.status == 'ok') {
                    download.file = result.data.filename;
                    download._file = 'assets/files/download/'+download.file;
                    $scope.$apply();
                }
                else if(result.status == 'size') alert(result.data.message);
            });
        });
    }

    $scope.download_file_delete = function(download) {
        download.file = '';
        download._file = '';
    }

    $scope.download_add = function(download) {
        if(typeof(download.name) == 'undefined' || download.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(download.file) == 'undefined' || download.file == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳檔案');
            $('#message').modal();
            return false;
        }

        $scope.ajax('download/add', download, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/download/download_edit/'+result.data.download.id+'/add';
            }
        });
    }

    $scope.download_edit = function(download) {
        if(typeof(download.name) == 'undefined' || download.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(download.file) == 'undefined' || download.file == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳檔案');
            $('#message').modal();
            return false;
        }

        $scope.ajax('download/edit', download, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.download_del = function(download) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('download/delete_soft', download, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.download_get($scope.search, $scope.pagination.page);
                return false;
            }
        });
    }

    $scope.download_enable = function(download) {
        $scope.ajax('download/edit', download, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.download_move_up = function(download) {
        var index = $scope.downloads.indexOf(download);
        if(index == 0) return false;
        
        $scope.ajax('download/move_up', download, function(result) {
            if(result.status == 'ok') {
                $scope.download_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.download_move_down = function(download) {
        var index = $scope.downloads.indexOf(download);
        if(index == $scope.downloads.length-1) return false;
        $scope.ajax('download/move_down', download, function(result) {
            if(result.status == 'ok') {
                $scope.download_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.init_lists = function() {
        $scope.downloads = json_downloads;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_download = function() {
        $scope.download = json_download;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});