angular.module('app', [])
.controller('log', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.show_errorLog = function(id) {
        if($('#errorLogs .id'+id+' .message').hasClass('hide')) $('#errorLogs .id'+id+' .message').removeClass('hide');
        else $('#errorLogs .message').addClass('hide');
    }

    $scope.del_errorLogs = function() {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('error_log/del_all', {}, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('網站紀錄刪除完成');
                $('#message').modal();

                $('#errorLogs .errorLog').remove();
            }
        });
    }

    $scope.del_errorLog = function(id) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('error_log/del', {id:id}, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('刪除完成');
                $('#message').modal();

                $('#errorLogs .id'+id).remove();
            }
        });
    }

    $scope.init = function() {
    }
});