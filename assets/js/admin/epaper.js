app.controller('epaper', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.upload_epaper_content_image = function() {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'epaper', function(result) {
                tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<img src="assets/images/epaper/'+result.data.filename+'" />');
                $scope.epaper.content = tinymce.editors.inputContent.getContent();
            });
        });
    }

    $scope.get_epaper = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = 'id DESC';
        $scope.ajax('epaper/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.epapers = result.data.epapers;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.add_epaper = function(epaper) {
        if(typeof(epaper.name) == 'undefined' || epaper.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('epaper/add', epaper, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('新增完成');
                $('#message').modal();
                $('#message').on('hidden.bs.modal', function () {
                    location.href = 'admin/epaper/edit/'+result.data.epaper.id;
                });
            }
            else {
                $('#message .title').text('訊息');
                $('#message .message').text(result.message);
                $('#message').modal();
            }
        });
    }

    $scope.edit_epaper = function(epaper) {
        if(typeof(epaper.name) == 'undefined' || epaper.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('epaper/edit', epaper, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('儲存完成');
                $('#message').modal();
            }
        });
    }

    $scope.del_epaper = function(epaper) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('epaper/del', epaper, function(result) {
            if(result.status == 'ok') {
                $scope.get_epaper($scope.search, $scope.pagination.page);
                $('#message .title').text('訊息');
                $('#message .message').text('刪除完成');
                $('#message').modal();
                return false;
            }
        });
    }

    $scope.enable_epaper = function(epaper) {
        $scope.ajax('epaper/enable', epaper, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('商品修改完成');
                $('#message').modal();
            }
        });
    }

    $scope.init_lists = function() {
        $scope.epapers = json_epapers;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').replace(new RegExp(related+'$'), target));

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_edit = function() {
        $scope.epaper = json_epaper;
    }
});