app.controller('order', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.orders_get = function(search, page, sort) {
        if(sort) {
            eval('$scope.sort.'+sort+' = !$scope.sort.'+sort);
            eval('sort = ($scope.sort.'+sort+' ? sort+" DESC" : sort+" ASC")');
        }

        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = sort ? sort : 'id DESC';
        $scope.ajax('order/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.orders = result.data.orders;
                $scope.sum = result.data.sum;
                $scope.pagination = result.data.pagination;
                $scope.show.status = false;
            }
        });
    }

    $scope.orders_count = function(count) {
        $scope.pagination.count = count;
        $scope.orders_get($scope.search, 1);
    }

    $scope.orders_change_status = function(status_name, status) {
        if(confirm('確認變更?') === false) return false;

        eval('var data = {id: [], '+status_name+': status};');

        $.each($scope.orders, function(i, o) {
            if(o._select) data.id.push(o.id);
        });
        $scope.ajax('order/edits', data, function(result) {
            if(result.status == 'ok') {
                var _status_name = '';
                $.each(eval('$scope.'+status_name.replace('status', 'statuses')), function(i, s) {
                    if(s.id == status) _status_name = s.name;
                });

                $.each($scope.orders, function(i, o) {
                    if(o._select) {
                        eval('o.'+status_name+' = status;');
                        eval('o._'+status_name+' = _status_name;');
                    }
                });
            }
        });
    }

    $scope.orders_del = function() {
        if(confirm('確認刪除') === false) return false;
        
        var data = {id: []};
        $.each($scope.orders, function(i, o) {
            if(o._select) data.id.push(o.id);
        });
        $scope.ajax('order/deletes_soft', data, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');

                $scope.orders_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.order_select_all = function() {
        var result;
        $timeout(function() {
            result = $scope.orders._select ? true : false;
            $scope.show.status = result;
            $.each($scope.orders, function(i, o) {
                o._select = result;
            });
        });
    }

    $scope.order_select = function() {
        var select_all = $scope.orders._select;
        var status_show = false;
        $timeout(function() {
            $.each($scope.orders, function(i, o) {
                select_all = (o._select ? select_all : false);
                status_show = (o._select ? true : status_show);
            });
            $scope.orders._select = select_all;
            $scope.show.status = status_show;
        });
    }

    $scope.order_detail = function(order) {
        $scope.order = order;
        $('#order_detail').modal();
        $('#order_detail').on('shown.bs.modal', function (e) {
            var body_height = $('body').height();
            var modal_height = $(this).height();
            if(modal_height > body_height) $('body').height(modal_height);
        });
        $('#order_detail').on('hidden.bs.modal', function (e) {
            $('body').height('auto');
        });
    }

    $scope.edit_order = function(order) {
        $scope.ajax('order/edit', order, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '儲存完成');
            }
        });
    }

    $scope.del_order = function(order) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('order/delete_soft', order, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');

                $scope.orders_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.print_order_detail = function() {
        var iframe = document.getElementById('print_detail');
        iframe.focus();
        iframe.contentWindow.print();
    }

    $scope.print_order_address = function() {
        var iframe = document.getElementById('print_address');
        iframe.focus();
        iframe.contentWindow.print();
    }

    $scope.orders_print_detail = function() {
        var id = [];
        $.each($scope.orders, function(i, o) {
            if(o._select) id.push(o.id);
        });
        $scope.search.id = id;
        $scope.search.count = $scope.pagination.count;
        $scope.search.page = $scope.pagination.page;
        $scope.search.sort = $scope.search.sort ? $scope.search.sort : 'id DESC';

        var search = $scope.search;
        if(id) {
            search.count = id.length;
            search.page = 1;
        }

        $('body').append('<form action="admin/order/print_detail_all" method="post" target="print_detail" id="postToIframe"></form>');
        $.each(search, function(n, v) {
            $('#postToIframe').append('<input type="hidden" name="'+n+'" value="'+v+'" />');
        });
        $('#postToIframe').submit().remove();

        $('#print_detail').load(function(){
            var iframe = document.getElementById('print_detail');
            iframe.focus();
            iframe.contentWindow.print();
        });
    }

    $scope.orders_print_address = function() {
        var id = [];
        $.each($scope.orders, function(i, o) {
            if(o._select) id.push(o.id);
        });
        $scope.search.id = id;
        $scope.search.count = $scope.pagination.count;
        $scope.search.page = $scope.pagination.page;
        $scope.search.sort = $scope.search.sort ? $scope.search.sort : 'id DESC';

        var search = $scope.search;
        if(id) {
            search.count = id.length;
            search.page = 1;
        }

        $('body').append('<form action="admin/order/print_address_all" method="post" target="print_address" id="postToIframe"></form>');
        $.each(search, function(n, v) {
            $('#postToIframe').append('<input type="hidden" name="'+n+'" value="'+v+'" />');
        });
        $('#postToIframe').submit().remove();

        $('#print_address').load(function(){
            var iframe = document.getElementById('print_address');
            iframe.focus();
            iframe.contentWindow.print();
        });
    }

    $scope.order_download_excel = function() {
        var id = [];
        $.each($scope.orders, function(i, o) {
            if(o._select) id.push(o.id);
        });
        $scope.search.id = id;
        $scope.search.count = $scope.pagination.count;
        $scope.search.page = $scope.pagination.page;
        $scope.search.sort = $scope.search.sort ? $scope.search.sort : 'id DESC';

        var search = $scope.search;
        if(id) {
            search.count = id.length;
            search.page = 1;
        }

        $('body').append('<form action="admin/order/download_excel_all" method="post" target="print_address" id="postToIframe"></form>');
        $.each(search, function(n, v) {
            $('#postToIframe').append('<input type="hidden" name="'+n+'" value="'+v+'" />');
        });
        $('#postToIframe').submit().remove();
    }

    $scope.init_lists = function() {
        $scope.statuses = json_statuses;
        $scope.statuses_remit = json_statuses_remit;
        $scope.statuses_pay = json_statuses_pay;
        $scope.statuses_ship = json_statuses_ship;
        $scope.statuses_return = json_statuses_return;
        $scope.statuses_refund = json_statuses_refund;
        $scope.orders = json_orders;
        $scope.orders._select = false;
        $scope.sum = json_sum;
        $scope.search = json_search;
        $scope.pagination = json_pagination;
        $scope.sort = {};
        $scope.show = {status: false};

        $(function() {
            if(location.hash) {
                var index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                var related = $(e.relatedTarget).attr('href').replace('#', '');
                var target = $(e.target).attr('href').replace('#', '');
                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_edit = function() {
        $scope.order = json_order;
        $scope.statuses_remit = json_statuses_remit;
        $scope.statuses_pay = json_statuses_pay;
        $scope.statuses_ship = json_statuses_ship;
        $scope.statuses_return = json_statuses_return;
        $scope.statuses_refund = json_statuses_refund;
    }

    $scope.init_print = function() {
        $scope.order = json_order;
        $scope.statuses_remit = json_statuses_remit;
        $scope.statuses_pay = json_statuses_pay;
        $scope.statuses_ship = json_statuses_ship;
        $scope.statuses_return = json_statuses_return;
        $scope.statuses_refund = json_statuses_refund;
    }

    $scope.init_prints = function() {
        $scope.orders = json_orders;
    }
});