app.controller('contact', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.reply_contact = function(contact) {
        if(confirm('確認回應留言') === false) return false;
        if(!contact.reply_content) alert('請輸入回應內容');

        $('#loading').modal();

        $scope.ajax('contact/reply', contact, function(result) {
            $('#loading').modal('hide');
            $('#message .title').text('訊息');
            $('#message .message').text('留言已回應');
            $('#message').modal();
            $('#message').on('hidden.bs.modal', function () {
                location.href = 'admin/contact/lists';
            });
        });
    }

    $scope.del_contact = function(contact) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('contact/del', contact, function(result) {
            $('#message .title').text('訊息');
            $('#message .message').text('留言刪除完成');
            $('#message').modal();
            $('#message').on('hidden.bs.modal', function () {
                location.href = 'admin/contact/lists';
            });
        });
    }

    $scope.init_lists = function() {
        $scope.contacts = json_contacts;
    }

    $scope.init_edit = function() {
        $scope.contact = json_contact;
    }
});