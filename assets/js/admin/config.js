app.controller('config', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.upload_logo = function() {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'site', function(result) {
                if(result.status == 'ok') {
                    $scope.config.site_logo = result.data.filename;
                    $scope.config._site_logo = 'assets/images/site/'+$scope.config.site_logo;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.del_logo = function() {
        $scope.config.site_logo = '';
        $scope.config._site_logo = '';
    }

    $scope.edit_config = function() {
        $scope.config.allpay_test = $scope.config.allpay_test ? 1 : 0;
        $scope.config.dongtuo_test = $scope.config.dongtuo_test ? 1 : 0;
        $scope.config._paysystems = $scope.paysystems;

        $scope.ajax('config/edit', $scope.config, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('網站設定儲存完成');
                $('#message').modal();
            }
        });

        $.each($scope.companies, function(i, c) {
            $scope.ajax('company/edit', c, function(result) {});
        });
    }

    $scope.mail_account_get = function() {
        var search = {
            count: 9999,
            page: 1,
            sort: 'id ASC',
            delete: 0
        };
        $scope.ajax('mail_account/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.mail_accounts = result.data.mail_accounts;
            }
        });
    }

    $scope.mail_account_add = function() {
        $scope.mail_account = {enable: 1};
        $('#email_detail').modal();
    }

    $scope.mail_account_edit = function(mail_account) {
        $scope.mail_account = mail_account;
        $('#email_detail').modal();
    }

    $scope.mail_account_save = function(mail_account) {
        if(mail_account.id) {
            $scope.ajax('mail_account/edit', mail_account, function(result) {
                if(result.status == 'ok') {
                    $('#message .title').text('訊息');
                    $('#message .message').text('儲存完成');
                    $('#message').modal();

                    $('#email_detail').modal('hide');
                }
            });
        }
        else {
            $scope.ajax('mail_account/add', mail_account, function(result) {
                if(result.status == 'ok') {
                    $('#message .title').text('訊息');
                    $('#message .message').text('儲存完成');
                    $('#message').modal();

                    $('#email_detail').modal('hide');
                    $scope.mail_account_get();
                }
            });
        }
    }

    $scope.mail_account_enable = function(mail_account) {
        $scope.ajax('mail_account/edit_attribute', mail_account, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('儲存完成');
                $('#message').modal();
            }
        });
    }

    $scope.mail_account_delete = function(mail_account) {
        $scope.ajax('mail_account/delete_soft', mail_account, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('刪除完成');
                $('#message').modal();

                $scope.mail_account_get();
            }
        });
    }

    $scope.company_edit = function(company) {
        if(typeof(company.name) == 'undefined' || company.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }
        if(typeof(company.address) == 'undefined' || company.address == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入地址');
            $('#message').modal();
            return false;
        }

        $scope.ajax('company/edit', company, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.edit_payment = function(payment) {
        $scope.ajax('config/payment', payment, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('儲存完成');
                $('#message').modal();
            }
        });
    }

    $scope.edit_shipping = function(shipping) {
        $scope.ajax('config/shipping', shipping, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('儲存完成');
                $('#message').modal();
            }
        });
    }

    $scope.init_edit = function() {
        $scope.config = json_config;
        $scope.mail_accounts = json_mail_accounts;
        $scope.companies = json_companies;
        $scope.payments = json_payments;
        $scope.shippings = json_shippings;

        $(function() {
            $('#emails_add_btn').hide();
            
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');

                if(target == 'emails') $('#emails_add_btn').show();
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                
                location.hash = target;
                $(window).scrollTop(0);

                $('#emails_add_btn').hide();
                if(target == 'emails') $('#emails_add_btn').show();
            });
        });
    }
});