app.controller('backup', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.backup_get = function() {
        $scope.ajax('backup/get', {}, function(result) {
            if(result.status == 'ok') {
                $scope.backups = result.data.backups;
            }
        });
    }

    $scope.backup_backup = function() {
        $('#loading').modal();

        var date = moment().format('YYYYMMDD')+'_'+moment().format('HHmm');
        $scope.ajax('backup/backup/'+date, {}, function(result) {
            $('#loading').modal('hide');
            
            if(result.status == 'ok') {
                $scope.backup_get();
                alert_message('success', '訊息', '備份完成');
                return false;
            }
        });
    }

    $scope.backup_delete = function(backup) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('backup/delete', backup, function(result) {
            if(result.status == 'ok') {
                $scope.backup_get();
                alert_message('success', '訊息', '刪除完成');
                return false;
            }
        });
    }

    $scope.init_lists = function() {
        $scope.backups = json_backups;
    }
});