String.prototype.reverse = function(){return this.split("").reverse().join("");};
var app = angular.module('app', [])
.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, element, attr) {
            if(attr.ngClick || attr.href === '' || attr.href === '#'){
                element.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
})
.directive('ngSelect', function($timeout) {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            scope.$watch(attr.ngModel, function(newValue, oldValue) {
                $timeout(function() {
                    $(element).val(newValue);
                    $(element).selectpicker('render');
                });
            });
        }
    }
})
.directive('ngCheckbox', function($timeout, $parse) {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            $timeout(function() {
                if(ngModel.$modelValue == 1) $(element).checkbox('check');
                $(element).on('toggle', function(event) {
                    $parse(attr.ngModel).assign(scope, ($(this).prop("checked") === true ? 1 : 0));
                    scope.$apply();
                    scope.$eval(attr.ngChange);
                });
            });
            scope.$watch(function() {return ngModel.$modelValue;}, function(newValue, oldValue) {
                $timeout(function() {
                    if(newValue == 1) $(element).checkbox('check');
                    else $(element).checkbox('uncheck');
                });
            }, true);
        }
    }
})
.directive('ngRadio', function($timeout) {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            $timeout(function() {
                if(scope.$eval(attr.ngModel) === parseInt(attr.value)) $(element).radio('check');
                $(element).on('toggle', function(event) {
                    ngModel.$setViewValue(parseInt(attr.value));
                    scope.$apply();
                });
            });
        }
    }
})
.directive('ngDatetimepicker', function($timeout) {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            $timeout(function() {
                $(element).datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    dayViewHeaderFormat: 'YYYY / MM'
                });
                $(element).on('blur', function(event) {
                    ngModel.$setViewValue($(element).val());
                    scope.$apply();
                });
            });
        }
    }
})
.directive('ngSortable', function($timeout) {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {
            var startIndex, stopIndex, moveItem;
            $(element).sortable({
                containment: '#sortable',
                handle: '.sortable',
                scroll:false,
                helper:'clone',
                start: function (event, ui) {
                    startIndex = ($(ui.item).index());
                },
                stop: function (event, ui) {
                    var stopIndex = ($(ui.item).index());
                    $timeout(function() {
                            ngModel.$modelValue[startIndex]._sortable_stop = stopIndex;
                            scope.$eval(attr.ngChange);
                    }, 0);
                }
            });
        }
    }
})
.directive('ngEditor', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            tinymce.init({
                document_base_url: url_base,
                content_css : [
                    "assets/normalize/3.0.3/normalize.css",
                    "assets/Animate.css/3.5.1/animate.css",
                    "assets/font-awesome/4.5.0/css/font-awesome.min.css",
                    "assets/owl.carousel/2.0.0-beta.3/assets/owl.carousel.css",
                    "assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.default.css",
                    "assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.ctm.css",
                    "assets/owl.carousel/2.0.0-beta.3/assets/owl.theme.brd.css",
                    "assets/pure-css3-dropdown-megamenu/css/css3menu.css",
                    "assets/css/base.css",
                    "assets/css/main.css",
                    "assets/css/common.css",
                    "assets/css/admin/editor.css"],
                body_id : 'editor',
                body_class : attrs.editorBodyclass,
                language : 'zh_TW',
                selector: 'textarea[ng-editor][ng-model="'+attrs.ngModel+'"]',
                menubar: false,
                statusbar: false,
                plugins: [
                    "advlist autolink autosave link lists charmap hr pagebreak",
                    "wordcount visualblocks visualchars code insertdatetime nonbreaking",
                    "table directionality textcolor textcolor"
                ],
                toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
                toolbar2: "bullist numlist | outdent indent blockquote | link unlink code | forecolor backcolor",
                toolbar3: "table | hr removeformat | subscript superscript | charmap | ltr rtl | visualchars visualblocks nonbreaking pagebreak",
                toolbar_items_size: 'small',
                style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ],
                element_format: "html",
                schema: "html4",
                extended_valid_elements: "div[*],span[*],style[*]",
                valid_children: "+body[style],*[*]",
                valid_elements: '*[*]',
                trim_span_elements: false,
                force_p_newlines : false,
                cleanup: false,
                forced_root_block: false,
                force_br_newlines: true,
                verify_html: false,
                paste_remove_styles_if_webkit: false,
                height: 480,
                setup: function(editor) {
                    editor.on('init', function(e) {
                        this.save();
                    });
                    editor.on('change', function(e) {
                        ngModel.$setViewValue(editor.getContent());
                        scope.$apply();
                    });
                    editor.on('click', function(e) {
                        ngModel.$setViewValue(editor.getContent());
                        scope.$apply();
                    });
                    editor.on('keyup', function(e) {
                        ngModel.$setViewValue(editor.getContent());
                        scope.$apply();
                    });
                },
                autosave_ask_before_unload: false
            });
        }
    }
})
.filter('range', function() {
    return function(input) {
        var low, high;
        switch (input.length) {
            case 1:
                low = 0;
                high = parseInt(input[0]) - 1;
                break;
            case 2:
                low = parseInt(input[0]);
                high = parseInt(input[1]);
                break;
            default:
                return input;
        }
        var result = [];
        for(var i = low; i <= high; i++) {
            result.push(i);
        }
        return result;
    };
});
Array.prototype.last = function() {return this[this.length-1];}
var url_base = $(document).find('base').attr('href');
var alert_message = function(classname, title, message, iconname) {
    $('#alert').attr('class', '').addClass('alert alert-'+classname);
    $('#alert .container .content .title').text(title);
    $('#alert .container .content .message').text(message);

    $('#alert').slideDown({duration: 'slow', queue: false}).delay(5000).slideUp();
    $('#alert').hover(function() {
        $(this).stop().slideDown('fast');
    }, function() {
        $(this).delay(1000).slideUp();
    });

    $('#alert .container .close').click(function() {
        $('#alert').hide();
    });
    if(iconname) $('#alert .container .content .icon').attr('class', '').addClass('icon '+iconname);
}
$(function() {
    $('a[href^="#"]').click(function(e) {e.preventDefault();});
    $('select.selectpicker').selectpicker({style: 'btn-primary', size: 10});
    $('.checkbox input:checkbox').checkbox();
    $(':radio').radio();
    $('input.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
    $('input.datetimepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', dayViewHeaderFormat: 'YYYY / MM'});
});