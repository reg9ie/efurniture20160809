app.controller('news', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(angular.copy(data)),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.news_get = function(search, page) {
        search.page = page;
        $scope.ajax('news/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.newses = result.data.newses;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.news_upload_content_image = function(news) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'news', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<img src="assets/images/news/'+result.data.filename+'" />');
                    news.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.news_upload_content_en_image = function(news) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'news', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<img src="assets/images/news/'+result.data.filename+'" />');
                    news.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.news_upload_content_images = function(news) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'news');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<div><img src="assets/images/news/'+result.data.filenames[i]+'" /></div>');
                    });
                    news.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.news_upload_content_en_images = function(news) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'news');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/news/'+result.data.filenames[i]+'" /></div>');
                    });
                    news.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.news_upload_image = function(news) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'news', function(result) {
                if(result.status == 'ok') {
                    news.image = result.data.filename;
                    news._image = 'assets/images/news/'+news.image;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.news_del_image = function(news) {
        news.image = '';
        news._image = '';
    }

    $scope.news_add = function(news) {
        if(typeof(news.name) == 'undefined' || news.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(news.content) == 'undefined' || news.content == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入內容');
            $('#message').modal();
            return false;
        }

        $scope.ajax('news/add', news, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/news/news_edit/'+result.data.news.id+'/add';
            }
        });
    }

    $scope.news_edit = function(news) {
        if(typeof(news.name) == 'undefined' || news.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(news.content) == 'undefined' || news.content == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入內容');
            $('#message').modal();
            return false;
        }

        $scope.ajax('news/edit', news, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.news_del = function(news) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('news/delete_soft', news, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.news_get($scope.search, $scope.pagination.page);
                return false;
            }
        });
    }

    $scope.news_enable = function(news) {
        $scope.ajax('news/edit', news, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.news_move_up = function(news) {
        var index = $scope.newses.indexOf(news);
        if(index == 0) return false;
        
        $scope.ajax('news/move_up', news, function(result) {
            if(result.status == 'ok') {
                $scope.news_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.news_move_down = function(news) {
        var index = $scope.newses.indexOf(news);
        if(index == $scope.newses.length-1) return false;
        $scope.ajax('news/move_down', news, function(result) {
            if(result.status == 'ok') {
                $scope.news_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.init_lists = function() {
        $scope.newses = json_newses;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_news = function() {
        $scope.news = json_news;
        $scope.ntypes = json_ntypes;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});