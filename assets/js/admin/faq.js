app.controller('faq', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(angular.copy(data)),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.article_get = function(search, page) {
        search.page = page;
        $scope.ajax('article/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.articles = result.data.articles;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.article_upload_content_image = function(article) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'article', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<img src="assets/images/article/'+result.data.filename+'" />');
                    article.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.article_upload_content_en_image = function(article) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'article', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<img src="assets/images/article/'+result.data.filename+'" />');
                    article.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.article_upload_content_images = function(article) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'article');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<div><img src="assets/images/article/'+result.data.filenames[i]+'" /></div>');
                    });
                    article.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.article_upload_content_en_images = function(article) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'article');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/article/'+result.data.filenames[i]+'" /></div>');
                    });
                    article.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.article_add = function(article) {
        if(typeof(article.name) == 'undefined' || article.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }

        if(typeof(article.content) == 'undefined' || article.content == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入內容');
            $('#message').modal();
            return false;
        }

        $scope.ajax('article/add', article, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/faq/article_edit/'+result.data.article.id+'/add';
            }
        });
    }

    $scope.article_edit = function(article) {
        if(typeof(article.name) == 'undefined' || article.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }

        if(typeof(article.content) == 'undefined' || article.content == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入內容');
            $('#message').modal();
            return false;
        }

        $scope.ajax('article/edit', article, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.article_del = function(article) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('article/delete_soft', article, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.article_get($scope.search, $scope.pagination.page);
                return false;
            }
        });
    }

    $scope.article_enable = function(article) {
        $scope.ajax('article/edit_attribute', article, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.article_move_up = function(article) {
        var index = $scope.articles.indexOf(article);
        if(index == 0) return false;
        
        $scope.ajax('article/move_up', article, function(result) {
            if(result.status == 'ok') {
                $scope.article_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.article_move_down = function(article) {
        var index = $scope.articles.indexOf(article);
        if(index == $scope.articles.length-1) return false;
        $scope.ajax('article/move_down', article, function(result) {
            if(result.status == 'ok') {
                $scope.article_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.acategory_get = function() {
        $scope.ajax('acategory/get', {}, function(result) {
            if(result.status == 'ok') {
                $scope.acategories = result.data.acategories;
            }
        });
    }

    $scope.acategory_add = function(acategory) {
        if(typeof(acategory.name) == 'undefined' || acategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('acategory/add', acategory, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/faq/acategory_edit/'+result.data.acategory.id+'/add';
            }
        });
    }

    $scope.acategory_edit = function(acategory) {
        if(typeof(acategory.name) == 'undefined' || acategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('acategory/edit', acategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.acategory_del = function(acategory) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('acategory/del', acategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.acategory_get();
            }
        });
    }

    $scope.acategory_enable = function(acategory) {
        $scope.ajax('acategory/edit_attribute', acategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.acategory_move_up = function(acategory) {
        var index = $scope.acategories.indexOf(acategory);
        if(index == 0) return false;
        
        $scope.ajax('acategory/move_up', acategory, function(result) {
            if(result.status == 'ok') {
                $scope.acategory_get();
            }
        });
    }

    $scope.acategory_move_down = function(acategory) {
        var index = $scope.acategories.indexOf(acategory);
        if(index == $scope.acategories.length-1) return false;

        $scope.ajax('acategory/move_down', acategory, function(result) {
            if(result.status == 'ok') {
                $scope.acategory_get();
            }
        });
    }

    $scope.init_lists = function() {
        $scope.articles = json_articles;
        $scope.acategories = json_acategories;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_article = function() {
        $scope.acategories = json_acategories;
        $scope.article = json_article;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }

    $scope.init_acategory = function() {
        $scope.acategory = json_acategory;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});