app.controller('error_log', function ($scope, $http, $timeout, $filter) {
    var error_log_auto_reload;

    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.error_log_show_detail = function(error_log) {
        $scope.error_log = error_log;
        $('#error_log_detail').modal();
    }

    $scope.error_log_get = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = '`id` DESC';
        $scope.ajax('error_log/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.error_logs = result.data.error_logs;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.error_log_auto_reload = function(auto_reload, sec) {
        clearInterval(error_log_auto_reload);
        error_log_auto_reload = setInterval(function() {
            if(auto_reload === false) return false;
            $scope.error_log_get($scope.search, $scope.pagination.page);
        }, sec * 1000);
    }

    $scope.error_log_delete_force = function(error_log) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('error_log/delete_force', error_log, function(result) {
            if(result.status == 'ok') {
                $scope.error_log_get($scope.search, $scope.pagination.page);
                alert_message('success', '訊息', '刪除完成');
                return false;
            }
        });
    }

    $scope.error_log_delete_force_all = function() {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('error_log/delete_force_all', {}, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '商品修改完成');
            }
        });
    }

    $scope.init_lists = function() {
        $scope.error_logs = json_error_logs;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $scope.auto_reload = true;
        $scope.auto_reload_sec = 5;

        $scope.error_log_auto_reload($scope.auto_reload, $scope.auto_reload_sec);
    }
});