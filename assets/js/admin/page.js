app.controller('page', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(angular.copy(data)),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        $.ajaxFileUpload({
            url: 'api/'+url,
            secureuri: false,
            fileElementId:'file',
            dataType: 'json',
            data: {'type': type, 'path': path},
            success: function(data) {
                if(typeof(callback) !== 'undefined') callback(data);
            }
        });
    }

    $scope.page_image_upload = function(page) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'page', function(result) {
                if(result.status == 'ok') {
                    page.images = [{
                        filename: result.data.filename,
                        thumbname: result.data.thumbname,
                        _filename: 'assets/images/page/'+result.data.filename,
                        _thumbname: 'assets/images/page/'+result.data.thumbname,
                        url: '',
                        name: '',
                        title: '',
                        content: ''
                    }];
                    $scope.$apply();
                }
            });
        });
    }

    $scope.page_image_en_upload = function(page) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'page', function(result) {
                if(result.status == 'ok') {
                    page.images_en = [{
                        filename: result.data.filename,
                        thumbname: result.data.thumbname,
                        _filename: 'assets/images/page/'+result.data.filename,
                        _thumbname: 'assets/images/page/'+result.data.thumbname,
                        url: '',
                        name: '',
                        title: '',
                        content: ''
                    }];
                    $scope.$apply();
                }
            });
        });
    }

    $scope.page_images_upload = function(page) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'page');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        page.images.push({
                            filename: result.data.filenames[i],
                            thumbname: result.data.thumbnames[i],
                            _filename: 'assets/images/page/'+result.data.filenames[i],
                            _thumbname: 'assets/images/page/'+result.data.thumbnames[i],
                            url: '',
                            name: '',
                            title: '',
                            content: ''
                        });
                    });
                    $scope.$apply();
                }
            });
        });
    }

    $scope.page_images_move_up = function(page, index) {
        if(index == 0) return false;
        
        var image = page.images[index];
        page.images.splice(index, 1);
        page.images.splice(index-1, 0, image);
    }

    $scope.page_images_move_down = function(page, index) {
        if(index == page.images.length-1) return false;

        var image = page.images[index];
        page.images.splice(index, 1);
        page.images.splice(index+1, 0, image);
    }

    $scope.page_images_del = function(page, index) {
        page.images.splice(index, 1);
    }

    $scope.page_images_en_upload = function(page) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'page');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        page.images_en.push({
                            filename: result.data.filenames[i],
                            thumbname: result.data.thumbnames[i],
                            _filename: 'assets/images/page/'+result.data.filenames[i],
                            _thumbname: 'assets/images/page/'+result.data.thumbnames[i],
                            url: '',
                            name: '',
                            title: '',
                            content: ''
                        });
                    });
                    $scope.$apply();
                }
            });
        });
    }

    $scope.page_images_en_move_up = function(page, index) {
        if(index == 0) return false;
        
        var image = page.images_en[index];
        page.images_en.splice(index, 1);
        page.images_en.splice(index-1, 0, image);
    }

    $scope.page_images_en_move_down = function(page, index) {
        if(index == page.images_en.length-1) return false;

        var image = page.images_en[index];
        page.images_en.splice(index, 1);
        page.images_en.splice(index+1, 0, image);
    }

    $scope.page_images_en_del = function(page, index) {
        page.images_en.splice(index, 1);
    }

    $scope.page_content_image_upload = function(page) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'page', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<img src="assets/images/page/'+result.data.filename+'" />');
                    page.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.page_content_en_image_upload = function(page) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'page', function(result) {
                if(result.status == 'ok') {
                    tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<img src="assets/images/page/'+result.data.filename+'" />');
                    page.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.page_content_images_upload = function(page) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'page');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContent.execCommand('mceInsertContent', false, '<div><img src="assets/images/page/'+result.data.filenames[i]+'" /></div>');
                    });
                    page.content = tinymce.editors.inputContent.getContent();
                }
            });
        });
    }

    $scope.page_content_en_images_upload = function(page) {
        $('#files').unbind('change').val('').click().change(function(event) {
            var data = new FormData();
            var files = event.target.files;
            $.each(files, function(key, value) {
                data.append('files['+key+']', value);
            });
            data.append('type', 'image');
            data.append('path', 'page');
            $.ajax({
                url: 'api/file/uploads',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(result) {
                    $.each(result.data.filenames, function(i, f) {
                        tinymce.editors.inputContentEn.execCommand('mceInsertContent', false, '<div><img src="assets/images/page/'+result.data.filenames[i]+'" /></div>');
                    });
                    page.content = tinymce.editors.inputContentEn.getContent();
                }
            });
        });
    }

    $scope.page_edit = function(page) {
        if(typeof(page.name) == 'undefined' || page.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }

        $scope.ajax('page/edit', page, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.init_lists = function() {
        $scope.pages = json_pages;
    }

    $scope.init_page = function() {
        $scope.page = json_page;
    }
});