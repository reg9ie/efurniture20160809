app.controller('point', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.add_point = function(point) {
        if(!point.user_id) {
            alert('請選擇使用者');
            return false;
        }
        if(!point.name || point.name.length == 0) {
            alert('請輸入名稱');
            return false;
        }
        if(!point.point || point.point.length == 0) {
            alert('請輸入購物金');
            return false;
        }
        $scope.ajax('point/add', point, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('購物金新增完成');
                $('#message').modal();
                $('#message').on('hidden.bs.modal', function () {
                    location.href = 'admin/point/edit/'+result.data.point.id;
                });
            }
            else {
                $('#message .title').text('訊息');
                $('#message .message').text(result.message);
                $('#message').modal();
            }
        });
    }

    $scope.edit_point = function(point) {
        $scope.ajax('point/edit', point, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('購物金儲存完成');
                $('#message').modal();
            }
            else {
                $('#message .title').text('訊息');
                $('#message .message').text(result.message);
                $('#message').modal();
            }
        });
    }

    $scope.enable_point = function(point) {
        $scope.ajax('point/enable', point, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('使用者編輯完成');
                $('#message').modal();

                $scope.get_point($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.del_point = function(point) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('point/del', point, function(result) {
            if(result.status == 'ok') {
                $('#message .title').text('訊息');
                $('#message .message').text('使用者刪除完成');
                $('#message').modal();

                $scope.get_point($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.get_point = function(search, page) {
        search.count = $scope.pagination.count;
        search.page = page;
        search.sort = '`created_at` DESC';
        $scope.ajax('point/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.points = result.data.points;
                $scope.pagination = result.data.pagination;
            }
        });
    }

    $scope.init_lists = function() {
        $scope.points = json_points;
        $scope.search = json_search;
        $scope.pagination = json_pagination;
    }

    $scope.init_edit = function() {
        $scope.point = json_point;
    }
});