app.controller('board', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.ajax_upload  = function(url, type, path, callback) {
        var client = new XMLHttpRequest();
        var formData = new FormData();
        formData.append('file', document.getElementById('file').files[0]);
        formData.append('type', type);
        formData.append('path', path);
        client.open('POST', 'api/'+url, true);
        client.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
                console.log(percentComplete + '% uploaded');
            }
        };
        client.onload = function(){
            console.log(client.status);
            console.log(client.statusText);
            console.log(client.response);
            if(client.statusText == 'OK') {
                if(typeof(callback) !== 'undefined') callback((client.response ? JSON.parse(client.response) : ''));
            }
        }
        client.send(formData);
    }

    $scope.board_upload_image = function(board) {
        $('#file').unbind('change').val('').click().change(function(event) {
            $scope.ajax_upload('file/upload', 'image', 'board', function(result) {
                if(result.status == 'ok') {
                    board.image = result.data.filename;
                    board._image = 'assets/images/board/'+board.image;
                    $scope.$apply();
                }
            });
        });
    }

    $scope.board_del_image = function(board) {
        board.image = '';
        board._image = '';
    }

    $scope.board_get = function(search, page) {
        search.page = page;
        $scope.ajax('board/get', search, function(result) {
            if(result.status == 'ok') {
                $scope.boards = result.data.boards;
                $scope.pagination = result.data.pagination;
            }
        });
    }
    
    $scope.board_add = function(board) {
        if(typeof(board.name) == 'undefined' || board.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(board.image) == 'undefined' || board.image == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳圖片');
            $('#message').modal();
            return false;
        }

        $scope.ajax('board/add', board, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/board/board_edit/'+result.data.board.id+'/add';
            }
        });
    }

    $scope.board_edit = function(board) {
        if(typeof(board.name) == 'undefined' || board.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入標題');
            $('#message').modal();
            return false;
        }
        if(typeof(board.image) == 'undefined' || board.image == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請上傳圖片');
            $('#message').modal();
            return false;
        }

        $scope.ajax('board/edit', board, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.board_del = function(board) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('board/delete_soft', board, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.board_get($scope.search, $scope.pagination.page);
                return false;
            }
        });
    }

    $scope.board_enable = function(board) {
        $scope.ajax('board/edit_attribute', board, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.board_move_up = function(board) {
        var index = $scope.boards.indexOf(board);
        if(index == 0) return false;
        
        $scope.ajax('board/move_up', board, function(result) {
            if(result.status == 'ok') {
                $scope.board_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.board_move_down = function(board) {
        var index = $scope.boards.indexOf(board);
        if(index == $scope.boards.length-1) return false;
        $scope.ajax('board/move_down', board, function(result) {
            if(result.status == 'ok') {
                $scope.board_get($scope.search, $scope.pagination.page);
            }
        });
    }

    $scope.bcategory_get = function() {
        $scope.ajax('bcategory/get', {}, function(result) {
            if(result.status == 'ok') {
                $scope.bcategories = result.data.bcategories;
            }
        });
    }

    $scope.bcategory_add = function(bcategory) {
        if(typeof(bcategory.name) == 'undefined' || bcategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('bcategory/add', bcategory, function(result) {
            if(result.status == 'ok') {
                location.href = 'admin/board/bcategory_edit/'+result.data.bcategory.id+'/add';
            }
        });
    }

    $scope.bcategory_edit = function(bcategory) {
        bcategory.enable = bcategory.enable ? 1 : 0;
        if(typeof(bcategory.name) == 'undefined' || bcategory.name == '') {
            $('#message .title').text('訊息');
            $('#message .message').text('請輸入名稱');
            $('#message').modal();
            return false;
        }

        $scope.ajax('bcategory/edit', bcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.bcategory_del = function(bcategory) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('bcategory/delete_soft', bcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '刪除完成');
                $scope.bcategory_get();
                return false;
            }
        });
    }

    $scope.bcategory_enable = function(bcategory) {
        $scope.ajax('bcategory/edit_attribute', bcategory, function(result) {
            if(result.status == 'ok') {
                alert_message('success', '訊息', '修改完成');
            }
        });
    }

    $scope.bcategory_move_up = function(bcategory) {
        var index = $scope.bcategories.indexOf(bcategory);
        if(index == 0) return false;
        
        $scope.ajax('bcategory/move_up', bcategory, function(result) {
            if(result.status == 'ok') {
                $scope.bcategory_get();
            }
        });
    }

    $scope.bcategory_move_down = function(bcategory) {
        var index = $scope.bcategories.indexOf(bcategory);
        if(index == $scope.bcategories.length-1) return false;

        $scope.ajax('bcategory/move_down', bcategory, function(result) {
            if(result.status == 'ok') {
                $scope.bcategory_get();
            }
        });
    }

    $scope.init_lists = function() {
        $scope.bcategories = json_bcategories;
        $scope.boards = json_boards;
        $scope.search = json_search;
        $scope.pagination = json_pagination;

        $(function() {
            var index, related, target;
            if(location.hash) {
                related = $('#tabs a:eq(0)').attr('href').replace('#', '');
                target = location.hash.replace('#', '');
                var href = $('#add_btn').attr('href').reverse();
                $('#add_btn').attr('href', href.replace(related.reverse(), target.reverse()).reverse());

                index = $('#tabs a').index($('#tabs a[href="'+location.hash+'"]'));
                $('#tabs a:eq('+index+')').tab('show');
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                related = $(e.relatedTarget).attr('href').replace('#', '');
                target = $(e.target).attr('href').replace('#', '');
                $('#add_btn').attr('href', $('#add_btn').attr('href').reverse().replace(related.reverse(), target.reverse()).reverse());

                location.hash = target;
                $(window).scrollTop(0);
            });
        });
    }

    $scope.init_board = function() {
        $scope.bcategories = json_bcategories;
        $scope.board = json_board;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }


    $scope.init_bcategory = function() {
        $scope.bcategory = json_bcategory;
        $scope.bcategories = json_bcategories;

        var directory = $.url().attr('directory').split('/');
        if(directory.last() == 'add') alert_message('success', '訊息', '新增完成');
    }
});