$(function(){
    function resizeWin(){
        var itemWidth=337.5;
        var WinWidth=$(window).width();
        if(WinWidth>400){
            var item=Math.floor(WinWidth/itemWidth);
            $("#container").width(item*itemWidth+"px"); 
        }else{
            $("#container").width(1*itemWidth+"px"); 
        }
    };
    resizeWin();
    $(window).bind("resize", function() {
        resizeWin();
    });

    $('.element-item').mouseover(function(){
        $(this)
        .find('.ctmItem')
        .stop()
        .css('background-color', 'rgba(244,247,251,1)');
        $(this)
        .find('.close')
        .css('opacity','1');
    });
    $('.element-item').mouseout(function(){
        $('.ctmItem')
        .stop()
        .css('background-color', 'rgba(244,247,251,0)');
        $('.close')
        .css('opacity','0');
    });

    function product_wish_del(product_id, callback) {
        return $.post('api/wish/del', {product_id: product_id}, function(result) {
            var data = jQuery.parseJSON(result);
            if(typeof(callback) !== 'undefined') callback(data);
        });
    }
    $('.product_wish_del').click(function() {
        var div = $(this);
        var product_id = div.attr('data-product_id');
        if(product_id) {
            product_wish_del(product_id, function(result) {
                if(result.status == 'ok') {
                    div.parent().parent().remove();
                    var $container = $('.grid').isotope({
                        filter: '*',
                        itemSelector: '.element-item',
                        layoutMode: 'fitRows',
                        getSortData: {
                            name: '.name',
                            symbol: '.symbol',
                            number: '.number parseInt',
                            category: '[data-category]',
                        }
                    });
                }
            });
        }
    });
});