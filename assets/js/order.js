app.controller('order', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    };

    $scope.order_paysystem_bank_update = function(order) {
        if(!order.paysystem_bank_account) {
            alert('請輸入匯出帳號後五碼');
            return false;
        }

        var send_data = {
            id: order.id,
            payment_code: order.payment_code,
            paysystem_bank_name: order.paysystem_bank_name,
            paysystem_bank_code: order.paysystem_bank_code,
            paysystem_bank_account: order.paysystem_bank_account,
            paysystem_bank_price: order.paysystem_bank_price,
            paysystem_bank_user: order.paysystem_bank_user,
            paysystem_bank_at: order.paysystem_bank_at,
        };

        modal_show('匯款資料更新中...');
        $scope.ajax('order/edit_payment_atm', send_data, function(result) {
            if(result.status == 'ok') {
                alert('編輯成功');
                modal_hide();
            }
        });
    }

    $scope.init = function() {
        $scope.order = json_order;
    }
});