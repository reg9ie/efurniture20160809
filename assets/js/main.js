/*owl-carousel*/
$(function(){
    $('#owl-carousel-slider').owlCarousel({
        items: 1,
        nav: true,
        dotsEach:true,
        autoplay: true,
        autoplayTimeout:7000,
        autoplayHoverPause:true,
        loop: true,
        navText:['',''],
        transitionStyle:true,
        fallbackEasing:'swing',
        responsive:{
          0:{
                nav: false,
          },
          480:{
                nav: false,
          },
          1280:{
                nav:true,
          }
        }
    });
});

/*line_box*/
$(function() {
  // 滑鼠旋停
  $('.line_box a').mouseover(function(){
    $('.line_box .line')
      .stop()
      .animate({
        width: $(this).outerWidth(),
        left: $(this).position().left},'fast');
  });


  // sele
  if($('.line_box .sele').position()) {
    $('.line_box .line').css({ 
        width: $('.line_box .sele').outerWidth(),
        left: $('.line_box .sele').position().left,
      });
  }

})

/*isotope*/
$(function() {
  // init Isotope
  var $container = $('.grid').isotope({
    filter: '*',
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',

    }
  });

  // filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };
 
  // bind filter button click
  $('#filters a').click(function(event){
      event.preventDefault();
  });
  
  $('#filters').on( 'click', '.button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
  });

  // bind sort button click
  $('#sorts').on( 'click', '.button', function() {
    var sortByValue = $(this).attr('data-sort-by');
    $container.isotope({ sortBy: sortByValue });
  });
  
  // change sele class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', '.button', function() {
      $buttonGroup.find('.sele').removeClass('sele');
      $( this ).addClass('sele');
    });
  });
  
});


      $(function(){
          resizeWin();    
          $(window).bind("resize", function() {
              resizeWin();
          }); 
      });


      function resizeWin(){
          var itemWidth=270;
          var WinWidth=$('#container').width();
          if(WinWidth>400){
              var item=Math.floor(WinWidth/itemWidth);
              $("#container").width(item*itemWidth+"px"); 
          }else{
              $("#container").width(1*itemWidth+"px"); 
          }
      }

      // weight: function() {
      //       $(window).bind("resize", function() {
      //             var itemWidth=270;
      //             var WinWidth=$(window).width();
      //             if(WinWidth>400){
      //                 var item=Math.floor(WinWidth/itemWidth);
      //                 $("#container").width(item*itemWidth+"px"); 
      //             }else{
      //                 $("#container").width(1*itemWidth+"px"); 
      //             }
                  
      //       }); //end resizeWin
      // }

$(function(){
    $('#owl-carousel-ctm').owlCarousel({
        // loop: true,
        items: 4,
        nav:true,
        navText:"",
        slideBy:4,
        dots:false,
        responsive:{
          480: {items: 1},
          768: {items: 2},
          1024: {items: 3},
          1280: {items: 4}
        }
    });

    $('#owl-carousel-brd').owlCarousel({
        // loop: true,
        items: 1,
        nav: true,
        navText: "",
        slideBy: 4,
        dots: false,
        responsive: {
          480: {items: 1},
          768: {items: 2},
          1024: {items: 3},
          1280: {items: 4}
        }
    });

    function product_wish_add(product_id, callback) {
        return $.post('api/wish/add', {product_id: product_id}, function(result) {
            var data = jQuery.parseJSON(result);

            if(typeof(callback) !== 'undefined') callback(data);
        });
    }
    $('.product_wish_add').click(function() {
        var div = $(this);
        var product_id = div.attr('data-product_id');
        var type = div.attr('data-type');
        if(product_id) {
            product_wish_add(product_id, function(result) {
                if(result.status == 'ok') {
                    if(type == 'home') div.css('color', '#3085a3');
                    if(type == 'product_detail') div.css('opacity', '1');
                }
            });
        }
    });
});