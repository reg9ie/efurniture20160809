app.controller('cart', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(angular.copy(data)),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }

    $scope.cart_update  = function(cart, _cart) {
        cart.payment = _cart.payment;
        cart.payment_id = _cart.payment_id;
        cart.payment_name = _cart.payment_name;
        cart.payment_code = _cart.payment_code;
        cart.payment_use = _cart.payment_use;
        cart.shipping = _cart.shipping;
        cart.shipping_id = _cart.shipping_id;
        cart.shipping_name = _cart.shipping_name;
        cart.shipping_code = _cart.shipping_code;
        cart.shipping_price = _cart.shipping_price;
        cart.shipping_free = _cart.shipping_free;
        cart.cvs_type = _cart.cvs_type;
        cart.cvs_code = _cart.cvs_code;
        cart.cvs_name = _cart.cvs_name;
        cart.cvs_telephone = _cart.cvs_telephone;
        cart.cvs_zip = _cart.cvs_zip;
        cart.cvs_address = _cart.cvs_address;
        cart.products = _cart.products;
        cart.count = _cart.count;
        cart.price_all = _cart.price_all;
        cart.price_discount = _cart.price_discount;
        cart.price_shipping = _cart.price_shipping;
        cart.price_total = _cart.price_total;
    }

    $scope.cart_change_product_quantity = function(product) {
        $scope.cart_edit_product(product);
    }

    $scope.cart_less_product_quantity = function(product) {
        if(product.quantity > 1) {
            product.quantity = parseInt(product.quantity) - 1;

            $scope.cart_edit_product(product);
        }
    }

    $scope.cart_more_product_quantity = function(product) {
        product.quantity = parseInt(product.quantity) + 1;

        if(product.quantity > product.stock) product.quantity = product.stock;

        $scope.cart_edit_product(product);
    }

    $scope.cart_edit_product  = function(product) {
        $scope.ajax('cart/edit', product, function(result) {
            if(result.status == 'ok') {
                $scope.cart_update($scope.cart, result.data.cart);
            }
        });
    }

    $scope.cart_del_product  = function(product) {
        if(confirm('確認刪除') === false) return false;

        $scope.ajax('cart/del', product, function(result) {
            if(result.status == 'ok') {
                $scope.cart_update($scope.cart, result.data.cart);
            }
        });
    }

    $scope.cart_set_shipping  = function() {
        var data = {shipping_id: $scope.cart.shipping_id};
        $scope.ajax('cart/shipping', data, function(result) {
            if(result.status == 'ok') {
                $scope.cart_update($scope.cart, result.data.cart);
            }
        });

        $scope.cart_show_payments();
    }

    $scope.cart_set_payment  = function() {
        var data = {payment_id: $scope.cart.payment_id};
        $scope.ajax('cart/payment', data, function(result) {
            if(result.status == 'ok') {
                $scope.cart_update($scope.cart, result.data.cart);
            }
        });
    }

    $scope.cart_set_recipient  = function(cart) {
        var send_data = {
            recipient_name: cart.recipient_name,
            recipient_gender: cart.recipient_gender,
            recipient_cellphone: cart.recipient_cellphone,
            recipient_telephone: cart.recipient_telephone,
            recipient_zip: cart.recipient_zip,
            recipient_address: cart.recipient_address,
            recipient_email: cart.recipient_email,
            recipient_note: cart.recipient_note
        };
        $scope.ajax('cart/recipient', send_data, function(result) {});
    }

    $scope.cart_set_point = function(all_point) {
        $scope._input_count++;
        $scope.cart._input_count = $scope._input_count;
        $scope.cart.point_use = parseInt($scope.cart.point_use);

        if(all_point <= 0) $scope.cart.point_use = 0;
        if($scope.cart.point_use > all_point) $scope.cart.point_use = all_point;

        var data = {_input_count: $scope.cart._input_count,point_use: $scope.cart.point_use};

        $scope.ajax('cart/point', data, function(result) {
            if(result.status == 'ok' && result.data._input_count == $scope._input_count) {
                $scope.cart_update($scope.cart, result.data.cart);
            }
        });
    }

    $scope.cart_go_cvs = function(cart) {
        var data = {
            recipient_name: cart.recipient_name,
            recipient_gender: cart.recipient_gender,
            recipient_cellphone: cart.recipient_cellphone,
            recipient_email: cart.recipient_email,
            recipient_note: cart.recipient_note
        };
        $scope.ajax('cart/recipient', data, function(result) {
            location.href = url_site('cart/cvs');
        });
    }

    $scope.order_create = function(cart) {
        if(!cart.shipping_id) { alert('請選擇運送方式'); return false; }
        if(!cart.payment_id) { alert('請選擇付款方式'); return false; }
        if(!cart.recipient_name) { alert('請輸入收件人姓名'); return false; }
        if(!cart.recipient_cellphone) { alert('請輸入收件人電話'); return false; }
        if(!cart.recipient_email) { alert('請輸入收件人電子信箱'); return false; }
        if(cart.shipping_code == 'delivery' && (!cart.recipient_zip || !cart.recipient_address)) { alert('請輸入收件人地址'); return false; }
        if((cart.shipping_code == 'family' || cart.shipping_code == '711') && !cart.cvs_name) { alert('請選擇超商門市'); return false; }

        var send_data = angular.copy(cart);
        send_data.products = null;

        modal_show('訂單處理中，請稍候...');
        $scope.ajax('order/create', send_data, function(result) {
            if(result.status == 'ok') {
                location.href = url_site(config_lang+'/cart/to_payment');
            }
            else {
                alert(result.message);
                modal_hide();
            }
            console.log(result)
        });
    }

    $scope.init_menu = function() {
        $scope._input_count = 0;

        $scope.cart = cart;
    }

    $scope.init_cart = function() {
        $scope._input_count = 0;

        $scope.payments = json_payments;
        $scope.shippings = json_shippings;
        $scope.cart = cart;
    }

    $scope.init_done = function() {
        $scope._input_count = 0;

        $scope.order = json_order;
    }
});