app.controller('contact', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    }
    
    $scope.send  = function(contact) {
        if(contact.name == undefined || contact.name == '') {
            alert('請輸入姓名');
            return false;
        }
        if(contact.title == undefined || contact.title == '') {
            alert('請輸入標題');
            return false;
        }
        if(contact.content == undefined || contact.content == '') {
            alert('請輸入您的訊息');
            return false;
        }

        modal_show('處理中...');
        $scope.ajax('contact/add', contact, function(result) {
            if(result.status == 'ok') {
                $scope.contact = {};
                modal_hide();
                alert('成功.');
            }
        });
    }

    $scope.init = function() {
        $scope.contact = {};
    }
});