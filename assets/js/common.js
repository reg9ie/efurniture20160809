String.prototype.reverse = function(){return this.split("").reverse().join("");};
var app = angular.module('app', ['ngSanitize'])
.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, element, attr) {
            if(attr.ngClick || attr.href === '' || attr.href === '#'){
                element.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
})
.filter('range', function() {
    return function(input) {
        var low, high;
        switch (input.length) {
            case 1:
                low = 0;
                high = parseInt(input[0]) - 1;
                break;
            case 2:
                low = parseInt(input[0]);
                high = parseInt(input[1]);
                break;
            default:
                return input;
        }
        var result = [];
        for(var i = low; i <= high; i++) {
            result.push(i);
        }
        return result;
    };
});
var url_base = $(document).find('base').attr('href');
var url_site = function(uri) {
    return URI(uri).absoluteTo(url_base).toString();
}
var uri_segment = function(n) {
    var uri_base = $.url(url_base).attr('path');
    var uri_now = $.url().attr('path');
    
    return uri_now.replace(uri_base, '').split('/')[n];
}
var validate_email = function(text) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(text);
}
var modal_show = function(content) {
    $('#modal .content').text(content);
    $('#modal').css('width', '100%').css('height', '100%').show();
    $('body').css('overflow', 'hidden');
};
var modal_hide = function() {
    $('#modal').hide();
    $('body').css('overflow', 'visible');
};
var product_search = function(text) {
    if(!text || text == undefined) return false;
    text = text.replace(/[!|\'|\"|\/|\(|\)|\*]/gm, '');
    location.href = url_site('product/search/'+encodeURIComponent(text));
};
$(function(){
    $('.search input.keyword').keyup(function(event){
        if(event.keyCode == 13) product_search($(this).val());
    });
});