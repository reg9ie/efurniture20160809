	function initMap(){
		 var styleArray =[
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#4f595d"
            },
            {
                "visibility": "on"
            }
        ]
    }
]




		var myCenter = new google.maps.LatLng(24.168980, 120.641695);
		var myLatLng = {


			center:myCenter,
			styles: styleArray,		
			zoom:15,
			zoomControl: true,
			scaleControl: false,
			scrollwheel: false,
			disableDoubleClickZoom: true,
            draggable: true,
			mapTypeId:google.maps.MapTypeId.ROADMAP,
            mapTypeControl:false,
            scaleControl:true

			};


		var map = new google.maps.Map(document.getElementById("googleMap"),myLatLng);
		var marker = new google.maps.Marker({

			position:myCenter,
			icon:'assets/images/main/_jq/googleIcom.png'});
			marker.setMap(map);
			

			}

google.maps.event.addDomListener(window, 'load', initMap);


