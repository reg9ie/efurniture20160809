app.controller('member', function ($scope, $http, $timeout, $filter) {
    $scope.ajax = function(url, data, callback) {
        $http({
            method: 'POST',
            url: 'api/'+url,
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(result){
            if(typeof(callback) !== 'undefined') callback(result);
        });
    };

    $scope.set_dates = function(year, month) {
        $scope.dates = [];
        var days = moment(year+'-'+month, 'YYYY-MM').daysInMonth();
        for (var i = 1; i <= days; i++) {
            $scope.dates.push( ('0'+i.toString()).slice(-2).toString() );
        }
    }

    $scope.user_register = function(user) {
        if(!user.account || user.account.length == 0) {
            alert('請輸入帳號');
            return false;
        }
        if(!user.password || user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }
        if(user.password != user.confirm) {
            alert('請確認密碼是否相符');
            return false;
        }
        if(!user.birthday_year || user.birthday_year.length == 0) {
            alert('請輸入生日年份');
            return false;
        }
        if(!user.birthday_month || user.birthday_month.length == 0) {
            alert('請輸入生日月份');
            return false;
        }
        if(!user.birthday_day || user.birthday_day.length == 0) {
            alert('請輸入生日日期');
            return false;
        }
        if(!user.readed) {
            alert('請確認閱讀服務條款');
            return false;
        }
        if(!user.captcha || user.captcha.length == 0) {
            alert('請輸入驗證碼');
            return false;
        }
        
        if(!validate_email(user.account)) {
            alert('請確認E-mail格式');
            return false;
        }

        modal_show('註冊中...');
        $scope.ajax('user/register', user, function(result) {
            if(result.status == 'ok') {
                alert('註冊成功');
                location.href = url_base;
            }
            else {
                alert(result.message);
                modal_hide();
            }
        });
    };

    $scope.user_forget_password = function(user) {
        if(!user.email) {
            alert('請輸入會員E-mail');
            return false;
        }
        if(!validate_email(user.email)) {
            alert('請確認E-mail格式');
            return false;
        }

        if(confirm('確認重新設定密碼?') === false) return false;

        modal_show('密碼重新設定中...');
        $scope.ajax('user/forget_password', user, function(result) {
            user.email = '';

            alert('密碼重新設定成功');
            modal_hide();
        });
    }

    $scope.user_edit_info = function(user) {
        if(!user.name || user.name.length == 0) {
            alert('請輸入姓名');
            return false;
        }
        if(!user.gender || user.gender == 0) {
            alert('請選擇性別');
            return false;
        }
        if(!user.cellphone || user.cellphone.length == 0) {
            alert('請輸入行動電話');
            return false;
        }
        if(!user.address || user.address.length == 0) {
            alert('請輸入收件地址');
            return false;
        }
        if(!user.birthday || user.birthday.length == 0) {
            alert('請輸入生日');
            return false;
        }
        // if(!user.email || user.email.length == 0) {
        //     alert('請輸入E-mail');
        //     return false;
        // }
        // if(!user.zip || user.zip.length == 0) {
        //     alert('請輸入郵遞區號');
        //     return false;
        // }
        // if(!validate_email(user.email)) {
        //     alert('請確認E-mail格式');
        //     return false;
        // }

        $scope.ajax('user/edit_info', user, function(result) {
            if(result.status == 'ok') {
                alert('變更成功');
            }
        });
    };

    $scope.user_change_password = function(user) {
        if(!user.password || user.password.length < 8) {
            alert('密碼請輸入8位數以上');
            return false;
        }
        if(user.password != user.confirm) {
            alert('請確認密碼是否相符');
            return false;
        }

        $scope.ajax('user/change_password', user, function(result) {
            if(result.status == 'ok') {
                user.password = '';
                user.confirm = '';
                alert('變更成功');
            }
            else {
                alert(result.message);
            }
        });
    };

    $scope.orders_show_payment_atm = function(order) {
        order._show = !order._show;
        if(!order.paysystem_type) order.paysystem_type = 'remit_atm';
    }

    $scope.order_edit_payment_atm = function(order) {
        if(order.paysystem_type == 'remit_atm') {
            if(!order.paysystem_bank_name) {
                alert('請輸入匯出銀行名稱');
                return false;
            }
            if(!order.paysystem_bank_code) {
                alert('請輸入匯出銀行代號');
                return false;
            }
            if(!order.paysystem_bank_user) {
                alert('請輸入匯款人');
                return false;
            }
            if(!order.paysystem_bank_account) {
                alert('請輸入匯出帳號後五碼');
                return false;
            }
        }
        else if(order.paysystem_type == 'remit_save') {
            if(!order.paysystem_bank_name) {
                alert('請輸入匯出局名');
                return false;
            }
            if(!order.paysystem_bank_account) {
                alert('請輸入匯出局號');
                return false;
            }
        }
        if(!order.paysystem_bank_price) {
            alert('請輸入匯款總金額');
            return false;
        }
        if(!order._paysystem_bank_at) {
            alert('請輸入轉帳匯款時間');
            return false;
        }

        modal_show('匯款資料更新中...');
        $scope.ajax('order/edit_payment_atm', order, function(result) {
            if(result.status == 'ok') {
                alert('編輯成功');
                order._show = !order._show;
                modal_hide();
            }
        });
    }

    $scope.init_orders = function() {
        $scope.orders = json_orders;
    }

    $scope.init_member = function() {
        $scope.user = json_user;
        
        $scope.dates = [];
        $scope.set_dates($scope.user.birthday_year, $scope.user.birthday_month);
    };
});