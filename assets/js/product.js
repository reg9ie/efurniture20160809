$(function($) {
    var images = [];
    $('#slider .bxslider li img').each(function(i, img) {
        images.push($(img).attr('src'));
    });
    $('#slider .bxslider').bxSlider({
        controls:false,
        mode:'fade',
        speed: 400,
        auto: false,
        autoControls: false,
        perloadImages:'all',
        // pagerSelector: '#slider-pager',
        buildPager: function(slideIndex) {
            return '<img src="' + images[slideIndex] + '" class="thumb" />';
        }
    });

    $('#owl-carousel-brd').owlCarousel({
        items: 1,
        nav: true,
        navText: "",
        slideBy: 4,
        // loop: true,
        dots: false,
        responsive: {
          480: {items: 1},
          768: {items: 3},
          1024: {items: 3},
          1366: {items: 4}
        }
    });

    function product_search() {
        var search_text = $('#search_text').val();
        var uri = new URI(location);
        var pcategory_id = uri.segment(1);
        uri.path('product/' + pcategory_id);
        uri.setSearch('search_text', search_text);
        location = uri.toString();
    }
    $('#search_btn').click(function() {
        product_search();
    });
    $('#search_text').keyup(function(event){
        if(event.keyCode == 13) product_search();
    });
});