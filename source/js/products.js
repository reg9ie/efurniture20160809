/*line_box*/
$(function() {
  // マウスオーバーのアニメーション
  $('.line_box a').mouseover(function(){
    $('.line_box .line')
      .stop()
      .animate({
        width: $(this).outerWidth(),
        left: $(this).position().left},'fast');
  });


  // 初期表示時に、現在アクティブなメニューへ移動
  $('.line_box .line').css({ 
    width: $('.line_box .sele').outerWidth(),
    left: $('.line_box .sele').position().left,
  });

})



/*isotope*/
$(function() {
  // init Isotope
  var $container = $('.grid').isotope({
    filter: '*',
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',

    }
  });

  // filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };
 
  // bind filter button click
  $('#filters a').click(function(event){
      event.preventDefault();
  });
  
  $('#filters').on( 'click', '.button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
  });

  // bind sort button click
  $('#sorts').on( 'click', '.button', function() {
    var sortByValue = $(this).attr('data-sort-by');
    $container.isotope({ sortBy: sortByValue });
  });
  
  // change sele class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', '.button', function() {
      $buttonGroup.find('.sele').removeClass('sele');
      $( this ).addClass('sele');
    });
  });
  
});

      $(function(){
          resizeWin();    
          $(window).bind("resize", function() {
              resizeWin();
          }); 
      });


      function resizeWin(){
          var itemWidth=270;
          var WinWidth=$('#container').width();
          if(WinWidth>400){
              var item=Math.floor(WinWidth/itemWidth);
              $("#container").width(item*itemWidth+"px"); 
          }else{
              $("#container").width(1*itemWidth+"px"); 
          }
      }

      //       weight: function() {
      //       $(window).bind("resize", function() {
      //             var itemWidth=270;
      //             var WinWidth=$(window).width();
      //             if(WinWidth>400){
      //                 var item=Math.floor(WinWidth/itemWidth);
      //                 $("#container").width(item*itemWidth+"px"); 
      //             }else{
      //                 $("#container").width(1*itemWidth+"px"); 
      //             }
                  
      //       }); //end resizeWin
      // }