$(function($) {
    var pager = ['pdtBox01.jpg', 'pdtBox02.jpg', 'pdtBox03.jpg', 'pdtBox04.jpg'];
    $('.bxslider').bxSlider({
        controls:false,
        mode:'fade',
        speed: 400,
        auto: false,
        autoControls: false,
        perloadImages:'all',
        // pagerSelector: '#slider-pager',
        buildPager: function(slideIndex) {
            return '<img src="images/' + pager[slideIndex] + '" class="thumb" />';
        }
    });
});


$(function(){
    $('#owl-carousel-brd').owlCarousel({
        items: 1,
        nav:true,
        navText:"",
        slideBy:4,
        loop: true,
        dots:false,
        responsive:{
          480:{
            items:1
          },
          768:{
            items:3
          },
          1024:{
            items:3
          },
          1366:{
            items:4
          }
        }
    });
});