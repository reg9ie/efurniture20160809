<?php
    
    ini_set('memory_limit', '-1');
    ini_set('upload_tmp_dir', '/tmp');
    ini_set('file_uploads', 'On');
    ini_set('upload_max_filesize', '100M');
    ini_set('post_max_size', '100M');
    ini_set('max_execution_time', '1000');
    ini_set('max_input_time', '-1');
    ini_set('max_input_vars', '10000');

    // system
    $configEnvironment = 'production'; // development // testing // production
    $configEncryptionKey = 'gfhdlFLDJHS4632798fhjsdlk5y428fhsdk64328fhksd';

    // ftp
    $configFtpServer = '106.185.37.229';
    $configFtpUser = 'efurn';
    $configFtpPassword = 'FDJJgrlc5780';

    // db
    $configDbDatabase = 'efurn';
    $configDbUsername = 'efurn';
    $configDbPassword = 'FDJJgrlc5780';

    // currency
    $configNumberOfDecimalPoints = 0;
    
    // domain
    $configUrlDomain = 'efurn.x10.tw'; //mumin.com.tw
    $configUrlMain = $configUrlDomain; //'www.'.
    $configUrlBase = 'http://'.$configUrlMain.'/';
    if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] != $configUrlMain) {
        header('Location: '.$configUrlBase);
        exit;
    }

    // languages
    $lang = 'tw';
    $langs = ['tw']; //['tw', 'en']
    $uri = $_SERVER['REQUEST_URI'];
    $uris = explode('/', $uri);
    if(isset($uris[1])) {
        if($uris[1] == 'admin' || $uris[1] == 'api') {}
        elseif(in_array($uris[1], $langs)) $lang = $uris[1];
    }
    $langURI = (count($langs) > 1 ? $lang : '');
    define('configLang', $lang);
    define('configLangURI', $langURI);
    
    $configRoute['default_controller'] = 'home';
    $configRoute['404_override'] = '';
    $configRoute['api/(:any)'] = 'api/$1';
    $configRoute['admin'] = 'admin/home';
    $configRoute['admin/(:any)'] = 'admin/$1';
    if($lang == 'en') {}
    elseif($lang == 'tw') {
        // $configRoute['login'] = 'login/index';
        // $configRoute['logout'] = 'logout/index';
        // $configRoute['member'] = 'member/index';
        // $configRoute['member/(:any)'] = 'member/$1';
        // $configRoute['cart'] = 'cart/index';
        // $configRoute['cart/(:any)'] = 'cart/$1';
        // $configRoute['order'] = 'order/index';
        // $configRoute['order/(:any)'] = 'order/$1';
        $configRoute['product'] = 'product/index';
        $configRoute['product/(:any)'] = 'product/index/$1';
        $configRoute['custom'] = 'custom/index';
        $configRoute['custom/(:any)'] = 'custom/index/$1';
        $configRoute['wish'] = 'wish/index';
        $configRoute['faq'] = 'faq/index';
        $configRoute['faq/(:any)'] = 'faq/index/$1';
        $configRoute['support'] = 'support/index';
        $configRoute['contact'] = 'contact/index';
        $configRoute['news'] = 'news/index';
        $configRoute['news/(:any)'] = 'news/index/$1';
        $configRoute['about'] = 'about/index';
        $configRoute['/'] = 'home/index';
        $configRoute['/(:any)'] = 'page/index/$1';
    }

    // path
    $configPathCI = dirname(__FILE__).'/codeIgniter/2.1.4';
    $configPathBase = dirname(__FILE__).'/';

    // index.php
    define('configEnvironment', $configEnvironment);
    define('configPathCI', $configPathCI);
    define('configPathCISystem', configPathCI.'/system');
    define('configPathCIApplication', configPathCI.'/application');

    // config
    define('configUrlDomain', $configUrlDomain);
    define('configUrlBase', $configUrlBase);
    define('configUrlIndexPage', '');
    define('encryption_key', $configEncryptionKey);
    define('configUriProtocol', 'AUTO');
    //'AUTO'            Default - auto detects
    //'PATH_INFO'       Uses the PATH_INFO
    //'QUERY_STRING'    Uses the QUERY_STRING
    //'REQUEST_URI'     Uses the REQUEST_URI
    //'ORIG_PATH_INFO'  Uses the ORIG_PATH_INFO
    define('configEnable_hooks', TRUE);
    define('configLanguage', 'zh-TW');
    define('configLog_threshold', 1);
    define('configCompress_output', FALSE);
    define('config_sess_use_database', FALSE);
    define('config_sess_table_name', 'ci_sessions');
    // CREATE TABLE IF NOT EXISTS  `ci_sessions` (
    //     session_id varchar(40) DEFAULT '0' NOT NULL,
    //     ip_address varchar(45) DEFAULT '0' NOT NULL,
    //     user_agent varchar(120) NOT NULL,
    //     last_activity int(10) unsigned DEFAULT 0 NOT NULL,
    //     user_data text NOT NULL,
    //     PRIMARY KEY (session_id),
    //     KEY `last_activity_idx` (`last_activity`)
    // );

    // ftp
    define('configFtpServer', $configFtpServer);
    define('configFtpUser', $configFtpUser);
    define('configFtpPassword', $configFtpPassword);

    // database
    define('configDbType', 'mysql');
    define('configDbHostname', 'localhost');
    define('configDbDatabase', $configDbDatabase);
    define('configDbUsername', $configDbUsername);
    define('configDbPassword', $configDbPassword);
    define('configDbChar_set', 'utf8');
    define('configDbPconnect', FALSE);
    
    // currency
    define('configNumberOfDecimalPoints', $configNumberOfDecimalPoints);
    
    // autoload
    $CONFIGAUTOLOAD['packages'] = array();
    $CONFIGAUTOLOAD['libraries'] = array('session', 'user_agent', 'error_log_library', 'backup_library', 'db_library', 'template_library');
    $CONFIGAUTOLOAD['helper'] = array('url', 'html', 'string', 'array', 'date', 'form', 'file', 'directory', 'email', 'image', 'phpactiverecord', 'cookie', 'sort', 'tree');

    $CONFIGAUTOLOAD['config'] = array();
    $CONFIGAUTOLOAD['language'] = array();
    $CONFIGAUTOLOAD['model'] = array();
    $CONFIGAUTOLOAD['sparks'] = array('php-activerecord/0.0.2', 'phpass/1.0.0', 'underscore/1.3.1');

    // routes
    $CONFIGROUTE = $configRoute;

    // php-activerecord
    $PHPACTIVERECORD['query'] = array();

    // other
    define('configTimezone', 'Asia/Taipei');
    date_default_timezone_set(configTimezone);
    
    define('configPathBase', $configPathBase);
    define('configPathWeb', configPathBase.'web/');
    define('configPathBackup', configPathBase.'backup/');
    define('configPathImage', configPathBase.'assets/images/');
    define('configPathFile', configPathBase.'assets/files/');
    define('configPathFont', configPathBase.'assets/fonts/');
    define('configPathFlash', configPathBase.'assets/flashs/');
    
    define('configPathImageTemp', configPathImage.'temp/');
    define('configPathImageSite', configPathImage.'site/');
    define('configPathImageNews', configPathImage.'news/');
    define('configPathImageFaq', configPathImage.'faq/');
    define('configPathImageAbout', configPathImage.'about/');
    define('configPathImagePage', configPathImage.'page/');
    define('configPathImagePhoto', configPathImage.'photo/');
    define('configPathImageAlbum', configPathImage.'album/');
    define('configPathImageArticle', configPathImage.'article/');
    define('configPathImageAcategory', configPathImage.'acategory/');
    define('configPathImageProduct', configPathImage.'product/');
    define('configPathImagePcategory', configPathImage.'pcategory/');
    define('configPathImageShop', configPathImage.'shop/');
    define('configPathImageStore', configPathImage.'store/');
    define('configPathImageCompany', configPathImage.'company/');
    define('configPathImageActivity', configPathImage.'activity/');
    define('configPathImageSignup', configPathImage.'signup/');
    define('configPathImageCategory', configPathImage.'category/');
    define('configPathImageLink', configPathImage.'link/');
    define('configPathImageImage', configPathImage.'image/');
    define('configPathImageAvatar', configPathImage.'avatar/');
    define('configPathImageAd', configPathImage.'ad/');
    define('configPathImageBoard', configPathImage.'board/');
    define('configPathImageEpaper', configPathImage.'epaper/');
    
    define('configUrlAdmin', configUrlBase.'admin/');
    define('configUrlImage', configUrlBase.'assets/images/');
    define('configUrlFile', configUrlBase.'assets/files/');

    define('configUrlImageTemp', configUrlImage.'temp/');
    define('configUrlImageSite', configUrlImage.'site/');
    define('configUrlImageNews', configUrlImage.'news/');
    define('configUrlImageFaq', configUrlImage.'faq/');
    define('configUrlImageAbout', configUrlImage.'about/');
    define('configUrlImagePage', configUrlImage.'page/');
    define('configUrlImagePhoto', configUrlImage.'photo/');
    define('configUrlImageAlbum', configUrlImage.'album/');
    define('configUrlImageArticle', configUrlImage.'article/');
    define('configUrlImageAcategory', configUrlImage.'acategory/');
    define('configUrlImageProduct', configUrlImage.'product/');
    define('configUrlImagePcategory', configUrlImage.'pcategory/');
    define('configUrlImageShop', configUrlImage.'shop/');
    define('configUrlImageStore', configUrlImage.'store/');
    define('configUrlImageCompany', configUrlImage.'company/');
    define('configUrlImageActivity', configUrlImage.'activity/');
    define('configUrlImageSignup', configUrlImage.'signup/');
    define('configUrlImageCategory', configUrlImage.'category/');
    define('configUrlImageLink', configUrlImage.'link/');
    define('configUrlImageImage', configUrlImage.'image/');
    define('configUrlImageAvatar', configUrlImage.'avatar/');
    define('configUrlImageAd', configUrlImage.'ad/');
    define('configUrlImageBoard', configUrlImage.'board/');
    define('configUrlImageEpaper', configUrlImage.'epaper/');
    
    define('configImageThumbnailWidth', 200);
    define('configImageThumbnailHeight', 200);
    define('configImageAllowedTypes', 'jpg|jpeg|png|gif');
    define('configImageMaxSize', 10*1024);

    define('configPathFileTemp', configPathFile.'temp/');
    define('configPathFileDownload', configPathFile.'download/');
    define('configPathFileNews', configPathFile.'news/');
    define('configPathFileArticle', configPathFile.'article/');
    define('configPathFileProduct', configPathFile.'product/');
    
    define('configUrlFileTemp', configUrlFile.'temp/');
    define('configUrlFileDownload', configUrlFile.'download/');
    define('configUrlFileNews', configUrlFile.'news/');
    define('configUrlFileArticle', configUrlFile.'article/');
    define('configUrlFileProduct', configUrlFile.'product/');

    define('configFileAllowedTypes', configImageAllowedTypes.'|pdf|doc|docx|rtf|odt|sxw|ps|txt|csv|xls|ods|ppt|odp|zip|rar|7z');
    define('configFileMaxSize', 100*1024);